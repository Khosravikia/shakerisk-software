<div align="center">

# SHAKERISK
<br/>
by

**Farid Khosravikia, Ph.D.**

<br/>

**The University of Texas at Austin**

**August 2020**
</div>

<br/>

# TABLE OF CONTENT

[[_TOC_]]


# SOFTWARE DEVELOPMENT TEAM

Farid Khosravikia, Ph.D.
- Postdoctoral scholar, The University of Texas at Austin

Patricia Clayton, Ph.D.
- Assistant professor, The University of Texas at Austin

# INTRODUCTION

*ShakeRisk* is open-source, object-oriented software with a graphical user interface for risk, reliability, and resilience assessment of the built environment to natural hazards. *ShakeRisk* provides a platform to integrate artificial intelligence, systems engineering, structural and earthquake engineering research fields to simulate the responses of civil infrastructure at both structural and system scales in a reliable and computationally efficient way. Adopting clean architecture principles (Martin, 2018) and object-oriented programming language in the design of *ShakeRisk*, it can be readily extended by adding features (i.e., new data sources, models, analyses, and user interfaces) and customizing existing ones without the need to modify existing code.

The software was initially developed throughout Farid Khosravikia’s Ph.D. studies (Khosravikia, 2020) at The University of Texas at Austin under the supervision of Patricia Clayton from 2016 to 2020. It was developed at The University of Texas at Austin as part of research projects evaluating the risks of induced earthquakes in the Central United States on the built environment. These projects were funded by the Texas Department of Transportation (Projects 0-6916 and 5-6916), the Texas State legislature through the TexNet Seismic Monitoring Program, and the Center for Integrated Seismicity Research (CISR).

# VERSION AND FEATURES

*ShakeRisk* 0.1.0 (i.e., the first version of *ShakeRisk*) was published alongside Farid Khosravikia’s Ph.D. dissertation (2020) and is available at this GitLab repository. It consistsed of the data, models, and analyses presented in the dissertation, which focus on estimating regional ground shaking levels as well as regional levels of damage and ensuing repair costs to highway bridges for future seismic events.

In *ShakeRisk* 0.2.1 (i.e., the second version of *ShakeRisk*), previous features are expanded to support estimation of buildings and transportation networks levels of damages and repair cost. Command line interface and graphical interface has also being through an overhaul, to accommodate the changes, make using *ShakeRisk* more intuitive and boost its extendibility  .
# PROGRAMMING LANGUAGE

The programming language used in this application software is Python, alongside some freely available libraries, including NumPy, SciPy, Pandas, Scikit-learn, TensorFlow2, Keras, Matplotlib, Plotly, Folium, and Django. The structural design of *ShakeRisk* is based on clean architecture principles (Martin, 2018), which can be readily extended by adding new features (i.e., new data sources, models, analyses, and user interfaces) and customizing existing ones without the need to modify existing code. In this architecture, an object-oriented programming language is used to orchestrate the communication of data, models, and analyses inside of *ShakeRisk* to facilitate the extendibility of the software. *ShakeRisk* provides a variety of probabilistic analysis tools and methods, including Monte Carlo sampling methods that yield probability distributions of responses.

# GRAPHICAL USER INTERFACE

Figure 1 shows the graphical user interface (GUI) of *ShakeRisk*. This graphical user interface, which is on a web platform, is designed to facilitate the use of this multi-model analysis tool in academia and engineering practice. React library has been used to generate the HTML, CSS, and JavaScript of the graphical interface. As seen in Figure 1, \textit{ShakeRisk} GUI consist of set of 5 main panes which are briefly introduced in following section.

<div align="center">
<img src="images/020/A screenshot of the user interface.jpg" width="600px">

Figure 1: A screenshot of the user interface of *ShakeRisk* 0.2.1.
</div>

**Objects Pane**

This pane presents a list of all object types available in *ShakeRisk* separated into Models and Analysis tabs. For ease of use, each tab is also separated into sections by utilizing accordion styled menu. By clicking on an object, further actions such as adding, deleting or modifying objects of that type becomes available at object editor pane. Section~\ref{sec:model_analysis_structure} provides more detail on model and analysis objects.

**Object Editor Pane**

Editor pane is responsible for viewing and editing objects. After choosing an object type from objects pane, a list of all defined objects of that type becomes available at this pane as shown in Figure 2. "Add new" button is used to create a new object and two buttons next to each defined object are for renaming and deleting existing ones.

By clicking an already defined object or the "Add New" option, the editor goes into editing mode where properties of an object can be assigned or altered(Figure 3). After editing the object, "Save" button will store the changes and "Discard" button will get back to list mode without any modification done to the object.

<div align="center">
<img src="images/020/A screenshot of the object editor in list mode.jpg" width="400px">

Figure 2: A screenshot of object editor in list mode *ShakeRisk* 0.2.0.
</div>

<div align="center">
<img src="images/020/A screenshot of the object editor in edit mode.jpg" width="400px">

Figure 3: A screenshot of object editor in edit mode *ShakeRisk* 0.2.0.
</div>

**Run Analysis Pane**

All defined analysis objects will automatically appear in run analysis pane at "Choose Analysis" selector. After choosing the required analysis and pressing "Run" button, a JSON request will be created and sent to the server for processing.
After running an analysis, the results are presented in three different formats. Text format containing general result of analysis, appearing at output console, graphical format presented at visualization pane and file format saved at user defined directory.

In case defined models and analysis will be required for future uses, import/export features are also included. "Export JSON" button, as the name suggests, will export all created objects into a JSON file which can later be imported into *ShakeRisk* using "Import JSON" button. Note that Importing JSON will clear any objects already defined in the interface. Exported JSON file can also be directly used with command line as explained in Installation and Launching Section.

**Visualization Pane**

After processing an analysis any graphical output will be presented at visualization pane. Each graph is displayed in a separate boxes. Header of each box includes a title in the format of: "analysis name : graph name" and also provides buttons to download, maximize and close the graph. Interactive graphs are downloaded as a single fully functional HTML file, which can be opened via any web browser and static graphs are downloaded as image files. Visualization pane can be scrolled horizontally and as a result there is no limit on number of shown figures at once.

**Output Console**

Output console is used for showing messages, which mainly focuses on announcing analysis start, finish, general results and, possible errors. It also informs users of successfully created, deleted or renamed objects.


# DATA, MODELS, AND ANALYSES IN *SHAKERISK*

*ShakeRisk* has a growing database and library of models and analyses. They are implemented to identify or estimate the occurrence, magnitude, location, and intensity of hazards, the performance of structures and infrastructure, and the ensuing consequences, such as economic, socioeconomic, and environmental consequences. Users can also implement new repositories and models in *ShakeRisk* and use them for their analysis. To find a list of the available databases and libraries and to see how new data and models can be added to *ShakeRisk* 0.2.0., one may visit the operating manual available in this repository. 

# REQUEST ANALYSIS IN *SHAKERISK*

*ShakeRisk*, as shown in Figure 4, provides two methods of requesting a new analysis: (1) using the graphical user interface and (2) through a command line interface.

As of version 0.2.0 of *ShakeRisk*, graphical user interface generates a JSON file which can be used by command line interface and hands it to command line interface for processing. In other words, graphical user interface acts as an additional graphical layer on top on command line interface which facilitates creating, managing and viewing results of analysis. This approach as apposed to two interaction methods being handled separately, makes it possible to change method of requesting analysis as needed, since graphical user interface has import/export feature for generated JSON file, and also omits repetitions in development of shared tasks between the methods.



1. Graphical interface: Each analysis has its own object type in *ShakeRisk*. Once an analysis object is created and submitted, graphical user interface generates its respective JSON file and sends it to Django server using AJAX, a JavaScript Library, without reloading the page. In this case, Django server receives the JSON file and hands to command line interface. Command line interface, checks the validity of the request, converts it to a *ShakeRisk* compatible format, and sends it to the project core, where the calculations for processing the request are conducted. Analysis results are then returned to the command line interface from the project core. If there are no errors, command line interface sends the result to *ShakeRisk* output manager which is responsible for generating output in form of text, graphs, and files and any conversion necessary to HTML(and CSS and/or JavaScript) format. Output files are saved to user defined directory and generated text and graph outputs are returned to Django server and then retrieved by graphical user interface and presented to user.
	
2. Command Line Interface: *ShakeRisk* can also be launched through the command line interface. This format is built for cases when users want to directly run the analysis with the software without using the graphical user interface. One of the key motivations for generating this format is to run parallel analyses using high-performance computers. In this case, a JSON file containing the desired model and analysis objects is passed to the software. As seen in Figure 2, the command line interpreter receives the JSON file by running its Python file using the file path as an argument and sends it to the project core to process the request. This makes it possible to use the software without running the Django server and allows a user to schedule multiple analyses using scripts. If users use command line interface for running an analysis, generated output files are saved at a location determined by users and text outputs are printed in console used to run the Python file.

A detailed explanation of objects structure, definitions, and input types can be found at Section 6 of operating manual. 

<div align="center">
<img src="images/020/Illustration of the methods of requesting analysis.jpg" width="500px">

Figure 4: Illustration of the methods of requesting analysis in *ShakeRisk* 0.2.0.
</div>

## Running the demo
*ShakeRisk* comes with a fully functional demo to help user getting started with the software. This comes in form of JSON file in "demo" directory of this repository. It contains an example for each analysis available in *ShakeRisk*. To use the demo file, install and run graphical user interface as instructed in Installation section and use "import" button to import the demo.json in "demo" directory. Use Run Analysis Pane to select and run loaded analysis or edit them using Objects Pane and Object Editor Pane. More instructions or using the demo file is available in operation manual.  

# STRUCTURE AND DESIGN ARCHITECTURE

*ShakeRisk* is developed using clean architecture (Martin, 2018) computer programming principles such that it can be continuously growing, meaning existing features can be improved, and new features can be implemented. Clean architecture philosophy divides the computer program elements into different levels (as shown in Figure ‎5), starting with high-level rules at the core and ending with low-level tools in outer layers. The essential feature of the clean architecture design is the dependency rule among layers, stating that source code dependencies can only point inwards. That is, the dependency among layers in clean architecture is only defined in one direction, which is going inward from outer to the inner layer. This way, codes written in inner layers do not rely on those in the outer layers. It helps to encapsulate the logic behind the software at the core while keeping it separated from the delivery mechanism, databases, and tools in outer rings. For example, the logic behind calculating damage to structures should not depend on whether structure data come from an offline SQL database or an online source.

<div align="center">
<img src="images/010/A schematic view of the clean architecture.png" width="400px">

Figure 5: A schematic view of the clean architecture
</div>

Although this rule does not directly relate to the concept of inversion of control, it brings the concept of inversion of control in the application software. Inversion of control (Johnson and Foote, 1988) is a principle in software engineering by which the control of objects in the program is transferred to containers or frameworks. Inversion of control is used to increase modularity of the program and make it extensible, and it has applications in object-oriented programming. Thus, this one-directional dependency rule, which forms the basis of clean architecture, makes it easy to add new features (i.e., data sources, models, analysis, and user interfaces) and customize existing ones without the need to modify existing code. The following sections provide a brief introduction of each layer of the clean architecture.

**ENTITIES**

Entities, as demonstrated in Figure 5, form the innermost layer in clean architecture and are global objects containing the most high-level rules. They are not affected by any other layers, and the data structure and methods in this layer are created independently of other layers in the software. Each entity consists of a list of attributes defining that entity and a method to validate whether the entity object can be created in the software given the information in the software or from users. 

As an example, “bridge” is defined as an entity with its own attributes (e.g., latitude, longitude, span number, and so on) and validation method in the *ShakeRisk* software. When bridge data are imported from a database or by users, the validation method validates whether the data satisfy all attributes defined for a bridge entity. If so, a bridge object with the attributes already defined in the bridge entity is created to be used in the analysis requested by users. Otherwise, it sends an error indicating that the imported data do not satisfy the attributes defined for the entity in the software. This way, the attributes assigned to the bridge object are independent of how the data are imported and used in the software. It solely depends on the bridge entity description. “Fragility models” and “Vs30 maps” are other examples of the entities defined in *ShakeRisk*.

**USE CASES**

The second inner layer consists of use case classes. For every analysis, a use case class is defined and assigned in the software to that particular analysis. The use case class contains the required calculation code for conducting the corresponding analysis.

When users request a particular analysis, an object is created from the corresponding use case class for that analysis and is called from the interface adapter layer (which will be discussed shortly) through a request model. Each use case consists of its own request model in the software. As shown in Figure ‎6, each time the use case is called, the interface adapter layer creates a request object using the request model class. Then, the adopter feeds the request information to the request object and sends it to the use case object. The request information consists of the list of the data, models, and other inputs provided by users for running the requested analysis. 

Once the use case is called and receives the required information (i.e., data, models, and other assumptions listed in the request model object) from the interface adapter layer, it processes the request according to the logic defined in the code to conduct the analysis. Thereafter, as illustrated in Figure ‎6, the use case returns the results in the structure defined by a response model to the interface adapter layer. Similar to the request model, each use case has its own response model. After each analysis, the response model creates a response object to define the structure of the results received from the use case object. The output of the response model is sent back to the interface adapters to present the results of the analysis in the outer layer to the user.

A use case only depends on the definition of the corresponding analyses and entities. Changes in use cases are not expected to influence the entities available in the system, and use cases are typically independent of changes in the external layers such as databases, user interface, or any of the existing models in the software. 

**INTERFACE ADAPTERS**

This layer is the interface between the outermost details level and the application layer (i.e., use case layer). It consists of a set of adapters (e.g., gateways, controller, and presenters) to handle the information flow (e.g., data and user requests) between use cases and the objects in the outer layer (e.g., frameworks, databases, user interfaces). 

A gateway is an object to encapsulate access to an external system or resource. In particular, as seen in Figure ‎6, it converts data in the outer layer to a format defined in the corresponding entity and sends it to a use case object.  Recall that when users request analysis, the associated use case is called in *ShakeRisk* through a request model, which lists the information required for conducting the analysis. Part of this information is data (e.g., Vs30 information for estimating ground shaking levels analysis). Data in the architecture presented in Figure ‎5 are in the outer layer, and they can come from three different sources: an internal database repository, external data fetched online, and external data provided by users. In *ShakeRisk*, the repository handler manages this converting process and deals with all three different database sources. By doing so, the objects in the inner layers, like use cases and entities, are independent of the initial source and structure of the database. As a result, a database can be easily updated, changed, and re-structured in *ShakeRisk* without any changes in the internal layers (e.g., use cases and entities).

The controller, as seen in Figure 6, is an object to receive the request from the outer layer, convert it into a request model format, and pass it to the corresponding use case. In *ShakeRisk*, requests from the outer layer come from either the graphical interface or command line interface using a JSON file. 

All requests received from the ShakeRisk graphical interface are handled by the Django server. The server receives the request from the front-end, validates it, and converts it to a form suitable for the use case the user requested. The Django server is also responsible for converting the returned results from the use case through the response request model to a form suitable for the front-end.

When *ShakeRisk* is called from the command line interface, the command line interpreter in *ShakeRisk* receives the request from the outer layer, converts the requests into a request model, and passes it to the corresponding use case. The command line interpreter works in the same fashion as the Django server does in the graphical user interface of the software. Both are controller objects in the interface adapter layer.

The presenter, as seen in Figure ‎6, is an object to get results from the use case through the response model and transform it into a proper format before passing it to the outer layer. In the web version of *ShakeRisk*, this process is handled by the Django server.

**DATABASE, MODELS, AND USER-INTERFACE**

This layer is the outermost layer on the diagram presented in Figure ‎5. It is the largest layer of the software since there are a variety of databases and libraries. The material in this layer is constantly changing, and being in the outermost layer prevents these changes from affecting other inner layers of the software. This layer of *ShakeRisk* consists of the following tools:

**Databases**: Recall that the data in *ShakeRisk* can come from three different sources: an internal database repository, which consists of a SQL database; external data fetched online; and external data provided by users. A list of the internal repositories in *ShakeRisk* is presented in the operating manual. Being in the outer layer, the structure and content of the data can be updated and changed without any effect on the code written in the core levels (i.e., use cases and entities).

**Models:** The list of the available models in *ShakeRisk* is provided in the operating manual. Note that, users can also implement new models into *ShakeRisk* by uploading a script file of their model or by using input analyses like “ANN-based Model.” All these models are in the outer layer to ensure that any changes in the model libraries will not affect the objects defined in the project core (i.e., use cases and entities).

**Graphical User Interface:** The graphical user interface (i.e., web-interface) is in this layer. The JSON files required for launching *ShakeRisk* from the command line interface are also considered to be in this layer.

<div align="center">
<img src="images/010/Process of conducting analysis in ShakeRisk.png" width="600px">

Figure 6: Process of conducting analysis in *ShakeRisk*
</div>

# INSTALLATION AND LAUNCHING SHAKERISK

The following steps guide users through the process of installing *ShakeRisk* on their computer system.

**Step 1: Download *ShakeRisk* files**

Acquire *ShakeRisk* files either by cloning the repository or downloading them directly.

**Step 2: Installation of the Working Environment**

Install Anaconda for Python 3.7 according to your operating system using the following link: 

https://www.anaconda.com/products/individual#Downloads

Installation instructions are available here: https://docs.anaconda.com/anaconda/install/

**Step 3: Installation of Required Libraries in a New Environment**

Open the Anaconda Prompt (instructions [here](https://docs.anaconda.com/anaconda/install/verify-install/\#conda)) and run the command below, to add "conda-forge" to the channels list. 

`conda config --append channels conda-forge`

Change the current directory to your *ShakeRisk* folder, and Then run the command below to create a new environment and install all required packages in it, automatically. You can change the environment name by editing "EnvName".

`conda create --name EnvName --file requirements.txt`

If you are asked for confirmation, press `y" and then hit Enter (Return on Mac OS) and wait for all packages to be installed.


**Step 4: Activating Environment**

Use the command below to activate the newly created environment. (Change “EnvName” to the name you chose when creating the environment in step 3.)

`conda activate EnvName`

Note: Make sure to activate the environment after opening Anaconda Prompt and before launching *ShakeRisk* in future uses of *ShakeRisk* as well.

**Step 5: Launching and Running *ShakeRisk* Using the Command Line**

After activating the environment as described in step 3, change the current directory to your *ShakeRisk* folder in the Anaconda Prompt (if it is not already changed) and run the command below. In this command, `path\_to\_json\_file` contains the directory path of the JSON file required for running an analysis in *ShakeRisk*. Users may change the `path\_to\_json\_file` according to the directory of the JSON file on their computer. JSON file can be exported from graphical user interface. There is also a  JSON file in "demo" directory which can be used as a reference.

`python cli\handler.py path_to_json_file`

**Step 6: Launching and Running *ShakeRisk* Using the Graphical User-Interface**

After activating the environment as described in step 3, change the current directory to your *ShakeRisk* folder in the Anaconda Prompt (if it is not already changed) and run the command below:

`python DjangoFront\manage.py runserver`

pen the link printed in the Anaconda Prompt in your browser. (Usually http://127.0.0.1:8000/ )

# REFERENCES
- Johnson, R. E., and Foote, B., 1988. Designing reusable classes. Journal of object-oriented programming, 1(2), 22–35.
- Khosravikia, F., 2020. Machine-learning-based Models, Methods, and Software for Intensity, Vulnerability, and Risk Assessment of Central U.S. Induced Earthquakes. Doctoral Dissertation, The University of Texas at Austin.
- Martin, R. C., 2018. Clean architecture: a craftsman’s guide to software structure and design. Prentice Hall Press.



