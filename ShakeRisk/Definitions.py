import os
from pathlib import Path

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DATABASE_PATH = Path(ROOT_DIR, "data\\repository.sqlite")

# Keywords used as "source" options determining where data/model should be fetched from
LOCAL_DATA_KEYWORD = "shakerisk"
DATAFRAME_DATA_KEYWORD = "dataframe"


"""Custom exceptions raised in different parts of software with an informative message.
    This is meant to distinguish manually raised exceptions from others, specially since the messages of these exceptions
    will be used in response object and printed in graphical user interface console"""


class ResourceError(Exception):
    pass


class ObjectCreationError(Exception):
    pass


class ErrorMessages:
    # Data
    @staticmethod
    def no_data(data):
        return 'No data found for "{}" object ({}) with provided properties.'.format(
            data.name, type(data).__name__)

    @staticmethod
    def required_columns(data):
        return 'Provided data for "{}" object ({}) does not have required columns.'.format(
            data.name, type(data).__name__)

    @staticmethod
    def required_classification(data):
        return 'Provided data for "{}" object ({}) does not have required classifications.'.format(
                        data.name, type(data).__name__)

    # Output
    @staticmethod
    def write_file_access(exception):
        return "An error happened while writing to file. Make sure file is not open in another application.\n {}".format(
                str(exception))

    @staticmethod
    def write_file_general(exception):
        return "An error happened while writing to file.\n {}:".format(str(exception))

    # CLI
    @staticmethod
    def analysis_not_found(analysis_name):
        return '"{}" not found in defined analysis.'.format(analysis_name)

    @staticmethod
    def object_name_not_found(object_name, object_type):
        return '"{}" not found in defined "{}"(s)'.format(object_name, object_type)

    @staticmethod
    def key_not_found(object_type, object_name, key):
        return '"{}" key, not found in defined "{}" object named "{}"'.format(key, object_type,  object_name )

    @staticmethod
    def failed_in_server():
        return 'Process failed in server-side.'


# Todo: Decide where to read limit states from
LIMIT_STATES = ["No Damage", "Slight", "Moderate", "Extensive", "Complete"]
# Limit states compatible with database
LIMIT_STATES_DATABASE = ["no_damage", "slight", "moderate", "extensive", "complete"]
LIMIT_STATES_DISPLAY = {
    LIMIT_STATES_DATABASE[0]: 'No Damage',
    LIMIT_STATES_DATABASE[1]: 'Slight',
    LIMIT_STATES_DATABASE[2]: 'Moderate',
    LIMIT_STATES_DATABASE[3]: 'Extensive',
    LIMIT_STATES_DATABASE[4]: 'Complete',

}
LIMIT_STATES_COLORS = [[0, 0, 0], [0, 1, 0], [1, 1, 0], [1, 0.6471, 0], [1, 0, 0]]


# Todo: Read IM periods from database
class IMPeriods(type):
    def __getitem__(self, item):
        if item == 'PGV':
            return -1
        elif item == 'PGA':
            return 0
        else:
            return float(item)


class IM_PERIODS(metaclass=IMPeriods):
    pass


# Todo: Read IM names from database
class IMNames(type):
    def __getitem__(self, item):
        if item == 'PGV':
            return 'PGV (cm/s)'
        elif item == 'PGA':
            return 'PGA (cm/s\u00b2)'
        else:
            return 'Sa(' + item + ')'


class IM_DISPLAY_NAMES(metaclass=IMNames):
    pass
# IM_DISPLAY_NAMES = {
#     'PGA': 'PGA (cm/s\u00b2)',
#     'PGV': 'PGV (cm/s)'
# }


# Todo: Read IM units from database
class IMUnits(type):
    def __getitem__(self, item):
        if item == 'PGV':
            return 'cm/s'
        else:
            return 'cm/s2'


class IM_Units(metaclass=IMUnits):
    pass

# IM_Units = {
#     'PGA': 'cm/s^2',
#     'PGV': 'cm/s'
# }


IM_DISPLAY_LIMITS = {
    'PGA': [0, 1200],
    'PGV': [0, 25],
    '1.000': [0, 140]
}


CUSTOM_BRIDGE_CLASSES = ['MCSTEEL', 'MSSTEEL', 'MSPC', 'MSRC', 'MCRC-Slab', 'MSRC-Slab']
CUSTOM_BRIDGE_CLASSES_COLORS = [[0.301, 0.745, 0.933], [0, 0.447, 0.741], [1, 0, 0], [0, 0.75, 0.75],
                                [0.466, 0.674, 0.188], [0.929, 0.694, 0.125]]

ML_MODEL_TYPE_DICT = {
    'TensorFlow': 1,
    1: 'TensorFlow',
    'Scikit-Learn': 2,
    2: 'Scikit-Learn'
}


JET_COLORS = [(0.0, 0.0, 0.52), (0.0, 0.0, 0.56), (0.0, 0.0, 0.6), (0.0, 0.0, 0.64), (0.0, 0.0, 0.68), (0.0, 0.0, 0.72),
              (0.0, 0.0, 0.76), (0.0, 0.0, 0.8), (0.0, 0.0, 0.84), (0.0, 0.0, 0.88), (0.0, 0.0, 0.96), (0.0, 0.0, 1.0),
              (0.0, 0.04, 1.0), (0.0, 0.08, 1.0), (0.0, 0.12, 1.0), (0.0, 0.16, 1.0), (0.0, 0.2, 1.0), (0.0, 0.24, 1.0),
              (0.0, 0.28, 1.0), (0.0, 0.32, 1.0), (0.0, 0.36, 1.0), (0.0, 0.4, 1.0), (0.0, 0.44, 1.0), (0.0, 0.48, 1.0),
              (0.0, 0.52, 1.0), (0.0, 0.56, 1.0), (0.0, 0.6, 1.0), (0.0, 0.64, 1.0), (0.0, 0.68, 1.0), (0.0, 0.72, 1.0),
              (0.0, 0.76, 1.0), (0.0, 0.8, 1.0), (0.0, 0.84, 1.0), (0.0, 0.88, 1.0), (0.0, 0.92, 1.0), (0.0, 0.96, 1.0),
              (0.0, 1.0, 1.0), (0.04, 1.0, 0.96), (0.08, 1.0, 0.92), (0.12, 1.0, 0.88), (0.16, 1.0, 0.84),
              (0.2, 1.0, 0.8), (0.24, 1.0, 0.76), (0.28, 1.0, 0.72), (0.32, 1.0, 0.68), (0.36, 1.0, 0.64),
              (0.4, 1.0, 0.6), (0.44, 1.0, 0.56), (0.48, 1.0, 0.52), (0.52, 1.0, 0.48), (0.56, 1.0, 0.44),
              (0.6, 1.0, 0.4), (0.64, 1.0, 0.36), (0.68, 1.0, 0.32), (0.72, 1.0, 0.28), (0.76, 1.0, 0.24),
              (0.8, 1.0, 0.2), (0.84, 1.0, 0.16), (0.88, 1.0, 0.12), (0.92, 1.0, 0.08), (0.96, 1.0, 0.04),
              (1.0, 1.0, 0.0), (1.0, 0.96, 0.0), (1.0, 0.92, 0.0), (1.0, 0.88, 0.0), (1.0, 0.84, 0.0), (1.0, 0.8, 0.0),
              (1.0, 0.76, 0.0), (1.0, 0.72, 0.0), (1.0, 0.68, 0.0), (1.0, 0.64, 0.0), (1.0, 0.6, 0.0), (1.0, 0.56, 0.0),
              (1.0, 0.52, 0.0), (1.0, 0.48, 0.0), (1.0, 0.44, 0.0), (1.0, 0.4, 0.0), (1.0, 0.36, 0.0), (1.0, 0.32, 0.0),
              (1.0, 0.28, 0.0), (1.0, 0.24, 0.0), (1.0, 0.2, 0.0), (1.0, 0.16, 0.0), (1.0, 0.12, 0.0), (1.0, 0.08, 0.0),
              (1.0, 0.04, 0.0), (1.0, 0.0, 0.0), (0.96, 0.0, 0.0), (0.92, 0.0, 0.0), (0.88, 0.0, 0.0), (0.84, 0.0, 0.0),
              (0.8, 0.0, 0.0), (0.76, 0.0, 0.0), (0.72, 0.0, 0.0), (0.68, 0.0, 0.0), (0.64, 0.0, 0.0), (0.6, 0.0, 0.0),
              (0.56, 0.0, 0.0), (0.52, 0.0, 0.0)]
