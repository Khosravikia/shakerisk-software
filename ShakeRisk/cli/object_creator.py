from Definitions import ObjectCreationError, ErrorMessages
from cli.objects_IDs import CLASS_IDS
from infrastructure.output_manager.output_types import OUTPUT_PATH


class ObjectCreator:

    def __init__(self, method_name, method_type, json_data, model_form_data, method_form_data):
        self.method_name = method_name
        self.method_type = method_type
        self.json_data = json_data
        self.model_form_data = model_form_data
        self.method_form_data = method_form_data

    def _create_object(self, object_name, object_type):
        object_dict = {"name": object_name}

        # try:
        if object_name not in self.json_data['model'][object_type]:
            raise ObjectCreationError(ErrorMessages.object_name_not_found(object_name, object_type))

        for key, property_info in self.model_form_data[object_type]['form'].items():

            if key not in self.json_data['model'][object_type][object_name]:
                raise ObjectCreationError(ErrorMessages.key_not_found(object_type, object_name, key))

            property_value = self.json_data['model'][object_type][object_name][key]

            should_evaluate = True
            # If property has a dependency which is not met, return None
            if bool(property_info.get('dependency', False)):
                if self.json_data['model'][object_type][object_name][property_info['dependency']['when']] !=\
                        property_info['dependency']['is']:
                    should_evaluate = False
            # If property is empty string or the value "none", return None
            if (not property_value) or (property_value == "none"):
                should_evaluate = False

            if should_evaluate:
                if property_info['elementType'] == 'input-object':
                    object_dict.update({
                        key: self._create_object(property_value, property_info['elementSelectObjectType'])
                    })
                elif property_info['elementType'] == 'input-param':
                    defined_dict = CLASS_IDS[property_info['elementInputParamType']]
                    object_dict.update({
                        key: defined_dict['handler'](defined_dict['types'], self.json_data, property_value).execute()
                    })
                else:
                    object_dict.update({key: property_value})
            else:
                object_dict.update({key: None})
        # except KeyError as exp:
        #     print("Error occurred when trying to create object of type {} with the name {}".format(object_type, object_name))
        #     raise exp

        return CLASS_IDS[object_type].from_dict(object_dict)

    # def _evaluate_param(self, entered_param_name):
    #     for param_object_name in CLASS_IDS['params'].keys():
    #         if bool(self.json_data['model'].get(param_object_name, False)):
    #             for defined_param_name, values in self.json_data['model'][param_object_name].items():
    #                 if defined_param_name == entered_param_name:
    #                     return CLASS_IDS['params'][param_object_name](values)

    def execute(self):
        # Todo: Take care of code duplication between this function and "_create_object"
        models_to_create = {}  # A dictionary of properties which are objects and need to be created for the request
        request_dict = {"name": self.json_data['analyseToRun']}  # A dictionary which will be converted to request object for the use case
        # try:
        for property_name, property_info in self.method_form_data[self.method_type]['form'].items():

            if property_name not in self.json_data['method'][self.method_type][self.method_name]:
                raise ObjectCreationError(ErrorMessages.key_not_found(self.method_type, self.method_name, property_name))

            property_value = self.json_data['method'][self.method_type][self.method_name][property_name]

            should_evaluate = True
            # If property has a dependency which is not met, return None
            if bool(property_info.get('dependency', False)):
                if self.json_data['method'][self.method_type][self.method_name][property_info['dependency']['when']] !=\
                        property_info['dependency']['is']:
                    should_evaluate = False
            # If property is empty string or the value "none", return None
            if (not property_value) or (property_value == "none"):
                should_evaluate = False

            if should_evaluate:
                if property_info['elementType'] == 'input-object':  # Recursively create object properties
                    models_to_create.update({property_name: {
                        "object_type": property_info['elementSelectObjectType'],
                        "object_name": property_value}
                    })
                elif property_info['elementType'] == 'input-param':
                    defined_dict = CLASS_IDS[property_info['elementInputParamType']]
                    request_dict.update({
                        property_name:
                            defined_dict['handler'](defined_dict['types'], self.json_data, property_value).execute()
                    })

                else:
                    request_dict.update({property_name: property_value})
            else:
                request_dict.update({property_name: None})

        for property_name, object_info in models_to_create.items():
            request_dict.update(
                {property_name: self._create_object(object_info["object_name"], object_info["object_type"])})

        # except KeyError as exp:
        #     print("Error occurred when trying to create object of type {} with the name {}".format(self.method_type, self.method_name))
        #     raise exp

        return request_dict
        # # Set output path to None if no value is available in request,
        # # which will skip the output file creation in output manager
        # output_path = None
        # if bool(request_dict.get(OUTPUT_PATH, False)):
        #     output_path = request_dict[OUTPUT_PATH]
        #
        # request_object = CLASS_IDS[self.method_type]['request'].from_dict(request_dict)
        # usecase = CLASS_IDS[self.method_type]['usecase']
        # return [request_object, usecase, output_path]
