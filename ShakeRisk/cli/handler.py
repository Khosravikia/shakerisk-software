import sys
# import os
from pathlib import Path
import shutil
import json
import logging

# Add ShakeRisk as Module
# sys.path.append(os.path.dirname(sys.path[0]))
sys.path.append(str(Path(sys.path[0]).parent))

from Definitions import ROOT_DIR, ResourceError, ObjectCreationError, ErrorMessages
from core.methods.utils import Timing
from cli.object_creator import ObjectCreator
from cli.objects_IDs import CLASS_IDS
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.handler import Handler as OutputHandler  # , create_output_from_error
from infrastructure.output_manager.output_types import FIGURE, CONSOLE, OUTPUT_PATH


class JSONHandler:

    def __init__(self, json_data):
        self.json_data = json_data
        self.analysis_name = json_data['analyseToRun']
        self.model_form_data = json.load(
            open(Path(ROOT_DIR, 'DjangoFront/react/frontend/src/constants/Forms/modelFormsStructures.json')))
        self.method_form_data = json.load(
            open(Path(ROOT_DIR, 'DjangoFront/react/frontend/src/constants/Forms/methodFormsStructures.json')))

    def _get_analysis_type(self):
        for type_name, type_values in self.json_data['method'].items():
            for name in type_values:
                if self.analysis_name == name:
                    return type_name

    def execute(self):

        timing_total = Timing('Run')

        try:
            analysis_type = self._get_analysis_type()
            if analysis_type is None:
                # return OutputHandler.create_failed_output('"{}" not found in defined analysis.'.format(self.analysis_name))
                return OutputHandler.create_failed_output(ErrorMessages.analysis_not_found(self.analysis_name))

                # return create_output_from_error(res.ResponseFailure.build_parameters_error(
                #     '"{}" not found in defined analysis.'.format(self.analysis_name)))
                # return {'STATUS': 400, CONSOLE: ['"{}" not in defined analysis.'.format(self.analysis_name)]}

            request_dict = ObjectCreator(
                self.analysis_name, analysis_type, self.json_data, self.model_form_data, self.method_form_data).execute()

            # Set output path to None if no value is available in request,
            # which will skip the output file creation in output manager
            output_path = request_dict.get(OUTPUT_PATH, None)

            # Create request object from dictionary
            # Use case will return invalid response if request is not valid (No need to check request object here)
            request_object = CLASS_IDS[analysis_type]['request'].from_dict(request_dict)
            usecase = CLASS_IDS[analysis_type]['usecase']

            timing_total.log("Object Creation")
            timing = Timing('"{}" Analysis'.format(request_object.name if request_object else ''))

            response = usecase.execute(request_object)

            timing.log_end()
            if not response:
                return OutputHandler.create_failed_output(response, self.analysis_name)
                # return create_output_from_error(response)
                # return {'STATUS': 400, CONSOLE: [response.message]}

            timing = Timing('Output Generation')

            usecase_output = CLASS_IDS[analysis_type]['output']  # (self.request_object, response).execute()
            output = OutputHandler(usecase_output, request_object, response, output_path).execute()
            timing.log_end()

        except ObjectCreationError as ex:
            logging.exception("Exception happened while running the analysis.")
            return OutputHandler.create_failed_output(ex, self.analysis_name)
            # return OutputObject.from_exception(ex)
            # return create_output_from_error(res.ResponseFailure.build_parameters_error(ex))

        except ResourceError as ex:
            logging.exception("Exception happened while running the analysis.")
            return OutputHandler.create_failed_output(ex, self.analysis_name)
            # return OutputObject.from_exception(ex)
            # return create_output_from_error(res.ResponseFailure.build_resource_error(ex))

        except Exception as ex:
            logging.exception("Exception happened while running the analysis.")
            # return OutputHandler.create_failed_output("Process failed in server-side.", self.analysis_name)
            return OutputHandler.create_failed_output(ErrorMessages.failed_in_server(), self.analysis_name)

            # return OutputObject.from_failed_msg("Process failed in server-side.")
            # return create_output_from_error(res.ResponseFailure.build_system_error("Process failed in server-side."))
            # output = {'STATUS': 500, CONSOLE: [res.ResponseFailure.build_system_error(ex).message]}

        # except ResourceError as ex:
        #     logging.exception("Exception happened while running the analysis")
        #     response = res.ResponseFailure.build_system_error(ex)
        # except Exception as ex:
        #     logging.exception("Exception happened while running the analysis")
        #     return {'STATUS': 500, CONSOLE: ['Internal server error.']}

        timing_total.log_end()
        return output


if __name__ == '__main__':
    script, json_path = sys.argv
    json_obj = json.load(open(json_path))

    handler = JSONHandler(json_obj)
    result = handler.execute()

    result.print_console()
