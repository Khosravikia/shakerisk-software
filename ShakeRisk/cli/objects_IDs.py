from cli.imports import *

# Todo: Do not instantiate usecase class input here
CLASS_IDS = {

    # Params
    'random_variable': {
        'handler': RandomVariableHelper,
        'types': {
            'param_constant': param_constant,
            'param_discrete_uniform': param_discrete_uniform,
            'param_discrete_poisson': param_discrete_poisson,
            'param_continuous_normal': param_continuous_normal,
            'param_continuous_lognormal': param_continuous_lognormal,
            'param_continuous_uniform': param_continuous_uniform,
            'param_continuous_exponential': param_continuous_exponential
        }
    },
    'ml_model_options': {
        'handler': MLModelHelper,
        'types': {
            'MLM_ANN': DenseNNOptions,
            'MLM_random_forest': RandomForestOptions,
            'MLM_SVM': SVMOptions,
            'MLM_linear': LinearOptions
        }
    },
    'param_text': {
        'handler': TextParamHelper,
        'types': {
            'param_text': None
        }
    },

    # Entities
    "earthquake": Earthquake,
    "intensity_model": IntensityModel,
    "bridge": Bridge,
    "bridge_fragility_model": BridgeFragilityModel,
    "bridge_construction_cost_model": BridgeConstructionCostModel,
    "bridge_repair_cost_model": BridgeRepairCostModel,
    "bridge_restoration_model": BridgeRestorationModel,
    "building": Building,
    "building_fragility_model": BuildingFragilityModel,
    "building_construction_cost_model": BuildingConstructionCostModel,
    "building_repair_cost_model": BuildingRepairCostModel,
    "transportation_network": TransportationNetwork,
    "system_model_frank_wolfe": SystemModelFrankWolfe,
    "vs30": Vs30,
    "nbi_data_source": NBIDataSource,
    "time_interval": TimeInterval,
    "rectangular_area": RectangularArea,
    "location_point": Point,
    "location_multiple_points": MultiplePoints,
    "location_polygon": Polygon,
    "data_frame": DataFrame,
    "MLM_trained_model": MLTrainedModel,
    "loss_function": LossFunction,

    # Usecases
    "shaking_evaluation":
        {'request': ShakingLevelEstimationRequest,
         'usecase': ShakingLevelEstimationUseCase(Vs30Repository(), GmmLoader(), SpatialCorrelationLoader()),
         'output': ShakingLevelEstimationOutput
         },
    "bridge_damage_estimation":
        {'request': BridgeDamageEstimationRequest,
         'usecase': BridgeDamageEstimationUseCase(Vs30Repository(), BridgeFragilityRepository(),
                                                  BridgeRepository(), BridgeConstructionCostRepository(),
                                                  BridgeRepairCostRepository(), GmmLoader(),
                                                  SpatialCorrelationLoader()),
         'output': BridgeDamageEstimationOutput
         },
    "bridge_repair_cost_estimation":
        {'request': BridgeRepairCostEstimationRequest,
         'usecase': BridgeRepairCostEstimationUseCase(Vs30Repository(), BridgeFragilityRepository(),
                                                      BridgeRepository(), BridgeConstructionCostRepository(),
                                                      BridgeRepairCostRepository(), GmmLoader(),
                                                      SpatialCorrelationLoader()),
         'output': BridgeRepairCostEstimationOutput
         },
    "bridge_inspection_prioritization":
        {'request': BridgeInspectionPrioritizationRequest,
         'usecase': BridgeInspectionPrioritizationUseCase(Vs30Repository(), BridgeFragilityRepository(),
                                                          BridgeRepository(), GmmLoader(),
                                                          SpatialCorrelationLoader()),
         'output': BridgeInspectionPrioritizationOutput
         },
    "building_damage_estimation":
        {'request': BuildingDamageEstimationRequest,
         'usecase': BuildingDamageEstimationUseCase(Vs30Repository(), BuildingFragilityRepository(),
                                                    BuildingRepository(), BuildingConstructionCostRepository(),
                                                    BuildingRepairCostRepository(), GmmLoader(),
                                                    SpatialCorrelationLoader()),
         'output': BuildingDamageEstimationOutput
         },
    "building_repair_cost_estimation":
        {'request': BuildingRepairCostEstimationRequest,
         'usecase': BuildingRepairCostEstimationUseCase(Vs30Repository(), BuildingFragilityRepository(),
                                                        BuildingRepository(), BuildingConstructionCostRepository(),
                                                        BuildingRepairCostRepository(), GmmLoader(),
                                                        SpatialCorrelationLoader()),
         'output': BuildingRepairCostEstimationOutput
         },
    "building_inspection_prioritization":
        {'request': BuildingInspectionPrioritizationRequest,
         'usecase': BuildingInspectionPrioritizationUseCase(Vs30Repository(), BuildingFragilityRepository(),
                                                            BuildingRepository(), GmmLoader(),
                                                            SpatialCorrelationLoader()),
         'output': BuildingInspectionPrioritizationOutput
         },
    "network_damage_estimation":
        {'request': NetworkDamageEstimationRequest,
         'usecase': NetworkDamageEstimationUseCase(Vs30Repository(), BridgeFragilityRepository(),
                                                   BridgeRepository(), BridgeConstructionCostRepository(),
                                                   BridgeRepairCostRepository(), BridgeRestorationRepository(),
                                                   TransportationNetworkRepository(),
                                                   GmmLoader(), SpatialCorrelationLoader()),
         'output': NetworkDamageEstimationOutput
         },
    "network_inspection_prioritization":
        {'request': NetworkInspectionPrioritizationRequest,
         'usecase': NetworkInspectionPrioritizationUseCase(Vs30Repository(), BridgeFragilityRepository(),
                                                           BridgeRepository(), BridgeConstructionCostRepository(),
                                                           BridgeRepairCostRepository(), BridgeRestorationRepository(),
                                                           TransportationNetworkRepository(),
                                                           GmmLoader(), SpatialCorrelationLoader()),
         'output': NetworkInspectionPrioritizationOutput
         },
    "earthquake_data":
        {'request': EqDataGetRequest,
         'usecase': EqDataGetUseCase(),
         'output': EqDataGetOutput
         },
    "nbi_data_conversion":
        {'request': BridgeNbiConvertRequest,
         'usecase': BridgeNbiConvertUseCase(NBIRepository()),
         'output': BridgeNBIConvertOutput
         },
    "dataframe_save":
        {
            'request': DataFrameSaveRequest,
            'usecase': DataFrameSaveUseCase(),
            'output': DataFrameSaveOutput
        },
    "ML_based_model":
        {
            'request': MLBasedModelRequest,
            'usecase': MLBasedModelUseCase(MLBasedModelCreator()),
            'output': MLBasedModelOutput
        },
    "monte_carlo_simulation":
        {
            'request': MonteCarloSimulationRequest,
            'usecase': MonteCarloSimulationUseCase(),
            'output': MonteCarloSimulationOutput
        }
}
