from Definitions import ROOT_DIR
from cli.helpers.utils import find_param_with_same_name


class TextParamHelper:
    # Todo: Use pathlib to join path. Keep in mind there is text formatting option
    keywords = {
        "DEMO_DIR": ROOT_DIR + "\\..\\demo"
    }

    def __init__(self, types: dict, json_data: dict, property_value: str):
        self.types = types
        self.json_data = json_data
        self.property_value = property_value

    def execute(self):
        property_value = [x.strip() for x in self.property_value.split(',') if x != '']
        # If first entry of property_value matches a text parameter object, return object "text" value,
        # else return property value itself after possible formatting
        [_, values] = find_param_with_same_name(self.json_data, self.types, property_value[0], required=False)
        if values is None:
            return self.property_value
        else:
            if len(property_value) == 1:
                return self.keywords_check(values['text'])
            else:
                return self.keywords_check(values['text'].format(*property_value[1:]))

    def keywords_check(self, text: str):
        for keyword, value in self.keywords.items():
            text = text.replace(keyword, value)
        return text
