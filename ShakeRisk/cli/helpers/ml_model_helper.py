from cli.helpers.utils import find_param_with_same_name


class MLModelHelper:
    def __init__(self, types: dict, json_data: dict, property_value: str):
        self.types = types
        self.json_data = json_data
        self.property_value = property_value

    def execute(self):
        [param_type, values] = find_param_with_same_name(self.json_data, self.types, self.property_value)
        values.update({'model_name': param_type})  # Add ML model name to values
        return self.types[param_type].from_dict(values)  # Return options object of respective ML model
