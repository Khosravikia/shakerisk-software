from Definitions import ObjectCreationError


def find_param_with_same_name(json_data, types, property_value, required=True):
    # Go through possible "types" for the parameter
    for param_type in types.keys():
        # check if "type" is defined in JSON data
        if bool(json_data['model'].get(param_type, False)):
            # Go through defined objects of the "type" in JSON data
            for defined_param_name, values in json_data['model'][param_type].items():
                #  Use values when the object with the target name is found
                if defined_param_name == property_value:
                    return [param_type, values]
    else:
        if required:
            raise ObjectCreationError(
                '"{}" could not be found in any of the possible related object types.\n Possible related objects: {}'.format(
                    property_value, types.keys()
                ))
        return [None, None]
