import numpy as np
from cli.helpers.utils import find_param_with_same_name
from core.methods.random_generation_methods import get_random_value
# Todo: check https://numpy.org/doc/stable/reference/random/index.html for new way of generating randoms


class RandomVariableHelper:

    def __init__(self, types: dict, json_data: dict, property_value: str):
        self.types = types
        self.json_data = json_data
        self.property_value = property_value

    def execute(self):

        # Convert to float and return if entered value is a number
        if isinstance(self.property_value, str):
            if self.property_value.replace('.', '', 1).replace('-', '', 1).isdigit():
                return float(self.property_value)

            else:
                [param_type, values] = find_param_with_same_name(self.json_data, self.types, self.property_value)
                return self.types[param_type](values)
        elif isinstance(self.property_value, int) or isinstance(self.property_value, float):
            return self.property_value
        else:
            raise ValueError("{} is not supported as a numeric parameter input".format(type(self.property_value)))


def param_constant(values):
    return values['value']


def param_discrete_uniform(values):
    return get_random_value('discrete', 'uniform', values['minimum'], values['maximum'], values['n'])
    # return np.random.choice(np.linspace(values['minimum'], values['maximum'], values['n']))


def param_discrete_poisson(values):
    return get_random_value('discrete', 'possion', values['lam'])


def param_continuous_normal(values):
    return get_random_value('continuous', 'normal', values['loc'], values['scale'])
    # return np.random.normal(values['loc'], values['scale'])


def param_continuous_lognormal(values):
    return get_random_value('continuous', 'lognormal', values['mean'], values['sigma'])
    # return np.random.lognormal(values['mean'], values['sigma'])


def param_continuous_uniform(values):
    return get_random_value('continuous', 'uniform', values['minimum'], values['maximum'])
    # return np.random.uniform(values['minimum'], values['maximum'])


def param_continuous_exponential(values):
    return get_random_value('continuous', 'exponential', values['scale'])
    # return np.random.exponential(values['scale'])
