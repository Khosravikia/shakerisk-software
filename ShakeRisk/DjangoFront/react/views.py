import logging
import os
import json
import shutil

from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.http import HttpResponse, JsonResponse
from django.conf import settings

from cli.handler import JSONHandler
from infrastructure.output_manager.output_types import INFO, STATUS, SUCCESS


class FrontendAppView(View):
    """
    Serves the compiled frontend entry point (only works if you have run `npm
    run build`).
    """

    def get(self, request):
        try:
            with open(os.path.join(settings.REACT_APP_DIR, 'build', 'index.html')) as f:
                return HttpResponse(f.read())
        except FileNotFoundError:
            logging.exception('Production build of app not found')
            return HttpResponse(
                """
                This URL is only used when you have built the production
                version of the app. Visit http://localhost:3000/ instead, or
                run `yarn run build` to test the production version.
                """,
                status=501,
            )


# Todo: Take care of CSRF if going online
@csrf_exempt
def send_run_json(request):
    if request.method == 'POST':
        # print(request.body)
        json_data = json.loads(request.body)

        handler = JSONHandler(json_data)
        result = handler.execute()

        result.print_console()

        status = 200
        if not result:
            status = 400
        return JsonResponse(result.to_dict(), status=status)
    
    return HttpResponse("Request received!")
