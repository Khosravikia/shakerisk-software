import methodFormStructuer from './methodFormsStructures.json';
import modelFormStructuer from './modelFormsStructures.json';

export const modelFormsStructures = modelFormStructuer;
export const methodFormsStructures = methodFormStructuer;