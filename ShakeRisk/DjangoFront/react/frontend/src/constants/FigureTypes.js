export const FIGURE = 'FIGURE'

export const FOLIUM_MAP = 'FOLIUM_MAP';
export const PLOTLY_GRAPH = 'PLOTLY_GRAPH'
export const MATPLOTLIB_FIGURE = 'MATPLOTLIB_FIGURE'
export const DATAFRAME_TABLE = 'DATAFRAME_TABLE'

export const CONSOLE = 'CONSOLE'

export const INFO = 'INFO'
// export const HAS_WARNING = 'HAS_WARNING'
// export const HAS_ERROR = 'HAS_ERROR'
export const ANALYSIS_GIVEN_NAME = 'ANALYSIS_GIVEN_NAME'