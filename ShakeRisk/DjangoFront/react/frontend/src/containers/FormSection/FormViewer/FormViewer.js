import React, {Component} from 'react'
import { connect } from 'react-redux';

import * as actionCreators from '../../../store/actions/index';

import classes from './FormViewer.module.css';

import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';
import formObjecGenerator from '../../../components/Generators/FormObjectGenerator/formObjectGenerator';
import {modelFormsStructures, methodFormsStructures} from '../../../constants/Forms/formStructures';

import logger  from '../../../components/logger/logger';

class FormViewer extends Component {

    
    state = {
        selectedObjectCategory: null,
        selectedObjectType : null,
        form: null,
        // {
    //         name: {
    //             elementType: 'input',
    //             elementConfig: {
    //                 type: 'text',
    //                 placeholder: 'Your Name'
    //             },
    //             value: '',
    //             validation: {
    //                 required: true
    //             },
    //             valid: false,
    //             touched: false
    //         },
    //         street: {
    //             elementType: 'input',
    //             elementConfig: {
    //                 type: 'text',
    //                 placeholder: 'Street'
    //             },
    //             value: '',
    //             validation: {
    //                 required: true
    //             },
    //             valid: false,
    //             touched: false
    //         },
    //         zipCode: {
    //             elementType: 'input',
    //             elementConfig: {
    //                 type: 'text',
    //                 placeholder: 'ZIP Code'
    //             },
    //             value: '',
    //             validation: {
    //                 required: true,
    //                 minLength: 5,
    //                 maxLength: 5,
    //                 isNumeric: true
    //             },
    //             valid: false,
    //             touched: false
    //         },
    //         country: {
    //             elementType: 'input',
    //             elementConfig: {
    //                 type: 'text',
    //                 placeholder: 'Country'
    //             },
    //             value: '',
    //             validation: {
    //                 required: true
    //             },
    //             valid: false,
    //             touched: false
    //         },
    //         email: {
    //             elementType: 'input',
    //             elementConfig: {
    //                 type: 'email',
    //                 placeholder: 'Your E-Mail'
    //             },
    //             value: '',
    //             validation: {
    //                 required: true,
    //                 isEmail: true
    //             },
    //             valid: false,
    //             touched: false
    //         },
    //         deliveryMethod: {
    //             elementType: 'select',
    //             elementConfig: {
    //                 options: [
    //                     {value: 'fastest', displayValue: 'Fastest'},
    //                     {value: 'cheapest', displayValue: 'Cheapest'}
    //                 ]
    //             },
            //     value: 'fastest',
            //     validation: {},
            //     valid: true
            // }
        // },
        formIsValid: false,
        loading: false
    }

    static getDerivedStateFromProps(props, state) {
        // Re-run the form genration whenever the selectedObjectType changes.
        // Use memorization helpers if this gets too complicated.
        if ( props.selectedObjectType !== state.selectedObjectType ||
             props.selectedObjectCategory !== state.selectedObjectCategory ) 
        {
            // Import and generate form object based on selected objecty category
            const form = formObjecGenerator((props.selectedObjectCategory === 'model')?
             modelFormsStructures[props.selectedObjectType]['form'] :methodFormsStructures[props.selectedObjectType]['form']
             , props.selectedObjectCategory, props.selectedObjectType, props.storedObjects, props.selectedObjectName);
            
            // Todo: do not repeat this function from input change handler to check whole form validity
            let formIsValid = true;
            for (let inputIdentifier in form) {
                formIsValid = form[inputIdentifier].valid && formIsValid;
            }
            return ({
                    selectedObjectCategory: props.selectedObjectCategory,
                    selectedObjectType : props.selectedObjectType,
                    form: form,
                    formIsValid: formIsValid
            });
        }
        return null;
      }


    submitHandler = ( event ) => {
        event.preventDefault();
        this.setState( { loading: true } );
        const formData = {};

        //Convert number type inputs from string to actuall floats
        for (let formElementIdentifier in this.state.form) {
            let convertToFloat = false;
            
            if (this.state.form[formElementIdentifier]["elementType"] ===  "input")
                if (this.state.form[formElementIdentifier]["elementConfig"]["type"] === "number")
                    convertToFloat = true;
        
            convertToFloat? 
            formData[formElementIdentifier] = parseFloat(this.state.form[formElementIdentifier].value) :
            formData[formElementIdentifier] = this.state.form[formElementIdentifier].value;
            
        }
        this.props.onSubmitForm(formData);
        this.setState( { loading: false } );

        if(this.props.selectedObjectName == null){
            logger.info('Object "' + formData['objectName'] + '" has been successfully created.');
        }
        else{
            logger.info('Object "' + formData['objectName'] + '" has been successfully updated.');
        }

    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }
        
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            // const pattern = /^\d+$/;
            const pattern = /^[+-]?\d+(\.\d+)?$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.form
        };
        const updatedFormElement = { 
            ...updatedForm[inputIdentifier]
        };

        const target = event.target;
        updatedFormElement.value = target.type === 'checkbox' ? target.checked : target.value;
        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;
        
        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        this.setState({form: updatedForm, formIsValid: formIsValid});
        
        // if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') // if development mode
        // {
        //     const warn = Math.random() < 0.5
        //     if(warn)
        //         logger.warn("This is a warning");
        //     else
        //         logger.error("This is an error");
        // }
        
    }

    render () {
 
        let form;
        
        if ( this.state.loading ) {
            form = <Spinner />;
        }
        else if(this.props.selectedObjectType){            
                
            const formElementsArray = [];
            for (let key in this.state.form) {
                formElementsArray.push({
                    id: key,
                    config: this.state.form[key]
                });
            }
            

            form = (
                <form onSubmit={this.submitHandler}>
                    {formElementsArray.map(formElement => (
                        <Input 
                            key={formElement.id}
                            label={formElement.config.elementLabel}
                            elementType={formElement.config.elementType}
                            elementConfig={formElement.config.elementConfig}
                            value={formElement.config.value}
                            invalid={!formElement.config.valid}
                            shouldValidate={formElement.config.validation}
                            shouldHide={formElement.config.dependency? (this.state.form[formElement.config.dependency.when].value === formElement.config.dependency.is? false : true ): false}
                            touched={formElement.config.touched}
                            disabled={formElement.config.elementConfig.disabled}
                            changed={(event) => this.inputChangedHandler(event, formElement.id)}/>
                    ))}
                    <Button style={{float: "left"}} btnType="Success" disabled={!this.state.formIsValid}>Save</Button>
                    
                </form>
                );
        }
        else{
            form = <div>Please select an object.</div>;
        }
        return (
            <div className={classes.ContactData}>
                <div className={classes.FormHeader}>
                    <span style={{borderRight:'solid #cbcbcb 1px'}}>Property</span>
                    <span>Value</span></div>
                {form}
                
                <Button btnType="Primary" clicked={() => this.props.onDiscardFrom()}>Discard</Button>
                {/* <Button btnType="Success" clicked={this.props.onSelectObjectType}>Select</Button> */}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        storedObjects: state.form.storedObjects,
        selectedObjectCategory: state.form.selectedObjectCategory,
        selectedObjectType: state.form.selectedObjectType,
        selectedObjectName: state.form.selectedObjectName
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmitForm: (formData) => dispatch(actionCreators.formSubmit(formData)),
        onDiscardFrom: () => dispatch(actionCreators.setEditMode(false))
        
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(FormViewer)