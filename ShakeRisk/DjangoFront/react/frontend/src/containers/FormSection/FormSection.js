import React, {PureComponent} from 'react'
import { connect } from 'react-redux'
import classes from './FormSection.module.css'
import FormViewer from './FormViewer/FormViewer';
import ObjectListViewer from './ObjectListViewer/ObjectListViewer';

class FormSection extends PureComponent  {

    
    render () {
        
        return(
            <div className={classes.ObjectList}>
                <div className={classes.Header}> 
                    <span className={classes.Title}>
                        {this.props.categoryDisplayName? this.props.categoryDisplayName: 'Object Viewer'} : {this.props.typeDisplayName}
                        {this.props.isEditMode? this.props.selectedObjectName? ' : ' + this.props.selectedObjectName : ' : New' : ''} 
                    </span>
                </div>
                <div className={classes.ListHolder}>
                    {this.props.isEditMode? <FormViewer></FormViewer> : <ObjectListViewer></ObjectListViewer>}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        isEditMode: state.form.isEditMode,
        categoryDisplayName: state.form.categoryDisplayName,
        typeDisplayName: state.form.typeDisplayName,
        selectedObjectName: state.form.selectedObjectName
    };
};

// const mapDispatchToProps = dispatch => {
//     return {
//         onSubmitForm: (formData) => dispatch(actionCreators.formSubmit(formData)),
//         onSelectObjectType: (objectType) => dispatch(actionCreators.selectObjectType("earthquake"))
        
//     };
// };
export default connect(mapStateToProps)(FormSection)