import React, {Component} from 'react'
import { connect } from 'react-redux';

import * as actionCreators from '../../../store/actions/index';
import findNameUsages from './findNameUsages';
import classes from './ObjectListViewer.module.css';
import deleteIcon from '../../../images/icons/delete.png';
import renameIcon from '../../../images/icons/rename.png';
// import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
// import Spinner from '../../../components/UI/Spinner/Spinner';
import logger from '../../../components/logger/logger'

class ObjectListViewer extends Component {

    state = {
        objectNameTobeRenamed : null,
        objectNewname: null
    }

    addNewText = "Add New";

    clickHandler = (objectName) =>{
        // Do not edit object if user is selecting the name (so user can copy the name without going to edit mode)
        if(!window.getSelection().toString().length){
        this.props.onSelectObjectName(objectName, true);
        }
    }

    deleteClickHandler = (objectName) => {
        this.props.onDeleteObject(this.props.selectedObjectCategory, this.props.selectedObjectType, objectName);
        logger.info('Object "' + objectName + '" has been successfully deleted.');
    }

    //Edit name functions:
    editNameClickHandler = (objectName) => {
        this.setState({objectNameTobeRenamed: objectName,
            objectNewname: objectName});
    }
    onChangeHandler = (event) => {
        this.setState({objectNewname: event.target.value});
    }
    editNameSubmitHandler = (event) => {
        event.preventDefault();
        const oldName = this.state.objectNameTobeRenamed;
        const newName = this.state.objectNewname;
        // Discard if new name is same as old one
        if (this.state.objectNameTobeRenamed ===  this.state.objectNewname){
            this.setState( {objectNameTobeRenamed: null,
                objectNewname: null})
        }
        else{
            // Method names are not used in any other object, so search only if model name is editted
            let relatedObjects = [];
            if(this.props.selectedObjectCategory === 'model'){
                relatedObjects = findNameUsages(this.props.storedObjects, this.props.selectedObjectType, this.state.objectNameTobeRenamed);
            }
            
            this.props.onchangeObjectName(this.props.selectedObjectCategory, this.props.selectedObjectType,
                 this.state.objectNameTobeRenamed, this.state.objectNewname, relatedObjects);
            
            let loggerText = 'Object "' +  oldName +  '" has been successfully renamed to "' + newName +'" .';
            // Print number of affected objects by rename, if any.
            if(relatedObjects.length > 0){
                loggerText += '\n' + relatedObjects.length +  ' object(s) were affected by the rename.';
            }
            logger.info(loggerText);
        }
    }

    render () {
        let list =[];
        if (this.props.selectedObjectType){
            // Get list of selected object type if any
            // Without try, will throw error if no previous object of this type is defined
            try{
                list = Object.keys(this.props.storedObjects[this.props.selectedObjectCategory][this.props.selectedObjectType]);
            } catch{
    
            }

            // If item name is same as the one to be renamed render a form, otherwise render normally
            list = list.map((item) => {
                if(item !== this.state.objectNameTobeRenamed){
                    return (<li key={item} className={classes.Li}>
                        <span className={classes.Item} onClick={() => this.clickHandler(item)}>{item}</span>

                        <span className={[classes.EditIcon, 'tooltip-parent'].join(' ')} onClick={() => {this.editNameClickHandler(item)}}>
                            <img  src={renameIcon} alt='close'></img>
                            <span className='tooltip'>Rename</span>
                        </span>

                        <span className={[classes.DeleteIcon, 'tooltip-parent'].join(' ')} onClick={() => {this.deleteClickHandler(item)}}>
                            <img src={deleteIcon} alt='delete'></img>
                            <span className='tooltip'>Delete</span>
                        </span>
                        
                    </li>);
                }
                else{
                    return (<li key={item} className={classes.Li}>
                        <form className={classes.EditNameForm} onSubmit={(event) => this.editNameSubmitHandler(event)}>
                            <input type="text" value={this.state.objectNewname} onChange={(event) => this.onChangeHandler(event)}></input>
                            <Button style={{margin:0, padding:0, width:"20%"}}>Save</Button>
                        </form>
                    </li>

                    )
                }
                
            });

            // Header: Show object category and header
            // list.unshift(<li key="Header">
            //     <span className={[classes.Item, classes.Header].join(' ')} >{this.props.categoryDisplayName} : {this.props.typeDisplayName}</span>
            // </li>)

            // "Add new" lits item
            list.push(<li style={{zIndex:1}}key={this.addNewText} className={classes.Li}>
                <span className={[classes.Item, classes.NewItem].join(' ')} onClick={() => this.clickHandler(null)}>{this.addNewText}</span>
            </li>)
        
            
        }  
        else{
            list = null;
        }
        return (
                    <ul className={classes.Ul}>
                        {list? list : <div>Please select an object.</div>}
                    </ul>
        );
    }
}

const mapStateToProps = state => {
    return {
        storedObjects: state.form.storedObjects,
        selectedObjectCategory: state.form.selectedObjectCategory,
        selectedObjectType: state.form.selectedObjectType,
        categoryDisplayName: state.form.categoryDisplayName,
        typeDisplayName: state.form.typeDisplayName
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSetEditMode: () => dispatch(actionCreators.setEditMode(true)),
        onSelectObjectName: (objectName, isEditMode) => dispatch(actionCreators.selectObjectName(objectName, isEditMode)),
        onDeleteObject: (objectCategory, objectType, objectName, isEditMode) =>
         dispatch(actionCreators.objectDelete(objectCategory, objectType, objectName, isEditMode)),
        onchangeObjectName: (objectCategory, objectType, objectOldName, objectNewName, relatedObjects, isEditMode) =>
         dispatch(actionCreators.objectChangeName(objectCategory, objectType, objectOldName, objectNewName, relatedObjects, isEditMode))        
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(ObjectListViewer)