import modelForms from '../../../constants/Forms/modelFormsStructures.json';
import methodForms from '../../../constants/Forms/methodFormsStructures.json';


const findNameUsages = (storedObjects, selectedObjectType, oldObjectName) => {
    let possibleRelations = [];
    let renamedObjectParamType = false;
    if('info' in modelForms[selectedObjectType]){
        if('param' in modelForms[selectedObjectType]['info']){
            renamedObjectParamType = modelForms[selectedObjectType]['info']['param']['type'];
        }
    }
    possibleRelations = possibleRelations.concat(findPossibleRelations(modelForms, selectedObjectType, 'model',renamedObjectParamType));
    possibleRelations = possibleRelations.concat(findPossibleRelations(methodForms, selectedObjectType, 'method', renamedObjectParamType));

    const relatedObjects= relatedStoredObjects(storedObjects, possibleRelations, oldObjectName);
    return relatedObjects;
}

const findPossibleRelations = (formStructure, selectedObjectType, category,renamedObjectParamType) => {
    const possibleRealtions = [];
    for(const objetType in formStructure){
        for(const property in formStructure[objetType]['form']){
            // If property is directly linked to an object
            if(formStructure[objetType]['form'][property]['elementType'] === 'input-object'){
                if(formStructure[objetType]['form'][property]['elementSelectObjectType'] === selectedObjectType){
                    possibleRealtions.push({category: category, type: objetType, property: property})
                }
            }
            //If object renamed is of type param
            else if(renamedObjectParamType){
                // If property can be linked to a param object
                if(formStructure[objetType]['form'][property]['elementType'] === 'input-param'){
                    if(formStructure[objetType]['form'][property]['elementInputParamType'] === renamedObjectParamType){
                        possibleRealtions.push({category: category, type: objetType, property: property})
                    }
                }
            }
            
        }
    }
    return possibleRealtions;
}

const relatedStoredObjects = (storedObjects, possibleRelations, oldObjectName) => {
    const relatedObjects = [];
    possibleRelations.forEach(rel => {
        if(rel['category'] in storedObjects){
            if(rel['type'] in storedObjects[rel['category']]){
                if(Object.keys(storedObjects[rel['category']][rel['type']]).length >= 1){
                    for(const storedObject in storedObjects[rel['category']][rel['type']]){
                        const storedObjectPropertyName = storedObjects[rel['category']][rel['type']][storedObject][rel['property']];
                        if(oldObjectName === storedObjectPropertyName){
                            relatedObjects.push({...rel, 'name': storedObject});
                        }                        
                    }
                }
            }
        }
    })

    return relatedObjects;
}

export default findNameUsages