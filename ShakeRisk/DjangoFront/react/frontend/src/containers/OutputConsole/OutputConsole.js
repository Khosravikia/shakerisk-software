import React, {Component} from 'react';
import { connect } from 'react-redux';
import classes from './OutputConsole.module.css';
import * as actionCreators from '../../store/actions/index';
import { logLevelCodes } from '../../components/logger/logger';
// import eraseIcon from '../../images/icons/erase.png';
class OutputConsole extends Component {


    onClearOutputConsole = () =>{
        this.props.clearOutputConsole();
    }

    componentDidMount(){
        this.outputConsole.scrollTop = this.outputConsole.scrollHeight - this.outputConsole.clientHeight
    };

    componentDidUpdate(){
        // this.outputConsole.scrollTop = this.outputConsole.scrollHeight - this.outputConsole.clientHeight
        this.outputConsole.scrollTop = this.outputConsole.scrollHeight;
    };

    render (){
        
        

        return(
            <div className={classes.OutputConsole}>
                <div className={classes.Header}> 
                    <span className={classes.Title}>Output Console </span>


                    <span className={[classes.ClearButton, 'tooltip-parent'].join( ' ')} onClick={() => this.onClearOutputConsole()}>
                        {/* <img src={eraseIcon} alt='clear'></img> */}
                        Clear
                        <span className='tooltip'>Clear</span>
                    </span>
                </div>
                <div className={classes.Messages} ref={(inputEl) => {this.outputConsole= inputEl}}>
                    {
                        this.props.outputConsoleLines.map((line, index) => {
                            const lineClass = [classes.Line];
                            lineClass.push(classes[Object.keys(logLevelCodes)[line[1]]]);
                            return (
                                <p className={lineClass.join(' ')} key={index}>
                                    {/* <span className={classes.Time}>{line[0].toLocaleTimeString()}</span>
                                    <br></br>
                                    {line[2]} */}
                                    <span className={classes.Time}>
                                        {line[0]? line[0]?.toLocaleTimeString(): null}</span>
                                        {line[0]? "|" : null }{line[2]}
                                </p>
                            )
                        })
                    }
                </div>
                {/* <button className={classes.ClearButton} onClick={() => this.onClearOutputConsole()}> Clear</button> */}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        outputConsoleLines : state.output.outputLines
    }
}

const mapDispatchToProps = dispatch => {
    return {
        clearOutputConsole : () => dispatch(actionCreators.clearOutputConsole())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OutputConsole)