import React, {Component} from 'react'
import { connect } from 'react-redux';
import axios from 'axios';

import classes from './Footer.module.css';
import * as actionCreators from '../../store/actions/index';
import Button from '../../components/UI/Button/Button';
import { methodListStructure } from '../../constants/Lists/listStructures'
import { INFO, ANALYSIS_GIVEN_NAME ,FIGURE, CONSOLE} from '../../constants/FigureTypes';
import logger from '../../components/logger/logger'
//Temp:
// import sampleFigureResult from './Damage_estimation_response.json';
// import sampleFigureResult2 from './My regional repair cost.json';

class Footer extends Component {

    inputElementRef = React.createRef();

    state = {
        selectedName : '',
        showOptions: false
    }

    componentDidUpdate(){
        if(this.state.showOptions)
            this.inputElementRef.current.focus();
    };


    /////////////////////////

    createMethodSelectList = (methodsList, definedMethods, index=1) => {
        let tempList = [];
        let createdObjectFound = false;

        methodsList.forEach((methodListItem) => {
            if(!('values' in methodListItem))  // If list item does not have a child, check if its type is defined
            {
                if (methodListItem.id in definedMethods)  // If item with same type has a list in defined methods
                {
                    if(Object.keys(definedMethods[methodListItem.id]).length !== 0)  // Check if list of defined method is empty
                    {
                        tempList.push({value: methodListItem.name, key:methodListItem.id, class: classes['Heading' + index]})
                        // Add option for each defined item
                        Object.keys(definedMethods[methodListItem.id]).forEach((option) => {
                            tempList.push( {value: option, class: classes['Option' + index]})
                        });
                        createdObjectFound = true;
                    }
                }
            }
            else{
                // If list item has "values" key, run the same for its children
                let receivedList, childCreatedObjectFound;
                [receivedList, childCreatedObjectFound] = this.createMethodSelectList(methodListItem.values, definedMethods, index+1);
                if(childCreatedObjectFound){
                    // If children of list item have created item, add a heading for it
                    tempList.push({value: methodListItem.name, key:methodListItem.id, class: classes['Heading' + index]});
                    tempList = tempList.concat(receivedList)
                }
                createdObjectFound = createdObjectFound || childCreatedObjectFound;
                
            }
            
        });

        return [tempList, createdObjectFound];

    }

    customInputChangeHandler = (value) => {
        this.setState({selectedName: value, showOptions: false});
    }

    showOption = () =>{
        this.setState({showOptions: true});
    }
    hidOption = () =>{
        this.setState({showOptions: false});
    }
    ////////////////////////

    download = (content, fileName, contentType) => {
        var a = document.createElement("a");
        var file = new Blob([content], {type: contentType});
        a.href = URL.createObjectURL(file);
        a.download = fileName;
        a.click();
        URL.revokeObjectURL(a.href);
    }

    runHandler = () => {
        const analyseToRun = this.state.selectedName;
        const debugMode = "None" // None | Download | Load
        logger.info('Processing "' + analyseToRun + '" analysis...');
        if(debugMode === "Download" || debugMode === "None")
        { 
            const outputObject = {analyseToRun: analyseToRun, model:this.props.storedObjects['model'], method: this.props.storedObjects['method']};
            // let analysisFinishedMsg = "";
            let consoleOutput = [];
            // let runSuccessful = false;

            axios.post('/sendrunrequest',outputObject )
            .then(response => {

                // runSuccessful =true;
                // let finishStatus = " successfully";
                // if(response.data[INFO][HAS_ERROR]){
                //     finishStatus = " with errors"
                // }
                // else if(response.data[INFO][HAS_WARNING]){
                //     finishStatus = " with warnings"
                // }
                // analysisFinishedMsg = ['info', 'Processing "' + analyseToRun + '" analysis finished' + finishStatus + '.'];
                if(response.data[CONSOLE].length){
                    consoleOutput = response.data[CONSOLE]
                }

                if(debugMode === "Download")
                {
                    //Temp (Download response)
                    const outputJSON = JSON.stringify(response.data, null, 2);
                    this.download(outputJSON, analyseToRun + '.json', 'application/json');
                }

                // Show Figures if any
                const analysisGivenName = response.data[INFO][ANALYSIS_GIVEN_NAME]
                for (const key in response.data[FIGURE]){
                    response.data[FIGURE][key].forEach((figure) => {
                        const insertTime = new Date();
                        this.props.onAddFigure({
                            'figureType': key, 
                            'figureTitle': analysisGivenName + ': ' + Object.keys(figure)[0], 
                            'key': key + Object.keys(figure)[0]+ insertTime.toLocaleTimeString() + insertTime.getMilliseconds(),
                            data: Object.values(figure)[0]});
                        })
                }       
                
            })
            .catch(result => {
                console.log("[Footer.js] run response error:",result);
                // analysisFinishedMsg = ['error', 'Processing "' + analyseToRun + '" analysis failed.\n'];
                if(result.response){
                    if(result.response.data.hasOwnProperty(CONSOLE)){
                        consoleOutput = result.response.data[CONSOLE];
                    }
                    else{
                        logger.error('Request failed in server-side');
                    }
                }
                else if (result.message === "Network Error"){
                    logger.error('Could not connect to the server.');
                }
                else{
                    logger.error('Request failed in server-side');
                }
               
            }).finally(() => {
                // If there is console response from analysis, add the analysis finished message to the first one
                // if(consoleOutput.length){
                //     consoleOutput[0] = analysisFinishedMsg + '\n' + consoleOutput[0];
                // }

                // let output = analysisFinishedMsg + '\n';

                // consoleOutput.forEach((info) => {
                //     output += info + "\n\n";
                // })

                // console.log("[Footer.js] consoleOutput before: ", consoleOutput)
                // consoleOutput.unshift(analysisFinishedMsg);
                // console.log("[Footer.js] consoleOutput after: ", consoleOutput)

                logger.multiline(consoleOutput);
                // if(runSuccessful){
                //     logger.info(output);
                // }
                // else{
                //     logger.error(output);
                // }
            });
        }

        // Temp load result from file
        // if (debugMode === "Load")
        // {
        //     let data = sampleFigureResult;
        //     for (const key in data[FIGURE]){
        //         data[FIGURE][key].forEach((figure) => {
        //             const insertTime = new Date();
        //             this.props.onAddFigure({
        //                 'figureType': key, 
        //                 'figureTitle':Object.keys(figure)[0], 
        //                 'key': key + Object.keys(figure)[0]+ insertTime.toLocaleTimeString() + insertTime.getMilliseconds(),
        //                 data: Object.values(figure)[0]});
        //             })
        //     };
        //     console.log("[Footer.js] run response error:",data[CONSOLE][0]);
        //     data[CONSOLE].forEach((info) => {
        //         logger.info(info);
        //     })

        //     data = sampleFigureResult2;
        //     for (const key in data[FIGURE]){
        //         data[FIGURE][key].forEach((figure) => {
        //             const insertTime = new Date();
        //             this.props.onAddFigure({
        //                 'figureType': key, 
        //                 'figureTitle':Object.keys(figure)[0], 
        //                 'key': key + Object.keys(figure)[0]+ insertTime.toLocaleTimeString() + insertTime.getMilliseconds(),
        //                 data: Object.values(figure)[0]});
        //             })
        //     };
        //     console.log("[Footer.js] run response error:",data[CONSOLE][0]);
        //     data[CONSOLE].forEach((info) => {
        //         logger.info(info);
        //     })
        // }
    }

    exportJsonHandler = () => {
        // const outputObject = runObjectGenerator(this.props.storedObjects, this.state.selectedName, modelFormsStructures, methodFormsStructures);
        
        //If model/method is empty, an empty dict should be saved, not "undefined".
        // "undefined" creates problems after loading and trying to store the first object of category
        const outputObject = {
            analyseToRun: this.state.selectedName,
            model:this.props.storedObjects['model'] ? this.props.storedObjects['model'] : {},
            method: this.props.storedObjects['method']? this.props.storedObjects['method'] : {}
        };
        const outputJSON = JSON.stringify(outputObject, null, 2);
        const fileName = this.state.selectedName? this.state.selectedName: 'ShakeRisk Export';
        this.download(outputJSON, fileName + '.json', 'application/json');
    }

    importJsonHandler = () => {
        var input = document.createElement('input');
        input.type = 'file';
        input.accept = '.json'

        input.onchange = e => { 

            // getting a hold of the file reference
            var file = e.target.files[0]; 

            // setting up the reader
            var reader = new FileReader();
            reader.readAsText(file,'UTF-8');

            // here we tell the reader what to do when it's done reading...
            reader.onload = readerEvent => {
                var content = readerEvent.target.result; // this is the content!
                try{
                    const json = JSON.parse(content);
                    this.props.onLoadStoredObjects(json);
                    logger.info('JSON file loaded successfully');
                }
                catch{
                    logger.error('Selected file is Not a valid JSON file.');
                    return;
                }
            }

        }
        input.click()
    }

    render (){
        
        ////////////////////////
        let options = []
        let selectedName;
        let runDisabled = true;
        if(this.props.storedObjects.method){
          options = this.createMethodSelectList(methodListStructure, this.props.storedObjects.method)[0];
        }
        if(options.length){
            selectedName = 'Please choose an analysis'
            // If selected name in state exists in options, show it as selected name in UI
            options.some((option) => {
                if (option.value === this.state.selectedName){
                    selectedName = this.state.selectedName;
                    runDisabled = false;
                    return true;
                }
                return false;
            });
        }
        else{
            options.push({value: 'No analysis defined yet', class: classes['Option1']})
            selectedName = options[0].value
        }

        const contentClasses = [classes.DropdownContent]

        if (this.state.showOptions)
            contentClasses.push(classes.Show);
        else
            contentClasses.push(classes.Hide)
        ////////////////////////
        
        // const customClasses = {input: 'RunInput', inputElement: 'RunInputElement', label: 'RunLabel'}
        return(
            <div className={classes.Footer}>
                
                <div className={classes.MethodSelection}>
                <div className={classes.CustomLabel} onClick={() => this.showOption()}>
                    Choose Analysis:
                </div>
                <div className={classes.Dropdown} tabIndex="-1">
                    <div className={classes.CustomSelect} onClick={() => this.showOption()} >
                        {selectedName}
                        <span className={classes.Marker}></span>
                    </div>
                    <div className={contentClasses.join(' ')} tabIndex="0" ref={this.inputElementRef} onBlur={() => this.hidOption()} >
                        {options.map(option => {
                            // if (option.type === 'header'){
                            if (option.class.includes('Heading')){
                                return (
                                    <div key={option.key} className={option.class}>
                                        {option.value}
                                    </div>
                                )
                            }
                            return (
                                <div key={option.value} className={option.class}
                                    onMouseDown={() => this.customInputChangeHandler(option.value)}
                                >
                                    {option.value}
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
             
                
                <div className={classes.ButtonSection}>
                    <Button btnType="success" additionalClass={"RunButton"} clicked={this.runHandler} disabled={runDisabled}>Run</Button>
                    <Button btnType="success" style={{minWidth:"max-content"}} additionalClass={"RunButton"} clicked={this.exportJsonHandler}>Export JSON</Button>
                    <Button btnType="success" style={{minWidth:"max-content"}} additionalClass={"RunButton"} clicked={this.importJsonHandler}>Import JSON</Button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        storedObjects: state.form.storedObjects
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSelectObjectType: (objectCategory, objectType, isEditMode ) => dispatch(actionCreators.selectObjectType(objectCategory, objectType, isEditMode )),
        onAddFigure: (payload) => dispatch(actionCreators.addFigure(payload)),
        onAddLineToOutputConsole: (payload) => dispatch(actionCreators.addLineToOutputConsole(payload)),
        onLoadStoredObjects: (payload) => dispatch(actionCreators.loadStoredObject(payload))
        
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer)