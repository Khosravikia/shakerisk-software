import React, {Component, Fragment} from 'react'
import ModelMethodViewer from './ModelMethodList/ModelMethodList';
import FormSection from './FormSection/FormSection';
import FigureViewer from './FigureViewer/FigureViewer';
import OutputConsole from './OutputConsole/OutputConsole';
import Footer from './Footer/Footer';
import Header from '../components/UI/Header/Header';
import Row from '../hoc/Layout/Row/Row';
import Column from '../hoc/Layout/Column/Column';

import './MainContainer.css';

class MainContainer extends Component {

    render (){
        
        return(
            <Fragment>
                <Row height="5">
                    <Header></Header>
                </Row>
                <Row height="90">
                    <Column width="20">
                        <ModelMethodViewer></ModelMethodViewer>
                    </Column>
                    <Column width="80">
                        <Row height="60">
                            <Column width="100">
                                <FigureViewer></FigureViewer>
                            </Column>
                        </Row>
                        <Row height="40">
                            <Column width="50">
                                <FormSection></FormSection>
                            </Column>
                            <Column width="50">
                                <OutputConsole></OutputConsole>
                            </Column>
                        </Row>
                    </Column>
                </Row>
                <Row height="5">
                    <Footer></Footer>
                </Row>
            </Fragment>
        );
    }
}

export default MainContainer