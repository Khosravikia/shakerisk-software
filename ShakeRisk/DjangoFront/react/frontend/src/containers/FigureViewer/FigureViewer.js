import React, {Component} from 'react'
import { connect } from 'react-redux';
import classes from './FigureViewer.module.css';
import Figure from '../../components/Figure/Figure';
// import logo from '../../images/logo512.png';
import * as actionCreators from '../../store/actions/index';
import { FOLIUM_MAP, MATPLOTLIB_FIGURE, PLOTLY_GRAPH } from '../../constants/FigureTypes';

class FigureViewer extends Component {
    state = { maximizedIndex: null}

    onMaximizeHandler = (index) => {
        this.setState({maximizedIndex: index});
    };

    onRestoreDownHandler = () => {
        this.setState({maximizedIndex: null});
    };

    onRemoveFigureHandler = (index) => {
        if(this.state.maximizedIndex === index)
            this.setState({maximizedIndex: null});
        this.props.onDeleteFigure(index);
    };

    onDownloadFigure = (fileName, dataType, data, mimeType='text/plain') => {
            var link = document.createElement('a');
            // mimeType = mimeType || 'text/plain';
            switch (dataType){
                case PLOTLY_GRAPH:
                    link.setAttribute('download', fileName + '.html');
                    link.setAttribute('href', 'data:' + mimeType + ';charset=utf-8,' + encodeURIComponent(data));
                    break;
                case FOLIUM_MAP:
                    link.setAttribute('download', fileName + '.html');
                    link.setAttribute('href', 'data:' + mimeType + ';charset=utf-8,' + encodeURIComponent(data));
                    break;
                case MATPLOTLIB_FIGURE:
                    link.setAttribute('download', fileName + '.png');
                    link.setAttribute('href', data);
                    break;
                default:
                    break;
            }
            link.click();
    }

    render (){


        return(
            <div className={classes.FigureViewer}>
                {this.props.figures.length !== 0? this.props.figures.map((figure, index) => {
                    return (
                        <Figure
                            key={figure.key} 
                            figIndex={index} 
                            onDeletClick={this.onRemoveFigureHandler}
                            maximized={index === this.state.maximizedIndex}
                            onMaximizeClick={this.onMaximizeHandler}
                            onResotreDownClick={this.onRestoreDownHandler}
                            onDownloadClick={this.onDownloadFigure}
                            figureType={figure.figureType}
                            figureTitle={figure.figureTitle}
                            figureData={figure.data}>
                        </Figure>
                    )
                }) : <div className={classes.NoImage}></div>}

                {/* {this.props.figures.map((figure) => {
                    // return <span>figure</span>
                    return (<img src={figure} alt="Not loaded"></img>)
                }) } */}
                {/* <Figure>
                    <img src={logo} alt="Sample"></img>
                </Figure>
                <Figure>
                    <img src={logo} alt="Sample"></img>
                </Figure>
                <Figure>
                    <img src={logo} alt="Sample"></img>
                </Figure>
                <Figure>
                    <img src={logo} alt="Sample"></img>
                </Figure> */}
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        figures: state.figure.figures
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onDeleteFigure: (index ) => dispatch(actionCreators.removeFigure(index))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FigureViewer)
