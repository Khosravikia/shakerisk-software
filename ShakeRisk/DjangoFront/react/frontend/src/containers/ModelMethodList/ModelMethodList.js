import React, {Component} from 'react'
import { connect } from 'react-redux';
import classes from './ModelMethodList.module.css';

import * as actionCreators from '../../store/actions/index';
import ListItemGenerator from '../../components/Generators/ListItemGenerator/ListItemGenerator';
import Tabs from '../../components/UI/Tabs/Tabs';
import { modelListStructuer, methodListStructure} from '../../constants/Lists/listStructures';

class ModelMethodViewer extends Component {

    state = {
        // List of expanded model/method items
        model : [],
        method : []
    }

    checkboxChangeHandler = (category, id, checked) => {
        // let newList;
        // switch (category) {
        //     case 'model':
        //         newList = this.state.expandedModels
        //         break;
        //     case 'method':
        //         newList = this.state.expandedModels
        //         break;
        //     default:
        //         break;
        // }
        let newList = this.state[category]
        if(checked){
            newList = [...newList, id]
        }
        else{
            const index = newList.indexOf(id)
            newList = [...newList.slice(0, index), ...newList.slice(index+1)]
        }
        this.setState({[category]: newList})
    }

    clickHander = (category, itemId, displayName ) => {
        // Todo: Make category display name selection atuomatic
        const categoryDisplayName = (category === 'model')? 'Model' : 'Method';
        this.props.onSelectObjectType(category, itemId,categoryDisplayName,  displayName, false);
    };

    render (){
        
        // const modelList = 
        // <ul className="accordion">
        //     {MODELLISTVALUES.map(i => (
        //         <ListItem item={i} key={i.id} category="model" onClick={this.clickHander}/>
        //     ))}
        // </ul>;

        // const methodList = 
        // <ul className="accordion">
        //     {METHODLISTVALUES.map(i => (
        //         <ListItem item={i} key={i.id} category="method" onClick={this.clickHander}/>
        //     ))}
        // </ul>;

        // const test = <ListItemGenerator values={MODELLISTVALUES} category="test" onClick={this.clickHander}></ListItemGenerator>

        return(
            <div className={classes.ModelMethodList}>
            
            <Tabs> 
                <div label="Models"> 
                    <ListItemGenerator 
                        selectedValue={this.props.selectedObjectType} 
                        values={modelListStructuer} 
                        category="model" 
                        onClick={this.clickHander} 
                        onChange={this.checkboxChangeHandler}
                        expandedList={this.state.model}
                    />
                </div> 
                <div label="Analysis"> 
                    <ListItemGenerator 
                        selectedValue={this.props.selectedObjectType} 
                        values={methodListStructure} category="method" 
                        onClick={this.clickHander} 
                        onChange={this.checkboxChangeHandler}
                        expandedList={this.state.method}
                    />
                </div>
            </Tabs>
                {/* <br></br>
                <ul className="cd-accordion">
                    <li className="cd-accordion__item">
                        <input className="cd-accordion__input" type="checkbox" name ="group-1" id="group-1" />
                        <label className="cd-accordion__label" htmlFor="group-1"><span>Group 1</span></label>

                        <ul className="cd-accordion__sub">
                        <li className="cd-accordion__item">
                            <input className="cd-accordion__input" type="checkbox" name ="sub-group-1" id="sub-group-1" />
                            <label className="cd-accordion__label" htmlFor="sub-group-1"><span>Sub Group 1</span></label>

                            <ul className="cd-accordion__sub">
                            <li className="cd-accordion__item"><a className="cd-accordion__label" href="#0"><span>Image</span></a></li>
                            <li className="cd-accordion__item"><a className="cd-accordion__label" href="#0"><span>Image</span></a></li>
                            <li className="cd-accordion__item"><a className="cd-accordion__label" href="#0"><span>Image</span></a></li>
                            </ul>
                        </li>
                    
                        <li className="cd-accordion__item"><a className="cd-accordion__label" href="#0"><span>Image</span></a></li>
                        <li className="cd-accordion__item"><a className="cd-accordion__label" href="#0"><span>Image</span></a></li>
                        </ul>
                    </li>

                    <li className="cd-accordion__item cd-accordion__item--has-children">
                        <a className="cd-accordion__label cd-accordion__label--icon-img" href="#0"><span>Image</span></a>
                    </li>
                    <li className="cd-accordion__item cd-accordion__item--has-children">
                        <a className="cd-accordion__label cd-accordion__label--icon-img" href="#0"><span>Image</span></a>
                    </li>
                    </ul> */}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        selectedObjectType: state.form.selectedObjectType
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSelectObjectType: (objectCategory, objectType,categoryDisplayName, typeDisplayName, isEditMode ) => dispatch(actionCreators.selectObjectType(objectCategory, objectType,categoryDisplayName, typeDisplayName, isEditMode ))
        
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModelMethodViewer)