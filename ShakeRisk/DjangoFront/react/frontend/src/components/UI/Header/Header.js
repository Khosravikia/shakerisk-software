import React from 'react';
import classes from './Header.module.css';
import { Link } from "react-router-dom";
const Header = () =>{

    return (
        <div className={classes.Header}>
            <span className={classes.Title}>
                <Link to="/">ShakeRisk V0.2.1</Link>
            </span>
            <div className={classes.Navigation}>
                <span className={classes.Links}><Link to='/'>Home</Link></span>
                <span className={classes.Links}><Link to='/about'>About</Link></span>
                <span className={classes.Links}>
                    <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/Khosravikia/shakerisk-software/-/blob/master/ShakeRisk_0-1-0_OperatingManual.pdf">Help</a>
                </span>
            </div>
        </div>
    )

}

export default Header