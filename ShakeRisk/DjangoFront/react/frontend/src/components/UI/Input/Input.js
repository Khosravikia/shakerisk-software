import React from 'react';

import classes from './Input.module.css';

const input = ( props ) => {
    const classToUse = props.customClasses? props.customClasses : 
        {input: "Input", inputElement: "InputElement", label: "Label"}
        
    let inputElement = null;
    switch ( props.elementType ) {
        case ( 'input' ):
            if(props.elementConfig.type === 'checkbox'){
                inputElement = <input
                className={classes[classToUse.inputElement]}
                {...props.elementConfig}
                checked={props.value}
                onChange={props.changed}
                disabled={props.disabled} />;
            }
            else{
            inputElement = <input
                className={classes[classToUse.inputElement]} 
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed}
                disabled={props.disabled} />;
            }
            break;
        case ( 'textarea' ):
            inputElement = <textarea
                className={classes[classToUse.inputElement]}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
            break;
        case ( 'select' ):
            inputElement = (
                <select
                    className={classes[classToUse.inputElement]}
                    value={props.value}
                    onChange={props.changed}>
                    {props.elementConfig.options.map(option => (
                        <option key={option.value} value={option.value}>
                            {option.displayValue}
                        </option>
                    ))}
                </select>
            );
            break;
        default:
            inputElement = <input
                className={classes[classToUse.inputElement]}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
    }

    return (
        <div className={props.elementConfig.type === 'hidden' || props.shouldHide? classes.NotDisplayed : classes[classToUse.input]}> 
            {props.label? <label className={classes[classToUse.label]}>{props.label}</label> : null}
            {inputElement}
        </div>
    );

};

export default input;