import React, { Component } from 'react';

import * as classes from './Tab.module.css';

class Tab extends Component {
 
  
    onClick = () => {
      const { label, onClick } = this.props;
      onClick(label);
    }
  
    render() {
      const {
        onClick,
        props: {
          activeTab,
          label,
        },
      } = this;
  
      let className = [classes.Item];
  
      if (activeTab === label) {
        className.push(classes.ItemActive);
      }
  
      return (
        <li
          className={className.join(' ')}
          onClick={onClick}
        >
          {label}
        </li>
      );
    }
  }
  
  export default Tab;