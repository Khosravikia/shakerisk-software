import React, {Component} from 'react'

import * as classes from './Tabs.module.css';
import Tab from './Tab/Tab';

class Tabs extends Component{

    state = {
      // Todo: make default open tab configurable
        activeTab: "Models"
    }

    onClickTabItem = (tab) => {
        this.setState({ activeTab: tab });
      }

    render() {

        const children = this.props.children;
        const activeTab = this.state.activeTab;

        const tabList = this.props.children.map((child) => {
            const { label } = child.props;

            return (
              <Tab
                activeTab={activeTab}
                key={label}
                label={label}
                onClick={this.onClickTabItem}
              />
            );
        });

        let tabContent = children.filter((child) => {
            if (child.props.label !== activeTab) return false;
            return true;
        });

        if (tabContent.length){
            tabContent = tabContent[0].props.children;
        }
        else{
            tabContent = <span>Please select a tab to continue</span>
        }

        return (
           <div className={classes.Tabs}>
              <ol className={classes.TabsList} >
                {tabList}
              </ol>
              <div className={classes.TabContent}>
                {tabContent}
              </div>
            </div>
          );
    }
}

export default Tabs;