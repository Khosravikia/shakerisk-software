import  { store } from '../../index';
import * as actionCreators from '../../store/actions/index';

export const logLevelCodes = {
    'info': 0,
    'warn': 1,
    'error': 2
}

// Methods are static because logger class is not instantiated for usage
class logger  {

    static dispatch = (logLevel, msg, addDate=true) => {
        const lineArray = [addDate? new Date(): null, logLevel, msg];
        store.dispatch(actionCreators.addLineToOutputConsole(lineArray));
    }

    static info = (msg) => {
        this.dispatch(logLevelCodes.info, msg);
    }

    static warn = (msg) => {
        this.dispatch(logLevelCodes.warn, msg);
    }

    static error = (msg) => {
        this.dispatch(logLevelCodes.error, msg);
    }

    static multiline = (messages) => {
        let addDate = true;
        messages.forEach((msg) => {
            this.dispatch(logLevelCodes[msg[0]], msg[1], addDate);
            addDate = false;
        })
    }
}

export default logger;