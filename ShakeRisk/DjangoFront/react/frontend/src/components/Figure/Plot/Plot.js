import React from 'react';
import classes from './Plot.module.css';
import * as figureTypes from '../../../constants/FigureTypes';



const Plot = (props) => {

    

    let plot;
    switch (props.type){
        case figureTypes.FOLIUM_MAP:
            plot = <iframe width="100%" title="plot" frameBorder="0" style={{display:'block'}} srcDoc={props.data}></iframe>
            break;

        case figureTypes.PLOTLY_GRAPH:
            // plot = <div dangerouslySetInnerHTML={{__html: props.data}}></div>
            plot = <iframe width="100%" height="100%" frameBorder="0" style={{display:'block'}} title="plot" srcDoc={props.data}></iframe>
            break;

        case figureTypes.MATPLOTLIB_FIGURE:
            plot = <span className={classes.Image}><img src={props.data} alt="Not loaded"></img></span>
            break;
        
        case figureTypes.DATAFRAME_TABLE:
            plot = <span className={classes.Table} dangerouslySetInnerHTML={{__html: props.data}}></span>
            break;

        default:
            break;
    }
    return (
        <div>
            {plot}
        </div>
    )
}

export default Plot;