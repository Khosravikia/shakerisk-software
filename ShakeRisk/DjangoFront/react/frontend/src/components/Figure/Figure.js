import React from 'react';
import classes from './Figure.module.css';
import Plot from './Plot/Plot';
import closeIcon from '../../images/icons/close.png';
import maximizeIcon from '../../images/icons/maximize.png';
import restoreIcon from '../../images/icons/restore.png';
import downloadIcon from '../../images/icons/download.png';
const Figure = (props) => {

    const figClass = [classes.Figure]

    if (props.maximized)
        figClass.push(classes.Maximized)

    return (
        <div className={figClass.join(' ')}>
            <div className={classes.Header}> 
                <span className={classes.Title}>{props.figureTitle} </span>

                {/* <span 
                className={classes.FullScreenButton}
                onClick={props.maximized? () => props.onResotreDownClick() : () => props.onMaximizeClick(props.figIndex)}
                >+</span> */}
                <span className={[classes.FullScreenButton, 'tooltip-parent'].join(' ')} onClick={props.maximized? () => props.onResotreDownClick() : () => props.onMaximizeClick(props.figIndex)}>
                    <img src={props.maximized? restoreIcon : maximizeIcon} alt={props.maxized? 'restore' : 'maximize'}></img>
                    <span className='tooltip'>{props.maximized? 'Restore' : 'Maximize'}</span>
                </span>
                {/* <span className={classes.CloseButton} onClick={
                    () => {props.onDeletClick(props.figIndex)}}
                >✕</span> */}
                <span className={[classes.CloseButton, 'tooltip-parent'].join(' ')} onClick={() => {props.onDeletClick(props.figIndex)}}>
                    <img src={closeIcon} alt='close'></img>
                    <span className='tooltip'>Close</span>
                </span>
                <span className={[classes.DownloadButton, 'tooltip-parent'].join(' ')} onClick={() => {props.onDownloadClick(props.figureTitle, props.figureType, props.figureData)}}>
                    <img src={downloadIcon} alt='download'></img>
                    <span className='tooltip'>Download</span>
                </span>

            </div>

            <div className={classes.Plot}>
                <Plot type={props.figureType} data={props.figureData}></Plot>
            </div>
        </div>
    )
}

export default Figure;