import React, { Fragment } from 'react';
import Row from '../../hoc/Layout/Row/Row';
import Header from '../UI/Header/Header';
import classes from './about.module.css';
import faridkhosravikia from '../../images/faridkhosravikia.jpg';
import patriciaclayton from '../../images/patriciaclayton.jpg';
import ellenmrathje from '../../images/ellenmrathje.jpg';

const About = (props) => {

    return (
        <Fragment>
            <Row height="5">
                <Header></Header>
            </Row>
            <Row height="95">
                <div className={classes.Content}>
                    <h1>ShakeRisk</h1>

                    <p>ShakeRisk is an open-source web application for risk, reliability, and resilience assessment of civil infrastructure in both structural and urban scales.</p>

                    <p>ShakeRisk integrates

                        <ul>
                            <li>Artificial Intelligence</li>
                            <li>Systems Engineering</li>
                            <li>Structural and earthquake Engineering</li>
                        </ul>
                        <br></br>
                        research fields to innovate at the intersection of these areas.
                    </p>

                    <p>
                        It is being developed at The University of Texas at Austin as part of the projects evaluating the risk of the induced earthquakes in the Central U.S. on built environments, funded by
                        <ul>
                            <li>Texas Department of Transportation</li>
                            <li>Texas State legislature through the TexNet Seismic Monitoring Program and the Center for Integrated Seismicity Research (CISR)</li>
                        </ul>
                        <br></br>
                        The application was initially developed over the course of Farid Khosravikia’ PhD studies at The University of Texas at Austin under supervision of Patricia Clayton.
                    </p>

                    <p>
                        <b>Programming Language: </b><br></br>
                        HTML, CSS, JavaScript, SQL, Python (Numpy, SciPy, Pandas, scikit-learn, TensorFlow2, Keras, Matplotlib, Plotly, Django)
                    </p>

                    <p>
                        <b>License:</b> <br></br>
                        ShakeRisk is an open-source web application. By using ShakeRisk, you hereby accept the terms of the End-user License Agreement. This includes citing the following references in any form of dissemination that makes use of ShakeRisk.
                    </p>

                    <p>GitLab Link to the application: Will be updated shortly</p>

                    <p>
                        <b>Developer Team:</b>
                        <p>
                            <b>Farid Khosravikia</b>  <br></br>
                            <div>
                                <img src={faridkhosravikia} alt="Farid Khosravikia" ></img>
                            </div>
                            PhD Candidate, Department of Civil, Architectural and Environmental Engineering, The University of Texas at Austin
                            <br></br>
                            <br></br>
                            Developed the first version of the ShakeRisk from 2016 to 2020 and are the leading developer since 2020.
                        </p>
                        <p>
                            <b>Patricia Clayton</b>  <br></br>
                            <div>
                                <img src={patriciaclayton} alt="Patricia Clayton" ></img>
                            </div>
                            Assistant professor, Department of Civil, Architectural and Environmental Engineering, The University of Texas at Austin.
                            <br></br>
                            <br></br>
                            Mentoring the development of ShakeRisk since 2016 to 2020.
                        </p>
                        <p>
                            <b>Ellen M. Rathje</b>  <br></br>
                            <div>
                                <img src={ellenmrathje} alt="Ellen M. Rathje" ></img>
                            </div>
                            Professor, Department of Civil, Architectural and Environmental Engineering, The University of Texas at Austin.
                            <br></br>
                            <br></br>
                            Mentoring the development of ShakeRisk since 2016 to 2020.
                        </p>
                    </p>
                </div>
            </Row>
        </Fragment>
    )
}

export default About;