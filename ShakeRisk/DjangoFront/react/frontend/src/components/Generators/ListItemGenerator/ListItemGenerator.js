import React from 'react'

import './ListItemGenerator.css';

import ListItem from './ListItem/ListItem';

const ListItemGenerator = (props) => {
    return (
        <ul className="accordion">
            {props.values.map(i => (
                <ListItem 
                    selectedValue={props.selectedValue} 
                    item={i} key={i.id} 
                    category={props.category} 
                    onClick={props.onClick} 
                    onChange={props.onChange}
                    expandedList={props.expandedList}
                />
            ))}
        </ul>
    )
}

export default ListItemGenerator;