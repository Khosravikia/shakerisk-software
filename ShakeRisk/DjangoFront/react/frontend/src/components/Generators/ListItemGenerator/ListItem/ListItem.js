import React, {Fragment} from 'react';


function ListItem(props) {
    const item = props.item;

    let children = null;
    if (item.values && item.values.length) {
      children = (
        <ul className="accordion__sub">
          {item.values.map(i => (
            <ListItem 
              selectedValue={props.selectedValue} 
              item={i} 
              key={i.id} 
              category={props.category} 
              onClick={props.onClick}
              onChange={props.onChange}
              expandedList={props.expandedList}
            />
          ))}
        </ul>
      );
    }
    
    const classNames =["accordion__item"]
    let output = null;
    if(children){
        classNames.push("accordion__item--has-children");
        output = 
          <li className={classNames.join(' ')} >
                <input className="accordion__input" type="checkbox" checked={props.expandedList.includes(item.id)} name ={item.name} id={item.name} onChange={(event) => props.onChange(props.category, item.id, event.target.checked)}/>
                <label className="accordion__label" htmlFor={item.name}><span>{item.name}</span></label>
            {children} 
          </li>
    }
    else{
      if ( item.id === props.selectedValue)
      {
        classNames.push("accordion__item--selected");
      }
      output = 
        <li className={classNames.join(' ')} onClick={() => props.onClick(props.category, item.id, item.name )}>
          {item.name}
        </li>
    }
    
    return (
      
      // <li className={classes.join(' ')} onClick={props.onClick}>
      //   {/* {item.name} */}
      //   {children? <Fragment>
      //       <input className="cd-accordion__input" type="checkbox" name ={item.name} id={item.name} />
      //       <label className="cd-accordion__label" htmlFor={item.name}><span>{item.name}</span></label>
      //   </Fragment>
      //    : item.name}
        
      //   {children} 
      // </li>
      <Fragment>
        {output}
      </Fragment>
    );
  }

export default ListItem;