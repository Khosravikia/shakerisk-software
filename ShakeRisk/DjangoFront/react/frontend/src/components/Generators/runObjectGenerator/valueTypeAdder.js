/**
 * Recieves an object (method or model) values and its form object. Changes every value to an object with
 * "value" and "type" keys.
 * "value" is same as old value, If value is a name of another object, "type" shows that object type, else
 * it is null.
 * @param {object} objectValues 
 * @param {object} formObject 
 */
const valueTypeAdder = (objectValues, formObject) => {
    
    // console.log('[valueTypeAdder.js] objectValues: ', objectValues, ' formObject: ', formObject);

    const outputValues = {...objectValues};

    for (const key in formObject){
        if (formObject[key]['elementType'] === 'input-object'){
            outputValues[key] = {value: objectValues[key], type: formObject[key]['elementSelectObjectType']}
        }
        else{
            outputValues[key] = {value: objectValues[key], type: null}
        }
    }
    // console.log('[valueTypeAdder.js] outputValues: ', outputValues);
    return outputValues;
}

export default valueTypeAdder;