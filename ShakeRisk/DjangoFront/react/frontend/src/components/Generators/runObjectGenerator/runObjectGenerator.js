import valueTypeAdder from './valueTypeAdder';


const runObjectGenerator = (storedObjects, objectName, methodFormObject, modelFormObject) => {

        
    const outputObject = { analyseToRun: objectName }
    
    // Find and write all value types
    const categories = ['model', 'method'];
    const forms = [modelFormObject, methodFormObject];
    
    categories.forEach((category, index) => {
        console.log("[runObjectGenerator.js] category: ", category);
        for (const type in storedObjects[category]){
            console.log("[runObjectGenerator.js] type: ", type);
            for (const name in storedObjects[category][type]){
                console.log("[runObjectGenerator.js] valueTypeAdder: ", valueTypeAdder(storedObjects[category][type][name], forms[index][type]['form']));
                storedObjects[category][type][name] = valueTypeAdder(storedObjects[category][type][name], forms[index][type]['form'])
            }
        }
    });

    outputObject['model'] = storedObjects['model'];
    outputObject['method'] = storedObjects['method'];

    console.log("[runObjectGenerator.js] ouptputObject: ", outputObject);
    return outputObject;

}

export default runObjectGenerator;