
const runObjectGenerator = (storedObjects, objectName, methodFormObject) => {

    // Find object type
    let objectType, objectValues;

    for (const type in storedObjects['method']) {
        for (const name in storedObjects['method'][type]){
            if ( name === objectName)
            {
                objectType = type;
                objectValues = storedObjects['method'][type][name];
            }
        }
    }

    // Find related form
    const objectForm = methodFormObject[objectType]['form'];
    console.log("[runObjectGenerator.js] objectValues: ", objectValues, " objectForm: ", objectForm);

    const outputObject = {model: storedObjects['model'],
     analyze: {name: objectValues['objectName'], type: objectValues['objectType'], values: {}}};
    // Get values types
    for (const key in objectForm){
        if (key in objectValues){
            outputObject['analyze']['values'][key] = 
                {value: objectValues[key], type: objectForm[key]['elementSelectObjectType']};
        }
    }
    
    console.log("[runObjectGenerator.js] ouptputObject: ", outputObject);
    return outputObject;

}

export default runObjectGenerator;