// Create select input for parameter of type "input-object"
const selectGenerator = (storedObjects, rawElement, previousValue = null) => {

    //Check if property is required
    let required = false;
    if(rawElement.validation){
        if(rawElement.validation.required)
        {
            required = true;
        }
    }

    let optionItems = [];
    // Get all defined objects names accroding to rawElement.elementSelectObjectType
    if('model' in storedObjects)
        if (rawElement.elementSelectObjectType in storedObjects.model)
            optionItems = Object.keys(storedObjects.model[rawElement.elementSelectObjectType]);
        
    let optionsObject;
    // Convert objects names to option list or return a default if no object of desired type is found
    // None is an option if property is not required otherwise 'No object found' with value of '' is shown
    optionsObject = optionItems.map((item) => {
        return {value: item, displayValue: item};
    });
    if(!required){
        optionsObject.unshift({value:'none', displayValue: 'None'})
    }else if(optionsObject.length === 0){
        optionsObject = [ {value:'', displayValue: 'No object found'}]
    }
    return {
        ...rawElement,
        elementType: 'select',
        elementConfig: {
            options: optionsObject
        },
        value: (optionItems.includes(previousValue))? previousValue : optionsObject[0].value,
        validation: rawElement.validation? rawElement.validation : null,
        valid: optionItems.length? true : !rawElement.validation? true : false,
        touched: true
    }
}



export default selectGenerator;