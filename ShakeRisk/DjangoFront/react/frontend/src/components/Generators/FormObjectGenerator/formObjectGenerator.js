import selectObjectGenerator from './selectObjectGenerator';

/**
 * Generate a usable form object for [FormViewer.js] by adding objectCategory, objectType, and
 *  objectName fields and adding value, valid, and touched props to all fields.
 * @param {raw form object} rawFormObject 
 * @param {string}objectCategory Category of object
 * @param {string} objectType Type of the object
 * @param {object} storedObjects Objects stored in redux. Used for both creating selects based on other
 *  defined objects and getting current object values if it is being edited.
 */

const formObjectGenerator = (rawFormObject, objectCategory, objectType, storedObjects, objectName=null) =>{

    const objectValues = objectName? storedObjects[objectCategory][objectType][objectName] : null;

    let formObject = {};
    formObject['objectCategory'] = {
        elementType: 'input',
        elementConfig: {
            type: 'hidden',
        },
        value:objectCategory,
        valid: true,
        touched: false
    };
    formObject['objectType'] = {
        elementType: 'input',
        elementConfig: {
            type: 'hidden',
        },
        value:objectType,
        valid: true,
        touched: false
    };
    formObject['objectName'] = {
        elementType: 'input',
        elementLabel: 'Object Name',
        elementConfig: {
            type: 'text',
            placeholder: 'Object Name',
            disabled: objectValues? true : false
        },
        validation: {
            required: true
        },
        value: objectValues? objectValues.objectName: '',
        valid: objectValues? true: false,
        touched: false
    };

    for (var key in rawFormObject){
        const rawElement = rawFormObject[key];
        let element;
        
        if (rawElement.elementType === 'input-object'){ // Handling inputs related to other objects
            element = selectObjectGenerator(storedObjects, rawElement, objectValues? objectValues[key] : null);
        }
        else{
            element = {
                ...rawElement,
                // For select element, first option value is assigned if no previous value is given
                value: objectValues? objectValues[key] : (rawElement.elementType === 'select')? rawElement.elementConfig.options[0].value : '',
                // Valid cases: 1.values are being loaded 2.Element is of type "validFormElementTypes" 3. No validation fields
                valid: objectValues? true: validFormElementTypes.includes(rawFormObject[key]['elementType'])? true: !rawElement.validation? true: false,
                touched: false
            }

            // Handling checkboxes
            if(element.elementType === 'input')
            {
                if(element.elementConfig.type === 'checkbox')
                {
                    element.value = objectValues? objectValues[key] : false;
                    element.valid = true;
                }
            }
        }
        formObject[key] = element;
    }
    
    return formObject;
}

//Form elements which are always valid
const validFormElementTypes = ['select', 'hidden'];

export default formObjectGenerator;