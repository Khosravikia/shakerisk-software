import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, compose } from 'redux'; //applyMiddleware
import { Provider } from 'react-redux';
import axios from 'axios';

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
// Reducers
import formReducer from './store/reducers/form';
import figureReducer from './store/reducers/figure';
import outputReducer from './store/reducers/outputConsole';


// Axios:
axios.defaults.baseURL = 'http://127.0.0.1:8000';
axios.defaults.headers.post['Content-Type']='application/json'
axios.interceptors.request.use(request => {
  //Edit request if required. (Like adding header or ...)
  // console.log("[index.js] axios request: ", request);
  return request;
}, error => {
  // console.log("[index.js] axios request error: ", error);
  return Promise.reject(error);
});
axios.interceptors.response.use(respone => {
  //Edit respone if required.
  // console.log("[index.js] axios respone: ", respone);
  return respone;
}, error => {
  // console.log("[index.js] axios respone error: ", error);
  return Promise.reject(error);
});


const rootReducer = combineReducers({
  form: formReducer,
  figure: figureReducer,
  output: outputReducer
});

// For redux development tool
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Do not export store if server-side rendering is used
export const store = createStore(rootReducer, composeEnhancers());


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
