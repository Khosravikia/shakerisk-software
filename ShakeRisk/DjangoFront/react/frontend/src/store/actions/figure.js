import * as actionTypes from './actionTypes';

export const addFigure = (payload) => {
    return {
        type: actionTypes.ADD_FIGURE,
        payload: payload
    }
}

export const removeFigure = (index) => {
    return {
        type: actionTypes.REMOVE_FIGURE,
        index: index
    }
}