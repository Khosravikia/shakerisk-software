import * as actionTypes from './actionTypes';

export const addLineToOutputConsole = (payload) =>{
    return {
        type: actionTypes.ADD_LINE,
        payload: payload
    }
}

export const clearOutputConsole = () =>{
    return {
        type: actionTypes.CLEAR_CONSOLE
    }
}