import * as actionTypes from './actionTypes';

export const formSubmit = (payLoad, isEditMode=null) => {
    return {
        type: actionTypes.FORM_SUBMIT,
        payLoad: payLoad
    }
}

export const objectDelete = (objectCategory, objectType, objectName, isEditMode=null) => {
    return {
        type: actionTypes.DELETE_OBJECT,
        objectCategory: objectCategory,
        objectType: objectType,
        objectName: objectName,
        isEditMode: isEditMode
    }
}

export const objectChangeName = (objectCategory, objectType, objectOldName, objectNewName, relatedObjects, isEditMode=null) => {
    return {
        type: actionTypes.CHANGE_OBJECT_NAME,
        objectCategory: objectCategory,
        objectType: objectType,
        objectOldName: objectOldName,
        objectNewName: objectNewName,
        relatedObjects: relatedObjects,
        isEditMode: isEditMode
    }
}

export const selectObjectType = (objectCategory, objectType , categoryDisplayName, typeDisplayName, isEditMode=null) => {
     return {
         type: actionTypes.SELECT_OBJECT_TYPE,
         objectCategory: objectCategory,
         categoryDisplayName: categoryDisplayName,
         objectType: objectType,
         typeDisplayName: typeDisplayName,
         isEditMode: isEditMode}
    }

export const setEditMode = (isEditMode) => {
    return {
        type: actionTypes.SET_EDIT_MODE,
        isEditMode: isEditMode
    }
}

export const selectObjectName = (objectName, isEditMode=null) => {
    return {
        type: actionTypes.SELECT_OBJECT_NAME,
        objectName: objectName,
        isEditMode: isEditMode
    }
}

export const loadStoredObject = (objectToloadFrom, isEditMode=null) => {
    return {
        type: actionTypes.LOAD_STORED_OBJECTS,
        objectToloadFrom: objectToloadFrom
    }
}

