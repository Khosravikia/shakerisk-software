// Export all actions from all files.
// Use a new export for each file.
// This makes auto import possible

export {
    formSubmit,
    objectDelete,
    objectChangeName,
    selectObjectType,
    setEditMode,
    selectObjectName,
    loadStoredObject
} from './form'

export {
    addFigure,
    removeFigure
} from './figure'

export {
    clearOutputConsole,
    addLineToOutputConsole
} from './outputConsole'