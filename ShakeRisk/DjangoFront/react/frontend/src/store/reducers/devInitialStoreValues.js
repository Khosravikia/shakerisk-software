const form = {
  selectedObjectCategory: 'method',
    categoryDisplayName: 'Method',
    selectedObjectType: 'bridge_damage_estimation',
    typeDisplayName: 'Damage',
    selectedObjectName: 'Irving Bridge Damage (PGV)',
    selectedObjectNameToRename: null,
    storedObjects: {
      model: {
        MLM_ANN: {
          'Artificial Neural Network Options': {
            objectCategory: 'model',
            objectType: 'MLM_ANN',
            objectName: 'Artificial Neural Network Options',
            number_of_neurons: '4, 4',
            validation_fraction: 0.2,
            learning_rate: 0.01,
            epochs_num: 998,
            early_stop_patience: 5,
            loss_function: 'mse',
            optimizer: 'Adam',
            input_activation_function: 'relu, relu'
          }
        },
        MLM_random_forest: {
          'Random Forest Options': {
            objectCategory: 'model',
            objectType: 'MLM_random_forest',
            objectName: 'Random Forest Options',
            n_estimators: 20,
            bootstrap: true,
            max_features: '2',
            max_samples: 0.75,
            criterion: 'mse',
            min_samples_split: 2,
            min_samples_leaf: 1
          }
        },
        MLM_SVM: {
          'Support Vector Machine Options': {
            objectCategory: 'model',
            objectType: 'MLM_SVM',
            objectName: 'Support Vector Machine Options',
            kernel: 'rbf',
            gamma: 'scale',
            c: 1,
            epsilon: 0.1
          }
        },
        param_continuous_uniform: {
          'Continuos Uniform [5-5.3]': {
            objectCategory: 'model',
            objectType: 'param_continuous_uniform',
            objectName: 'Continuos Uniform [5-5.3]',
            minimum: 5,
            maximum: 5.3
          }
        },
        location_point: {
          'University of Dallas, Irving': {
            objectCategory: 'model',
            objectType: 'location_point',
            objectName: 'University of Dallas, Irving',
            latitude: 32.844,
            longitude: -96.911
          },
          'Anaheim City Hall': {
            objectCategory: 'model',
            objectType: 'location_point',
            objectName: 'Anaheim City Hall',
            latitude: 33.8366,
            longitude: -117.9143
          }
        },
        earthquake: {
          'Irving Earthquake': {
            objectCategory: 'model',
            objectType: 'earthquake',
            objectName: 'Irving Earthquake',
            magnitude: 'Continuos Uniform [5-5.3]',
            location: 'University of Dallas, Irving',
            depth: '5'
          },
          'Anaheim Earthquake': {
            objectCategory: 'model',
            objectType: 'earthquake',
            objectName: 'Anaheim Earthquake',
            magnitude: '5',
            location: 'Anaheim City Hall',
            depth: '5'
          }
        },
        rectangular_area: {
          California: {
            objectCategory: 'model',
            objectType: 'rectangular_area',
            objectName: 'California',
            min_lat: 23.58150731889538,
            max_lat: 41.797,
            min_lng: -123.24,
            max_lng: -114.29
          }
        },
        time_interval: {
          '[2019-2020]': {
            objectCategory: 'model',
            objectType: 'time_interval',
            objectName: '[2019-2020]',
            start_date: '2019-01-01',
            end_date: '2020-01-01'
          }
        },
        transportation_network: {
          Anaheim: {
            objectCategory: 'model',
            objectType: 'transportation_network',
            objectName: 'Anaheim',
            source: 'shakerisk',
            location_name: 'Anaheim'
          }
        },
        building_construction_cost_model: {
          HAZUS: {
            objectCategory: 'model',
            objectType: 'building_construction_cost_model',
            objectName: 'HAZUS',
            source: 'shakerisk',
            model_name: 'hazus',
            dataframe: 'none',
            required_classification: ''
          }
        },
        system_model_frank_wolfe: {
          'Frank Wolfe': {
            objectCategory: 'model',
            objectType: 'system_model_frank_wolfe',
            objectName: 'Frank Wolfe',
            algorithm: 'FW',
            convergence: 0.000001
          }
        },
        MLM_trained_model: {
          'Artificial Neural Network Model': {
            objectCategory: 'model',
            objectType: 'MLM_trained_model',
            objectName: 'Artificial Neural Network Model',
            model_path: 'Trained Artificial Neural Network Model Path',
            additional_data_path: 'Trained Artificial Neural Network Info Path',
            input_names_orders: 'mw,vs30,rhypo'
          }
        },
        intensity_model: {
          'Khosravikia (PGV) - Deterministic': {
            objectCategory: 'model',
            objectType: 'intensity_model',
            objectName: 'Khosravikia (PGV) - Deterministic',
            source: 'shakerisk',
            gmm_model_name: 'khosravikia_etal_2019',
            trained_model: 'none',
            measure: 'PGV',
            probabilistic: 'deterministic',
            spatial_correlation: 'jayram_baker_2009'
          },
          'Zalachoris (PGA) - Deterministic': {
            objectCategory: 'model',
            objectType: 'intensity_model',
            objectName: 'Zalachoris (PGA) - Deterministic',
            source: 'shakerisk',
            gmm_model_name: 'zalachoris_rathje_2019',
            trained_model: 'none',
            measure: 'PGA',
            probabilistic: 'deterministic',
            spatial_correlation: 'jayram_baker_2009'
          },
          'Khosravikia (PGA) - Deterministic': {
            objectCategory: 'model',
            objectType: 'intensity_model',
            objectName: 'Khosravikia (PGA) - Deterministic',
            source: 'shakerisk',
            gmm_model_name: 'khosravikia_etal_2019',
            trained_model: 'none',
            measure: 'PGA',
            probabilistic: 'deterministic',
            spatial_correlation: 'jayram_baker_2009'
          },
          'Trained Artificial Neural Network (PGA)': {
            objectCategory: 'model',
            objectType: 'intensity_model',
            objectName: 'Trained Artificial Neural Network (PGA)',
            source: 'MLM_trained_model',
            gmm_model_name: 'khosravikia_etal_2019',
            trained_model: 'Artificial Neural Network Model',
            measure: 'PGA',
            probabilistic: 'deterministic',
            spatial_correlation: 'jayram_baker_2009'
          }
        },
        building: {
          TX: {
            objectCategory: 'model',
            objectType: 'building',
            objectName: 'TX',
            source: 'shakerisk',
            filter_state: 'TX',
            filter_location: 'none',
            dataframe: 'none'
          },
          'Custom (DataFrame)': {
            objectCategory: 'model',
            objectType: 'building',
            objectName: 'Custom (DataFrame)',
            source: 'dataframe',
            filter_state: '',
            filter_location: 'none',
            dataframe: 'Building DataFrame'
          }
        },
        bridge: {
          TX: {
            objectCategory: 'model',
            objectType: 'bridge',
            objectName: 'TX',
            source: 'shakerisk',
            filter_state: 'TX',
            filter_location: 'none',
            dataframe: 'none'
          },
          CA: {
            objectCategory: 'model',
            objectType: 'bridge',
            objectName: 'CA',
            source: 'shakerisk',
            filter_state: 'CA',
            filter_location: 'none',
            dataframe: 'none'
          },
          'Custom (DataFrame - TX)': {
            objectCategory: 'model',
            objectType: 'bridge',
            objectName: 'Custom (DataFrame - TX)',
            source: 'dataframe',
            filter_state: '',
            filter_location: 'none',
            dataframe: 'Bridge (TX) DataFrame'
          },
          'Custom (DataFrame - CA)': {
            objectCategory: 'model',
            objectType: 'bridge',
            objectName: 'Custom (DataFrame - CA)',
            source: 'dataframe',
            filter_state: '',
            filter_location: 'none',
            dataframe: 'Bridge (CA) DataFrame'
          }
        },
        vs30: {
          TX: {
            objectCategory: 'model',
            objectType: 'vs30',
            objectName: 'TX',
            source: 'shakerisk',
            mapping: 'Updated',
            filter_state: 'TX',
            filter_location: 'none',
            constant: null,
            dataframe: 'none'
          },
          'Constant (760)': {
            objectCategory: 'model',
            objectType: 'vs30',
            objectName: 'Constant (760)',
            source: 'constant',
            mapping: 'USGS',
            filter_state: '',
            filter_location: 'none',
            constant: 760,
            dataframe: 'none'
          },
          'Custom (DataFrame)': {
            objectCategory: 'model',
            objectType: 'vs30',
            objectName: 'Custom (DataFrame)',
            source: 'dataframe',
            mapping: 'USGS',
            filter_state: '',
            filter_location: 'none',
            constant: null,
            dataframe: 'Vs30 DataFrame'
          }
        },
        bridge_fragility_model: {
          'Khosravikia (PGV)': {
            objectCategory: 'model',
            objectType: 'bridge_fragility_model',
            objectName: 'Khosravikia (PGV)',
            source: 'shakerisk',
            measure: 'PGV',
            model_name: 'khosravikia_etal_2018',
            dataframe: 'none',
            required_classification: ''
          },
          'Khosravikia (PGA)': {
            objectCategory: 'model',
            objectType: 'bridge_fragility_model',
            objectName: 'Khosravikia (PGA)',
            source: 'shakerisk',
            measure: 'PGA',
            model_name: 'khosravikia_etal_2018',
            dataframe: 'none',
            required_classification: ''
          },
          'HAZUS (PGA)': {
            objectCategory: 'model',
            objectType: 'bridge_fragility_model',
            objectName: 'HAZUS (PGA)',
            source: 'shakerisk',
            measure: 'PGA',
            model_name: 'hazus',
            dataframe: 'none',
            required_classification: ''
          },
          'Custom (PGA - DataFrame)': {
            objectCategory: 'model',
            objectType: 'bridge_fragility_model',
            objectName: 'Custom (PGA - DataFrame)',
            source: 'dataframe',
            measure: 'PGA',
            model_name: 'hazus',
            dataframe: 'Bridge Fragility DataFrame',
            required_classification: 'khosravikia_etal_2018'
          },
          'HAZUS(PGV)': {
            objectCategory: 'model',
            objectType: 'bridge_fragility_model',
            objectName: 'HAZUS(PGV)',
            source: 'shakerisk',
            measure: 'PGV',
            model_name: 'hazus',
            dataframe: 'none',
            required_classification: ''
          }
        },
        bridge_construction_cost_model: {
          'TX From TxDot': {
            objectCategory: 'model',
            objectType: 'bridge_construction_cost_model',
            objectName: 'TX From TxDot',
            source: 'shakerisk',
            model_name: 'tx_txdot',
            dataframe: 'none',
            required_classification: ''
          },
          HAZUS: {
            objectCategory: 'model',
            objectType: 'bridge_construction_cost_model',
            objectName: 'HAZUS',
            source: 'shakerisk',
            model_name: 'hazus',
            dataframe: 'none',
            required_classification: ''
          },
          'Custom (DataFrame)': {
            objectCategory: 'model',
            objectType: 'bridge_construction_cost_model',
            objectName: 'Custom (DataFrame)',
            source: 'dataframe',
            model_name: 'hazus',
            dataframe: 'Bridge Construction Cost  (DataFrame)',
            required_classification: 'hazus'
          }
        },
        bridge_repair_cost_model: {
          'Basoz, Mander': {
            objectCategory: 'model',
            objectType: 'bridge_repair_cost_model',
            objectName: 'Basoz, Mander',
            source: 'shakerisk',
            model_name: 'basoz_mander_1999',
            dataframe: 'none',
            required_classification: ''
          },
          'Custom (DataFrame)': {
            objectCategory: 'model',
            objectType: 'bridge_repair_cost_model',
            objectName: 'Custom (DataFrame)',
            source: 'dataframe',
            model_name: 'basoz_mander_1999',
            dataframe: 'Bridge Repair Cost Ratio DataFrame',
            required_classification: ''
          }
        },
        bridge_restoration_model: {
          HAZUS: {
            objectCategory: 'model',
            objectType: 'bridge_restoration_model',
            objectName: 'HAZUS',
            source: 'shakerisk',
            model_name: 'hazus',
            dataframe: 'none',
            required_classification: ''
          },
          'Custom (DataFrame)': {
            objectCategory: 'model',
            objectType: 'bridge_restoration_model',
            objectName: 'Custom (DataFrame)',
            source: 'dataframe',
            model_name: 'hazus',
            dataframe: 'Bridge Restoration DataFrame',
            required_classification: 'hazus'
          }
        },
        building_fragility_model: {
          'HAZUS (PGA)': {
            objectCategory: 'model',
            objectType: 'building_fragility_model',
            objectName: 'HAZUS (PGA)',
            source: 'shakerisk',
            measure: 'PGA',
            model_name: 'hazus',
            seismic_consideration: 'pre code',
            dataframe: 'none',
            required_classification: ''
          },
          'Custom (PGA - DataFrame)': {
            objectCategory: 'model',
            objectType: 'building_fragility_model',
            objectName: 'Custom (PGA - DataFrame)',
            source: 'dataframe',
            measure: 'PGA',
            model_name: 'hazus',
            seismic_consideration: 'pre code',
            dataframe: 'Building Fragility DataFrame',
            required_classification: 'hazus'
          }
        },
        building_repair_cost_model: {
          HAZUS: {
            objectCategory: 'model',
            objectType: 'building_repair_cost_model',
            objectName: 'HAZUS',
            source: 'shakerisk',
            model_name: 'hazus',
            dataframe: 'none',
            required_classification: ''
          },
          'Custom (DataFrame)': {
            objectCategory: 'model',
            objectType: 'building_repair_cost_model',
            objectName: 'Custom (DataFrame)',
            source: 'dataframe',
            model_name: 'hazus',
            dataframe: 'Building Repair Cost Ratio DataFrame',
            required_classification: ''
          }
        },
        param_text: {
          'Output Path': {
            objectCategory: 'model',
            objectType: 'param_text',
            objectName: 'Output Path',
            text: 'DEMO_DIR\\output'
          },
          'Trained Artificial Neural Network Model Path': {
            objectCategory: 'model',
            objectType: 'param_text',
            objectName: 'Trained Artificial Neural Network Model Path',
            text: 'DEMO_DIR\\ML_Trained_Models\\trained_MLM_ANN'
          },
          'Trained Artificial Neural Network Info Path': {
            objectCategory: 'model',
            objectType: 'param_text',
            objectName: 'Trained Artificial Neural Network Info Path',
            text: 'DEMO_DIR\\ML_Trained_Models\\trained_MLM_ANN_info.json'
          },
          'Templates Path': {
            objectCategory: 'model',
            objectType: 'param_text',
            objectName: 'Templates Path',
            text: 'DEMO_DIR\\templates\\{}'
          }
        },
        data_frame: {
          'Worldwide Earthquake DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Worldwide Earthquake DataFrame',
            data_source: 'manual',
            headings_names: 'Year, 5-5.9, 6-6.9, 7-7.9, 8+',
            data: '2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019;1344,1224,1201,1203,1515,1693,1712,2074,1768,1896,2209,2276,1401,1453,1574,1419,1550,1455,1674,1492;146,121,127,140,141,140,142,178,168,144,150,185,108,123,143,127,130,104,117,135;14,15,13,14,14,10,9,14,12,16,23,19,12,17,11,18,16,6,16,9;1,1,0,1,2,1,2,4,0,1,1,1,2,2,1,1,0,1,1,1;',
            path: '',
            data_types: 'str, int, int, int, int'
          },
          'Bridge (TX) DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Bridge (TX) DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path,bridge\\bridge_TX.csv',
            data_types: ''
          },
          'Bridge Fragility DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Bridge Fragility DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path,bridge\\bridge_fragility_model.csv',
            data_types: ''
          },
          'Bridge Construction Cost  (DataFrame)': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Bridge Construction Cost  (DataFrame)',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path,bridge\\bridge_construction_cost_model.csv',
            data_types: ''
          },
          'Bridge Repair Cost Ratio DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Bridge Repair Cost Ratio DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path, bridge\\bridge_repair_cost_model.csv',
            data_types: ''
          },
          'Vs30 DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Vs30 DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path, vs30.csv',
            data_types: ''
          },
          'Building DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Building DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path, building\\building.csv',
            data_types: ''
          },
          'Building Fragility DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Building Fragility DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path, building\\building_fragility_model.csv',
            data_types: ''
          },
          'Building Repair Cost Ratio DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Building Repair Cost Ratio DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path, building\\building_repair_cost_model.csv',
            data_types: ''
          },
          'Bridge (CA) DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Bridge (CA) DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path, bridge\\bridge_CA.csv',
            data_types: ''
          },
          'Bridge Restoration DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Bridge Restoration DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path, bridge\\bridge_restoration_model.csv',
            data_types: ''
          },
          'NBI Data DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'NBI Data DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path,nbi.csv',
            data_types: ''
          },
          'Demo Ground Motion DataFrame': {
            objectCategory: 'model',
            objectType: 'data_frame',
            objectName: 'Demo Ground Motion DataFrame',
            data_source: 'csv',
            headings_names: '',
            data: '',
            path: 'Templates Path, ground_motions.csv',
            data_types: ''
          }
        }
      },
      method: {
        dataframe_save: {
          'Worldwide Earthquake': {
            objectCategory: 'method',
            objectType: 'dataframe_save',
            objectName: 'Worldwide Earthquake',
            dataframe: 'Worldwide Earthquake DataFrame',
            output_path: 'Output Path'
          }
        },
        ML_based_model: {
          'Artificial Neural Network': {
            objectCategory: 'method',
            objectType: 'ML_based_model',
            objectName: 'Artificial Neural Network',
            dataframe: 'Demo Ground Motion DataFrame',
            input_columns: 'magnitude, vs30, rhypo',
            output_columns: 'pga',
            train_fraction: 0.8,
            pre_processing: 'standard_scaling',
            output_transformation: 'lognormal',
            ml_model_options: 'Artificial Neural Network Options',
            use_mixed_effects: false,
            me_columns: '',
            output_path: 'Output Path'
          },
          'Random Forest': {
            objectCategory: 'method',
            objectType: 'ML_based_model',
            objectName: 'Random Forest',
            dataframe: 'Demo Ground Motion DataFrame',
            input_columns: 'magnitude, vs30, rhypo',
            output_columns: 'pga',
            train_fraction: 0.8,
            pre_processing: 'none',
            output_transformation: 'lognormal',
            ml_model_options: 'Random Forest Options',
            use_mixed_effects: false,
            me_columns: '',
            output_path: 'Output Path'
          },
          'Support Vector Machine': {
            objectCategory: 'method',
            objectType: 'ML_based_model',
            objectName: 'Support Vector Machine',
            dataframe: 'Demo Ground Motion DataFrame',
            input_columns: 'magnitude, vs30, rhypo',
            output_columns: 'pga',
            train_fraction: 0.8,
            pre_processing: 'standard_scaling',
            output_transformation: 'lognormal',
            ml_model_options: 'Support Vector Machine Options',
            use_mixed_effects: false,
            me_columns: '',
            output_path: 'Output Path'
          }
        },
        bridge_repair_cost_estimation: {
          'Irving Bridge Loss': {
            objectCategory: 'method',
            objectType: 'bridge_repair_cost_estimation',
            objectName: 'Irving Bridge Loss',
            run_num: 20,
            deg_limit: 0.2,
            earthquake: 'Irving Earthquake',
            vs30: 'TX',
            intensity_model: 'Khosravikia (PGV) - Deterministic',
            bridge: 'TX',
            fragility_model: 'Khosravikia (PGV)',
            construction_cost: 'TX From TxDot',
            repair_cost_model: 'Basoz, Mander',
            output_path: 'Output Path'
          }
        },
        shaking_evaluation: {
          'Irving Shaking Evaluation': {
            objectCategory: 'method',
            objectType: 'shaking_evaluation',
            objectName: 'Irving Shaking Evaluation',
            deg_limit: 0.2,
            earthquake: 'Irving Earthquake',
            vs30: 'TX',
            intensity_model: 'Khosravikia (PGV) - Deterministic',
            output_path: 'Output Path'
          }
        },
        bridge_inspection_prioritization: {
          'Irving Bridge Inspection': {
            objectCategory: 'method',
            objectType: 'bridge_inspection_prioritization',
            objectName: 'Irving Bridge Inspection',
            state_name: 'Slight',
            state_min_prob: 0.4,
            deg_limit: 0.2,
            earthquake: 'Irving Earthquake',
            vs30: 'TX',
            intensity_model: 'Khosravikia (PGV) - Deterministic',
            bridge: 'TX',
            fragility_model: 'Khosravikia (PGV)',
            output_path: 'Output Path'
          }
        },
        building_repair_cost_estimation: {
          'Irving Building Loss': {
            objectCategory: 'method',
            objectType: 'building_repair_cost_estimation',
            objectName: 'Irving Building Loss',
            run_num: 20,
            deg_limit: 0.2,
            earthquake: 'Irving Earthquake',
            vs30: 'TX',
            building: 'TX',
            construction_cost: 'HAZUS',
            intensity_model: 'Zalachoris (PGA) - Deterministic',
            fragility_model: 'HAZUS (PGA)',
            repair_cost_model: 'HAZUS',
            output_path: 'Output Path'
          }
        },
        building_inspection_prioritization: {
          'Irving Building Inspection': {
            objectCategory: 'method',
            objectType: 'building_inspection_prioritization',
            objectName: 'Irving Building Inspection',
            state_name: 'Moderate',
            state_min_prob: 0.75,
            deg_limit: 0.2,
            earthquake: 'Irving Earthquake',
            vs30: 'TX',
            intensity_model: 'Zalachoris (PGA) - Deterministic',
            building: 'TX',
            fragility_model: 'HAZUS (PGA)',
            output_path: 'Output Path'
          }
        },
        network_inspection_prioritization: {
          'Anaheim Network Inspection': {
            objectCategory: 'method',
            objectType: 'network_inspection_prioritization',
            objectName: 'Anaheim Network Inspection',
            deg_limit: 0.1,
            cost_per_hour: 0.9,
            earthquake: 'Anaheim Earthquake',
            vs30: 'Constant (760)',
            bridge: 'CA',
            intensity_model: 'Khosravikia (PGA) - Deterministic',
            fragility_model: 'HAZUS (PGA)',
            construction_cost: 'HAZUS',
            repair_cost_model: 'Basoz, Mander',
            restoration_model: 'HAZUS',
            transportation_network: 'Anaheim',
            network_model: 'Frank Wolfe',
            output_path: 'Output Path'
          }
        },
        earthquake_data: {
          'California 2019 Earthquake Data': {
            objectCategory: 'method',
            objectType: 'earthquake_data',
            objectName: 'California 2019 Earthquake Data',
            min_magnitude: 4,
            area: 'California',
            time_interval: '[2019-2020]',
            output_path: 'Output Path'
          }
        },
        nbi_data_conversion: {
          'TX - 2019 NBI Data Conversion': {
            objectCategory: 'method',
            objectType: 'nbi_data_conversion',
            objectName: 'TX - 2019 NBI Data Conversion',
            source: 'nbi_web',
            state: 'TX',
            year: '2019',
            dataframe: 'none',
            on_system: true,
            non_culvert: true,
            output_path: 'Output Path'
          },
          'Custom NBI Data Conversion (DataFrame)': {
            objectCategory: 'method',
            objectType: 'nbi_data_conversion',
            objectName: 'Custom NBI Data Conversion (DataFrame)',
            source: 'dataframe',
            state: '',
            year: '',
            dataframe: 'NBI Data DataFrame',
            on_system: false,
            non_culvert: false,
            output_path: ''
          }
        },
        network_damage_estimation: {
          'Anaheim Network Damage': {
            objectCategory: 'method',
            objectType: 'network_damage_estimation',
            objectName: 'Anaheim Network Damage',
            deg_limit: 0.1,
            cost_per_hour: 0.9,
            earthquake: 'Anaheim Earthquake',
            vs30: 'Constant (760)',
            intensity_model: 'Khosravikia (PGA) - Deterministic',
            bridge: 'CA',
            fragility_model: 'HAZUS (PGA)',
            construction_cost: 'HAZUS',
            repair_cost_model: 'Basoz, Mander',
            restoration_model: 'HAZUS',
            transportation_network: 'Anaheim',
            network_model: 'Frank Wolfe',
            output_path: 'Output Path'
          },
          'Anaheim Network Damage (DataFrame)': {
            objectCategory: 'method',
            objectType: 'network_damage_estimation',
            objectName: 'Anaheim Network Damage (DataFrame)',
            deg_limit: 0.2,
            cost_per_hour: 0.9,
            earthquake: 'Anaheim Earthquake',
            vs30: 'Constant (760)',
            intensity_model: 'Zalachoris (PGA) - Deterministic',
            bridge: 'Custom (DataFrame - CA)',
            fragility_model: 'HAZUS (PGA)',
            construction_cost: 'Custom (DataFrame)',
            repair_cost_model: 'Custom (DataFrame)',
            restoration_model: 'Custom (DataFrame)',
            transportation_network: 'Anaheim',
            network_model: 'Frank Wolfe',
            output_path: ''
          }
        },
        building_damage_estimation: {
          'Irving Building Damage': {
            objectCategory: 'method',
            objectType: 'building_damage_estimation',
            objectName: 'Irving Building Damage',
            deg_limit: 0.2,
            earthquake: 'Irving Earthquake',
            vs30: 'TX',
            building: 'TX',
            intensity_model: 'Zalachoris (PGA) - Deterministic',
            fragility_model: 'HAZUS (PGA)',
            construction_cost: 'HAZUS',
            repair_cost_model: 'HAZUS',
            output_path: 'Output Path'
          },
          'Irving Building Damage (DataFrame)': {
            objectCategory: 'method',
            objectType: 'building_damage_estimation',
            objectName: 'Irving Building Damage (DataFrame)',
            deg_limit: 0.2,
            earthquake: 'Irving Earthquake',
            vs30: 'Custom (DataFrame)',
            building: 'Custom (DataFrame)',
            intensity_model: 'Khosravikia (PGA) - Deterministic',
            fragility_model: 'Custom (PGA - DataFrame)',
            construction_cost: 'HAZUS',
            repair_cost_model: 'Custom (DataFrame)',
            output_path: ''
          }
        },
        bridge_damage_estimation: {
          'Irving Bridge Damage (PGV)': {
            objectCategory: 'method',
            objectType: 'bridge_damage_estimation',
            objectName: 'Irving Bridge Damage (PGV)',
            deg_limit: 0.05,
            earthquake: 'Irving Earthquake',
            vs30: 'TX',
            intensity_model: 'Khosravikia (PGV) - Deterministic',
            bridge: 'TX',
            fragility_model: 'Khosravikia (PGV)',
            construction_cost: 'TX From TxDot',
            repair_cost_model: 'Basoz, Mander',
            output_path: 'Output Path'
          },
          'Irving Bridge Damage (PGA)': {
            objectCategory: 'method',
            objectType: 'bridge_damage_estimation',
            objectName: 'Irving Bridge Damage (PGA)',
            deg_limit: 0.2,
            earthquake: 'Irving Earthquake',
            vs30: 'TX',
            intensity_model: 'Khosravikia (PGA) - Deterministic',
            bridge: 'TX',
            fragility_model: 'Khosravikia (PGA)',
            construction_cost: 'TX From TxDot',
            repair_cost_model: 'Basoz, Mander',
            output_path: 'Output Path'
          },
          'Irving Bridge Damage (PGA - Trained Model)': {
            objectCategory: 'method',
            objectType: 'bridge_damage_estimation',
            objectName: 'Irving Bridge Damage (PGA - Trained Model)',
            deg_limit: 0.2,
            earthquake: 'Irving Earthquake',
            vs30: 'TX',
            intensity_model: 'Trained Artificial Neural Network (PGA)',
            bridge: 'TX',
            fragility_model: 'Khosravikia (PGA)',
            construction_cost: 'TX From TxDot',
            repair_cost_model: 'Basoz, Mander',
            output_path: 'Output Path'
          },
          'Irving Bridge Damage (PGA - DataFrame)': {
            objectCategory: 'method',
            objectType: 'bridge_damage_estimation',
            objectName: 'Irving Bridge Damage (PGA - DataFrame)',
            deg_limit: 0.2,
            earthquake: 'Irving Earthquake',
            vs30: 'Custom (DataFrame)',
            intensity_model: 'Zalachoris (PGA) - Deterministic',
            bridge: 'Custom (DataFrame - TX)',
            fragility_model: 'Custom (PGA - DataFrame)',
            construction_cost: 'Custom (DataFrame)',
            repair_cost_model: 'Custom (DataFrame)',
            output_path: ''
          },
          Test: {
            objectCategory: 'method',
            objectType: 'bridge_damage_estimation',
            objectName: 'Test',
            deg_limit: 0.05,
            earthquake: 'Irving Earthquake',
            vs30: 'TX',
            intensity_model: 'Khosravikia (PGV) - Deterministic',
            bridge: 'TX',
            fragility_model: 'HAZUS(PGV)',
            construction_cost: 'HAZUS',
            repair_cost_model: 'Basoz, Mander',
            output_path: ''
          }
        }
      }
    }
}




export const formValues = form