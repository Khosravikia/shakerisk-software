import * as actionTypes from '../actions/actionTypes';

const initialState = {
    outputLines: [[new Date(), 0, "Output is initiated. "]]
}

const reducer = (State = initialState, action) => {

    switch(action.type){

        case actionTypes.ADD_LINE:
            return {
                ...State,
                outputLines: [...State.outputLines, action.payload]
            }
        case actionTypes.CLEAR_CONSOLE:
            return {
                ...State,
                outputLines: []
            }
        default:
            break;
    }
    return State;
}

export default reducer;