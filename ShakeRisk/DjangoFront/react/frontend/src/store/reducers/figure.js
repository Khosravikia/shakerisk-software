import * as actionTypes from '../actions/actionTypes';

const initialState = {
    figures: []
}

const reducer = (State = initialState, action) => {
    switch(action.type){
        case actionTypes.ADD_FIGURE:
            return {
                ...State,
                figures: [...State.figures,  {
                    'figureType': action.payload.figureType,
                    'figureTitle': action.payload.figureTitle,
                    'key': action.payload.key,
                    'data': action.payload.data
                }]
            }

        case actionTypes.REMOVE_FIGURE:
            return {
                ...State,
                figures: [...State.figures.slice(0, action.index), ...State.figures.slice(action.index+1)]
            };

        default:
            break;
    }

    return State;
}

export default reducer;