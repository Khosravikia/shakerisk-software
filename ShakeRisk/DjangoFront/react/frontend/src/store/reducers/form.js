import * as actionTypes from '../actions/actionTypes';
import { addObjectToForm, changeObjectName, deleteObjectFromStore, loadStoredObjects } from '../utility'
import { formValues } from './devInitialStoreValues';

let initialState = {}
const overrideDevMode = false;
if ((!process.env.NODE_ENV || process.env.NODE_ENV === 'development') && (!overrideDevMode)) // if development mode
{
  initialState = formValues;
}
else{
  initialState = {
    storedObjects: {},
    selectedObjectCategory: null,
    categoryDisplayName: null,
    selectedObjectType : null,
    typeDisplayName: null,
    selectedObjectName : null,
    isEditMode: false,
    selectedObjectNameToRename: null
  };
}

const reducer = (State = initialState, action) => {

    switch (action.type){
        case (actionTypes.FORM_SUBMIT):
            // var respone = addObjectToForm(State, action.payLoad);
            // console.log("[reducers/form.js] Form Submit state: ", respone);
            return {...addObjectToForm(State, action.payLoad),
                ...((action.isEditMode !== null) && {isEditMode: action.isEditMode})};

        case (actionTypes.DELETE_OBJECT):
            return {...deleteObjectFromStore(State, action.objectCategory, action.objectType, action.objectName),
                ...((action.isEditMode !== null) && {isEditMode: action.isEditMode})}

        case(actionTypes.CHANGE_OBJECT_NAME):
            return {...changeObjectName(State, action.objectCategory, action.objectType, action.objectOldName,
                 action.objectNewName, action.relatedObjects),
                selectedObjectNameToRename: null,
                ...((action.isEditMode !== null) && {isEditMode: action.isEditMode})}

        case(actionTypes.SELECT_OBJECT_TYPE):
            return {...State,
                selectedObjectCategory: action.objectCategory,
                selectedObjectType: action.objectType,
                categoryDisplayName: action.categoryDisplayName,
                typeDisplayName: action.typeDisplayName,
                ...((action.isEditMode !== null) && {isEditMode: action.isEditMode})}

        case(actionTypes.SET_EDIT_MODE):
            return {...State,
                isEditMode: action.isEditMode}

        case(actionTypes.SELECT_OBJECT_NAME):
            return {...State,
                selectedObjectName: action.objectName,
                ...((action.isEditMode !== null) && {isEditMode: action.isEditMode})
            }

        case(actionTypes.LOAD_STORED_OBJECTS):
            return loadStoredObjects(action.objectToloadFrom);

        default:
            break;
    }

    return State;
}

export default reducer;