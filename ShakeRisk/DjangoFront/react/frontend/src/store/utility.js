export const addObjectToForm = (state, newObject) =>{
    
    const newObjectCategory = newObject.objectCategory;
    const newObjectType = newObject.objectType;
    const newObjectName = newObject.objectName;

    // Create new category if it does not exists
    if(!(newObjectCategory in state.storedObjects))
        state.storedObjects[newObjectCategory] = {};

    // Create new type if it does not exists
    if(!(newObjectType in state.storedObjects[newObjectCategory]))
        state.storedObjects[newObjectCategory][newObjectType] = {};

    // Create new object with name if it does not exists
    if(!(newObjectName in state.storedObjects[newObjectCategory][newObjectType]))
     state.storedObjects[newObjectCategory][newObjectType][newObjectName] = {};

    const newState = {
        ...state,
        storedObjects: {...state.storedObjects,
            [newObjectCategory]: {
                ...state.storedObjects[newObjectCategory],
                [newObjectType]: {...state.storedObjects[newObjectCategory][newObjectType],
                    [newObjectName]: {
                        ...newObject
                    }
                }
            }
        }
    }
    // newState.storedObjects[newObjectType][newObjectName] = {...newObject};

    return newState;
}

// const removeProperty = ({[key]: keyValue, ...newObj}, key) => {
//     return [keyValue, newObj]
// };
const removeProperty = (original, property) => { let { [property]: propertyValue, ...newObj } = original;
 return [propertyValue, newObj] }

export const deleteObjectFromStore = (state, objectCategory, objectType, objectName) => {
    
    const [storedObjects, withoutStoredObjects] = removeProperty(state, 'storedObjects');
    const [category, withoutCategory] = removeProperty(storedObjects, objectCategory)
    const [type, withoutType] = removeProperty(category, objectType);
    const withoutObject = removeProperty(type, objectName)[1];

    const newState = {
        ...withoutStoredObjects,
        storedObjects: {...withoutCategory,
            [objectCategory]: {
                ...withoutType,
                [objectType]: {...withoutObject,
                }
            }
        }
    }

    return newState;
}

export const changeObjectName = (state, objectCategory, objectType, objectOldName, objectNewName, relatedObjects) => {
    const object = {...state.storedObjects[objectCategory][objectType][objectOldName]};
    
    object['objectName'] = objectNewName;

    let removedObject = deleteObjectFromStore(state, objectCategory, objectType, objectOldName);
    let newState = addObjectToForm(removedObject, object);
    
    // Change the name for linked property in related objects as well
    if(relatedObjects.length > 0){
        var objectTochange;
        relatedObjects.forEach( rel => {
            objectTochange = state.storedObjects[rel['category']][rel['type']][rel['name']]
            objectTochange[rel['property']] = objectNewName
            removedObject = deleteObjectFromStore(newState, rel['category'], rel['type'], rel['name']);
            newState = addObjectToForm(removedObject, objectTochange);
        });
    }

    return newState;
}

export const loadStoredObjects = (objectToloadFrom) => {
    const storedObjects = {}
    storedObjects['model'] = objectToloadFrom['model'];
    storedObjects['method'] = objectToloadFrom['method'];

    return {
        selectedObjectCategory: null,
        categoryDisplayName: null,
        selectedObjectType : null,
        typeDisplayName: null,
        selectedObjectName : null,
        isEditMode: false,
        selectedObjectNameToRename: null,
        storedObjects: storedObjects
    }
}
// newObject example:
// const newObject = {
//     objectCategory: "model",
//     objectType: "earthquake",
//     objectName: "My earthquake object",
//     mw: 5,
//     depth: 3
//     }
// }