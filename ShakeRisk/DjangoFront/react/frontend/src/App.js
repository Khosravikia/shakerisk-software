import React, {Component} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import MainContainer from './containers/MainContainer';
import About from './components/pages/about';
import './App.css';
// import {appendScript} from './customHooks/appendScript';

class App extends Component {

  // componentDidMount () {
  //   appendScript("https://cdn.plot.ly/plotly-latest.min.js");
  // }

  // componentDidMount() {
  //   document.title = 'ShakRisk V0.2.0'
  // }
  render(){
   
    return (
      <div className="App">
        <Router>
        <Switch>
          <Route path="/about">
            <About></About>
          </Route>
          <Route path="/help">
            <div>This is help<Link to='/'>Home</Link></div>
          </Route>
          <Route path="/">
            <MainContainer></MainContainer>
          </Route>
        </Switch>
        </Router>
        
      </div>
    );
    
  }
}

export default App;
