import React from 'react';

import classes from './Columns.module.css';

const Column = (props) => {

    const width = props.width + '%';

    return (
        <div className={classes.Column} style={{"width":width,  "minwidth":width}}>
            {props.children}
        </div>
    );
};

export default Column;