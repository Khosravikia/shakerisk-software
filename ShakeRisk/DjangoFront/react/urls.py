from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('sendrunrequest', views.send_run_json, name='send-run-request'),
    # ... the rest of the urlpatterns ...
    # must be catch-all for pushState to work
    url(r'^', views.FrontendAppView.as_view()),
]
