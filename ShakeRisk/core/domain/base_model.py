from abc import abstractproperty
from core.domain import base_entity as be


class BaseModel(be.BaseEntity):

    TYPE = abstractproperty()

    def __init__(self, name: str, source, model_name=None, dataframe=None, required_classification=None):
        self.name = name
        self.source = source
        self.model_name = model_name
        self.dataframe = dataframe
        self.required_classification = required_classification

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return cls(name=adict['name'],
                   source=adict['source'],
                   model_name=adict['model_name'],
                   dataframe=adict['dataframe'],
                   required_classification=adict['required_classification']
                   )
