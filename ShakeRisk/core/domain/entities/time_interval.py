from core.domain import base_entity as be


class TimeInterval(be.BaseEntity):

    def __init__(self, name: str, start_date, end_date):
        self.name = name
        self.start_date = start_date
        self.end_date = end_date

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return TimeInterval(name=adict['name'],
                            start_date=adict['start_date'],
                            end_date=adict['end_date'])


# Test:

if __name__ == '__main__':
    print('test')
