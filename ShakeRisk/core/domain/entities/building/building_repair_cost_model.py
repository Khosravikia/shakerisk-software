from core.domain.base_model import BaseModel


class BuildingRepairCostModel(BaseModel):
    TYPE = 'repair_cost_ratio'
