from core.domain.base_model import BaseModel
from core.domain import base_entity as be


class BuildingFragilityModel(BaseModel):
    TYPE = 'fragility'

    def __init__(self, name: str, source, measure, model_name=None, seismic_consideration=None,
                 dataframe=None, required_classification=None):
        super().__init__(name, source, model_name, dataframe, required_classification)
        self.measure = measure
        self.seismic_consideration = seismic_consideration

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return cls(name=adict['name'],
                   source=adict['source'],
                   measure=adict['measure'],
                   model_name=adict['model_name'],
                   seismic_consideration=adict['seismic_consideration'],
                   dataframe=adict['dataframe'],
                   required_classification=adict['required_classification'])
