from core.domain.base_model import BaseModel


class BuildingConstructionCostModel(BaseModel):
    TYPE = 'unit_construction_cost'
