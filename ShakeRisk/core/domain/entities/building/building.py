from core.domain import base_entity as be


class Building(be.BaseEntity):
    TYPE = 'building'

    def __init__(self, name: str, source, filter_state=None, filter_location=None, dataframe=None):
        self.name = name
        self.source = source
        self.filter_state = filter_state
        self.filter_location = filter_location
        self.dataframe = dataframe

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return Building(name=adict['name'],
                        source=adict['source'],
                        filter_state=adict['filter_state'],
                        filter_location=adict['filter_location'],
                        dataframe=adict['dataframe'])
