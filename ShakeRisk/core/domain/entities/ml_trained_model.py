import pandas as pd
from tensorflow.keras.models import load_model
import joblib
from core.domain import base_entity as be


# Todo: Refrain form using libraries like tensorflow in core

class MLTrainedModel(be.BaseEntity):

    def __init__(self, name: str,  model_path: str, additional_data_path: str,
                 input_names_orders: list):
        self.name = name
        self.model_path = model_path
        self.input_names_orders = input_names_orders
        self.additional_data_path = additional_data_path

    # def predict(self, input_data):
    #     if self.model_type == 'tensorflow':
    #         return self.model.predict(input_data)
    #     elif self.model_type == 'scikit_learn':
    #         return self.model.predict(input_data).reshape(-1, 1)

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        input_names_orders = None
        if adict['input_names_orders']:
            input_names_orders = [x.strip() for x in adict['input_names_orders'].split(',')]

        # if adict['model_type'] == 'tensorflow':
        #     model = load_model(adict['model_path'])
        # elif adict['model_type'] == 'scikit_learn':
        #     model = joblib.load(adict['model_path'])
        # else:
        #     raise TypeError('Model type of {} is not supported'.format(adict['model_type']))
        #
        # additional_data = None
        # if adict['additional_data_path']:
        #     additional_data = pd.read_csv(adict['additional_data_path'])

        return MLTrainedModel(name=adict['name'],
                              model_path=adict['model_path'],
                              additional_data_path=adict['additional_data_path'],
                              input_names_orders=input_names_orders,
                              # model=model,
                              # additional_data=additional_data
                              )
