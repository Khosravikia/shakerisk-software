from core.domain import base_entity as be


class NBIDataSource(be.BaseEntity):

    def __init__(self, name: str, source=None, path=None, state=None, year=None):
        self.name = name
        self.source = source
        self.path = path
        self.state = state
        self.year = year

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return NBIDataSource(name=adict['name'],
                             source=adict['source'],
                             path=adict['path'],
                             state=adict['state'],
                             year=adict['year'])


# Test:

if __name__ == '__main__':
    entity = Earthquake.from_dict({'magnitude': 5, 'latitude': 36, 'longitude': -90, 'depth': 5})
    if entity:
        print(entity)
    else:
        print(entity.errors)
