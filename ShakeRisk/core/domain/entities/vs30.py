from core.domain import base_entity as be


class Vs30(be.BaseEntity):

    def __init__(self, name: str, source, mapping, filter_state=None, filter_location=None, constant=None,
                 dataframe=None):
        self.name = name
        self.source = source
        self.mapping = mapping
        self.filter_state = filter_state
        self.filter_location = filter_location
        self.constant = constant
        self.dataframe = dataframe

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity
        return Vs30(name=adict['name'],
                    source=adict['source'],
                    mapping=adict['mapping'],
                    filter_state=adict['filter_state'],
                    filter_location=adict['filter_location'],
                    constant=adict['constant'],
                    dataframe=adict['dataframe'])

