from core.domain import base_entity as be


class Point(be.BaseEntity):

    def __init__(self, name: str, latitude, longitude):
        self.name = name
        self.latitude = latitude
        self.longitude = longitude

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return Point(name=adict['name'],
                     latitude=adict['latitude'],
                     longitude=adict['longitude'])

