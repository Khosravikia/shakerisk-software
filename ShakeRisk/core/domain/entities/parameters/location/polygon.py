from core.domain import base_entity as be


class Polygon(be.BaseEntity):

    def __init__(self, name: str, latitudes: list, longitudes: list):
        self.name = name
        self.latitudes = latitudes
        self.longitudes = longitudes

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        latitudes = [float(x) for x in adict['latitudes'].replace(' ', '').split(',')]
        longitudes = [float(x) for x in adict['longitudes'].replace(' ', '').split(',')]
        return Polygon(name=adict['name'],
                       latitudes=latitudes,
                       longitudes=longitudes)
