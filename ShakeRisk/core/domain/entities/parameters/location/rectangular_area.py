from core.domain import base_entity as be


class RectangularArea(be.BaseEntity):

    def __init__(self, name: str, min_lat, max_lat, min_lng, max_lng):
        self.name = name
        self.min_lat = min_lat
        self.max_lat = max_lat
        self.min_lng = min_lng
        self.max_lng = max_lng

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return RectangularArea(name=adict['name'],
                               min_lat=adict['min_lat'],
                               max_lat=adict['max_lat'],
                               min_lng=adict['min_lng'],
                               max_lng=adict['max_lng'])


# Test:

if __name__ == '__main__':
    print('test')
