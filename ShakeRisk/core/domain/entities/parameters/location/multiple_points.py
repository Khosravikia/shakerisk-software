from core.domain import base_entity as be


class MultiplePoints(be.BaseEntity):

    def __init__(self, name: str, latitudes, longitudes):
        self.name = name
        self.latitudes = [float(x) for x in latitudes.replace(' ', '').split(',')]
        self.longitudes = [float(x) for x in longitudes.replace(' ', '').split(',')]

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return MultiplePoints(name=adict['name'],
                              latitudes=adict['latitudes'],
                              longitudes=adict['longitudes'])
