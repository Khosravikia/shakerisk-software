from core.domain import base_entity as be


class LossFunction(be.BaseEntity):

    def __init__(self, name: str, variables: list, function_def: str, function):
        self.name = name
        self.variables = variables
        self.function_def = function_def
        self.function = function

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        variables = [x.strip() for x in adict['variables'].split(',')]

        exp = adict['function_def']
        for index, v in enumerate(variables):
            exp = exp.replace(v, 'variables[{}]'.format(index))

        def function_def(*variables):
            return eval(exp)

        return LossFunction(name=adict['name'],
                            variables=variables,
                            function_def=adict['function_def'],
                            function=function_def)
