import pandas as pd
from core.domain import base_entity as be


# Todo: Decide if this should return object with all information or dataframe itself

class DataFrame(be.BaseEntity):

    def __init__(self, name: str, data_source: str, headings_names: list, data: str, path: str, data_types: list,
                 dataframe: pd.DataFrame):
        self.name = name
        self.data_source = data_source
        self.headings_names = headings_names
        self.data = data
        self.path = path
        self.data_types = data_types
        self.dataframe = dataframe

    def __len__(self):
        return len(self.dataframe)

    def __iter__(self):
        for idx in range(0, len(self)):
            yield self.dataframe.iloc[idx, :]

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        headings_names = None
        if adict['headings_names']:
            headings_names = [x.strip() for x in adict['headings_names'].split(',')]

        data_types = None
        if adict['data_types']:
            data_types = [x.strip() for x in adict['data_types'].split(',')]

        if adict['data_source'] == 'manual':
            columns = [x for x in adict['data'].split(';')]  # Separate columns
            for index, col in enumerate(columns):  # Separate values in columns
                columns[index] = columns[index].split(',')

            data_dict = {}  # Create a dictionary of columns
            for index, heading in enumerate(headings_names):
                data_dict.update({heading: columns[index]})
            dataframe = pd.DataFrame(data_dict)

        elif adict['data_source'] == 'csv':
            dataframe = pd.read_csv(open(adict['path']))  # usecols=self.headings_names

        else:
            raise NotImplementedError('Selected source is not a valid value')

        # Convert data types if "data_types" is defined
        if data_types:
            if len(data_types) > 1:  # If more than one type is specified, convert each column respectively
                conversion_dict = {}
                for index, heading in enumerate(headings_names):
                    conversion_dict.update({heading: data_types[index]})
                dataframe = dataframe.astype(conversion_dict)
            else:  # if one type is specified, convert all columns to that
                dataframe = dataframe.astype(data_types[0])

        return DataFrame(name=adict['name'],
                         data_source=adict['data_source'],
                         headings_names=headings_names,
                         data=adict['data'],
                         path=adict['path'],
                         data_types=data_types,
                         dataframe=dataframe)
