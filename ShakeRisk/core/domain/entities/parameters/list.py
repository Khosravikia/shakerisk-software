from core.domain import base_entity as be


# Todo: Preformance check
# Todo: Not handling outside data in request itself
class List(be.BaseEntity):

    def __init__(self, name: str, items: list):
        self.name = name
        self.items = items

    def __len__(self):
        return len(self.items)

    def __iter__(self):
        for idx in range(0, len(self)):
            yield self.items[idx]

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        items = [x.strip() for x in adict['items'].split(',')]
        if items[0].replace('.', '', 1).replace('-', '', 1).isdigit():
            items = [float(x) for x in items]

        return List(name=adict['name'],
                    items=items)
