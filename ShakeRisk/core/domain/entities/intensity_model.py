from core.domain import base_entity as be
from core.domain.entities.ml_trained_model import MLTrainedModel


class IntensityModel(be.BaseEntity):

    def __init__(self, name: str, source, gmm_model_name, measure, trained_model: MLTrainedModel = None,
                 probabilistic=False, spatial_correlation=None):
        self.name = name
        self.source = source
        self.gmm_model_name = gmm_model_name
        self.measure = measure
        self.trained_model = trained_model
        self.probabilistic = probabilistic
        self.spatial_correlation = spatial_correlation

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        # Convert Probabilistic from a selectable property to a boolean
        probabilistic = False
        if adict['probabilistic'] == 'probabilistic':
            probabilistic = True

        return IntensityModel(name=adict['name'],
                              source=adict['source'],
                              gmm_model_name=adict['gmm_model_name'],
                              measure=adict['measure'],
                              trained_model=adict['trained_model'],
                              probabilistic=probabilistic,
                              spatial_correlation=adict['spatial_correlation'])


# Test:

if __name__ == '__main__':
    print('test')
