from core.domain.base_model import BaseModel


class BridgeRepairCostModel(BaseModel):
    TYPE = 'repair_cost_ratio'
