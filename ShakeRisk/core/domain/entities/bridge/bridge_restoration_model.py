from core.domain.base_model import BaseModel


class BridgeRestorationModel(BaseModel):
    TYPE = 'restoration'
