from core.domain.base_model import BaseModel


class BridgeConstructionCostModel(BaseModel):
    TYPE = 'unit_construction_cost'
