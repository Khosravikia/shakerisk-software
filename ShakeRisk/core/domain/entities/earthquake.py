from core.domain import base_entity as be
from core.domain.entities.parameters.location.point import Point


class Earthquake(be.BaseEntity):

    def __init__(self, name: str, magnitude, location: Point, depth):
        self.name = name
        self.magnitude = magnitude
        self.location = location
        self.depth = depth

    @classmethod
    def from_dict(cls, adict):

        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return Earthquake(name=adict['name'],
                          magnitude=adict['magnitude'],
                          location=adict['location'],
                          depth=adict['depth'])


# Test:

if __name__ == '__main__':
    entity = Earthquake.from_dict({'magnitude': 5, 'latitude': 36, 'longitude': -90, 'depth': 5})
    if entity:
        print(entity)
    else:
        print(entity.errors)
