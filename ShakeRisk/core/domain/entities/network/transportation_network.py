from core.domain import base_entity as be


class TransportationNetwork(be.BaseEntity):

    def __init__(self, name: str, source, location_name=None):
        self.name = name
        self.source = source
        self.location_name = location_name

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return TransportationNetwork(name=adict['name'],
                                     source=adict['source'],
                                     location_name=adict['location_name'])
