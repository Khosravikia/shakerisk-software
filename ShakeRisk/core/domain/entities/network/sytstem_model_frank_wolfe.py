from core.domain import base_entity as be


class SystemModelFrankWolfe(be.BaseEntity):

    def __init__(self, name: str, algorithm, convergence):
        self.name = name
        self.algorithm = algorithm
        self.convergence = convergence

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('Entity Object', ' cannot be empty')
            return invalid_entity

        return SystemModelFrankWolfe(name=adict['name'],
                                     algorithm=adict['algorithm'],
                                     convergence=adict['convergence'])


# Test:

if __name__ == '__main__':
    entity = Earthquake.from_dict({'magnitude': 5, 'latitude': 36, 'longitude': -90, 'depth': 5})
    if entity:
        print(entity)
    else:
        print(entity.errors)
