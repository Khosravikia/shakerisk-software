from abc import abstractmethod, ABC
import pandas as pd


class BaseDataframeEntity(ABC):

    def __init__(self, dataframe: pd.DataFrame):
        self.dataframe = dataframe

    def __len__(self):
        return len(self.dataframe)

    def __iter__(self):
        for idx in range(0, len(self)):
            yield self.dataframe.iloc[idx, :]
