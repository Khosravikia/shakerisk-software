import numpy as np


def standard_scaler(input_data, *args):
    if not args:
        means = np.mean(input_data, axis=0)
        stds = np.std(input_data, axis=0, ddof=1)
    else:
        means = args[0]
        stds = args[1]

    result = (input_data - means) / stds
    return [result, means, stds]


def min_max_scaler(input_data, *args):
    if not args:
        mins = -1
        maxs = 1
    else:
        mins = args[0]
        maxs = args[1]

    result = (maxs - mins) * (input_data - input_data.min()) / (input_data.max() - input_data.min()) + mins
    return [result, mins, maxs]


available_pre_processing = {
        'standard_scaling': standard_scaler,
        'minmax_scaling': min_max_scaler
}


def pre_process(input_data, pre_process_type: str, *args):
    return available_pre_processing[pre_process_type](input_data, *args)
