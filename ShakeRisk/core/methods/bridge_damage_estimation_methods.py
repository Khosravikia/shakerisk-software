import numpy as np
from scipy.stats import lognorm
import pandas as pd

from Definitions import LIMIT_STATES, LIMIT_STATES_DATABASE
from core.domain.entities.bridge.bridge_construction_cost_model import BridgeConstructionCostModel
from core.domain.entities.bridge.bridge_fragility_model import BridgeFragilityModel
from core.domain.entities.bridge.bridge_repair_cost_model import BridgeRepairCostModel
from core.methods.random_generation_methods import get_random_value


def state_prob(structures_limited, fragility_model: BridgeFragilityModel, im_convert_rate, return_state_probs=False):
    fragility_values = fragility_model.dataframe
    required_classification = fragility_model.required_classification
    loss_state_prob = np.ones((len(structures_limited), len(LIMIT_STATES_DATABASE)))

    # changed = 0
    # changed_list = []
    for i, (index, structure) in enumerate(structures_limited.iterrows()):

        fragility = fragility_values[
            fragility_values[required_classification] == structure[required_classification]].iloc[0]

        # Todo: Handle HAZUS Sa[1.0] better
        if required_classification == 'hazus' and fragility_model.measure == '1.000':
            if structure['i_shape']:
                # changed += 1
                # print('{}. Before: {}'.format(changed, fragility['slight:median']), end='|')
                fragility['slight:median'] = fragility['slight:median'] * min(1, 2.5*structure['im']/structure['sa_03'])
                # changed_list.append(fragility['slight:median'])
                # print('After: {}'.format(fragility['slight:median']))

        for ls in range(0, len(LIMIT_STATES_DATABASE) - 1):
            loss_state_prob[i, ls + 1] = lognorm.cdf(structure['im'] * im_convert_rate,
                                                     s=fragility[LIMIT_STATES_DATABASE[ls + 1] + ':dispersion'],
                                                     scale=fragility[LIMIT_STATES_DATABASE[ls + 1] + ':median'])

    if return_state_probs:
        return loss_state_prob

    # Calculate specific state probability | specific probability of state ls = probability of state ls -
    # probability of state (ls +1)
    for ls in range(0, len(LIMIT_STATES_DATABASE) - 1):
        loss_state_prob[:, ls] = loss_state_prob[:, ls] - loss_state_prob[:, ls + 1]

    # Fix values below zero (Sum of each row must remain exactly 1)
    loss_problems = (loss_state_prob < 0).any(axis=1)
    if loss_problems.sum() > 0:
        loss_state_prob[loss_state_prob < 0] = 0
        loss_state_prob[loss_problems] /= loss_state_prob[loss_problems].sum(axis=1).reshape(
            loss_state_prob[loss_problems].shape[0], 1)

    # Assign a state to each structures according to probabilities
    # Todo: check if ordered=True still returns right value for each bridge
    states = [np.random.choice(LIMIT_STATES_DATABASE, p=lp) for lp in loss_state_prob]
    return pd.Categorical(states, LIMIT_STATES_DATABASE, ordered=True)


def cost_calc(structures_limited, costs_model: BridgeConstructionCostModel,
              repair_ratios_models: BridgeRepairCostModel):
    structures_limited['Repair_Cost'] = 0

    costs = costs_model.dataframe
    repair_ratios = repair_ratios_models.dataframe
    required_classification = costs_model.required_classification
    # Get param columns indexes
    param_cols = costs.columns.str.startswith('param')
    for index, structure in structures_limited.iterrows():

        structure_state = structure['State']

        if structure_state != LIMIT_STATES_DATABASE[0]:
            # iloc is to make sure "class_cost" is pandas series. This is necessary for other steps.
            class_cost = costs[costs[required_classification] == structure[required_classification]].iloc[0, :]
            structures_limited.loc[index, 'Repair_Cost'] = get_random_value(
                'continuous', class_cost['distribution_name'], *class_cost.loc[param_cols]
            ) * repair_ratios[structure['State']][0]

            # if fragility_type == 'HAZUS':
            #     class_cost = costs[costs['BridgeClass'] == structure['hazus_class_id']]
            #     structures_limited.loc[index, 'Repair_Cost'] = np.random.uniform(low=class_cost['Min'],
            #                                                                      high=class_cost['Max']) * \
            #                                                    repair_ratios[structure['State']][0]
            # else:
            #     class_cost = costs[costs['BridgeClass'] == structure['custom_class']]
            #     structures_limited.loc[index, 'Repair_Cost'] = np.random.lognormal(mean=class_cost['Mu'],
            #                                                                        sigma=class_cost['Sigma']) * \
            #                                                    repair_ratios[structure['State']][0]

        # if structure_state != LIMIT_STATES[0] and structure_state != LIMIT_STATES[-1]:
        #     structures_limited.loc[index, 'Repair_Cost'] = np.random.uniform(low=class_cost['Min'],
        #                                                                   high=class_cost['Max'])*repair_ratios[structure['State']]
        # elif structure_state == LIMIT_STATES[-1]:
        #     if structure['Spannumber'] < 3:
        #         structures_limited.loc[index, 'Repair_Cost'] = np.random.uniform(low=class_cost['Min'],
        #                                                                       high=class_cost['Max'])
        #     else:
        #         structures_limited.loc[index, 'Repair_Cost'] = np.random.uniform(low=class_cost['Min'],
        #                                                                           high=class_cost['Max'])*2/structure['Spannumber']

    structures_limited['Repair_Cost'] = structures_limited['Repair_Cost'] * structures_limited[
        'structure_len'] * structures_limited['deck_width']

    return structures_limited['Repair_Cost'].values


# Todo: Make a general way of dealing with units
# Todo: If converter remains, take care of other types than PGA,PGV and SA
def im_conversion_rate(gmm: str, im: str):
    im_convert_rate = 1
    if im[0].isdigit() or im == 'PGA':  # PGA or SA
        im_convert_rate = 1.0 / 980.665
    elif im == 'PGV':
        im_convert_rate = 10

    return im_convert_rate
