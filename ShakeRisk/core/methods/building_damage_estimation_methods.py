import numpy as np
from scipy.stats import lognorm
import pandas as pd

from Definitions import LIMIT_STATES_DATABASE
from core.domain.entities.building.building_construction_cost_model import BuildingConstructionCostModel
from core.domain.entities.building.building_fragility_model import BuildingFragilityModel
from core.domain.entities.building.building_repair_cost_model import BuildingRepairCostModel
from core.methods.random_generation_methods import get_random_value


def state_prob(structures_limited, fragility_model: BuildingFragilityModel, im_convert_rate, return_state_probs=False):
    fragility_values = fragility_model.dataframe
    required_classification = fragility_model.required_classification
    loss_state_prob = np.ones((len(structures_limited), len(LIMIT_STATES_DATABASE)))

    for i, (index, structure) in enumerate(structures_limited.iterrows()):

        # if fragility_type == 'HAZUS':
        #     fragility = fragility_values[fragility_values['HAZUS Class', 'HAZUS Class'] == structure['hazus_class_id']]
        # else:
        #     fragility = fragility_values[
        #         fragility_values['Custom Class', 'Custom Class'] == structure['custom_class']]

        fragility = fragility_values[
            fragility_values[required_classification] == structure[required_classification]]

        for ls in range(0, len(LIMIT_STATES_DATABASE) - 1):
            loss_state_prob[i, ls + 1] = lognorm.cdf(structure['im'] * im_convert_rate,
                                                     s=fragility[LIMIT_STATES_DATABASE[ls + 1] + ':dispersion'],
                                                     scale=fragility[LIMIT_STATES_DATABASE[ls + 1] + ':median'])

    if return_state_probs:
        return loss_state_prob

    # Calculate specific state probability | specific probability of state ls = probability of state ls -
    # probability of state (ls +1)
    for ls in range(0, len(LIMIT_STATES_DATABASE) - 1):
        loss_state_prob[:, ls] = loss_state_prob[:, ls] - loss_state_prob[:, ls + 1]

    # Fix values below zero (Sum of each row must remain exactly 1)
    loss_problems = (loss_state_prob < 0).any(axis=1)
    if loss_problems.sum() > 0:
        loss_state_prob[loss_state_prob < 0] = 0
        loss_state_prob[loss_problems] /= loss_state_prob[loss_problems].sum(axis=1).reshape(
            loss_state_prob[loss_problems].shape[0], 1)

    # Assign a state to each structures according to probabilities
    # Todo: check if ordered=True still returns right value for each building
    states = [np.random.choice(LIMIT_STATES_DATABASE, p=lp) for lp in loss_state_prob]
    return pd.Categorical(states, LIMIT_STATES_DATABASE, ordered=True)

    # elif structure_type == 'building':
    #
    #     if fragility_type == 'HAZUS':
    #         loss_state_prob = np.ones(
    #             (len(structures_limited), len(fragility_values['HAZUS Class']), len(LIMIT_STATES)))
    #
    #         for i, (index, structure) in enumerate(structures_limited.iterrows()):
    #             for j, f_class in enumerate(fragility_values['HAZUS Class', 'HAZUS Class']):
    #
    #                 fragility = fragility_values[fragility_values['HAZUS Class', 'HAZUS Class'] == f_class]
    #
    #                 for ls in range(0, len(LIMIT_STATES) - 1):
    #                     loss_state_prob[i, j, ls + 1] = lognorm.cdf(structure['im'] * im_convert_rate,
    #                                                                 s=fragility[LIMIT_STATES[ls + 1], 'dispersion'],
    #                                                                 scale=fragility[LIMIT_STATES[ls + 1], 'Median'])
    #
    #         if return_state_probs:
    #             return loss_state_prob
    #
    #         # Calculate specific state probability | specific probability of state ls = probability of state ls -
    #         # probability of state (ls +1)
    #         for ls in range(0, len(LIMIT_STATES) - 1):
    #             loss_state_prob[:, :, ls] = loss_state_prob[:, :, ls] - loss_state_prob[:, :, ls + 1]
    #
    #         # Fix values below zero (Sum of each row must remain exactly 1)
    #         loss_problems = (loss_state_prob < 0).any(axis=2)
    #         if loss_problems.sum() > 0:
    #             loss_state_prob[loss_state_prob < 0] = 0
    #             loss_state_prob[loss_problems] /= loss_state_prob[loss_problems].sum(axis=1).reshape(
    #                 loss_state_prob[loss_problems].shape[0], 1)
    #
    #
    #         # Assign a state to each structures according to probabilities
    #         # for structure in loss_state_prob:
    #         #     for f_class in structure:
    #         #         states = np.random.choice(LIMIT_STATES, p=f_class)
    #         states = [[np.random.choice(LIMIT_STATES, p=lp) for lp in struct] for struct in loss_state_prob]
    #         return [pd.Categorical(state, LIMIT_STATES, ordered=True) for state in np.array(states).T]
    #
    #     else:
    #         raise NotImplementedError()

    # else:
    #     raise NotImplementedError('State probability calculation is not yet implemented for structure type of {}'
    #                               .format(structure_type))


def cost_calc(structures_limited, costs_model: BuildingConstructionCostModel,
              repair_ratios_models: BuildingRepairCostModel):
    structures_limited['Repair_Cost'] = 0

    repair_ratios = repair_ratios_models.dataframe
    required_classification = costs_model.required_classification

    for index, structure in structures_limited.iterrows():
        if structure['State'] != LIMIT_STATES_DATABASE[0]:
            structures_limited.loc[index, 'Repair_Cost'] = repair_ratios[structure['State']][0] * structure['cost']
        # if fragility_type == 'HAZUS':
        #     structures_limited.loc[index, 'Repair_Cost'] = repair_ratios[structure['State']][0] * structure['Cost']
        # else:
        #     raise NotImplementedError

    return structures_limited['Repair_Cost'].values


# Todo: Make a general way of dealing with units
# Todo: If converter remains, take care of other types than PGA,PGV and SA
def im_conversion_rate(gmm: str, im: str):
    im_convert_rate = 1
    if im[0].isdigit() or im == 'PGA':  # PGA or SA
        im_convert_rate = 1.0 / 980.665
    elif im == 'PGV':
        im_convert_rate = 10

    return im_convert_rate
