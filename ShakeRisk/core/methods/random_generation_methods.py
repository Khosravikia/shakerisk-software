import numpy as np


def get_random_value(r_type, r_name, *params, size=None):
    available_methods = {
        'discrete': {
            'uniform': discrete_uniform,
            'possion': discrete_possion
        },
        'continuous': {
            'uniform': continuous_uniform,
            'normal': continuous_normal,
            'lognormal': continuous_lognormal,
            'exponential': continuous_exponential

        }
    }

    return available_methods[r_type][r_name](*params, size=size)


# Params: minimum, maximum, n
def discrete_uniform(*params, size=None):
    if size is None:
        return np.random.choice(np.linspace(*params))
    else:
        raise NotImplementedError('Size is not yet implemented for discrete uniform random value.')


# Params: lam
def discrete_possion(*params, size=None):
    return np.random.poisson(params[0], size=size)


# Params: minimum, maximum
def continuous_uniform(*params, size=None):
    return np.random.uniform(params[0], params[1], size=size)


# Params: mean, sigma
def continuous_normal(*params, size=None):
    return np.random.normal(params[0], params[1], size=size)


# Params: mean, sigma
def continuous_lognormal(*params, size=None):
    return np.random.lognormal(params[0], params[1], size=size)


# Params: scale
def continuous_exponential(*params, size=None):
    return np.random.exponential(params[0], size=size)
