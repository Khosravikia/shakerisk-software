import numpy as np

# Converting to float is done to make sure a single value is returned. (Not a pandas or numpy array with a single element)
# This is essential for later json serialization.


def rmse(true_value, predicted_value):
    return float(((true_value - predicted_value) ** 2).sum() / len(predicted_value))


def r2(true_value, predicted_value):
    sse = np.sum((true_value - predicted_value) ** 2)
    tse = (len(true_value) - 1) * np.var(true_value, ddof=1)
    r2_score = 1 - (sse / tse)
    return float(r2_score)


def error_mean(true_value, predicted_value):
    return float(np.mean(np.abs(true_value - predicted_value)))


def error_std(true_value, predicted_value):
    return float(np.std(np.abs(true_value - predicted_value)))


available_evaluations = {
        'rmse': rmse,
        'r2': r2,
        'error_mean': error_mean,
        'error_std': error_std
}


def evaluate(evaluation_type: str, true_value, predicted_value):
    return available_evaluations[evaluation_type](true_value, predicted_value)
