import numpy as np


class GeoDistance:

    @property
    def available_distances(self):
        return {
            'repi': self.great_distance,
            'rhypo': self.great_distance_2d
        }

    def __init__(self, lat1, lng1, lat2=None, lng2=None, height=0):
        self.lat1 = lat1
        self.lng1 = lng1
        self.lat2 = lat2
        self.lng2 = lng2
        self.height = height

    def great_distance(self):
        lat1 = np.deg2rad(self.lat1)
        lng1 = np.deg2rad(self.lng1)
        lat2 = np.deg2rad(self.lat2)
        lng2 = np.deg2rad(self.lng2)

        diff_lat = lat1 - lat2
        diff_lng = lng1 - lng2
        d = np.sin(diff_lat / 2) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(diff_lng / 2) ** 2
        return 2 * 6371 * np.arcsin(np.sqrt(d))

    def great_distance_2d(self):
        return np.sqrt(np.power(self.great_distance(), 2) + np.power(self.height, 2))

    def great_distance_matrix(self):
        num_points = len(self.lat1)
        distance_matrix = np.zeros((num_points, num_points))
        for i in range(0, num_points):
            for j in range(i, num_points):
                distance_matrix[i, j:] = distance_matrix[j:, i] = \
                    GeoDistance(self.lat1[i], self.lng1[i], self.lat1[j:], self.lng1[j:]).great_distance()
            # print('\r {} / {}'.format(i, num_points), end='')
        return distance_matrix

    def store_distances(self, required_distances: list):
        available_distances = {
            'rhypo': self.great_distance_2d,
            'repi': self.great_distance
        }
        for dis in required_distances:
            setattr(self, dis, self.available_distances[dis]())

        return self


class AssignValueByGeoDistance:

    def __init__(self, lat_source, long_source, lat_target, long_target, values, deg_ste=0.02, source_indexes=None):
        if source_indexes is None:
            source_indexes = np.arange(len(lat_source))
        self.source = np.array([lat_source, long_source, values, source_indexes]).T
        self.target = np.array([lat_target, long_target]).T
        self.deg_step = deg_ste

    def limit_source_by_degree(self, center_point, deg_steps):
        return self.source[(self.source[:, 0] < center_point[0] + deg_steps) &
                           (self.source[:, 0] > center_point[0] - deg_steps) &
                           (self.source[:, 1] < center_point[1] + deg_steps) &
                           (self.source[:, 1] > center_point[1] - deg_steps)]

    def nearest(self, deg_step=None, return_indexes=False):
        if deg_step:
            self.deg_step = deg_step

        assigned_values = np.zeros(len(self.target))

        for i, row in enumerate(self.target):
            deg_steps = self.deg_step
            source_limited = self.limit_source_by_degree(row, deg_steps)

            while len(source_limited) == 0:
                deg_steps += deg_steps
                source_limited = self.limit_source_by_degree(row, deg_steps)

            distances = GeoDistance(source_limited[:, 0], source_limited[:, 1], row[0], row[1]).great_distance()

            if return_indexes:
                assigned_values[i] = source_limited[distances.argmin(), 3]
            else:
                assigned_values[i] = source_limited[distances.argmin(), 2]

        return assigned_values


if __name__ == '__main__':
    import pandas as pd
    from core.methods.geo_distance_methods import AssignValueByGeoDistance

    source = pd.read_csv('C:\\Users\\UR\\Desktop\\Shaking_Level.csv')
    target = pd.read_csv('C:\\Users\\UR\\Desktop\\damage_estimation.csv')

    result = AssignValueByGeoDistance(source['Latitude (deg)'], source['Longitude (deg)'], target['Latitude'],
                                      target['Longitude'],
                                      source['PGA']).nearest()

    print(result)

    from core.methods.geo_distance_methods import GeoDistance

    vs = pd.read_pickle('../../data/vs30s/TX.pkl')
    result = GeoDistance(vs['Latitude (deg)'].values, vs['Longitude (deg)'].values).great_distance_matrix()
