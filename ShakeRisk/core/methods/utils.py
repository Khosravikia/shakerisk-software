import time


class Timing:
    def __init__(self, task_name=None):
        self.start_time = time.perf_counter()
        self.previous_time = self.start_time
        self.task_name = task_name
        if task_name is not None:
            print("*{} started...".format(task_name))

    def set_previous_time(self, previous_time=None):
        if previous_time:
            self.previous_time = previous_time
        else:
            self.previous_time = time.perf_counter()

    def log(self, task_name="Elapse Time"):
        log_time = time.perf_counter()
        print("{} : {:.2f}(s)".format(task_name, log_time - self.previous_time))
        self.previous_time = log_time

    def log_end(self):
        log_time = time.perf_counter()
        print("*{} Total Time  : {:.2f}(s)".format(self.task_name, log_time - self.start_time))
        self.previous_time = log_time
