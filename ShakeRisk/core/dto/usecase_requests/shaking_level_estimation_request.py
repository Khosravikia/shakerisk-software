import warnings

from core.domain.entities.earthquake import Earthquake
from core.domain.entities.intensity_model import IntensityModel
from core.domain.entities.vs30 import Vs30
from core.dto.usecase_requests import request_object as req


class ShakingLevelEstimationRequest(req.ValidRequestObject):

    def __init__(self, name,  deg_limit: float, earthquake: Earthquake, vs30: Vs30, intensity_model: IntensityModel,):
        self.name = name
        self.deg_limit = deg_limit
        self.earthquake = earthquake
        self.vs30 = vs30
        self.intensity_model = intensity_model

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if not bool(adict):
            return invalid_req.add_error('Request object', 'cannot be empty')

        return ShakingLevelEstimationRequest(name=adict['name'],
                                             deg_limit=adict['deg_limit'],
                                             earthquake=adict['earthquake'],
                                             vs30=adict['vs30'],
                                             intensity_model=adict['intensity_model'])
