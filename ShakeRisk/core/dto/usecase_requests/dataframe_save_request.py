from core.dto.usecase_requests import request_object as req

from core.domain.entities.parameters.daraframe import DataFrame


class DataFrameSaveRequest(req.ValidRequestObject):

    def __init__(self, name,  dataframe: DataFrame, output_path: str):
        self.name = name
        self.dataframe = dataframe
        self.output_path = output_path

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if not bool(adict):
            return invalid_req.add_error('Request Object', 'cannot be empty')

        return DataFrameSaveRequest(name=adict['name'],
                                    dataframe=adict['dataframe'],
                                    output_path=adict['output_path'])
