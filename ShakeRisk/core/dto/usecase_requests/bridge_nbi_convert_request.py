from core.dto.usecase_requests import request_object as req


class BridgeNbiConvertRequest(req.ValidRequestObject):

    def __init__(self, name,  source, on_system: bool, non_culvert: bool, state=None, year=None, dataframe=None):
        self.name = name
        self.source = source
        self.state = state
        self.year = year
        self.dataframe = dataframe
        self.on_system = on_system
        self.non_culvert = non_culvert


    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if not bool(adict):
            return invalid_req.add_error('Request Object', 'cannot be empty')

        return BridgeNbiConvertRequest(name=adict['name'],
                                       source=adict['source'],
                                       state=adict['state'],
                                       year=adict['year'],
                                       dataframe=adict['dataframe'],
                                       on_system=adict['on_system'],
                                       non_culvert=adict['non_culvert'])
