from core.dto.usecase_requests import request_object as req
from core.domain.entities.parameters.loss_function import LossFunction


class MonteCarloSimulationRequest(req.ValidRequestObject):

    def __init__(self, name,  function: LossFunction, param1, param2):
        self.name = name
        self.function = function
        self.param1 = param1
        self.param2 = param2

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if not bool(adict):
            return invalid_req.add_error('Request object', 'cannot be empty')

        return MonteCarloSimulationRequest(name=adict['name'],
                                           function=adict['function'],
                                           param1=adict['param1'],
                                           param2=adict['param2'])
