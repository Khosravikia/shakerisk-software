import warnings

from core.domain.entities.earthquake import Earthquake
from core.domain.entities.intensity_model import IntensityModel
from core.domain.entities.bridge.bridge import Bridge
from core.domain.entities.bridge.bridge_fragility_model import BridgeFragilityModel
from core.domain.entities.bridge.bridge_construction_cost_model import BridgeConstructionCostModel
from core.domain.entities.bridge.bridge_repair_cost_model import BridgeRepairCostModel
from core.domain.entities.bridge.bridge_restoration_model import BridgeRestorationModel
from core.domain.entities.network.transportation_network import TransportationNetwork
from core.domain.entities.network.sytstem_model_frank_wolfe import SystemModelFrankWolfe
from core.domain.entities.vs30 import Vs30
from core.dto.usecase_requests import request_object as req


class NetworkDamageEstimationRequest(req.ValidRequestObject):

    def __init__(self, name, deg_limit: float, transportation_network: TransportationNetwork,
                 network_model: SystemModelFrankWolfe, cost_per_hour: float, earthquake: Earthquake, vs30: Vs30,
                 bridge: Bridge, construction_cost: BridgeConstructionCostModel, intensity_model: IntensityModel,
                 fragility_model: BridgeFragilityModel, repair_cost_model: BridgeRepairCostModel,
                 restoration_model: BridgeRestorationModel):
        self.name = name
        self.transportation_network = transportation_network
        self.network_model = network_model
        self.cost_per_hour = cost_per_hour
        self.deg_limit = deg_limit
        self.earthquake = earthquake
        self.vs30 = vs30
        self.bridge = bridge
        self.construction_cost = construction_cost
        self.intensity_model = intensity_model
        self.fragility_model = fragility_model
        self.repair_cost_model = repair_cost_model
        self.restoration_model = restoration_model

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if not bool(adict):
            return invalid_req.add_error('Request object', 'cannot be empty')

        if adict['fragility_model'].measure != adict['intensity_model'].measure:
            invalid_req.add_error(
                'Fragility model', 'Intensity measure of intensity model and fragility model cannot be different')
            return invalid_req

        # if adict['fragility_model'].fragility_type == 'HAZUS' and adict['intensity_model'].measure != 'PGA':
        #     adict['intensity_model'].measure = 'PGA'
        #     warnings.warn("Changing fragility model to PGA, because HAZUS only supports PGA.")

        return NetworkDamageEstimationRequest(name=adict['name'],
                                              transportation_network=adict['transportation_network'],
                                              deg_limit=adict['deg_limit'],
                                              network_model=adict['network_model'],
                                              cost_per_hour=adict['cost_per_hour'],
                                              earthquake=adict['earthquake'],
                                              vs30=adict['vs30'],
                                              bridge=adict['bridge'],
                                              construction_cost=adict['construction_cost'],
                                              intensity_model=adict['intensity_model'],
                                              fragility_model=adict['fragility_model'],
                                              repair_cost_model=adict['repair_cost_model'],
                                              restoration_model=adict['restoration_model'])
