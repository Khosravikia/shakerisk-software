import warnings

from core.domain.entities.building.building_construction_cost_model import BuildingConstructionCostModel
from core.domain.entities.earthquake import Earthquake
from core.domain.entities.intensity_model import IntensityModel
from core.domain.entities.building.building_fragility_model import BuildingFragilityModel
from core.domain.entities.building.building_repair_cost_model import BuildingRepairCostModel
from core.domain.entities.building.building import Building
from core.domain.entities.vs30 import Vs30
from core.dto.usecase_requests import request_object as req


class BuildingDamageEstimationRequest(req.ValidRequestObject):

    def __init__(self, name, deg_limit: float, earthquake: Earthquake, vs30: Vs30, building: Building,
                 construction_cost: BuildingConstructionCostModel, intensity_model: IntensityModel,
                 fragility_model: BuildingFragilityModel, repair_cost_model: BuildingRepairCostModel):
        self.name = name
        self.deg_limit = deg_limit
        self.earthquake = earthquake
        self.vs30 = vs30
        self.building = building
        self.construction_cost = construction_cost
        self.intensity_model = intensity_model
        self.fragility_model = fragility_model
        self.repair_cost_model = repair_cost_model

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if not bool(adict):
            return invalid_req.add_error('Request object', 'cannot be empty')

        # if adict['fragility_model'].fragility_type == 'HAZUS' and adict['intensity_model'].measure != 'PGA':
        #     adict['intensity_model'].measure = 'PGA'
        #     warnings.warn("Changing fragility model to PGA, because HAZUS only supports PGA.")

        if adict['fragility_model'].measure != adict['intensity_model'].measure:
            invalid_req.add_error(
                'Fragility model', 'Intensity measure of intensity model and fragility model cannot be different.')
            return invalid_req

        return BuildingDamageEstimationRequest(name=adict['name'],
                                               deg_limit=adict['deg_limit'],
                                               earthquake=adict['earthquake'],
                                               vs30=adict['vs30'],
                                               building=adict['building'],
                                               construction_cost=adict['construction_cost'],
                                               intensity_model=adict['intensity_model'],
                                               fragility_model=adict['fragility_model'],
                                               repair_cost_model=adict['repair_cost_model'])
