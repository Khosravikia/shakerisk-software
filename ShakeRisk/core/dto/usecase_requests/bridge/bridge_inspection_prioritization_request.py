import warnings

from core.domain.entities.earthquake import Earthquake
from core.domain.entities.intensity_model import IntensityModel
from core.domain.entities.bridge.bridge_fragility_model import BridgeFragilityModel
from core.domain.entities.bridge.bridge import Bridge
from core.domain.entities.vs30 import Vs30
from core.dto.usecase_requests import request_object as req


class BridgeInspectionPrioritizationRequest(req.ValidRequestObject):

    def __init__(self, name, state_name: str, state_min_prob: float, deg_limit: float, earthquake: Earthquake,
                 vs30: Vs30, bridge: Bridge, intensity_model: IntensityModel, fragility_model: BridgeFragilityModel):
        self.name = name
        self.state_name = state_name
        self.state_min_prob = state_min_prob
        self.deg_limit = deg_limit
        self.earthquake = earthquake
        self.vs30 = vs30
        self.bridge = bridge
        self.intensity_model = intensity_model
        self.fragility_model = fragility_model

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if not bool(adict):
            return invalid_req.add_error('Request object', 'cannot be empty')

        # if adict['fragility_model'].fragility_type == 'HAZUS' and adict['intensity_model'].measure != 'PGA':
        #     adict['intensity_model'].measure = 'PGA'
        #     warnings.warn("Changing fragility model to PGA, because HAZUS only supports PGA.")

        if adict['fragility_model'].measure != adict['intensity_model'].measure:
            invalid_req.add_error(
                'Fragility model', 'Intensity measure of intensity model and fragility model cannot be different')
            return invalid_req

        return BridgeInspectionPrioritizationRequest(name=adict['name'],
                                                     state_name=adict['state_name'],
                                                     state_min_prob=adict['state_min_prob'],
                                                     deg_limit=adict['deg_limit'],
                                                     earthquake=adict['earthquake'],
                                                     vs30=adict['vs30'],
                                                     bridge=adict['bridge'],
                                                     intensity_model=adict['intensity_model'],
                                                     fragility_model=adict['fragility_model'])
