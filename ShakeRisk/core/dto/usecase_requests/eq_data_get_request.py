from core.dto.usecase_requests import request_object as req
from core.domain.entities.time_interval import TimeInterval
from core.domain.entities.parameters.location.rectangular_area import RectangularArea


class EqDataGetRequest(req.ValidRequestObject):

    def __init__(self, name,  min_magnitude: float, time_interval: TimeInterval, area: RectangularArea):
        self.name = name
        self.min_magnitude = min_magnitude
        self.time_interval = time_interval
        self.area = area

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if not bool(adict):
            return invalid_req.add_error('Request Object', 'cannot be empty')

        return EqDataGetRequest(name=adict['name'],
                                min_magnitude=adict['min_magnitude'],
                                time_interval=adict['time_interval'],
                                area=adict['area'])
