

from core.dto.usecase_requests import request_object as req
from core.domain.entities.parameters.daraframe import DataFrame


class MLBasedModelRequest(req.ValidRequestObject):

    # ML model option objects are defined at core/dto/gateway_dtos/ml_model_options
    def __init__(self, name,  dataframe: DataFrame, input_columns: list, output_columns: list, train_fraction: float,
                 pre_processing: str, output_transformation: str, ml_model_options: object, use_mixed_effects: bool, me_columns: str,):
        self.name = name
        self.dataframe = dataframe
        self.input_columns = input_columns
        self.output_columns = output_columns
        self.train_fraction = train_fraction
        self.pre_processing = pre_processing
        self.output_transformation = output_transformation
        self.ml_model_options = ml_model_options
        self.use_mixed_effects = use_mixed_effects
        self.me_columns = me_columns

    @classmethod
    def from_dict(cls, adict):
        invalid_req = req.InvalidRequestObject()

        if not bool(adict):
            return invalid_req.add_error('Request object', 'cannot be empty')

        me_columns = ""
        if adict["use_mixed_effects"]:
            if len(adict["me_columns"]):
                me_columns = [x.strip() for x in adict['me_columns'].split(',')]
            else:
                return invalid_req.add_error('ME columns', 'cannot be empty when "use mixed effects" is selected')

        input_cols = [x.strip() for x in adict['input_columns'].split(',')]
        output_cols = [x.strip() for x in adict['output_columns'].split(',')]
        print(input_cols)
        return MLBasedModelRequest(name=adict['name'],
                                   dataframe=adict['dataframe'],
                                   input_columns=input_cols,
                                   output_columns=output_cols,
                                   train_fraction=adict['train_fraction'],
                                   pre_processing=adict['pre_processing'],
                                   output_transformation=adict['output_transformation'],
                                   ml_model_options=adict['ml_model_options'],
                                   use_mixed_effects=adict['use_mixed_effects'],
                                   me_columns=me_columns
                                   )
