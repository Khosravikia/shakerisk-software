from core.dto.usecase_responses import response_object as res


class MonteCarloSimulationResponse(res.ResponseSuccess):

    def __init__(self, result):
        super().__init__()
        self.result = result


