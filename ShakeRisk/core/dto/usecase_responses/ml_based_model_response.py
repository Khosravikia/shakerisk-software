from core.dto.usecase_responses import response_object as res
from core.dto.gateway_dtos.ml_model_options.ml_model_additional_info import MLModelAdditionalInfo


class MLBasedModelResponse(res.ResponseSuccess):

    def __init__(self, model, additional_info: MLModelAdditionalInfo, evaluations, model_object_type,
                 random_effect_values=None):
        super().__init__()
        self.model = model
        self.additional_info = additional_info
        self.evaluations = evaluations
        self.model_object_type = model_object_type
        self.random_effect_values = random_effect_values


