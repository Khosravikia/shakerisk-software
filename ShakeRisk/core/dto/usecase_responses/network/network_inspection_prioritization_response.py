from core.dto.usecase_responses import response_object as res


class NetworkInspectionPrioritizationResponse(res.ResponseSuccess):

    def __init__(self, recovery_data):
        super().__init__()
        self.recovery_data = recovery_data
