from core.dto.usecase_responses import response_object as res


class NetworkDamageEstimationResponse(res.ResponseSuccess):

    def __init__(self, bridge_data, tstts_before, tstts_after, indirect_cost, net, broken_links, shaking_levels):
        super().__init__()
        self.bridge_data = bridge_data
        self.tstts_before = tstts_before
        self.tstts_after = tstts_after
        self.indirect_cost = indirect_cost
        self.net = net
        self.broken_links = broken_links
        self.shaking_levels = shaking_levels
