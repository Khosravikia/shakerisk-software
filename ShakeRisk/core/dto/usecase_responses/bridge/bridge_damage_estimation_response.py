from core.dto.usecase_responses import response_object as res


class BridgeDamageEstimationResponse(res.ResponseSuccess):

    def __init__(self, damage_estimations, shaking_levels, required_classifications=None):
        super().__init__()
        self.damage_estimations = damage_estimations
        self.shaking_levels = shaking_levels
        self.required_classifications = required_classifications
