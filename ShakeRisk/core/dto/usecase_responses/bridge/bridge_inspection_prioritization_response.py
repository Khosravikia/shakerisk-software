from core.dto.usecase_responses import response_object as res


class BridgeInspectionPrioritizationResponse(res.ResponseSuccess):

    def __init__(self, inspections, shaking_levels):
        super().__init__()
        self.inspections = inspections
        self.shaking_levels = shaking_levels
