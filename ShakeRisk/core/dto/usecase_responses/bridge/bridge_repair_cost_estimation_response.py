from core.dto.usecase_responses import response_object as res


class BridgeRepairCostEstimationResponse(res.ResponseSuccess):

    def __init__(self, scenarios, required_classifications):
        super().__init__()
        self.scenarios = scenarios
        self.required_classifications = required_classifications
