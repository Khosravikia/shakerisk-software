from core.dto.usecase_responses import response_object as res


class BuildingInspectionPrioritizationResponse(res.ResponseSuccess):

    def __init__(self, inspections, shaking_levels, required_classifications):
        super().__init__()
        self.inspections = inspections
        self.shaking_levels = shaking_levels
        self.required_classifications = required_classifications
