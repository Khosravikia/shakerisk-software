from core.dto.usecase_responses import response_object as res


class BuildingRepairCostEstimationResponse(res.ResponseSuccess):

    def __init__(self, values):
        super().__init__(values)
