from core.domain import base_entity as be


class RandomForestOptions(be.BaseEntity):

    def __init__(self, model_name: str, n_estimators: int, bootstrap: bool, max_samples: float,
                 max_features: str = None, criterion: str = None, min_samples_split: int = None,
                 min_samples_leaf: int = None
                 # max_depth: int = None,
                 ):
        self.n_estimators = n_estimators
        self.bootstrap = bootstrap
        self.max_features = max_features
        self.max_samples = max_samples
        self.criterion = criterion
        # self.max_depth = max_depth
        self.min_samples_split = min_samples_split
        self.min_samples_leaf = min_samples_leaf
        self.model_name = model_name

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('DTO Object', ' cannot be empty')
            return invalid_entity

        # max_features supports string,int, float, and None
        max_features = None
        if isinstance(adict['max_features'], str):
            if len(adict['max_features']):
                if adict['max_features'].replace('.', '', 1).replace('-', '', 1).isdigit():
                    if "." in adict['max_features']:
                        max_features = float(adict['max_features'])
                    else:
                        max_features = int(adict['max_features'])
                else:
                    max_features = adict['max_features']
        else:  # if max_features arrives as int or float directly
            max_features = adict['max_features']

        return RandomForestOptions(model_name=adict['model_name'],
                                   n_estimators=adict['n_estimators'],
                                   bootstrap=adict['bootstrap'],
                                   max_features=max_features,
                                   max_samples=adict['max_samples'],
                                   criterion=adict['criterion'],
                                   # max_depth=adict['max_depth'],
                                   min_samples_split=adict['min_samples_split'],
                                   min_samples_leaf=adict['min_samples_leaf']
                                   )
