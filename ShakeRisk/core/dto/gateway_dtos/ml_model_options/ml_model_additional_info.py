import numpy as np
from core.domain import base_entity as be
from Definitions import ML_MODEL_TYPE_DICT


class MLModelAdditionalInfo(be.BaseEntity):

    class PreProcessingInfo:
        def __init__(self, pre_process_type: str, args):
            self.type: str = pre_process_type
            self.args = args

    class OutputTransformation:
        def __init__(self, transformation_type: str):
            self.type = transformation_type
    
    class RandomEffect:
        def __init__(self, std: float = None):
            self.std = std
    # class TrainDataResult:
    #     def __init__(self, rmse: float = None, r2: float = None, error_mean: float = None,
    #                  error_std: float = None):
    #         self.rmse = rmse
    #         self.r2 = r2
    #         self.error_mean = error_mean
    #         self.error_std = error_std
    #
    # class TestDataResult:
    #     def __init__(self, rmse: float = None, r2: float = None, error_mean: float = None,
    #                  error_std: float = None):
    #         self.rmse = rmse
    #         self.r2 = r2
    #         self.error_mean = error_mean
    #         self.error_std = error_std

    def __init__(self, pre_process_type: str, pre_process_args, output_transformation_type,
                 model_type: str, random_effect_std: float = None
                 # train_data_result: dict, test_data_result: dict
                 ):
        self.pre_processing = self.PreProcessingInfo(pre_process_type, pre_process_args)
        self.output_transformation = self.OutputTransformation(output_transformation_type)
        self.model_type = model_type
        self.random_effect = self.RandomEffect(random_effect_std)
        # self.train_data_result = self.TrainDataResult(**train_data_result)
        # self.test_data_result = self.TestDataResult(**test_data_result)

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('DTO Object', ' cannot be empty')
            return invalid_entity

        return MLModelAdditionalInfo(pre_process_type=adict['pre_processing']['type'],
                                     pre_process_args=[np.array(x) for x in adict['pre_processing']['args']],
                                     output_transformation_type=adict['output_transformation']['type'],
                                     model_type=ML_MODEL_TYPE_DICT[adict['model_type']],
                                     random_effect_std=adict['random_effect']['std']
                                     # train_data_result=adict['train_data_result'],
                                     # test_data_result=adict['test_data_result']
                                     )

    def to_dict(self):
        return {
            'pre_processing': {
                'type': self.pre_processing.type,
                'args': np.asarray(self.pre_processing.args).tolist() or []
            },
            'output_transformation': {
                'type': self.output_transformation.type
            },
            'model_type': ML_MODEL_TYPE_DICT[self.model_type],
            'random_effect': {
                'std': self.random_effect.std
            }
            # 'train_data_result': {
            #     'rmse': self.train_data_result.rmse,
            #     'r2': self.train_data_result.r2,
            #     'error_mean': self.train_data_result.error_mean,
            #     'error_std': self.train_data_result.error_std,
            # },
            # 'test_data_result': {
            #     'rmse': self.test_data_result.rmse,
            #     'r2': self.test_data_result.r2,
            #     'error_mean': self.test_data_result.error_mean,
            #     'error_std': self.test_data_result.error_std,
            # }
        }
