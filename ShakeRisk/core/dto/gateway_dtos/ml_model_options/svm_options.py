from core.domain import base_entity as be


class SVMOptions(be.BaseEntity):

    # C: for ‘rbf’, ‘poly’ and ‘sigmoid’
    # gamma: auto, scale or float
    def __init__(self, model_name: str, kernel: str, gamma, c: float, epsilon: float):
        self.kernel = kernel
        self.gamma = gamma
        self.c = c
        self.epsilon = epsilon
        self.model_name = model_name

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('DTO Object', ' cannot be empty')
            return invalid_entity

        # gamma supports both string and float
        if isinstance(adict['gamma'], str) and adict['gamma'].replace('.', '', 1).replace('-', '', 1).isdigit():
            adict['gamma'] = float(adict['gamma'])

        return SVMOptions(model_name=adict['model_name'],
                          kernel=adict['kernel'],
                          gamma=adict['gamma'],
                          c=adict['c'],
                          epsilon=adict['epsilon'],
                          )
