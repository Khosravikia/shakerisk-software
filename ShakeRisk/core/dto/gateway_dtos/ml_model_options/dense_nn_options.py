from core.domain import base_entity as be


class DenseNNOptions(be.BaseEntity):

    def __init__(self, model_name: str, number_of_neurons: list, validation_fraction: str, learning_rate: float,
                 epochs_num: int, early_stop_patience: int, loss_function: str, input_activation_function: list,
                 # output_activation_function: str,
                 # metrics: str,
                 optimizer: str ):
        self.model_name = model_name
        self.number_of_neurons = number_of_neurons
        self.validation_fraction = validation_fraction
        self.learning_rate = learning_rate
        self.epochs_num = epochs_num
        self.early_stop_patience = early_stop_patience
        self.loss_function = loss_function
        self.input_activation_function = input_activation_function
        # self.output_activation_function = output_activation_function
        self.optimizer = optimizer
        # self.metrics = metrics

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('DTO Object', ' cannot be empty')
            return invalid_entity

        num_neurons = [int(x.strip()) for x in adict['number_of_neurons'].split(',') if x != '']
        input_activation_function = [x.strip() for x in adict['input_activation_function'].split(',') if x != '']

        early_stop_patience = None
        if adict['early_stop_patience']:
            early_stop_patience = adict['early_stop_patience']
        return DenseNNOptions(model_name=adict['model_name'],
                              number_of_neurons=num_neurons,
                              validation_fraction=adict['validation_fraction'],
                              learning_rate=adict['learning_rate'],
                              epochs_num=adict['epochs_num'],
                              early_stop_patience=early_stop_patience,
                              loss_function=adict['loss_function'],
                              input_activation_function=input_activation_function,
                              # output_activation_function=adict['output_activation_function'],
                              optimizer=adict['optimizer'],
                              # metrics=adict['metrics']
                              )
