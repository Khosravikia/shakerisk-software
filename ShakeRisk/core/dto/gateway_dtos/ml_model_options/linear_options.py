from core.domain import base_entity as be


class LinearOptions(be.BaseEntity):

    def __init__(self, model_name, fit_intercept):
        self.model_name = model_name
        self.fit_intercept = fit_intercept

    @classmethod
    def from_dict(cls, adict):
        invalid_entity = be.InvalidEntityObject()

        if not bool(adict):
            invalid_entity.add_error('DTO Object', ' cannot be empty')
            return invalid_entity

        return LinearOptions(model_name=adict['model_name'],
                             fit_intercept=adict['fit_intercept'])
