from core.methods.utils import Timing
from core.interfaces.usecase_interface import IUseCase
from core.dto.usecase_requests.bridge.bridge_inspection_prioritization_request import \
    BridgeInspectionPrioritizationRequest
from core.interfaces.gateways.repositories.vs30_repository_interface import Vs30RepositoryInterface
from core.interfaces.gateways.repositories.bridge.bridge_fragility_model_repository_interface import \
    BridgeFragilityRepositoryInterface
from core.interfaces.gateways.repositories.bridge.bridge_repository_interface import BridgeRepositoryInterface
from core.interfaces.gateways.gmm_loader_interface import GmmLoaderInterface
from core.interfaces.gateways.spatial_correlations_loader_interface import SpatialCorrelationLoaderInterface
from core.methods.geo_distance_methods import GeoDistance, AssignValueByGeoDistance
from core.methods.bridge_damage_estimation_methods import state_prob, im_conversion_rate
from core.dto.usecase_responses.bridge.bridge_inspection_prioritization_response import \
    BridgeInspectionPrioritizationResponse as ResSuccess
from Definitions import LIMIT_STATES, IM_PERIODS


class BridgeInspectionPrioritizationUseCase(IUseCase):

    def __init__(self, vs30_repository: Vs30RepositoryInterface,
                 fragility_repository: BridgeFragilityRepositoryInterface,
                 bridge_repository: BridgeRepositoryInterface,
                 gmm_loader: GmmLoaderInterface,
                 spatial_correlation_loader: SpatialCorrelationLoaderInterface):
        self._vs30_repository = vs30_repository
        self._fragility_repository = fragility_repository
        self._bridge_repository = bridge_repository
        self._gmm_loader = gmm_loader
        self._spatial_correlation_loader = spatial_correlation_loader

    def process_request(self, request: BridgeInspectionPrioritizationRequest):

        timing = Timing()
        # Calculate shaking levels for Vs30s
        vs30s_limited = self._vs30_repository.get(vs30=request.vs30,
                                                  center_lat=request.earthquake.location.latitude,
                                                  center_long=request.earthquake.location.longitude,
                                                  deg_limit=request.deg_limit).dataframe
        # Initialise GMM model
        gmm_model = self._gmm_loader(
            request.intensity_model.source,
            request.intensity_model.gmm_model_name,
            request.intensity_model.trained_model)

        # Get vs30 distances from earthquake according to model required distances
        distances = GeoDistance(vs30s_limited['latitude'],
                                vs30s_limited['longitude'],
                                request.earthquake.location.latitude,
                                request.earthquake.location.longitude,
                                request.earthquake.depth).store_distances(
            required_distances=gmm_model.REQUIRED_DISTANCES)

        corr_matrix = None
        if request.intensity_model.probabilistic:
            distance_matrix = GeoDistance(vs30s_limited['latitude'].values,
                                          vs30s_limited['longitude'].values).great_distance_matrix()

            corr_matrix = self._spatial_correlation_loader(
                request.intensity_model.spatial_correlation).correlation_matrix(
                distance_matrix, IM_PERIODS[request.intensity_model.measure], vs30_clustering=True)

        # Assign IM to vs30s
        gmm_model(vs30s_limited['vs30'].values,
                  request.earthquake.magnitude,
                  distances,
                  request.intensity_model.measure)
        vs30s_limited = vs30s_limited.assign(
            im=gmm_model.predict(request.intensity_model.probabilistic, corr_matrix))

        timing.log('Shaking Level')

        # Load models
        fragility_model = self._fragility_repository.get(request.fragility_model)
        required_classifications = {
            fragility_model.required_classification
        }

        timing.log('Load Models')

        # Load structures, remove unknown if custom fragility is selected and apply degree limit
        structures_limited = self._bridge_repository.get(bridge=request.bridge,
                                                         center_lat=request.earthquake.location.latitude,
                                                         center_long=request.earthquake.location.longitude,
                                                         deg_limit=request.deg_limit,
                                                         required_classifications=required_classifications
                                                         ).dataframe

        timing.log('Load Structures')
        print('Number of structure: {}'.format(len(structures_limited)))

        # %% Assign IM to structures
        structures_limited = structures_limited.assign(im=AssignValueByGeoDistance(vs30s_limited['latitude'],
                                                                                   vs30s_limited['longitude'],
                                                                                   structures_limited['latitude'],
                                                                                   structures_limited['longitude'],
                                                                                   vs30s_limited['im']).nearest())

        timing.log('Assign IM')
        # No need to Convert median to mu. scipy.stats.lognorm uses median
        # if request_object.fragility_type == 'HAZUS':
        #     # for i in range(1, len(LIMIT_STATES)):  # First(0) is "No Damage" so start from 1
        #     #     fragility_values[LIMIT_STATES[i], 'Median'] = np.log(fragility_values[LIMIT_STATES[i], 'Median'])
        #     pass
        # else:
        #     raise NotImplementedError('Requested fragility type is not yet implemented')

        loss_state_prob = state_prob(structures_limited, fragility_model,
                                     im_conversion_rate(request.intensity_model.gmm_model_name,
                                                        request.intensity_model.measure),
                                     return_state_probs=True)

        state_index = LIMIT_STATES.index(request.state_name)
        structure_limited = structures_limited.assign(StateProb=loss_state_prob[:, state_index])

        timing.log('Assign State Probability')

        return ResSuccess(inspections=
                          structure_limited[structure_limited['StateProb'] > request.state_min_prob],
                          shaking_levels=vs30s_limited)
