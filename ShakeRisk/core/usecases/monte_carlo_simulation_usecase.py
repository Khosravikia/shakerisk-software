from core.interfaces.usecase_interface import IUseCase
from core.dto.usecase_requests.monte_carlo_simulation_request import MonteCarloSimulationRequest
from core.dto.usecase_responses.monte_carlo_simulation_response import MonteCarloSimulationResponse as res


class MonteCarloSimulationUseCase(IUseCase):

    def __init__(self):
        pass

    def process_request(self, request_object: MonteCarloSimulationRequest) -> res:

        result = request_object.function.function(request_object.param1, request_object.param2)
        return res(result)
