import pandas as pd

from core.methods.utils import Timing
from core.interfaces.usecase_interface import IUseCase
from core.dto.usecase_requests.shaking_level_estimation_request import ShakingLevelEstimationRequest
from core.interfaces.gateways.repositories.vs30_repository_interface import Vs30RepositoryInterface
from core.interfaces.gateways.gmm_loader_interface import GmmLoaderInterface
from core.interfaces.gateways.spatial_correlations_loader_interface import SpatialCorrelationLoaderInterface
from core.dto.usecase_responses import response_object as res

from Definitions import IM_PERIODS
from core.methods.geo_distance_methods import GeoDistance


class ShakingLevelEstimationUseCase(IUseCase):

    def __init__(self, vs30_repository: Vs30RepositoryInterface, gmm_loader: GmmLoaderInterface,
                 spatial_correlation_loader: SpatialCorrelationLoaderInterface):
        self._vs30_repository = vs30_repository
        self._gmm_loader = gmm_loader
        self._spatial_correlation_loader = spatial_correlation_loader

    def process_request(self, request: ShakingLevelEstimationRequest):

        timing = Timing()

        vs30s_limited = self._vs30_repository.get(vs30=request.vs30,
                                                  center_lat=request.earthquake.location.latitude,
                                                  center_long=request.earthquake.location.longitude,
                                                  deg_limit=request.deg_limit).dataframe

        timing.log('ead Vs30s')
        print('Number of Vs30s: {}'.format(len(vs30s_limited)))

        # Initialise GMM model
        gmm_model = self._gmm_loader(
            request.intensity_model.source,
            request.intensity_model.gmm_model_name,
            request.intensity_model.trained_model)

        # Get vs30 distances from earthquake according to model required distances
        distances = GeoDistance(vs30s_limited['latitude'],
                                vs30s_limited['longitude'],
                                request.earthquake.location.latitude,
                                request.earthquake.location.longitude,
                                request.earthquake.depth).store_distances(
            required_distances=gmm_model.REQUIRED_DISTANCES)

        # rhypos = GeoDistance(vs30s_limited['latitude'], vs30s_limited['longitude'],
        #                      request_object.eq_lat, request_object.eq_long,
        #                      request_object.eq_depth).great_distance_2d()

        timing.log('Assign Distance')

        # if request_object.model == 'ANN':
        #     ann = ANN(vs30s_limited['vs30'].values.reshape(-1, 1), request_object.eq_mw,
        #               distances.values.reshape(-1, 1), request_object.im)
        #     result = ann.predict()

        corr_matrix = None
        if request.intensity_model.probabilistic:
            distance_matrix = GeoDistance(vs30s_limited['latitude'].values,
                                          vs30s_limited['longitude'].values).great_distance_matrix()

            timing.log('Distant Matrix')

            corr_matrix = self._spatial_correlation_loader(request.intensity_model.spatial_correlation).correlation_matrix(
                distance_matrix, IM_PERIODS[request.intensity_model.measure], vs30_clustering=True)

            timing.log('Corr Matrix')

        # Assign IM to vs30s
        gmm_model(vs30s_limited['vs30'].values,
                  request.earthquake.magnitude,
                  distances,
                  request.intensity_model.measure)

        result = gmm_model.predict(request.intensity_model.probabilistic, corr_matrix)

        # result = self._gmm_loader(request_object.model, vs30s_limited['vs30'].values.reshape(-1, 1),
        #                           request_object.eq_mw, rhypos.values.reshape(-1, 1), request_object.im)\
        #     .predict(request_object.model_probabilistic, corr_matrix)

        timing.log('Assign IM')

        output = pd.DataFrame()
        output['longitude'] = vs30s_limited['longitude']
        output['latitude'] = vs30s_limited['latitude']
        output[request.intensity_model.measure] = result

        return res.ResponseSuccess(output)
