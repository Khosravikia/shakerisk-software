import copy
import numpy as np

from core.methods.utils import Timing
from core.interfaces.usecase_interface import IUseCase
from core.dto.usecase_requests.network.network_damage_estimation_request import NetworkDamageEstimationRequest
from core.interfaces.gateways.repositories.vs30_repository_interface import Vs30RepositoryInterface
from core.interfaces.gateways.repositories.bridge.bridge_fragility_model_repository_interface import \
    BridgeFragilityRepositoryInterface
from core.interfaces.gateways.repositories.bridge.bridge_construction_cost_repository_interface import \
    BridgeConstructionCostRepositoryInterface
from core.interfaces.gateways.repositories.bridge.bridge_repair_cost_repository_interface import \
    BridgeRepairCostRepositoryInterface
from core.interfaces.gateways.repositories.bridge.bridge_repository_interface import BridgeRepositoryInterface
from core.interfaces.gateways.repositories.bridge.bridge_restoration_repository_interface import \
    BridgeRestorationRepositoryInterface
from core.interfaces.gateways.repositories.network.transportaion_network_repository_interface import \
    TransportationNetworkRepositoryInterface
from core.interfaces.gateways.gmm_loader_interface import GmmLoaderInterface
from core.interfaces.gateways.spatial_correlations_loader_interface import SpatialCorrelationLoaderInterface
from core.methods.geo_distance_methods import GeoDistance, AssignValueByGeoDistance
from core.methods.bridge_damage_estimation_methods import state_prob, cost_calc, im_conversion_rate
from core.dto.usecase_responses.network.network_damage_estimation_response import \
    NetworkDamageEstimationResponse as ResSuccess
from Definitions import IM_PERIODS, LIMIT_STATES, ROOT_DIR, LIMIT_STATES_DATABASE
# Todo: Make this conditional/ Also dependency rule
# from infrastructure.network.WF.network import Network
from infrastructure.network.correspondence import get_broken_links


class NetworkDamageEstimationUseCase(IUseCase):

    def __init__(self, vs30_repository: Vs30RepositoryInterface,
                 fragility_repository: BridgeFragilityRepositoryInterface,
                 bridge_repository: BridgeRepositoryInterface,
                 construction_cost_repository: BridgeConstructionCostRepositoryInterface,
                 repair_cost_repository: BridgeRepairCostRepositoryInterface,
                 restoration_repository: BridgeRestorationRepositoryInterface,
                 transportation_network_repository: TransportationNetworkRepositoryInterface,
                 gmm_loader: GmmLoaderInterface,
                 spatial_correlation_loader: SpatialCorrelationLoaderInterface
                 ):
        self._vs30_repository = vs30_repository
        self._fragility_repository = fragility_repository
        self._bridge_repository = bridge_repository
        self._construction_cost_repository = construction_cost_repository
        self._repair_cost_repository = repair_cost_repository
        self._restoration_repository = restoration_repository
        self._transportation_network_repository = transportation_network_repository
        self._gmm_loader = gmm_loader
        self._spatial_correlation_loader = spatial_correlation_loader

    def process_request(self, request: NetworkDamageEstimationRequest):

        timing = Timing()

        print("deg_limit: ", request.deg_limit)
        # Calculate shaking levels for Vs30s
        vs30s_limited = self._vs30_repository.get(vs30=request.vs30,
                                                  center_lat=request.earthquake.location.latitude,
                                                  center_long=request.earthquake.location.longitude,
                                                  deg_limit=request.deg_limit).dataframe
        # Initialise GMM model
        gmm_model = self._gmm_loader(
            request.intensity_model.source,
            request.intensity_model.gmm_model_name,
            request.intensity_model.trained_model)

        # Get vs30 distances from earthquake according to model required distances
        distances = GeoDistance(vs30s_limited['latitude'],
                                vs30s_limited['longitude'],
                                request.earthquake.location.latitude,
                                request.earthquake.location.longitude,
                                request.earthquake.depth).store_distances(
            required_distances=gmm_model.REQUIRED_DISTANCES)

        corr_matrix = None
        if request.intensity_model.probabilistic:
            distance_matrix = GeoDistance(vs30s_limited['latitude'].values,
                                          vs30s_limited['longitude'].values).great_distance_matrix()

            corr_matrix = self._spatial_correlation_loader(
                request.intensity_model.spatial_correlation).correlation_matrix(
                distance_matrix, IM_PERIODS[request.intensity_model.measure], vs30_clustering=True)

        # Assign IM to vs30s
        gmm_model(vs30s_limited['vs30'].values,
                  request.earthquake.magnitude,
                  distances,
                  request.intensity_model.measure)
        vs30s_limited = vs30s_limited.assign(
            im=gmm_model.predict(request.intensity_model.probabilistic, corr_matrix))

        timing.log('Shaking Level')
        print('Number of Vs30s: {}'.format(len(vs30s_limited)))

        # Load models
        fragility_model = self._fragility_repository.get(request.fragility_model)
        unit_construction_cost_model = self._construction_cost_repository.get(request.construction_cost)
        repair_cost_ratio_model = self._repair_cost_repository.get(request.repair_cost_model)
        restoration_model = self._restoration_repository.get(request.restoration_model)
        # Get required facility classification names from models
        required_classifications = {
            fragility_model.required_classification,
            unit_construction_cost_model.required_classification,
            repair_cost_ratio_model.required_classification,
            restoration_model.required_classification
        }

        timing.log('Load Models')

        bridges_limited = self._bridge_repository.get(bridge=request.bridge,
                                                      center_lat=request.earthquake.location.latitude,
                                                      center_long=request.earthquake.location.longitude,
                                                      deg_limit=request.deg_limit,
                                                      required_classifications=required_classifications
                                                      ).dataframe

        timing.log('Load Structures')
        print('Number of structures: {}'.format(len(bridges_limited)))

        # %% Assign IM to structures

        bridges_limited = bridges_limited.assign(im=AssignValueByGeoDistance(vs30s_limited['latitude'],
                                                                             vs30s_limited['longitude'],
                                                                             bridges_limited['latitude'],
                                                                             bridges_limited['longitude'],
                                                                             vs30s_limited['im']).nearest())

        timing.log('Assign IM')

        # %%Calculate each state probability
        bridges_limited = bridges_limited.assign(State=state_prob(bridges_limited, fragility_model,
                                                                  im_conversion_rate(
                                                                      request.intensity_model.gmm_model_name,
                                                                      request.intensity_model.measure)))
        timing.log('Assign State')

        # %%Calculate Loss

        bridges_limited = bridges_limited.assign(Repair_Cost=cost_calc(bridges_limited,
                                                                       unit_construction_cost_model,
                                                                       repair_cost_ratio_model))

        timing.log('Assign Cost')

        bridges_limited['RepairTime'] = 0
        restorations = restoration_model.dataframe
        classification = restoration_model.required_classification
        for index, bridge in bridges_limited[bridges_limited['State'] != LIMIT_STATES_DATABASE[0]].iterrows():

            # if request.restoration_model.restoration_type == 'HAZUS':
            #     class_restoration = restorations[restorations['HAZUS Class']['HAZUS Class'] == bridge['hazus_class_id']]
            # elif request.restoration_model.restoration_type.startswith('custom'):
            #     class_restoration = restorations[restorations['BridgeClass'] == bridge['custom_class']]
            # else:
            #     raise NotImplementedError

            class_restoration = restorations[restorations[classification] == bridge[classification]]
            bridges_limited.loc[index, 'RepairTime'] = np.random.normal(
                loc=class_restoration[bridge['State'] + ':median'],
                scale=class_restoration[bridge['State'] + ':dispersion'])

        timing.log('Assign Repair Time')

        # %% Calculate Network Travel Time
        # Todo: Use case should know about methods of returned net
        net = self._transportation_network_repository.get(request.transportation_network)
        net.userEquilibrium(request.network_model.algorithm, 20, request.network_model.convergence,
                            net.averageExcessCost)
        # print("initial", net)
        tstt_before = 0
        for ij in net.link:
            tstt_before += net.link[ij].cost * net.link[ij].flow

        # Remove broken links
        net_after = copy.deepcopy(net)
        broken_links = get_broken_links(
            ROOT_DIR + "\\data\\network_models\\" + request.transportation_network.location_name + "\\" + request.transportation_network.location_name + ".geojson",
            bridges_limited)

        for k in broken_links.keys():
            # Todo: Alternative ways to close a link
            net_after.link[k].capacity = net_after.link[k].capacity * 0.1

        net_after.userEquilibrium(request.network_model.algorithm, 20,
                                  request.network_model.convergence,
                                  net_after.averageExcessCost)
        tstt_after = 0
        for ij in net_after.link:
            tstt_after += net_after.link[ij].cost * net_after.link[ij].flow

        # Todo: Convert traffic flow from 1992 to 2020
        indirect_cost = request.cost_per_hour * ((tstt_after - tstt_before) / 60) * 10

        timing.log('Network process')

        return ResSuccess(bridges_limited.sort_values('State', ascending=False),
                          tstt_before / 60, tstt_after / 60, indirect_cost, net=net,
                          broken_links=broken_links, shaking_levels=vs30s_limited)
