from core.methods.utils import Timing
from core.interfaces.usecase_interface import IUseCase
from core.dto.usecase_requests.building.building_damage_estimation_request import BuildingDamageEstimationRequest
from core.interfaces.gateways.repositories.vs30_repository_interface import Vs30RepositoryInterface
from core.interfaces.gateways.repositories.building.building_fragility_model_repository_interface import BuildingFragilityRepositoryInterface
from core.interfaces.gateways.repositories.building.building_construction_cost_repository_interface import BuildingConstructionCostRepositoryInterface
from core.interfaces.gateways.repositories.building.building_repair_cost_repository_interface import \
    BuildingRepairCostRepositoryInterface
from core.interfaces.gateways.repositories.building.building_repository_interface import BuildingRepositoryInterface
from core.interfaces.gateways.gmm_loader_interface import GmmLoaderInterface
from core.interfaces.gateways.spatial_correlations_loader_interface import SpatialCorrelationLoaderInterface
from core.methods.geo_distance_methods import GeoDistance, AssignValueByGeoDistance
from core.methods.building_damage_estimation_methods import state_prob, cost_calc, im_conversion_rate
from core.dto.usecase_responses.building.building_damage_estimation_response import BuildingDamageEstimationResponse as ResSuccess
from Definitions import IM_PERIODS


class BuildingDamageEstimationUseCase(IUseCase):

    def __init__(self, vs30_repository: Vs30RepositoryInterface, fragility_repository: BuildingFragilityRepositoryInterface,
                 building_repository: BuildingRepositoryInterface,
                 construction_cost_repository: BuildingConstructionCostRepositoryInterface,
                 repair_cost_repository: BuildingRepairCostRepositoryInterface,
                 gmm_loader: GmmLoaderInterface,
                 spatial_correlation_loader: SpatialCorrelationLoaderInterface
                 ):
        self._vs30_repository = vs30_repository
        self._fragility_repository = fragility_repository
        self._building_repository = building_repository
        self._construction_cost_repository = construction_cost_repository
        self._repair_cost_repository = repair_cost_repository
        self._gmm_loader = gmm_loader
        self._spatial_correlation_loader = spatial_correlation_loader

    def process_request(self, request: BuildingDamageEstimationRequest):

        timing = Timing()

        print("deg_limit: ", request.deg_limit)
        # Calculate shaking levels for Vs30s
        vs30s_limited = self._vs30_repository.get(vs30=request.vs30,
                                                  center_lat=request.earthquake.location.latitude,
                                                  center_long=request.earthquake.location.longitude,
                                                  deg_limit=request.deg_limit).dataframe

        # Initialise GMM model
        gmm_model = self._gmm_loader(
            request.intensity_model.source,
            request.intensity_model.gmm_model_name,
            request.intensity_model.trained_model)

        # Get vs30 distances from earthquake according to model required distances
        distances = GeoDistance(vs30s_limited['latitude'],
                                vs30s_limited['longitude'],
                                request.earthquake.location.latitude,
                                request.earthquake.location.longitude,
                                request.earthquake.depth).store_distances(
            required_distances=gmm_model.REQUIRED_DISTANCES)

        corr_matrix = None
        if request.intensity_model.probabilistic:
            distance_matrix = GeoDistance(vs30s_limited['latitude'].values,
                                          vs30s_limited['longitude'].values).great_distance_matrix()

            corr_matrix = self._spatial_correlation_loader(
                request.intensity_model.spatial_correlation).correlation_matrix(
                distance_matrix, IM_PERIODS[request.intensity_model.measure], vs30_clustering=True)

        # Assign IM to vs30s
        gmm_model(vs30s_limited['vs30'].values,
                  request.earthquake.magnitude,
                  distances,
                  request.intensity_model.measure)
        vs30s_limited = vs30s_limited.assign(
            im=gmm_model.predict(request.intensity_model.probabilistic, corr_matrix))

        timing.log("Shaking Level")
        print('Number of Vs30s: {}'.format(len(vs30s_limited)))

        # Load models
        fragility_model = self._fragility_repository.get(request.fragility_model)
        unit_construction_cost_model = self._construction_cost_repository.get(request.construction_cost)
        repair_cost_ratio_model = self._repair_cost_repository.get(request.repair_cost_model)

        # Required facility classification names
        required_classifications = {
            fragility_model.required_classification,
            unit_construction_cost_model.required_classification,
            repair_cost_ratio_model.required_classification
        }

        timing.log("Load Fragility")

        # Load structures
        structures_limited = self._building_repository.get(building=request.building,
                                                           center_lat=request.earthquake.location.latitude,
                                                           center_long=request.earthquake.location.longitude,
                                                           deg_limit=request.deg_limit,
                                                           required_classifications=required_classifications
                                                           ).dataframe

        timing.log("Load Structure")
        print('Number of structure: {}'.format(len(structures_limited)))

        # %% Assign IM to structures

        structures_limited = structures_limited.assign(im=AssignValueByGeoDistance(vs30s_limited['latitude'],
                                                                                   vs30s_limited['longitude'],
                                                                                   structures_limited['latitude'],
                                                                                   structures_limited['longitude'],
                                                                                   vs30s_limited['im']).nearest())

        timing.log("Assign IM")

        # %%Calculate each state probability
        structures_limited = structures_limited.assign(State=state_prob(structures_limited, fragility_model,
                                                                        im_conversion_rate(
                                                                            request.intensity_model.gmm_model_name,
                                                                            request.intensity_model.measure)))
        timing.log("Assign State")

        # %%Calculate Loss
        structures_limited = structures_limited.assign(Repair_Cost=cost_calc(structures_limited,
                                                                             unit_construction_cost_model,
                                                                             repair_cost_ratio_model))

        timing.log("Assign Cost")

        return ResSuccess(structures_limited.sort_values('State', ascending=False), vs30s_limited,
                          required_classifications)
