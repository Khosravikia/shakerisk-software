from core.interfaces.usecase_interface import IUseCase
from core.dto.usecase_responses import response_object as res
from core.dto.usecase_requests.dataframe_save_request import DataFrameSaveRequest


class DataFrameSaveUseCase(IUseCase):

    def __init__(self):
        pass

    def process_request(self, request_object: DataFrameSaveRequest):

        print(request_object.dataframe.dataframe)
        return res.ResponseSuccess(request_object.dataframe.dataframe)
