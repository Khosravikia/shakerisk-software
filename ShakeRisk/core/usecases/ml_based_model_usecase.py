import numpy as np
import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf

from core.methods.utils import Timing
from core.methods.pre_processing_methods import pre_process
from core.methods.evaluation_methods import evaluate
from core.interfaces.usecase_interface import IUseCase
from core.dto.usecase_requests.ml_based_model_request import MLBasedModelRequest
from core.dto.usecase_responses.ml_based_model_response import MLBasedModelResponse as res
from core.dto.gateway_dtos.ml_model_options.ml_model_additional_info import MLModelAdditionalInfo
from core.interfaces.gateways.ml_based_model_loader_interface import MLBasedModelLoaderInterface


class MLBasedModelUseCase(IUseCase):

    def __init__(self, ml_based_model_creator: MLBasedModelLoaderInterface):
        self.ml_based_model_creator = ml_based_model_creator

    def process_request(self, request: MLBasedModelRequest) -> res:

        timing = Timing(request.name)
        dataset = request.dataframe.dataframe
        # Splitting train and test
        train_dataset = dataset.sample(frac=request.train_fraction)
        test_dataset = dataset.drop(train_dataset.index)

        # Getting data information
        # train_stats = train_dataset.describe()
        # train_stats = train_stats[request_object.input_columns]
        # train_stats = train_stats.transpose()

        # Separating Outputs
        train_input = train_dataset[request.input_columns]
        train_output = train_dataset[request.output_columns]
        test_input = test_dataset[request.input_columns]
        test_output = test_dataset[request.output_columns]

        # Pre-processing
        pre_process_args = None
        if request.pre_processing is not None:
            [normed_train_data, *pre_process_args] = pre_process(train_input, request.pre_processing)
            [normed_test_data, *_] = pre_process(test_input, request.pre_processing, *pre_process_args)
        else:
            normed_train_data = train_input
            normed_test_data = test_input

        if request.output_transformation is not None:
            if request.output_transformation == "lognormal":
                train_output = np.log(train_output)
                test_output = np.log(test_output)

        timing.log('Pre-process')

        ml_model = self.ml_based_model_creator(request.ml_model_options)
        model = ml_model.fit(normed_train_data, train_output)

        timing.log('Fit Model')
        # Mixed model
        # if request_object.use_mixed_effects:
        #     print("ME effect columns: ".format(request_object.me_columns))
        #     residuals = train_output - ml_model.predict(normed_train_data)
        #     # residuals["Intercept"] = 1
        #     md = sm.MixedLM(residuals, np.ones(len(residuals)), groups=train_dataset[request_object.me_columns])
        #     mdf = md.fit()
        #     print(mdf.summary())

        # Todo: Mange memory and performance better in evaluation
        # Evaluation
        evaluations = []
        train_predicted = ml_model.predict(normed_train_data)
        evaluations.append({
            '': 'Train dataset result',
            'rmse': evaluate('rmse', train_output, train_predicted),
            'r2': evaluate('r2', train_output, train_predicted),
            'error_mean': evaluate('error_mean', train_output, train_predicted),
            'error_std': evaluate('error_std', train_output, train_predicted),
        })

        predicted = ml_model.predict(normed_test_data)
        evaluations.append({
            '': 'Test dataset result',
            'rmse': evaluate('rmse', test_output, predicted),
            'r2': evaluate('r2', test_output, predicted),
            'error_mean': evaluate('error_mean', test_output, predicted),
            'error_std': evaluate('error_std', test_output, predicted),
        })

        timing.log('Evaluate')

        # Todo: Apply ME to multiple parameters
        random_effect_values = None
        random_effect_std = None
        if request.use_mixed_effects:
            if(len(request.me_columns)>1):
                print("Only one ME  column is supported at the moment. ME effect column: {}".format(request.me_columns[0]))
            residuals = train_output - train_predicted

            # One Group
            # residuals["Intercept"] = 1
            md = smf.mixedlm("{} ~ 1".format(request.output_columns[0]), residuals, groups=train_dataset[request.me_columns[0]])
            mdf = md.fit()
            print(mdf.summary())
            # me_coef = float(mdf.summary().tables[1]['Coef.'].loc['Group Var'])
            random_effect_values = pd.DataFrame(mdf.random_effects).T
            random_effect_values.index.name = 'Group'
            random_effect_values.columns = ['Value']
            random_effect_std = float(random_effect_values.std())  # Float for being JSON serializable

            # # Two groups?
            # vcf = {}
            # if len(request_object.me_columns) ==2:
            #     col = request_object.me_columns[1]
            #     residuals[col] = train_dataset[col]
            #     vcf = ({
            #         "a": "0 + C({})".format(col)
            #     })
            # md = sm.MixedLM.from_formula(
            #     "{} ~ 1".format(request_object.output_columns[0]),
            #     groups=train_dataset[request_object.me_columns[0]],
            #     vc_formula=vcf,
            #     data=residuals
            # )
            # mdf = md.fit()
            # print(mdf.summary)
            # random_effect_values = pd.DataFrame(mdf.random_effects)

            # After applying random effect
            diffs = train_dataset[request.me_columns[0]].apply(lambda x: random_effect_values.loc[x])
            train_output_me = train_output - diffs.values

            ml_model_me = self.ml_based_model_creator(request.ml_model_options)
            _ = ml_model_me.fit(normed_train_data, train_output_me)

            train_predicted = ml_model_me.predict(normed_train_data)
            evaluations.append({
                '': 'ME:Train dataset result',
                'rmse': evaluate('rmse', train_output, train_predicted),
                'r2': evaluate('r2', train_output, train_predicted),
                'error_mean': evaluate('error_mean', train_output, train_predicted),
                'error_std': evaluate('error_std', train_output, train_predicted),
            })

            predicted = ml_model_me.predict(normed_test_data)
            evaluations.append({
                '': 'ME:Test dataset result',
                'rmse': evaluate('rmse', test_output, predicted),
                'r2': evaluate('r2', test_output, predicted),
                'error_mean': evaluate('error_mean', test_output, predicted),
                'error_std': evaluate('error_std', test_output, predicted),
            })

            timing.log('Random Effect')

        evaluations = pd.DataFrame(evaluations)
        additional_info = MLModelAdditionalInfo(pre_process_type=request.pre_processing,
                                                pre_process_args=pre_process_args,
                                                output_transformation_type=request.output_transformation,
                                                model_type=ml_model.MODEL_OBJECT_TYPE,
                                                random_effect_std=random_effect_std,
                                                # train_data_result, test_data_result
                                                )

        return res(model, additional_info, evaluations, ml_model.MODEL_OBJECT_TYPE, random_effect_values)
