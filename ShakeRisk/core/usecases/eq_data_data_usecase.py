import urllib.request, urllib.parse, urllib.error
from urllib.request import urlopen
import json
import ssl
import pandas as pd

from core.methods.utils import Timing
from core.interfaces.usecase_interface import IUseCase
from core.dto.usecase_responses import response_object as res
from core.dto.usecase_requests.eq_data_get_request import EqDataGetRequest


class EqDataGetUseCase(IUseCase):

    def __init__(self,):
        pass

    def process_request(self, request: EqDataGetRequest):

        timing = Timing()

        # Ignore SSL certificate errors
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        try:
            serviceurl = 'https://earthquake.usgs.gov/fdsnws/event/1/query.geojson?'
            parms = dict()
            parms['starttime'] = request.time_interval.start_date + ' 23:59:59'
            parms['endtime'] = request.time_interval.end_date + ' 23:59:59'
            parms['maxlatitude'] = request.area.max_lat
            parms['minlatitude'] = request.area.min_lat
            parms['maxlongitude'] = request.area.max_lng
            parms['minlongitude'] = request.area.min_lng
            parms['minmagnitude'] = request.min_magnitude
            parms['orderby'] = 'time'

            url = serviceurl + urllib.parse.urlencode(parms)

            print('\n Retrieving', url, '\n')
            html = urlopen(url, context=ctx).read().decode()
            print('Retrieved', len(html), 'characters\n')

            info = json.loads(html)
        except:
            print("'==== Failure To Retrieve ===='")

        timing.log('Download Data')

        time = list()
        mag = list()
        latitude = list()
        longitude = list()
        place = list()

        for a in info['features']:
            mag.append(a['properties']['mag'])
            time.append(a['properties']['time'])
            place.append(a['properties']['place'])
            longitude.append(a['geometry']['coordinates'][0])
            latitude.append(a['geometry']['coordinates'][1])

        temp = pd.DataFrame([mag, time, place, longitude, latitude]).transpose()
        temp.columns = ['mag', 'time', 'place', 'longitude', 'latitude']
        temp['year'] = pd.to_datetime(temp['time'], unit='ms').apply(lambda x: str(x)).str[:4].astype(int)

        def states(init):
            if 'Texas' in init:
                return 'Texas'
            else:
                return init.split(',')[-1].strip()
        temp['state'] = temp['place'].apply(states)

        timing.log('Format Data')

        return res.ResponseSuccess(temp)


if __name__ == '__main__':
    from core.dto.usecase_requests.eq_data_get_request_old import EqDataGetRequest
    req = EqDataGetRequest(start_time='2000-01-01', end_time='2020-05-21', min_latitude=25.57, max_latitude=37.265,
                           min_longitude=-107.007, max_longitude=-93.164, min_magnitude=3)

    res = EqDataGetUseCase().execute(req)
    print(res.value)
