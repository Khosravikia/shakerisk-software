import numpy as np
import pandas as pd

from core.methods.utils import Timing
from core.interfaces.usecase_interface import IUseCase
from core.dto.usecase_responses import response_object as res
from core.dto.usecase_requests.bridge_nbi_convert_request import BridgeNbiConvertRequest
from core.interfaces.gateways.repositories.bridge.nbi_repository_interface import NBIRepositoryInterface


class BridgeNbiConvertUseCase(IUseCase):

    def __init__(self,
                 nbi_repository: NBIRepositoryInterface):
        self._nbi_repository = nbi_repository

    def process_request(self, request: BridgeNbiConvertRequest):

        columns = ['FacilityID', 'FacilityType', 'Longitude', 'Latitude', 'Builtyear', 'Spannumber', 'Spanlength_m',
                   'Structurelength_m', 'Deckwidth_m', 'Skew_deg', 'Facility_Description', 'NBIclass', 'HAZUSclass',
                   'Customclass']

        raw_bridges = self._nbi_repository.get(request.source, request.state, request.year,
                                               request.dataframe)
        # Apply on_system and non_culvert options
        if request.on_system:
            raw_bridges = raw_bridges[raw_bridges['MAINTENANCE_021'] == 1]
        if request.non_culvert:
            raw_bridges = raw_bridges.drop(raw_bridges[raw_bridges['STRUCTURE_TYPE_043B'] == 19].index)
        raw_bridges = raw_bridges.reset_index()

        output_dataframe = pd.DataFrame(index=np.arange(0, len(raw_bridges)), columns=columns)

        output_dataframe['FacilityID'] = raw_bridges['STATE_CODE_001'].str.strip() + '_' \
                                         + raw_bridges['STRUCTURE_NUMBER_008'].str.strip()

        output_dataframe['FacilityType'] = 'Bridge'

        output_dataframe['Latitude'] = (pd.to_numeric(raw_bridges['LAT_016'].str[0:-6]) +
                                        pd.to_numeric(raw_bridges['LAT_016'].str[-6:-4]) / 60 +
                                        pd.to_numeric(raw_bridges['LAT_016'].str[-4:]) / 360000)

        output_dataframe['Longitude'] = (pd.to_numeric(raw_bridges['LONG_017'].str[0:-6]) +
                                         pd.to_numeric(raw_bridges['LONG_017'].str[-6:-4]) / 60 +
                                         pd.to_numeric(raw_bridges['LONG_017'].str[-4:]) / 360000) * -1

        output_dataframe['Builtyear'] = raw_bridges['YEAR_BUILT_027']

        output_dataframe['Spannumber'] = raw_bridges['MAIN_UNIT_SPANS_045'] + raw_bridges['APPR_SPANS_046']

        output_dataframe['Spanlength_m'] = raw_bridges['MAX_SPAN_LEN_MT_048']

        output_dataframe['Structurelength_m'] = raw_bridges['STRUCTURE_LEN_MT_049']

        output_dataframe['Deckwidth_m'] = raw_bridges['DECK_WIDTH_MT_052']

        output_dataframe['Skew_deg'] = raw_bridges['DEGREES_SKEW_034']

        def nbi_code(structure_kind_a: int, structure_kind_b: int):
            result = ''
            if 0 <= structure_kind_a <= 9:
                result = str(structure_kind_a)
            else:
                result = '100'
            if 0 <= structure_kind_b <= 9:
                result += '0' + str(structure_kind_b)
            elif 10 <= structure_kind_b <= 22:
                result += str(structure_kind_b)
            else:
                result += '00'
            return result

        output_dataframe['NBIclass'] = [nbi_code(a, b) for (a, b) in
                                        zip(raw_bridges['STRUCTURE_KIND_043A'], raw_bridges['STRUCTURE_TYPE_043B'])]

        output_dataframe['Facility_Description'] = (output_dataframe['Spannumber'].astype(str) + '-span;' +
                                                    output_dataframe['Skew_deg'].astype(str) + ' deg skew;' +
                                                    output_dataframe['Structurelength_m'].astype(str) +
                                                    ' m structure Length;' +
                                                    output_dataframe['Spanlength_m'].astype(str) +
                                                    ' m Max Span Length; Built ' +
                                                    output_dataframe['Builtyear'].astype(str) + ';NBI ' +
                                                    output_dataframe['NBIclass'])

        def check_seismic_consideration(conventional_non_ca, conventional_ca, seismic_design, state, year):
            # it is assumed conventional design belongs to before 1990s
            if year < 1990:
                if state == 'CA':
                    return conventional_ca
                else:
                    return conventional_non_ca
            elif year >= 1990:
                return seismic_design

        def hazus_classifier(nbi_class, structure_length, span_length, span_number, nbi_state, construction_year):
            result = ''
            if span_length > 150:  # major bridge
                return check_seismic_consideration('HWB1', 'HWB1', 'HWB2', nbi_state, construction_year)
            elif span_length <= 150:
                if span_number == 1:
                    return check_seismic_consideration('HWB3', 'HWB3', 'HWB4', nbi_state, construction_year)
                elif span_number > 1:
                    if nbi_class in ['101', '102', '103', '104', '105', '106']:  # RC
                        return check_seismic_consideration('HWB5', 'HWB6', 'HWB7', nbi_state, construction_year)
                    elif nbi_class in ['201', '202', '203', '204', '205', '206']:  # RC_Cont
                        return check_seismic_consideration('HWB10', 'HWB10', 'HWB11', nbi_state, construction_year)
                    elif nbi_class in ['301', '302', '303', '304', '305', '306']:  # Steel
                        if structure_length >= 20:
                            return check_seismic_consideration('HWB12', 'HWB13', 'HWB14', nbi_state, construction_year)
                        elif structure_length < 20:
                            return check_seismic_consideration('HWB24', 'HWB25', 'HWB14', nbi_state, construction_year)
                    elif nbi_class in ['401', '402', '403', '404', '405', '406', '407', '408', '409',
                                       '410']:  # Steel_Cont
                        if structure_length >= 20:
                            return check_seismic_consideration('HWB15', 'HWB15', 'HWB16', nbi_state, construction_year)
                        elif structure_length < 20:
                            return check_seismic_consideration('HWB26', 'HWB27', 'HWB16', nbi_state, construction_year)
                    elif nbi_class in ['501', '502', '503', '504', '505', '506']:  # PS Concrete
                        return check_seismic_consideration('HWB17', 'HWB18', 'HWB19', nbi_state, construction_year)
                    elif nbi_class in ['601', '602', '603', '604', '605', '606']:  # PS Concrete_Cont
                        return check_seismic_consideration('HWB22', 'HWB22', 'HWB23', nbi_state, construction_year)
                    else:
                        return 'HWB28'

        output_dataframe['HAZUSclass'] = [
            hazus_classifier(bridge_class, bridge_length, span_length, span_num, state, year)
            for (bridge_class, bridge_length, span_length, span_num, state, year) in
            zip(output_dataframe['NBIclass'], output_dataframe['Structurelength_m'],
                output_dataframe['Spanlength_m'], output_dataframe['Spannumber'],
                [request.state] * len(raw_bridges), output_dataframe['Builtyear'])]

        def custom_classifier(nbi_class: str):
            if nbi_class in ['402', '403']:
                return 'MCSTEEL'
            elif nbi_class in ['302', '303']:
                return 'MSSTEEL'
            elif nbi_class in ['502', '503']:
                return 'MSPC'
            elif nbi_class in ['102', '103']:
                return 'MSRC'
            elif nbi_class == '101':
                return 'MSRC-Slab'
            elif nbi_class == '201':
                return 'MCRC-Slab'
            else:
                return 'Unknown'

        output_dataframe['Customclass'] = [custom_classifier(nbi_class) for nbi_class in output_dataframe['NBIclass']]

        return res.ResponseSuccess(output_dataframe)
