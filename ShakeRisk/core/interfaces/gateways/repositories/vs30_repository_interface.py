from abc import abstractmethod, ABC
from core.domain.entities.vs30 import Vs30


class Vs30RepositoryInterface:

    @abstractmethod
    def get(self, vs30: Vs30, center_lat=None, center_long=None, deg_limit=None, required=True) -> Vs30:
        raise NotImplementedError

    @abstractmethod
    def get_local(self, vs30: Vs30, center_lat=None, center_long=None, deg_limit=None) -> Vs30:
        raise NotImplementedError

    @abstractmethod
    def get_constant(self, vs30: Vs30, center_lat=None, center_long=None, deg_limit=None) -> Vs30:
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, vs30: Vs30, center_lat=None, center_long=None, deg_limit=None) -> Vs30:
        raise NotImplementedError
