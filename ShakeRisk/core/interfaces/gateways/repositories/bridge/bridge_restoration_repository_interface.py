from abc import abstractmethod, ABC
from core.domain.entities.bridge.bridge_restoration_model import BridgeRestorationModel


class BridgeRestorationRepositoryInterface(ABC):

    @abstractmethod
    def get(self, restoration_model: BridgeRestorationModel, required=True) -> BridgeRestorationModel:
        raise NotImplementedError

    @abstractmethod
    def get_local(self, restoration_model: BridgeRestorationModel) -> BridgeRestorationModel:
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, restoration_model: BridgeRestorationModel):
        raise NotImplementedError
