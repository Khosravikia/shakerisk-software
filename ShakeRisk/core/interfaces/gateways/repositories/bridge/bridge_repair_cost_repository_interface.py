from abc import abstractmethod, ABC
from core.domain.entities.bridge.bridge_repair_cost_model import BridgeRepairCostModel


class BridgeRepairCostRepositoryInterface(ABC):

    @abstractmethod
    def get(self, repair_cost_model: BridgeRepairCostModel, required=True) -> BridgeRepairCostModel:
        raise NotImplementedError

    @abstractmethod
    def get_local(self, repair_cost_model: BridgeRepairCostModel) -> BridgeRepairCostModel:
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, repair_cost_model: BridgeRepairCostModel):
        raise NotImplementedError
