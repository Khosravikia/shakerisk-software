from abc import abstractmethod, ABC
import pandas as pd


class NBIRepositoryInterface(ABC):

    @abstractmethod
    def get(self, source, state=None, year=None, dataframe=None) -> pd.DataFrame:
        raise NotImplementedError

    @abstractmethod
    def get_nbi_web(self, source, state=None, year=None, dataframe=None) -> pd.DataFrame:
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, source, state=None, year=None, dataframe=None) -> pd.DataFrame:
        raise NotImplementedError

