from abc import abstractmethod, ABC
from core.domain.entities.bridge.bridge_fragility_model import BridgeFragilityModel


class BridgeFragilityRepositoryInterface(ABC):

    @abstractmethod
    def get(self, fragility_model: BridgeFragilityModel) -> BridgeFragilityModel:
        raise NotImplementedError

    @abstractmethod
    def get_local(self, fragility_model: BridgeFragilityModel) -> BridgeFragilityModel:
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, fragility_model: BridgeFragilityModel) -> BridgeFragilityModel:
        raise NotImplementedError
