from abc import abstractmethod, ABC
from core.domain.entities.bridge.bridge_construction_cost_model import BridgeConstructionCostModel


class BridgeConstructionCostRepositoryInterface(ABC):

    @abstractmethod
    def get(self, construction_cost_model: BridgeConstructionCostModel, required=True) -> BridgeConstructionCostModel:
        raise NotImplementedError

    @abstractmethod
    def get_local(self, construction_cost_model: BridgeConstructionCostModel) -> BridgeConstructionCostModel:
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, construction_cost_model: BridgeConstructionCostModel):
        raise NotImplementedError
