from abc import abstractmethod, ABC


# Todo define base repository class
from core.domain.entities.bridge.bridge import Bridge


class BridgeRepositoryInterface(ABC):

    @abstractmethod
    def get(self, bridge: Bridge, center_lat=None, center_long=None, deg_limit=None,
            required_classifications: set = None, required=True) -> Bridge:
        raise NotImplementedError

    @abstractmethod
    def get_local(self, bridge: Bridge, center_lat=None, center_long=None, deg_limit=None,
                  required_classifications: set = None) -> Bridge:
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, bridge: Bridge, center_lat=None, center_long=None, deg_limit=None,
                      required_classifications: set = None) -> Bridge:
        raise NotImplementedError

