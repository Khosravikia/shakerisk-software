from abc import abstractmethod, ABC


from core.domain.entities.network.transportation_network import TransportationNetwork


class TransportationNetworkRepositoryInterface(ABC):

    @abstractmethod
    def get(self, transportation_network: TransportationNetwork):
        raise NotImplementedError

    @abstractmethod
    def get_local(self, transportation_network: TransportationNetwork):
        raise NotImplementedError


