from abc import abstractmethod, ABC
from core.domain.entities.building.building_repair_cost_model import BuildingRepairCostModel


class BuildingRepairCostRepositoryInterface(ABC):

    @abstractmethod
    def get(self, repair_cost_model: BuildingRepairCostModel, required=True):
        raise NotImplementedError

    @abstractmethod
    def get_local(self, repair_cost_model: BuildingRepairCostModel):
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, repair_cost_model: BuildingRepairCostModel):
        raise NotImplementedError
