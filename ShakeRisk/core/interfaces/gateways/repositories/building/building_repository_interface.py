from abc import abstractmethod, ABC


# Todo define base repository class
from core.domain.entities.building.building import Building


class BuildingRepositoryInterface(ABC):

    @abstractmethod
    def get(self, building: Building, center_lat=None, center_long=None, deg_limit=None,
            required_classifications: set = None, required=True) -> Building:
        raise NotImplementedError

    @abstractmethod
    def get_local(self, building: Building, center_lat=None, center_long=None, deg_limit=None,
                  required_classifications: set = None) -> Building:
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, building: Building, center_lat=None, center_long=None, deg_limit=None,
                      required_classifications: set = None) -> Building:
        raise NotImplementedError

