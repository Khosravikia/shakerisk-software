from abc import abstractmethod, ABC
from core.domain.entities.building.building_construction_cost_model import BuildingConstructionCostModel


class BuildingConstructionCostRepositoryInterface(ABC):

    @abstractmethod
    def get(self, construction_cost_model: BuildingConstructionCostModel, required=False):
        raise NotImplementedError

    @abstractmethod
    def get_local(self, construction_cost_model: BuildingConstructionCostModel):
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, construction_cost_model: BuildingConstructionCostModel):
        raise NotImplementedError
