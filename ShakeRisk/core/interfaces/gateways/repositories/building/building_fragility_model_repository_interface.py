from abc import abstractmethod, ABC
from core.domain.entities.building.building_fragility_model import BuildingFragilityModel


class BuildingFragilityRepositoryInterface(ABC):

    @abstractmethod
    def get(self, fragility_model: BuildingFragilityModel, required=True) -> BuildingFragilityModel:
        raise NotImplementedError

    @abstractmethod
    def get_local(self, fragility_model: BuildingFragilityModel) -> BuildingFragilityModel:
        raise NotImplementedError

    @abstractmethod
    def get_dataframe(self, fragility_model: BuildingFragilityModel) -> BuildingFragilityModel:
        raise NotImplementedError
