from abc import abstractmethod, ABC


class ModelRepositoryInterface(ABC):

    @staticmethod
    def get_required_classification(model_type, structure_type, model_name):
        raise NotImplementedError
