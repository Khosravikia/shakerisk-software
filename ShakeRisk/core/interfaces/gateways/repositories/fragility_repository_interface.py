from abc import abstractmethod, ABC


class FragilityRepositoryInterface(ABC):

    @abstractmethod
    def get(self, source, path, im='PGA', structure_type='bridge'):
        raise NotImplementedError

    @abstractmethod
    def get_from_local(self, path, im='PGA', structure_type='bridge'):
        raise NotImplementedError

    @staticmethod
    def get_required_classification_id(name, structure_type):
        raise NotImplementedError
