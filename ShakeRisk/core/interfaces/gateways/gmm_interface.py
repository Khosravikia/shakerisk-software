from abc import ABC, abstractmethod, abstractproperty


class GmmInterface(ABC):

    # Todo: Create a static source for "required distances" strings
    # Available distances can be found at core.methods.geo_distance_methods => available_distances
    REQUIRED_DISTANCES = abstractproperty()

    @abstractmethod
    def __init__(self, vs30s, mw, rhypos, im, corr_matrix=None):
        raise NotImplementedError

    @abstractmethod
    def __call__(self, vs30s, mw, rhypos, im, corr_matrix=None):
        raise NotImplementedError

    @abstractmethod
    def predict(self, probabilistic=False, corr_matrix=None):
        raise NotImplementedError

    def predict_probabilistic_only(self, corr_matrix=None):
        raise NotImplementedError
