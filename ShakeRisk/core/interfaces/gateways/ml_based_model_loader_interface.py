from abc import abstractmethod, ABC
from core.interfaces.gateways.ml_based_model_interface import MLBasedModelInterface


class MLBasedModelLoaderInterface(ABC):

    @abstractmethod
    def __call__(self, ml_model_options) -> MLBasedModelInterface:
        raise NotImplementedError
