from abc import ABC, abstractmethod, abstractproperty


class MLBasedModelInterface(ABC):

    # Todo: Create a static source for "model object type" strings
    MODEL_OBJECT_TYPE = abstractproperty()

    @abstractmethod
    def __init__(self, ml_model_options, model=None):
        raise NotImplementedError

    @abstractmethod
    def fit(self, input_data, output_data):
        raise NotImplementedError

    @abstractmethod
    def predict(self, input_data):
        raise NotADirectoryError
