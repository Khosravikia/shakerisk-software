from abc import abstractmethod, ABC, ABCMeta


class SpatialCorrelationLoaderInterface(ABC):

    @abstractmethod
    def __call__(self, special_corr_name):
        raise NotImplementedError

    @abstractmethod
    def correlation_matrix(self, distance_matrix, period, vs30_clustering=True):
        raise NotImplementedError
