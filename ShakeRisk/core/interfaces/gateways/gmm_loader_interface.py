from abc import abstractmethod, ABC

from core.domain.entities.ml_trained_model import MLTrainedModel
from core.interfaces.gateways.gmm_interface import GmmInterface


class GmmLoaderInterface(ABC):

    @abstractmethod
    def __call__(self, source: str, model_name: str = None, trained_model: MLTrainedModel = None) -> GmmInterface:
        raise NotImplementedError

