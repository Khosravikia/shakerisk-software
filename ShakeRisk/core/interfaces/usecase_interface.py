from abc import abstractmethod
import core.dto.usecase_responses.response_object as res


class IUseCase:

    # Todo: Change back to try for production
    def execute(self, request_object):
        if not request_object:
            return res.ResponseFailure.build_from_invalid_request_object(request_object)

        return self.process_request(request_object)
        # try:
        #     return self.process_request(request_object)
        # except Exception as exc:
        #     return res.ResponseFailure.build_system_error(
        #         "{}: {}".format(exc.__class__.__name__, "{}".format(exc))
        #     )

    @abstractmethod
    def process_request(self, request_object):
        raise NotImplementedError
