# Import all GMM model classes here
from .khosravikia_etal_2019 import KhosravikiaEtal2019
from .boore_etal_2014 import BooreEtal2014
from .hassani_atkinson_2015 import HassaniAtkinson2015
from .zalachoris_rathje_2019 import ZalachorisRathje2019

from .custom_trained_model import CustomTrainedModel
