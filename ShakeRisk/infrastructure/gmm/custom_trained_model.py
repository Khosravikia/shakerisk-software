import pandas as pd
import numpy as np
import Definitions
from core.interfaces.gateways.gmm_interface import GmmInterface
from infrastructure.ml_based_model.ml_based_model_loader import MLBasedModelLoader
from infrastructure.gmm.utils import coefficients_to_pandas, unit_converter


class CustomTrainedModel(GmmInterface):
    AVAILABLE_DISTANCES = ['rhypo', 'repi']
    REQUIRED_DISTANCES = ['rhypo']
    REQUIRED_COLUMNS = ['vs30', 'mw', 'rhypo']

    def __init__(self, trained_model=None):
        # super().__init__(vs30s, mw, distances, im, corr_matrix)
        self.vs30s = None
        self.mw = None
        self.distances = None
        self.im = None
        self.corr_matrix = None
        self.trained_model = MLBasedModelLoader(trained_model)
        if self.trained_model.input_names_orders:
            required_distance = set(self.AVAILABLE_DISTANCES) & set(self.trained_model.input_names_orders)
            if len(required_distance) == 1:
                self.REQUIRED_DISTANCES = [required_distance.pop()]
            else:
                raise ValueError(
                    'When defining input orders for trained models,'
                    ' exactly one input should be a supported distance ({})'.format(
                        self.AVAILABLE_DISTANCES
                    ))

    def __call__(self, vs30s, mw, distances, im, corr_matrix=None):
        self.vs30s = vs30s
        self.mw = mw
        self.distances = getattr(distances, self.REQUIRED_DISTANCES[0])
        self.im = im

        if corr_matrix is not None:
            self.corr_matrix = corr_matrix

    def predict(self, probabilistic=False, corr_matrix=None):
        if corr_matrix is not None:
            self.corr_matrix = corr_matrix

        input_data = pd.DataFrame({
            self.REQUIRED_COLUMNS[0]: self.vs30s,
            self.REQUIRED_COLUMNS[1]: np.ones(self.vs30s.shape) * self.mw,
            self.REQUIRED_DISTANCES[0]: self.distances})

        if self.trained_model.input_names_orders:
            input_data = input_data[self.trained_model.input_names_orders]

        result = self.trained_model.predict(input_data)

        if probabilistic:
            return np.exp(np.log(result) + self.probabilistic_earthquake_value() +
                          self.probabilistic_vs30s_value())
        else:
            return unit_converter(result, 'cm/s2', target_im=self.im, lognormal=False)

    def probabilistic_earthquake_value(self):

        return np.random.normal(0, self.ann_values['STDs', 'eta'])

    def probabilistic_vs30s_value(self):
        std_matrix = np.eye(len(self.corr_matrix)) * self.ann_values['STDs', 'res']
        cov_matrix = std_matrix @ self.corr_matrix @ std_matrix

        return np.random.multivariate_normal(np.zeros(cov_matrix.shape[0]), cov_matrix)

    def predict_probabilistic_only(self, corr_matrix=None):
        if corr_matrix is not None:
            self.corr_matrix = corr_matrix
        return self.probabilistic_earthquake_value() + self.probabilistic_vs30s_value()
