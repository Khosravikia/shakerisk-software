from io import StringIO
import numpy as np
import pandas as pd

from Definitions import IM_Units


# Todo: Inform user clearly that selected GMM does not support selected intensity measure
def coefficients_to_pandas(coefficients_string, col_num=1, im_name=None):
    # df = pd.read_csv(StringIO(coefficients_string.replace(" ", "")), "|", index_col=0, header=list(range(col_num)))
    # if im_name:
    #     df = df['im_name']
    #     if not len(df):
    #         raise ValueError('Selected ground motion model, does support "{}" intensity measure'.format(im_name))
    return pd.read_csv(StringIO(coefficients_string.replace(" ", "")), "|", index_col=0, header=list(range(col_num)))


def unit_converter(values, source_unit: str, target_unit: str = None, target_im=None, lognormal=False):
    if target_im is not None:
        # if target_im == 'PGV':
        #     target_unit = 'cm/s'
        # else:
        #     target_unit = 'cm/s2'
        target_unit = IM_Units[target_im]

    if lognormal:
        values = np.exp(values)

    if source_unit == target_unit:
        return values

    if source_unit == 'g' and target_unit == 'cm/s2':
        values = values * 980.665
    elif source_unit == 'mm/s' and target_unit == 'cm/s':
        values = values * 10
    else:
        raise NotImplementedError

    return values
