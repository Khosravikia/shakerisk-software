from Definitions import LOCAL_DATA_KEYWORD
from core.domain.entities.ml_trained_model import MLTrainedModel
from core.interfaces.gateways.gmm_interface import GmmInterface
from core.interfaces.gateways.gmm_loader_interface import GmmLoaderInterface
from infrastructure.gmm import *  # Using __init__.py file, this statement will load all classes in gmm folder


class GmmLoader(GmmLoaderInterface):

    # Todo: Take care of repetitive names in here and Definitions
    gmm_list = {'khosravikia_etal_2019': KhosravikiaEtal2019,
                'boore_etal_2014': BooreEtal2014,
                'hassani_atkinson_2015': HassaniAtkinson2015,
                'zalachoris_rathje_2019': ZalachorisRathje2019,

                'custom_trained_model': CustomTrainedModel
                }

    def __call__(self, source: str, model_name: str = None, trained_model: MLTrainedModel = None) -> GmmInterface:
        if source == LOCAL_DATA_KEYWORD:
            return self.gmm_list[model_name]()
        elif source == 'MLM_trained_model':
            return self.gmm_list['custom_trained_model'](trained_model)
        else:
            raise TypeError('Source of type {} is not supported'.format(source))
