from typing import TypeVar, Generic
from abc import abstractmethod, abstractproperty
from Definitions import DATAFRAME_DATA_KEYWORD, LOCAL_DATA_KEYWORD

T = TypeVar('T')


class BaseRepository(Generic[T]):

    def get(self, entity: T, required=True) -> T:
        entity = self.sources[entity.source](entity)

        if required and len(entity) == 0:
            raise ValueError
        if self.required_columns is not None:
            if set(self.required_columns).issubset(entity.dataframe.dataframe.columns):
                raise ValueError(
                    "Not all required columns present in provided DataFrame for object of type: {} named: {}.".format(
                        T, entity.name
                    ))
        return entity

    @abstractmethod
    def get_local(self, entity: T) -> T:
        raise NotImplementedError

    def get_dataframe(self, entity: T) -> T:
        entity.dataframe = entity.dataframe.dataframe
        return entity

    sources = {
        LOCAL_DATA_KEYWORD: get_local,
        DATAFRAME_DATA_KEYWORD: get_dataframe
    }

    required_columns = None
