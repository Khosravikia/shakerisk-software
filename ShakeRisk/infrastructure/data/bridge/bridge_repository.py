import sqlite3

import pandas as pd

from Definitions import DATABASE_PATH, DATAFRAME_DATA_KEYWORD, LOCAL_DATA_KEYWORD
from core.domain.entities.bridge.bridge import Bridge
from core.interfaces.gateways.repositories.bridge.bridge_repository_interface import BridgeRepositoryInterface
from infrastructure.data.utils import check_returned_dataframe


class BridgeRepository(BridgeRepositoryInterface):

    required_columns = ['state_id', 'facility_id', 'longitude', 'latitude', 'construction_year', 'span_num',
                        'span_len', 'structure_len', 'deck_width', 'skew_degree']

    # Todo: Handle HAZUS Sa[1.0] better
    HAZUS_ISHAPES = ['HWB3', 'HWB4', 'HWB10', 'HWB11', 'HWB15', 'HWB16', 'HWB22', 'HWB23', 'HWB26', 'HWB27']

    def get(self, bridge: Bridge, center_lat=None, center_long=None, deg_limit=None,
            required_classifications: set = None, required=True):
        sources = {
            LOCAL_DATA_KEYWORD: self.get_local,
            DATAFRAME_DATA_KEYWORD: self.get_dataframe
        }

        bridge = sources[bridge.source](bridge, center_lat, center_long, deg_limit, required_classifications)
        check_returned_dataframe(bridge, required, self.required_columns, required_classifications)

        # Add I-shape to HAZUS bridges
        if 'hazus' in required_classifications:
            bridge.dataframe['i_shape'] = False
            bridge.dataframe.loc[bridge.dataframe['hazus'].isin(self.HAZUS_ISHAPES), 'i_shape'] = True

        return bridge

    def get_local(self, bridge: Bridge, center_lat=None, center_long=None, deg_limit=None,
                  required_classifications: set = None) -> Bridge:

        # Remove possible None from required_classifications
        required_classifications -= {None}

        con = sqlite3.connect(DATABASE_PATH)

        # Filter by state
        filter_state = None
        if bridge.filter_state:
            filter_state = con.cursor().execute(
                'SELECT state_id FROM state WHERE state_abbr = ?', [bridge.filter_state]).fetchone()[0]

        # # Filter by degree limit
        min_lat = None
        max_lat = None
        min_lng = None
        max_lng = None
        if deg_limit:
            min_lat = center_lat - deg_limit
            max_lat = center_lat + deg_limit
            min_lng = center_long - deg_limit
            max_lng = center_long + deg_limit

        # Add classes
        select_part = ""
        join_part = ""
        where_part = ""
        if len(required_classifications):
            for i, cls in enumerate(required_classifications):
                select_part += ",\n c{}.class_name AS '{}'".format(i, cls)
                join_part += """
                    LEFT JOIN
                       bridge_classification AS bc{0} ON bf.facility_unique_id = bc{0}.facility_unique_id
                       LEFT JOIN
                       class AS c{0} ON bc{0}.class_id = c{0}.class_id
                       LEFT JOIN
                       classification AS cl{0} ON c{0}.classification_id = cl{0}.classification_id
                """.format(i)
                where_part += """
                    c{0}.class_id IS NOT NULL AND 
                    cl{0}.classification_name = '{1}' AND
                """.format(i, cls)

        bridge_data = pd.read_sql('''
        SELECT bf.*
            {}
          FROM bridge_facility AS bf
            {}
         WHERE  {}
                (:filter_state IS Null OR bf.state_id = :filter_state) AND
               (bf.longitude < :max_lng AND 
                bf.longitude > :min_lng AND 
                bf.latitude < :max_lat AND 
                bf.latitude > :min_lat);
                                        '''.format(select_part, join_part, where_part),
                                  con,
                                  params={
                                      'filter_state': filter_state,
                                      'filter_area': bool(deg_limit),
                                      'min_lat': min_lat,
                                      'max_lat': max_lat,
                                      'min_lng': min_lng,
                                      'max_lng': max_lng
                                  }
                                  )

        # if not remove_unknown:
            # This section was changed to join, because static plot in bridge damages needs custom classes anyway.
        #     bridge_data = pd.read_sql('''
        #                                     SELECT      bf.*, bfc.custom_class
        #                                     FROM        bridge_facility as bf
        #                                     LEFT JOIN   bridge_facility_custom as bfc
        #                                     ON          bfc.facility_unique_id = bf.facility_unique_id
        #                                     WHERE       (:filter_state IS Null OR bf.state_id = :filter_state)
        #                                     AND         (NOT :filter_area
        #                                                     OR  (   bf.longitude < :max_lng
        #                                                         AND bf.longitude > :min_lng
        #                                                         AND bf.latitude < :max_lat
        #                                                         AND bf.latitude > :min_lat)
        #                                                 )
        #                                     ''',
        #                               con,
        #                               params={
        #                                   'filter_state': filter_state,
        #                                   'filter_area': bool(deg_limit),
        #                                   'min_lat': min_lat,
        #                                   'max_lat': max_lat,
        #                                   'min_lng': min_lng,
        #                                   'max_lng': max_lng
        #                               }
        #                               )
        #     # bridge_data = pd.read_sql('''
        #     #         SELECT  *
        #     #         FROM    bridge_facility as bf
        #     #         WHERE   (NOT :filter_area
        #     #                     OR  (   bf.longitude < :max_lng
        #     #                         AND bf.longitude > :min_lng
        #     #                         AND bf.latitude < :max_lat
        #     #                         AND bf.latitude > :min_lat)
        #     #                 )
        #     #         ''',
        #     #                           con,
        #     #                           params={
        #     #                               'filter_area': bool(deg_limit),
        #     #                               'min_lat': min_lat,
        #     #                               'max_lat': max_lat,
        #     #                               'min_lng': min_lng,
        #     #                               'max_lng': max_lng
        #     #                           }
        #     #                           )
        # else:
        #     bridge_data = pd.read_sql('''
        #                         SELECT      bf.*, bfc.custom_class
        #                         FROM        bridge_facility_custom as bfc
        #                         LEFT JOIN   bridge_facility as bf
        #                         ON          bfc.facility_unique_id = bf.facility_unique_id
        #                         WHERE       (:filter_state IS Null OR bf.state_id = :filter_state)
        #                         AND         (NOT :filter_area
        #                                         OR  (   bf.longitude < :max_lng
        #                                             AND bf.longitude > :min_lng
        #                                             AND bf.latitude < :max_lat
        #                                             AND bf.latitude > :min_lat)
        #                                     )
        #                         ''',
        #                               con,
        #                               params={
        #                                   'filter_state': filter_state,
        #                                   'filter_area': bool(deg_limit),
        #                                   'min_lat': min_lat,
        #                                   'max_lat': max_lat,
        #                                   'min_lng': min_lng,
        #                                   'max_lng': max_lng
        #                               }
        #                               )
        # # Old way:
        # bridge_data = pd.read_pickle(Definitions.ROOT_DIR + "\\data\\bridges\\" + bridge.filter_state + ".pkl")
        #
        # if remove_unknown:
        #     bridge_data = bridge_data.drop(bridge_data[bridge_data['Customclass'] == 'Unknown'].index)
        #
        # if deg_limit:
        #     bridge_data = bridge_data[
        #         (center_long - deg_limit < bridge_data['Longitude']) &
        #         (bridge_data['Longitude'] < center_long + deg_limit) &
        #         (center_lat - deg_limit < bridge_data['Latitude']) &
        #         (bridge_data['Latitude'] < center_lat + deg_limit)]

        bridge.dataframe = bridge_data
        return bridge

    def get_dataframe(self, bridge: Bridge, center_lat=None, center_long=None, deg_limit=None,
                      required_classification_ids: set = None):
        bridge_data = bridge.dataframe.dataframe

        # if remove_unknown:
        #     bridge_data = bridge_data.drop(bridge_data[bridge_data['custom_class'] == 'Unknown'].index)

        if deg_limit:
            bridge_data = bridge_data[
                (center_long - deg_limit < bridge_data['longitude']) &
                (bridge_data['longitude'] < center_long + deg_limit) &
                (center_lat - deg_limit < bridge_data['latitude']) &
                (bridge_data['latitude'] < center_lat + deg_limit)]
        bridge.dataframe = bridge_data
        return bridge
