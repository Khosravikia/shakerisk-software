import io
from urllib.request import urlopen
from urllib.parse import urljoin
import ssl
import numpy as np
import pandas as pd

from Definitions import DATAFRAME_DATA_KEYWORD
from core.interfaces.gateways.repositories.bridge.nbi_repository_interface import NBIRepositoryInterface


class NBIRepository(NBIRepositoryInterface):

    required_columns = ['STATE_CODE_001', 'STRUCTURE_NUMBER_008', 'FEATURES_DESC_006A', 'MAIN_UNIT_SPANS_045',
                        'APPR_SPANS_046', 'DEGREES_SKEW_034', 'STRUCTURE_LEN_MT_049', 'MAX_SPAN_LEN_MT_048',
                        'YEAR_BUILT_027', 'LAT_016', 'LONG_017', 'STRUCTURE_KIND_043A', 'STRUCTURE_TYPE_043B',
                        'DECK_WIDTH_MT_052', 'MAINTENANCE_021']
    columns_types = {'STATE_CODE_001': str, 'STRUCTURE_NUMBER_008': str, 'FEATURES_DESC_006A': str,
                     'MAIN_UNIT_SPANS_045': np.int32, 'APPR_SPANS_046': np.int32, 'DEGREES_SKEW_034': np.double,
                     'STRUCTURE_LEN_MT_049': np.double, 'MAX_SPAN_LEN_MT_048': np.double,
                     'YEAR_BUILT_027': np.int32,
                     'LAT_016': str, 'LONG_017': str, 'STRUCTURE_KIND_043A': np.int32,
                     'STRUCTURE_TYPE_043B': np.int32, 'DECK_WIDTH_MT_052': np.double, 'MAINTENANCE_021': np.int32
                     }

    def get(self, source, state=None, year=None, dataframe=None):
        sources = {
            'nbi_web': self.get_nbi_web,
            DATAFRAME_DATA_KEYWORD: self.get_dataframe
        }
        return sources[source](source, state, year, dataframe)

    def get_nbi_web(self, source, state=None, year=None, dataframe=None):
        # Ignore SSL certificate errors
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        # Generate URL like and read it
        path = '/bridge/nbi/' + year + '/delimited/' + state + year[-2:] + '.txt'
        url = urljoin('https://www.fhwa.dot.gov/', path)
        html = urlopen(url, context=ctx).read().decode()

        print('Retrieved', len(html), 'characters\n')

        data = io.StringIO(html)

        # required_columns = ['STATE_CODE_001', 'STRUCTURE_NUMBER_008', 'FEATURES_DESC_006A', 'MAIN_UNIT_SPANS_045',
        #                     'APPR_SPANS_046', 'DEGREES_SKEW_034', 'STRUCTURE_LEN_MT_049', 'MAX_SPAN_LEN_MT_048',
        #                     'YEAR_BUILT_027', 'LAT_016', 'LONG_017', 'STRUCTURE_KIND_043A', 'STRUCTURE_TYPE_043B',
        #                     'DECK_WIDTH_MT_052', 'MAINTENANCE_021']
        # columns_types = {'STATE_CODE_001': str, 'STRUCTURE_NUMBER_008': str, 'FEATURES_DESC_006A': str,
        #                  'MAIN_UNIT_SPANS_045': np.int32, 'APPR_SPANS_046': np.int32, 'DEGREES_SKEW_034': np.double,
        #                  'STRUCTURE_LEN_MT_049': np.double, 'MAX_SPAN_LEN_MT_048': np.double,
        #                  'YEAR_BUILT_027': np.int32,
        #                  'LAT_016': str, 'LONG_017': str, 'STRUCTURE_KIND_043A': np.int32,
        #                  'STRUCTURE_TYPE_043B': np.int32, 'DECK_WIDTH_MT_052': np.double, 'MAINTENANCE_021': np.int32
        #                  }
        return pd.read_csv(data, ',', usecols=self.required_columns, dtype=self.columns_types)

    def get_dataframe(self, source, state=None, year=None, dataframe=None):
        # required_columns = ['STATE_CODE_001', 'STRUCTURE_NUMBER_008', 'FEATURES_DESC_006A', 'MAIN_UNIT_SPANS_045',
        #                     'APPR_SPANS_046', 'DEGREES_SKEW_034', 'STRUCTURE_LEN_MT_049', 'MAX_SPAN_LEN_MT_048',
        #                     'YEAR_BUILT_027', 'LAT_016', 'LONG_017', 'STRUCTURE_KIND_043A', 'STRUCTURE_TYPE_043B',
        #                     'DECK_WIDTH_MT_052', 'MAINTENANCE_021']
        # columns_types = {'STATE_CODE_001': str, 'STRUCTURE_NUMBER_008': str, 'FEATURES_DESC_006A': str,
        #                  'MAIN_UNIT_SPANS_045': np.int32, 'APPR_SPANS_046': np.int32, 'DEGREES_SKEW_034': np.double,
        #                  'STRUCTURE_LEN_MT_049': np.double, 'MAX_SPAN_LEN_MT_048': np.double,
        #                  'YEAR_BUILT_027': np.int32,
        #                  'LAT_016': str, 'LONG_017': str, 'STRUCTURE_KIND_043A': np.int32,
        #                  'STRUCTURE_TYPE_043B': np.int32, 'DECK_WIDTH_MT_052': np.double, 'MAINTENANCE_021': np.int32
        #                  }
        return dataframe.dataframe[self.required_columns].astype(self.columns_types)

