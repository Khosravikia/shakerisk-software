import sqlite3

import pandas as pd

from Definitions import DATABASE_PATH, DATAFRAME_DATA_KEYWORD, LOCAL_DATA_KEYWORD
from core.domain.entities.bridge.bridge_restoration_model import BridgeRestorationModel
from core.interfaces.gateways.repositories.bridge.bridge_restoration_repository_interface import\
    BridgeRestorationRepositoryInterface
from infrastructure.data.model_repository import ModelRepository
from infrastructure.data.utils import check_returned_dataframe


class BridgeRestorationRepository(BridgeRestorationRepositoryInterface):
    FACILITY_TYPE = 'bridge'
    required_columns = ['slight:median', 'slight:dispersion', 'moderate:median', 'moderate:dispersion',
                        'extensive:median', 'extensive:dispersion', 'complete:median', 'complete:dispersion']

    def get(self, restoration_model: BridgeRestorationModel, required=True) -> BridgeRestorationModel:
        sources = {
            LOCAL_DATA_KEYWORD: self.get_local,
            DATAFRAME_DATA_KEYWORD: self.get_dataframe
        }

        restoration_model = sources[restoration_model.source](restoration_model)
        check_returned_dataframe(restoration_model, required, self.required_columns)

        return restoration_model


    def get_local(self, restoration_model: BridgeRestorationModel) -> BridgeRestorationModel:
        con = sqlite3.connect(DATABASE_PATH)

        restoration_model.required_classification = ModelRepository.get_required_classification(
            restoration_model.TYPE, self.FACILITY_TYPE, restoration_model.model_name
        )

        data = pd.read_sql(
            """
                SELECT r.*,
                       c.class_name AS {}
                    FROM restoration AS r
                           LEFT JOIN
                           models AS m ON r.model_id = m.model_id
                           LEFT JOIN
                           facility_type AS fs ON m.facility_type_id = fs.facility_type_id
                           LEFT JOIN
                           class AS c ON r.class_id = c.class_id
                 WHERE fs.facility_type_name = :structure_type AND 
                       m.model_name = :name 
            """.format(restoration_model.required_classification), con, params={
                'structure_type': self.FACILITY_TYPE,
                'name': restoration_model.model_name
            })
        restoration_model.dataframe = data

        con.close()

        # restoration_model.dataframe = pd.read_pickle(ROOT_DIR + "\\data\\restorations\\" +
        #                                              restoration_model.restoration_type + ".pkl")
        return restoration_model

    def get_dataframe(self, restoration_model: BridgeRestorationModel):
        restoration_model.dataframe = restoration_model.dataframe.dataframe
        return restoration_model
