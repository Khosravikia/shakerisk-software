import sqlite3

import pandas as pd
from Definitions import DATABASE_PATH, DATAFRAME_DATA_KEYWORD, LOCAL_DATA_KEYWORD

from core.domain.entities.bridge.bridge_construction_cost_model import BridgeConstructionCostModel
from core.interfaces.gateways.repositories.bridge.bridge_construction_cost_repository_interface import \
    BridgeConstructionCostRepositoryInterface
from infrastructure.data.model_repository import ModelRepository
from infrastructure.data.utils import check_returned_dataframe


class BridgeConstructionCostRepository(BridgeConstructionCostRepositoryInterface):
    FACILITY_TYPE = 'bridge'
    required_columns = ['distribution_name']

    def get(self, construction_cost_model: BridgeConstructionCostModel, required=True) -> BridgeConstructionCostModel:
        sources = {
            LOCAL_DATA_KEYWORD: self.get_local,
            DATAFRAME_DATA_KEYWORD: self.get_dataframe
        }

        construction_cost_model = sources[construction_cost_model.source](construction_cost_model)
        check_returned_dataframe(construction_cost_model, required, self.required_columns)

        return construction_cost_model

    def get_local(self, construction_cost_model: BridgeConstructionCostModel) -> BridgeConstructionCostModel:
        con = sqlite3.connect(DATABASE_PATH)

        construction_cost_model.required_classification = ModelRepository.get_required_classification(
            construction_cost_model.TYPE, self.FACILITY_TYPE, construction_cost_model.model_name
        )

        data = pd.read_sql(
            """
                SELECT u.*,
                       d.distribution_name,
                       c.class_name AS {}
                  FROM unit_construction_cost AS u
                       LEFT JOIN
                       models AS m ON u.model_id = m.model_id
                       LEFT JOIN
                       facility_type AS fs ON m.facility_type_id = fs.facility_type_id
                       LEFT JOIN
                       class AS c ON u.class_id = c.class_id
                       LEFT JOIN
                       distribution AS d ON u.distribution_id = d.distribution_id
                 WHERE fs.facility_type_name = :facility_type AND 
                       m.model_name = :name 
            """.format(construction_cost_model.required_classification), con, params={
                'facility_type': self.FACILITY_TYPE,
                'name': construction_cost_model.model_name
            })
        construction_cost_model.dataframe = data

        con.close()

        # construction_cost_model.dataframe = pd.read_pickle(ROOT_DIR + "\\data\\repair_costs\\" +
        #                                                    construction_cost_model.construction_cost_type + ".pkl")
        return construction_cost_model

    def get_dataframe(self, construction_cost_model: BridgeConstructionCostModel):
        construction_cost_model.dataframe = construction_cost_model.dataframe.dataframe
        return construction_cost_model
