import sqlite3

import numpy as np
import pandas as pd

from Definitions import DATABASE_PATH, DATAFRAME_DATA_KEYWORD, LOCAL_DATA_KEYWORD
from infrastructure.data.utils import filter_location_select, check_returned_dataframe
from core.domain.entities.vs30 import Vs30
from core.interfaces.gateways.repositories.vs30_repository_interface import Vs30RepositoryInterface


class Vs30Repository(Vs30RepositoryInterface):

    required_columns = ['longitude', 'latitude', 'vs30']

    def get(self, vs30: Vs30, center_lat=None, center_long=None, deg_limit=None, required=True) -> Vs30:
        sources = {
            LOCAL_DATA_KEYWORD: self.get_local,
            'constant': self.get_constant,
            DATAFRAME_DATA_KEYWORD: self.get_dataframe
        }

        vs30 = sources[vs30.source](vs30=vs30, center_lat=center_lat, center_long=center_long, deg_limit=deg_limit)
        check_returned_dataframe(vs30, required, self.required_columns)

        return vs30

    def get_local(self, vs30: Vs30, center_lat=None, center_long=None, deg_limit=None) -> Vs30:

        con = sqlite3.connect(DATABASE_PATH)

        # Filter by state
        filter_state = None
        if vs30.filter_state:
            filter_state = con.cursor().execute(
                'SELECT state_id FROM state WHERE state_abbr = ?', [vs30.filter_state]).fetchone()[0]

        # Choose vs30 mapping
        vs30_col = 'vs30'
        if vs30.mapping:
            if vs30.mapping == 'USGS':
                vs30_col = 'vs30_slope_old'
            elif vs30.mapping == 'Updated':
                vs30_col = 'vs30'

        # Filter by location
        filter_location_str = filter_location_select(center_lat, center_long, table_name='v',
                                                     rec_area=vs30.filter_location, deg_limit=deg_limit)
        # min_lat = None
        # max_lat = None
        # min_lng = None
        # max_lng = None
        # if deg_limit:
        #     min_lat = center_lat - deg_limit
        #     max_lat = center_lat + deg_limit
        #     min_lng = center_long - deg_limit
        #     max_lng = center_long + deg_limit

        vs30_data = pd.read_sql('''
        SELECT  v.longitude,
                v.latitude,
                {} as vs30
        FROM    vs30 as v
        WHERE   (:filter_state IS Null OR v.state_id = :filter_state)
                {}
        AND     (vs30 != 0)
        '''.format(vs30_col, filter_location_str),
                                con,
                                params={
                                    'filter_state': filter_state,
                                    # 'filter_area': bool(deg_limit),
                                    # 'min_lat': min_lat,
                                    # 'max_lat': max_lat,
                                    # 'min_lng': min_lng,
                                    # 'max_lng': max_lng
                                }
        )
        vs30.dataframe = vs30_data

        # AND     (NOT :filter_area
        #             OR  (   v.longitude < :max_lng
        #                 AND v.longitude > :min_lng
        #                 AND v.latitude < :max_lat
        #                 AND v.latitude > :min_lat)
        #         )

        # # Old way
        # vs30_data = pd.read_pickle(Definitions.ROOT_DIR + "\\data\\vs30s\\" + vs30.filter_state + ".pkl")
        #
        # if vs30.mapping:
        #     if vs30.mapping == 'USGS':
        #         vs30_data = vs30_data.drop(columns='vs30_slope_new')
        #     elif vs30.mapping == 'Updated':
        #         vs30_data = vs30_data.drop(columns='vs30_slope_old')
        #
        #     vs30_data.columns = ['Longitude (deg)', 'Latitude (deg)', 'Vs30 (m/s)']
        #
        # if deg_limit:
        #     vs30_data = vs30_data[(center_long - deg_limit < vs30_data['Longitude (deg)']) &
        #                           (vs30_data['Longitude (deg)'] < center_long + deg_limit) &
        #                           (center_lat - deg_limit < vs30_data['Latitude (deg)']) &
        #                           (vs30_data['Latitude (deg)'] < center_lat + deg_limit)]
        #
        # vs30.dataframe = vs30_data.drop(vs30_data[vs30_data['Vs30 (m/s)'] == 0].index)
        return vs30

    def get_constant(self, vs30: Vs30, center_lat=None, center_long=None, deg_limit=None) -> Vs30:
        long, lat = np.meshgrid(np.linspace(center_long - deg_limit,
                                            center_long + deg_limit, 50),
                                np.linspace(center_lat - deg_limit,
                                            center_lat + deg_limit, 50))
        vs30.dataframe = pd.DataFrame(
            data=[long.reshape(-1), lat.reshape(-1), np.ones(long.size) * vs30.constant],
            index=['longitude', 'latitude', 'vs30']).T
        return vs30

    def get_dataframe(self, vs30: Vs30, center_lat=None, center_long=None, deg_limit=None) -> Vs30:
        vs30_data = vs30.dataframe.dataframe
        vs30.dataframe = vs30_data[(center_long - deg_limit < vs30_data['longitude']) &
                                   (vs30_data['longitude'] < center_long + deg_limit) &
                                   (center_lat - deg_limit < vs30_data['latitude']) &
                                   (vs30_data['latitude'] < center_lat + deg_limit)]
        return vs30
