from Definitions import ResourceError, ErrorMessages
from core.domain.entities.parameters.location.rectangular_area import RectangularArea


def filter_location_select(center_lat, center_lng, table_name, include_first_and=True, rec_area: RectangularArea = None, deg_limit=None):
    if not (rec_area or deg_limit):
        return ''

    elif not rec_area:
        boundaries = [
            center_lat - deg_limit,
            center_lat + deg_limit,
            center_lng - deg_limit,
            center_lng + deg_limit
        ]
    elif not deg_limit:
        boundaries = [
            rec_area.min_lat,
            rec_area.max_lat,
            rec_area.min_lng,
            rec_area.max_lng
        ]
    else:
        boundaries = [
            max(rec_area.min_lat, center_lat - deg_limit),
            min(rec_area.max_lat, center_lat + deg_limit),
            max(rec_area.min_lng, center_lng - deg_limit),
            min(rec_area.max_lng, center_lng + deg_limit)
        ]
    start = ''
    if include_first_and:
        start = 'AND '
    condition = """{5}
        {0}.latitude > {1} AND
        {0}.latitude < {2} AND
        {0}.longitude  > {3} AND
        {0}.longitude  < {4}
    """.format(table_name, *boundaries, start)

    return condition


def check_returned_dataframe(data, required=False, required_columns: list = None, required_classifications: list = None):

    if required:
        if len(data.dataframe) == 0:
            raise ResourceError(ErrorMessages.no_data(data))
            # raise ResourceError('No data found for "{}" object ({}) with provided properties.'.format(
            #     data.name, type(data).__name__))

    if required_columns is not None:
        if not set(required_columns).issubset(data.dataframe.columns):
            raise ResourceError(ErrorMessages.required_columns(data))
            # raise ResourceError(
            #     'Provided data for "{}" object ({}) does not have required columns.'.format(
            #         data.name, type(data).__name__))

        if required_classifications is not None:
            if not set(required_classifications).issubset(data.dataframe.columns):
                raise ResourceError(ErrorMessages.required_classification(data))
                # raise ResourceError(
                #     'Provided data for "{}" object ({}) does not have required classifications.'.format(
                #         data.name, type(data).__name__))
