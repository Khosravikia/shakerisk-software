import sqlite3

import pandas as pd

from Definitions import DATABASE_PATH, DATAFRAME_DATA_KEYWORD, LOCAL_DATA_KEYWORD
from core.domain.entities.building.building_repair_cost_model import BuildingRepairCostModel
from core.interfaces.gateways.repositories.building.building_repair_cost_repository_interface import\
    BuildingRepairCostRepositoryInterface
from infrastructure.data.utils import check_returned_dataframe


class BuildingRepairCostRepository(BuildingRepairCostRepositoryInterface):
    FACILITY_TYPE = 'building'
    required_columns = ['slight', 'moderate', 'extensive', 'complete']

    def get(self, repair_cost_model: BuildingRepairCostModel, required=True):
        sources = {
            LOCAL_DATA_KEYWORD: self.get_local,
            DATAFRAME_DATA_KEYWORD: self.get_dataframe
        }

        repair_cost_model = sources[repair_cost_model.source](repair_cost_model)
        check_returned_dataframe(repair_cost_model, required, self.required_columns)

        return repair_cost_model

    def get_local(self, repair_cost_model: BuildingRepairCostModel):
        con = sqlite3.connect(DATABASE_PATH)

        data = pd.read_sql(
            """
            SELECT r.*
              FROM repair_cost_ratio AS r
                   LEFT JOIN
                   models AS m ON r.model_id = m.model_id
                   LEFT JOIN
                   facility_type AS fs ON m.facility_type_id = fs.facility_type_id
             WHERE fs.facility_type_name = :facility_type AND 
                       m.model_name = :name 
            """, con, params={
                'facility_type': self.FACILITY_TYPE,
                'name': repair_cost_model.model_name
            })
        repair_cost_model.dataframe = data

        con.close()
        # repair_cost_type = repair_cost_model.repair_cost_type + '_buildings'
        #
        # repair_cost_model.dataframe = pd.read_pickle(ROOT_DIR + "\\data\\repair_costs_ratios\\" + repair_cost_type + ".pkl")
        return repair_cost_model

    def get_dataframe(self, repair_cost_model: BuildingRepairCostModel):
        repair_cost_model.dataframe = repair_cost_model.dataframe.dataframe
        return repair_cost_model
