import sqlite3

import pandas as pd

from Definitions import DATABASE_PATH, DATAFRAME_DATA_KEYWORD, LOCAL_DATA_KEYWORD
from core.domain.entities.building.building_fragility_model import BuildingFragilityModel
from core.interfaces.gateways.repositories.building.building_fragility_model_repository_interface import\
    BuildingFragilityRepositoryInterface
from infrastructure.data.model_repository import ModelRepository
from infrastructure.data.utils import check_returned_dataframe


class BuildingFragilityRepository(BuildingFragilityRepositoryInterface):
    FACILITY_TYPE = 'building'
    required_columns = ['slight:median', 'slight:dispersion', 'moderate:median', 'moderate:dispersion',
                        'extensive:median', 'extensive:dispersion', 'complete:median', 'complete:dispersion']

    def get(self, fragility_model: BuildingFragilityModel, required=True) -> BuildingFragilityModel:
        sources = {
            LOCAL_DATA_KEYWORD: self.get_local,
            DATAFRAME_DATA_KEYWORD: self.get_dataframe
        }

        fragility_model = sources[fragility_model.source](fragility_model)
        check_returned_dataframe(fragility_model, required, self.required_columns)

        # if required and len(fragility_model.dataframe) == 0:
        #     raise ValueError('No fragility model found for provided options.' +
        #                      'Facility type: "{}", Model name: "{}", Intensity measure: "{}"'.format(
        #                          self.FACILITY_TYPE, fragility_model.model_name, fragility_model.measure
        #                      )
        #                      )

        return fragility_model

    def get_local(self, fragility_model: BuildingFragilityModel) -> BuildingFragilityModel:
        con = sqlite3.connect(DATABASE_PATH)

        fragility_model.required_classification = ModelRepository.get_required_classification(
            fragility_model.TYPE, self.FACILITY_TYPE, fragility_model.model_name
        )

        data = pd.read_sql("""
                            SELECT f.*,
                                   c.class_name AS {}
                              FROM fragility AS f
                                   LEFT JOIN
                                   models AS m ON f.model_id = m.model_id
                                   LEFT JOIN
                                   facility_type AS fs ON m.facility_type_id = fs.facility_type_id
                                   LEFT JOIN
                                   class AS c ON f.class_id = c.class_id
                                   LEFT JOIN
                                   intensity_measure AS im ON f.im_id = im.im_id
                             WHERE fs.facility_type_name = :facility_type AND 
                                   m.model_name = :name AND 
                                   im.im_name = :im;
                        """.format(fragility_model.required_classification), con, params={
            'facility_type': self.FACILITY_TYPE,
            'name': fragility_model.model_name,
            'im': fragility_model.measure
        })

        # Deal with "seismic_consideration" if more than one option is available
        if not data['seismic_consideration'].isna().all():
            if len(data['seismic_consideration'].unique()) > 1:
                if fragility_model.seismic_consideration:
                    data = data[data['seismic_consideration'] == fragility_model.seismic_consideration]
                else:
                    raise ValueError('A seismic consideration should be chosen when more than one is available.\n' +
                                     'available options: {}'.format(data['seismic_consideration'].unique())
                                     )


        fragility_model.dataframe = data

        con.close()

        # fragility_type = fragility_model.fragility_type + '_buildings'
        #
        # if fragility_type.startswith('HAZUS'):
        #     fragility_data = pd.read_pickle(ROOT_DIR + "\\data\\fragilities\\" + fragility_type + ".pkl")
        # else:
        #     fragility_data = pd.read_pickle(ROOT_DIR + "\\data\\fragilities\\" + fragility_type + '_' + im + ".pkl")
        # fragility_model.dataframe = fragility_data
        return fragility_model

    def get_dataframe(self, fragility_model: BuildingFragilityModel) -> BuildingFragilityModel:
        fragility_model.dataframe = fragility_model.dataframe.dataframe
        return fragility_model
