import sqlite3

from Definitions import DATABASE_PATH, DATAFRAME_DATA_KEYWORD, LOCAL_DATA_KEYWORD
from core.domain.entities.building.building_construction_cost_model import BuildingConstructionCostModel
from core.interfaces.gateways.repositories.building.building_construction_cost_repository_interface import\
    BuildingConstructionCostRepositoryInterface
from infrastructure.data.model_repository import ModelRepository
from infrastructure.data.utils import check_returned_dataframe


class BuildingConstructionCostRepository(BuildingConstructionCostRepositoryInterface):
    FACILITY_TYPE = 'building'

    def get(self, construction_cost_model: BuildingConstructionCostModel, required=False):
        sources = {
            LOCAL_DATA_KEYWORD: self.get_local,
            DATAFRAME_DATA_KEYWORD: self.get_dataframe
        }

        construction_cost_model = sources[construction_cost_model.source](construction_cost_model)
        check_returned_dataframe(construction_cost_model, required)

        return construction_cost_model

    def get_local(self, construction_cost_model: BuildingConstructionCostModel):
        con = sqlite3.connect(DATABASE_PATH)

        construction_cost_model.required_classification = ModelRepository.get_required_classification(
            construction_cost_model.TYPE, self.FACILITY_TYPE, construction_cost_model.model_name
        )

        # Buildings have their cost on the source data

        con.close()
        return construction_cost_model

    def get_dataframe(self, construction_cost_model: BuildingConstructionCostModel):
        return construction_cost_model  # Buildings have their cost on the source data
