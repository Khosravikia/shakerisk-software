import sqlite3

import pandas as pd

from Definitions import DATABASE_PATH, DATAFRAME_DATA_KEYWORD, LOCAL_DATA_KEYWORD
from core.domain.entities.building.building import Building
from core.interfaces.gateways.repositories.building.building_repository_interface import BuildingRepositoryInterface
from infrastructure.data.utils import check_returned_dataframe

HAZUS_BUILDING_CLASSES = ['W1', 'W2', 'S1L', 'S1M', 'S1H', 'S2L', 'S2M', 'S2H', 'S3',
                          'S4L', 'S4M', 'S4H', 'S5L', 'S5M', 'S5H', 'C1L', 'C1M', 'C1H', 'C2L',
                          'C2M', 'C2H', 'C3L', 'C3M', 'C3H', 'PC1', 'PC2L', 'PC2M', 'PC2H',
                          'RM1L', 'RM1M', 'RM2L', 'RM2M', 'RM2H', 'URML', 'URMM', 'MH']


class BuildingRepository(BuildingRepositoryInterface):
    required_columns = ['state_id', 'tract_id', 'longitude', 'latitude', 'cost']

    def get(self, building: Building, center_lat=None, center_long=None, deg_limit=None,
            required_classifications: set = None, required=True) -> Building:
        sources = {
            LOCAL_DATA_KEYWORD: self.get_local,
            DATAFRAME_DATA_KEYWORD: self.get_dataframe
        }

        building = sources[building.source](building, center_lat, center_long, deg_limit, required_classifications)
        check_returned_dataframe(building, required, self.required_columns, required_classifications)

        return building

    def get_local(self, building: Building, center_lat=None, center_long=None, deg_limit=None,
                  required_classifications: set = None) -> Building:
        required_classifications -= {None}

        con = sqlite3.connect(DATABASE_PATH)

        # Filter by state
        filter_state = None
        if building.filter_state:
            filter_state = con.cursor().execute(
                'SELECT state_id FROM state WHERE state_abbr = ?', [building.filter_state]).fetchone()[0]

        # # Filter by degree limit
        min_lat = None
        max_lat = None
        min_lng = None
        max_lng = None
        if deg_limit:
            min_lat = center_lat - deg_limit
            max_lat = center_lat + deg_limit
            min_lng = center_long - deg_limit
            max_lng = center_long + deg_limit

        # Add classes
        select_part = ""
        join_part = ""
        where_part = ""
        if len(required_classifications):
            for i, cls in enumerate(required_classifications):
                # Todo: Take care of 'cost'. It is defined with class, not unit construction cost.
                select_part += ",\n c{0}.class_name AS '{1}',\n bc{0}.cost AS cost".format(i, cls)
                join_part += """
                    LEFT JOIN
                       building_classification AS bc{0} ON bf.tract_unique_id = bc{0}.tract_unique_id
                       LEFT JOIN
                       class AS c{0} ON bc{0}.class_id = c{0}.class_id
                       LEFT JOIN
                       classification AS cl{0} ON c{0}.classification_id = cl{0}.classification_id
                """.format(i)
                where_part += """
                    c{0}.class_id IS NOT NULL AND 
                    cl{0}.classification_name = '{1}' AND
                """.format(i, cls)

        building_data = pd.read_sql('''
        SELECT bf.*
            {}
          FROM building_facility AS bf
            {}
         WHERE  {}
                (:filter_state IS Null OR bf.state_id = :filter_state) AND
               (bf.longitude < :max_lng AND 
                bf.longitude > :min_lng AND 
                bf.latitude < :max_lat AND 
                bf.latitude > :min_lat);
                                        '''.format(select_part, join_part, where_part),
                                    con,
                                    params={
                                        'filter_state': filter_state,
                                        'filter_area': bool(deg_limit),
                                        'min_lat': min_lat,
                                        'max_lat': max_lat,
                                        'min_lng': min_lng,
                                        'max_lng': max_lng
                                    }
                                    )

        # building_data = pd.read_pickle(Definitions.ROOT_DIR + "\\data\\buildings\\" + building.filter_state + ".pkl")
        # rows = []
        # for _, struct in building_data.iterrows():
        #     for clas in HAZUS_BUILDING_CLASSES:
        #         if struct[clas] != 0:
        #             temp_dict = {
        #                 'state_id': struct['state_id'],  # Needed in outputs
        #                 'tract_id': struct['tract_id'],
        #                 'longitude': struct['longitude'],
        #                 'latitude': struct['latitude'],
        #                 'hazus_class_id': clas,
        #                 'Cost': struct[clas]
        #             }
        #             rows.append(temp_dict)
        # building_data = pd.DataFrame(rows)

        # if deg_limit:
        #     building_data = building_data[
        #         (center_long - deg_limit < building_data['Longitude']) &
        #         (building_data['Longitude'] < center_long + deg_limit) &
        #         (center_lat - deg_limit < building_data['Latitude']) &
        #         (building_data['Latitude'] < center_lat + deg_limit)]

        building.dataframe = building_data
        return building

    def get_dataframe(self, building: Building, center_lat=None, center_long=None, deg_limit=None,
                      required_classifications: set = None):
        building_data = building.dataframe.dataframe
        if deg_limit:
            building.dataframe = building_data[
                (center_long - deg_limit < building_data['longitude']) &
                (building_data['longitude'] < center_long + deg_limit) &
                (center_lat - deg_limit < building_data['latitude']) &
                (building_data['latitude'] < center_lat + deg_limit)]

        building.dataframe = building_data
        return building
