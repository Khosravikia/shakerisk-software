from pathlib import Path
from core.domain.entities.network.transportation_network import TransportationNetwork
from core.interfaces.gateways.repositories.network.transportaion_network_repository_interface import TransportationNetworkRepositoryInterface
from infrastructure.network.WF.network import Network
from Definitions import ROOT_DIR, LOCAL_DATA_KEYWORD


class TransportationNetworkRepository(TransportationNetworkRepositoryInterface):

    def get(self, transportation_network: TransportationNetwork):
        sources = {
            LOCAL_DATA_KEYWORD: self.get_local
        }
        return sources[transportation_network.source](transportation_network)

    def get_local(self, transportation_network: TransportationNetwork):
        net = Network(
            str(Path(ROOT_DIR, "data\\network_models\\", transportation_network.location_name, transportation_network.location_name + "_net.tntp")),
            str(Path(ROOT_DIR, "data\\network_models\\", transportation_network.location_name, transportation_network.location_name + "_trips.tntp")))
        return net
