import sqlite3

from Definitions import ROOT_DIR, DATABASE_PATH
from core.interfaces.gateways.repositories.model_repository_interface import ModelRepositoryInterface


class ModelRepository(ModelRepositoryInterface):

    def __init__(self):
        pass

    # List item: [model_type, structure_type, name]
    @staticmethod
    def get_required_classification(model_type, structure_type, model_name):
        con = sqlite3.connect(DATABASE_PATH)
        cur = con.cursor()

        result = cur.execute("""
        SELECT cl.classification_name
          FROM models AS m
               LEFT JOIN
               facility_type AS ft ON m.facility_type_id = ft.facility_type_id
               LEFT JOIN
               classification AS cl ON m.required_classification_id = cl.classification_id
         WHERE m.model_type = ? AND 
               ft.facility_type_name = ? AND 
               m.model_name = ?
        """, [model_type, structure_type, model_name]).fetchone()

        required_classification_name = None
        if result:
            required_classification_name = result[0]

        con.close()
        return required_classification_name
