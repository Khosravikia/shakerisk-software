from core.interfaces.gateways.spatial_correlations_loader_interface import SpatialCorrelationLoaderInterface
from infrastructure.spatial_correlations import *  # Using __init__.py file, this statement will load all classes in gmm folder


class SpatialCorrelationLoader(SpatialCorrelationLoaderInterface):

    def __call__(self, spatial_corr_name):
        self.spatial_corr_name = spatial_corr_name

        # Todo: Take care of repetitive names in here and Definitions
        spatial_corr_list = {
            'jayram_baker_2009': JayramBaker2009,
                             }

        self.model = spatial_corr_list[self.spatial_corr_name]()

        return self

    def correlation_matrix(self, distance_matrix, period, vs30_clustering=True):
        return self.model.correlation_matrix(distance_matrix, period, vs30_clustering=True)
