import numpy as np


class JayramBaker2009:

    def correlation_matrix(self, distance_matrix, period, vs30_clustering=True):
        t = period
        if t == -1:
            t = 1

        b = 0
        if t < 1:
            if vs30_clustering:
                b = 8.5 + 17.2 * t
            else:
                b = 40.7 - 15.0 * t
        elif t >= 1:
            b = 22.0 + 3.7 * t

        corr_matrix = np.exp(-3 * distance_matrix / b)

        return corr_matrix


