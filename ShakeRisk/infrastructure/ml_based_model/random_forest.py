from sklearn.ensemble import RandomForestRegressor

from core.interfaces.gateways.ml_based_model_interface import MLBasedModelInterface
from core.dto.gateway_dtos.ml_model_options.random_forest_options import RandomForestOptions


class RandomForest(MLBasedModelInterface):

    MODEL_OBJECT_TYPE = 'Scikit-Learn'

    def __init__(self, ml_model_options: RandomForestOptions):
        self.ml_model_options = ml_model_options
        self.model: RandomForestRegressor

    def fit(self, input_data, output_data):
        model = RandomForestRegressor(n_estimators=self.ml_model_options.n_estimators,
                                      criterion=self.ml_model_options.criterion,
                                      max_features=self.ml_model_options.max_features,
                                      # max_depth=self.ml_model_options.max_depth,
                                      min_samples_split=self.ml_model_options.min_samples_split,
                                      min_samples_leaf=self.ml_model_options.min_samples_leaf,
                                      bootstrap=self.ml_model_options.bootstrap,
                                      )
        # Todo: Take care of 1d output
        model.fit(input_data, output_data.values.ravel())
        self.model = model
        return model

    def predict(self, input_data):
        return self.model.predict(input_data).reshape(-1, 1)
