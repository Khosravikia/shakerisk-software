from abc import abstractmethod

from core.interfaces.gateways.ml_based_model_interface import MLBasedModelInterface
from core.interfaces.gateways.ml_based_model_loader_interface import MLBasedModelLoaderInterface
from infrastructure.ml_based_model import *  # Using __init__.py file, this statement will load all classes in ml_based_model folder


class MLBasedModelCreator(MLBasedModelLoaderInterface):
    # Todo: Take care of repetitive names in here and Object Ids
    ml_model_list = {
        'MLM_ANN': DenseNN,
        'MLM_random_forest': RandomForest,
        'MLM_SVM': SVM,
        'MLM_linear': Linear
    }

    def __call__(self, ml_model_options) -> MLBasedModelInterface:
        return self.ml_model_list[ml_model_options.model_name](ml_model_options)
