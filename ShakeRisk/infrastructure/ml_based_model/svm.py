from sklearn.svm import SVR

from core.interfaces.gateways.ml_based_model_interface import MLBasedModelInterface
from core.dto.gateway_dtos.ml_model_options.svm_options import SVMOptions


class SVM(MLBasedModelInterface):

    MODEL_OBJECT_TYPE = 'Scikit-Learn'

    def __init__(self, ml_model_options: SVMOptions):

        self.ml_model_options = ml_model_options
        self.model: SVMOptions

    def fit(self, input_data, output_data):
        model = SVR(kernel=self.ml_model_options.kernel,
                    gamma=self.ml_model_options.gamma,
                    C=self.ml_model_options.c,
                    epsilon=self.ml_model_options.epsilon
                    )
        # Todo: Take care of 1d output
        model.fit(input_data, output_data.values.ravel())
        self.model = model
        return model

    def predict(self, input_data):
        return self.model.predict(input_data).reshape(-1, 1)
