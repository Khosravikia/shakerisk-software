import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

from core.interfaces.gateways.ml_based_model_interface import MLBasedModelInterface
from core.dto.gateway_dtos.ml_model_options.dense_nn_options import DenseNNOptions


class DenseNN(MLBasedModelInterface):

    MODEL_OBJECT_TYPE = 'TensorFlow'

    def __init__(self, ml_model_options: DenseNNOptions):

        self.ml_model_options = ml_model_options
        self.model: keras.Sequential

    def fit(self, input_data: pd.DataFrame, output_data: pd.DataFrame):
        # Build Model
        def build_model():
            layer_count = len(self.ml_model_options.number_of_neurons)

            model = keras.Sequential()
            # Input layer
            model.add(layers.Dense(self.ml_model_options.number_of_neurons[0],
                                   activation=self.ml_model_options.input_activation_function[0],
                                   input_shape=[len(input_data.columns)]
                                   ))

            # More layer if needed
            if layer_count > 1:
                for index in range(1, layer_count):
                    model.add(layers.Dense(self.ml_model_options.number_of_neurons[index],
                                           activation=self.ml_model_options.input_activation_function[index]))

            # Output layer
            # output_activation = None
            # if self.ml_model_options.output_activation_function:
            #     output_activation = self.ml_model_options.output_activation_function
            model.add(layers.Dense(len(output_data.columns),
                                   # activation=output_activation
                                   ))

            # Choose optimizer
            optimizer_dic = {
                'Adam': tf.keras.optimizers.Adam,
                'RMSprop': tf.keras.optimizers.RMSprop,
                'SGD': tf.keras.optimizers.SGD
            }
            if self.ml_model_options.optimizer in optimizer_dic:
                optimizer = optimizer_dic[self.ml_model_options.optimizer](self.ml_model_options.learning_rate)
            else:
                raise NotImplementedError('Selected optimizer is not implemented.')

            model.compile(loss=self.ml_model_options.loss_function,
                          optimizer=optimizer,
                          # metrics=[self.ml_model_options.metrics]
                          )
            return model

        model = build_model()
        model.summary()  # Print model summary

        callbacks = []
        if self.ml_model_options.early_stop_patience:
            callbacks.append(tf.keras.callbacks.EarlyStopping(monitor='loss', patience=self.ml_model_options.early_stop_patience))
        # callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=self.ml_model_options.early_stop_patience)
        history = model.fit(
            input_data.values, output_data.values,
            epochs=self.ml_model_options.epochs_num,
            validation_split=self.ml_model_options.validation_fraction, verbose=0,
            callbacks=callbacks
            # callbacks=[tfdocs.modeling.EpochDots()]
        )

        # hist = pd.DataFrame(history.history)
        # hist['epoch'] = history.epoch
        # hist.tail()
        self.model = model
        return self.model

    def predict(self, input_data):
        return self.model.predict(input_data)

        # dataset = self.dataframe.dataframe
        # # Splitting train and test
        # train_dataset = dataset.sample(frac=self.ml_model_options.train_fraction, random_state=0)
        # test_dataset = dataset.drop(train_dataset.index)
        #
        # # Getting data information
        # train_stats = train_dataset.describe()
        # train_stats = train_stats[self.input_columns]
        # train_stats = train_stats.transpose()
        #
        # # Separating Outputs
        # train_labels = train_dataset[self.output_columns]
        # train_dataset = train_dataset[self.input_columns]
        # test_labels = test_dataset[self.output_columns]
        # test_dataset = test_dataset[self.input_columns]
        #
        # # Normalize
        # def norm(x):
        #     return (x - train_stats['mean']) / train_stats['std']
        #
        # normed_train_data = norm(train_dataset)
        # normed_test_data = norm(test_dataset)
        #
        # # Build Model
        # def build_model():
        #     layer_count = len(self.ml_model_options.number_of_neurons)
        #
        #     model = keras.Sequential()
        #     # Input layer
        #     model.add(layers.Dense(self.ml_model_options.number_of_neurons[0],
        #                            activation=self.ml_model_options.input_activation_function[0],
        #                            input_shape=[len(train_dataset.keys())]))
        #
        #     # More layer if needed
        #     if layer_count > 1:
        #         for index in range(1, layer_count):
        #             model.add(layers.Dense(self.ml_model_options.number_of_neurons[index],
        #                                    activation=self.ml_model_options.input_activation_function[index]))
        #
        #     # Output layer
        #     output_activation = None
        #     if self.ml_model_options.output_activation_function:
        #         output_activation = self.ml_model_options.output_activation_function
        #     model.add(layers.Dense(len(self.output_columns), activation=output_activation))
        #
        #     # Choose optimizer
        #     optimizer_dic = {
        #         'Adam': tf.keras.optimizers.Adam,
        #         'RMSprop': tf.keras.optimizers.RMSprop,
        #         'SGD': tf.keras.optimizers.SGD
        #     }
        #     if self.ml_model_options.optimizer in optimizer_dic:
        #         optimizer = optimizer_dic[self.ml_model_options.optimizer]()
        #     else:
        #         raise NotImplementedError('Selected optimizer is not implemented.')
        #
        #     model.compile(loss=self.ml_model_options.loss_function,
        #                   optimizer=optimizer,
        #                   metrics=[self.ml_model_options.metrics])
        #     return model
        #
        # model = build_model()
        # model.summary()  # Print model summary
        #
        # history = model.fit(
        #     normed_train_data, train_labels,
        #     epochs=self.ml_model_options.epochs_num,
        #     validation_split=self.ml_model_options.validation_fraction, verbose=0,
        #     # callbacks=[tfdocs.modeling.EpochDots()]
        # )
        #
        # hist = pd.DataFrame(history.history)
        # hist['epoch'] = history.epoch
        # hist.tail()
        #
        # test_eval = model.evaluate(normed_test_data, test_labels, verbose=2)
        #
        # return [model, hist, test_eval]
