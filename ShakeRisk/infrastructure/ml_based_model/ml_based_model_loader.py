import json
from tensorflow.keras.models import load_model
import joblib

import numpy as np

from core.methods.pre_processing_methods import pre_process
from core.domain.entities.ml_trained_model import MLTrainedModel
from core.dto.gateway_dtos.ml_model_options.ml_model_additional_info import MLModelAdditionalInfo


class MLBasedModelLoader:

    def __init__(self, trained_model: MLTrainedModel):
        # self.model_type = trained_model.model_type
        self.model_path = trained_model.model_path
        self.input_names_orders = trained_model.input_names_orders
        self.additional_info_path = trained_model.additional_data_path
        self.additional_info: MLModelAdditionalInfo = self._load_model_additional_info()
        self.model = self._load_model()

    def _load_model(self):
        if self.additional_info.model_type == 'TensorFlow':
            return load_model(self.model_path)
        elif self.additional_info.model_type == 'Scikit-Learn':
            return joblib.load(self.model_path)
        else:
            raise TypeError('Saved model is not supported')

    def _load_model_additional_info(self):
        return MLModelAdditionalInfo.from_dict(json.load(open(self.additional_info_path)))

    def predict(self, input_data):

        if self.additional_info.pre_processing.type != "none":
            [input_data, *_] = pre_process(input_data, self.additional_info.pre_processing.type,
                                           *self.additional_info.pre_processing.args)

        predicted_output = None
        if self.additional_info.model_type == 'TensorFlow':
            predicted_output = self.model.predict(input_data)
        elif self.additional_info.model_type == 'Scikit-Learn':
            predicted_output = self.model.predict(input_data).reshape(-1, 1)

        if self.additional_info.output_transformation.type != "none":
            if self.additional_info.output_transformation.type == "lognormal":
                predicted_output = np.exp(predicted_output)

        return predicted_output
