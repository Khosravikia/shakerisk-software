from sklearn.linear_model import LinearRegression

from core.interfaces.gateways.ml_based_model_interface import MLBasedModelInterface
from core.dto.gateway_dtos.ml_model_options.linear_options import LinearOptions


class Linear(MLBasedModelInterface):

    MODEL_OBJECT_TYPE = 'Scikit-Learn'

    def __init__(self, ml_model_options: LinearOptions):
        self.ml_model_options = ml_model_options
        self.model: LinearRegression

    def fit(self, input_data, output_data):
        model = LinearRegression(fit_intercept=self.ml_model_options.fit_intercept)

        # Todo: Take care of 1d output
        model.fit(input_data, output_data.values.ravel())
        self.model = model
        return model

    def predict(self, input_data):
        return self.model.predict(input_data).reshape(-1, 1)
