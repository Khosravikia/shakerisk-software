from abc import abstractmethod
from core.dto.usecase_requests.request_object import ValidRequestObject
from core.dto.usecase_responses.response_object import ResponseSuccess


class OutputBase:

    @abstractmethod
    def __init__(self, request: ValidRequestObject, response: ResponseSuccess):
        raise NotImplementedError

    @abstractmethod
    def execute(self) -> dict:
        raise NotImplementedError
