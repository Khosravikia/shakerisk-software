import logging

from Definitions import ErrorMessages
from core.dto.usecase_responses.response_object import ResponseFailure
from .output_base import OutputBase
from .output_object import OutputObject
from .figure_utils import figure_html_converter
from .file_utils import write_file

# def create_output_from_error(response: ResponseFailure):
#     status_code = 400
#     if response.type == response.PARAMETERS_ERROR:
#         status_code = 400
#     elif response.type == response.SYSTEM_ERROR:
#         status_code = 500
#     elif response.type == response.RESOURCE_ERROR:
#         status_code = 404
#     return {
#         INFO: {
#             SUCCESS: False,
#             STATUS: status_code
#         },
#         CONSOLE: [response.message]}


class Handler:

    def __init__(self, usecase_output: OutputBase, request, response, output_path):
        self.usecase_output = usecase_output
        self.request = request
        self.response = response
        self.output_path = output_path

    def execute(self) -> OutputObject:
        output = self.usecase_output(self.request, self.response).execute()

        # Convert figures to HTML format
        # if bool(output.get(FIGURE, False)):
        #     if len(output[FIGURE]):
        #         for figure_type, figure_list in output[FIGURE].items():
        #             for index, figure in enumerate(figure_list):
        #                 output[FIGURE][figure_type][index] = {
        #                     next(iter(figure.keys())): figure_html_converter(figure_type, next(iter(figure.values())))}
        output.figure.apply(figure_html_converter, replace=True)

        # Write output files
        # output_path = getattr(self.request, OUTPUT_PATH, False)
        if self.output_path:
            for file_type, file_name, file in output.file.items():
                try:
                    write_file(file_type, self.output_path, file_name, file)
                except PermissionError as ex:
                    logging.exception("Exception happened while writing to file.")
                    output.console.add_error(ErrorMessages.write_file_access(ex))
                    # output.console.add_error(
                    #     "An error happened while writing to file."
                    #     " Make sure file is not open in another application.\n {}".format(
                    #         str(ex)))
                except Exception as ex:
                    logging.exception("Exception happened while writing to file.")
                    output.console.add_error(ErrorMessages.write_file_general(ex))
                    # output.console.add_error("An error happened while writing to file.\n {}:".format(str(ex)))
            # for write_type, data_list in output[FILE].items():
            #     for data in data_list:
            #         # Only skip file with error (write other files if any)
            #         try:
            #             write_file(write_type, self.output_path, next(iter(data.keys())), next(iter(data.values())))
            #         except PermissionError as ex:
            #             logging.exception("Exception happened when trying to write file.")
            #             output.console.add_error(
            #                 "An error happened when trying to write file."
            #                 " Make sure it is not open in another application.\n Error: {}".format(
            #                     str(ex)))
            #             # output[CONSOLE].append([
            #             #     CONSOLE_ERROR,
            #             #     "An error happened when trying to write file."
            #             #     " Make sure it is not open in another application.\n Error: {}".format(
            #             #         str(ex))])
            #         except Exception as ex:
            #             logging.exception("Exception happened when trying to write file.")
            #             output[CONSOLE].append(
            #                 "An error happened when trying to write file.\n {}:".format(str(ex)))

        # Add default info to output
        # output.info.name(self.request.name)
        output.info.name = self.request.name
        # output.update({
        #     INFO: {
        #         ANALYSIS_GIVEN_NAME: self.request.name,
        #         SUCCESS: True
        #     }})

        # Omit files from output and return
        output.file.clear()
        # output = output.to_dict()
        # output.pop(FILE, None)

        return output

    @staticmethod
    def create_failed_output(msg, analysis_name=None) -> OutputObject:
        error = ""
        if isinstance(msg, str):
            error = msg
        elif isinstance(msg, Exception):
            error = str(msg)
        elif isinstance(msg, ResponseFailure):
            error = msg.message
        else:
            error = msg

        output = OutputObject(analysis_name)
        output.info.success = False
        output.console.add_error(error)
        return output
