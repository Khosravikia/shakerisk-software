import json
import pickle
import joblib

from .output_types import *


def write_file(write_type, output_path, file_name, data):
    if write_type == DF_TO_CSV:
        data.to_csv(output_path + '\\' + file_name + '.csv', index=False)
    elif write_type == TF_MODEL_TO_FILE:
        data.save(output_path + '\\' + file_name)
    elif write_type == SK_LEARN_MODEL_TO_FILE:
        joblib.dump(data, output_path + '\\' + file_name + '.joblib')
    elif write_type == DICT_TO_JSON:
        json.dump(data, open(output_path + '\\' + file_name + '.json', 'w'), indent=4)
    elif write_type == OBJECT_TO_PICKLE_FILE:
        pickle.dump(data, open(output_path + '\\' + file_name + '.pickle', "wb"))
