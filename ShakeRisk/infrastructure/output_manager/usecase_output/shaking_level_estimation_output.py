import json

import folium
from folium.plugins import Fullscreen
import plotly.graph_objects as go
from branca.colormap import LinearColormap

from Definitions import IM_DISPLAY_NAMES, LIMIT_STATES, JET_COLORS, ROOT_DIR
from core.dto.usecase_requests.shaking_level_estimation_request import ShakingLevelEstimationRequest
from core.dto.usecase_responses.response_object import ResponseSuccess
from infrastructure.output_manager.figure_utils import plotly_fig2array
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *

# Todo: Take care of loading geoJsons

color_dict = {
    LIMIT_STATES[0]: 'black',
    LIMIT_STATES[1]: 'green',
    LIMIT_STATES[2]: 'beige',
    LIMIT_STATES[3]: 'orange',
    LIMIT_STATES[4]: 'red'
}


class ShakingLevelEstimationOutput(OutputBase):
    def __init__(self, request: ShakingLevelEstimationRequest, response: ResponseSuccess):
        self.df = response.value
        self.im = request.intensity_model.measure
        self.eq_lat = request.earthquake.location.latitude
        self.eq_long = request.earthquake.location.longitude
        self.eq_mw = request.earthquake.magnitude
        self.deg_limit = request.deg_limit

    def execute(self):
        [contour_fig, imap, imap_tract] = self.plot()

        # output = {
        #     FIGURE: {
        #         FOLIUM_MAP: [{'Map View': imap},
        #                      {'Map Tract View': imap_tract}],
        #         PLOTLY_GRAPH: [{'Shaking Level Graph': contour_fig}]
        #     },
        #     CONSOLE: [],
        #     FILE: {
        #         DF_TO_CSV: [{'shaking_evaluation': self.df}]
        #     }
        # }

        output = OutputObject()

        output.figure.add_folium_map('Map View', imap)
        output.figure.add_folium_map('Map Tract View', imap_tract)
        output.figure.add_plotly_graph('Shaking Level Graph', contour_fig)

        output.file.add_dataframe_to_csv('shaking_evaluation', self.df)

        return output

    def plot(self):
        fig_data = go.Contour(
            z=self.df[self.im],
            x=self.df['longitude'],  # horizontal axis
            y=self.df['latitude'],  # vertical axis
            colorscale='Jet',
            colorbar=dict(
                title=IM_DISPLAY_NAMES[self.im]),
            contours_coloring='heatmap',
            line_width=0,
            name='Shaking Levels'
        )

        # eq marker
        eq_marker = go.Scatter(x=[self.eq_long], y=[self.eq_lat], name='Earthquake',
                               marker_symbol='star', marker_size=15, marker_color="black")

        lay = go.Layout(font=dict(
            family="Times New Roman",
            size=12
        )
        )
        contour_fig = go.Figure(data=[fig_data, eq_marker], layout=lay)

        # Map
        imap = folium.Map(location=(self.eq_lat, self.eq_long), zoom_start=12, control_scale=True)

        # Change contour layout to get it ready for plotting on the map
        map_lay = go.Layout(
            paper_bgcolor='rgba(0,0,0,0)',
            plot_bgcolor='rgba(0,0,0,0)',
            xaxis={'visible': False},
            yaxis={'visible': False},
            margin={'l': 0, 'r': 0, 't': 0, 'b': 0}
        )

        map_fig = go.Figure(data=fig_data, layout=map_lay)
        map_fig.update_traces(showscale=False)
        img = plotly_fig2array(map_fig, 1000, 1000)

        folium.raster_layers.ImageOverlay(
            name='Shaking Levels',
            image=img,
            bounds=[[self.eq_lat - self.deg_limit, self.eq_long - self.deg_limit],
                    [self.eq_lat + self.deg_limit, self.eq_long + self.deg_limit]],
            opacity=0.5
        ).add_to(imap)

        # Color Bar
        LinearColormap(JET_COLORS, vmin=self.df[self.im].min(), vmax=self.df[self.im].max(),
                       caption='Shaking Level: {}'.format(IM_DISPLAY_NAMES[self.im])).add_to(imap)

        # Eq marker
        folium.Marker(location=[self.eq_lat, self.eq_long], icon=folium.Icon(
            color='black', icon='star'), tooltip='Magnitude: {}'.format(self.eq_mw)).add_to(imap)

        folium.LayerControl().add_to(imap)
        Fullscreen().add_to(imap)

        # Map
        imap_tract = folium.Map(location=(self.eq_lat, self.eq_long), zoom_start=12, control_scale=True)

        # Change contour layout to get it ready for plotting on the map
        map_lay = go.Layout(
            paper_bgcolor='rgba(0,0,0,0)',
            plot_bgcolor='rgba(0,0,0,0)',
            xaxis={'visible': False},
            yaxis={'visible': False},
            margin={'l': 0, 'r': 0, 't': 0, 'b': 0}
        )

        map_fig = go.Figure(data=fig_data, layout=map_lay)
        map_fig.update_traces(showscale=False)
        img = plotly_fig2array(map_fig, 1000, 1000)

        folium.raster_layers.ImageOverlay(
            name='Shaking Levels',
            image=img,
            bounds=[[self.eq_lat - self.deg_limit, self.eq_long - self.deg_limit],
                    [self.eq_lat + self.deg_limit, self.eq_long + self.deg_limit]],
            opacity=0.5
        ).add_to(imap_tract)

        # Color Bar
        LinearColormap(JET_COLORS, vmin=self.df[self.im].min(), vmax=self.df[self.im].max(),
                       caption='Shaking Level: {}'.format(IM_DISPLAY_NAMES[self.im])).add_to(imap_tract)

        # Eq marker
        folium.Marker(location=[self.eq_lat, self.eq_long], icon=folium.Icon(
            color='black', icon='star'), tooltip='Magnitude: {}'.format(self.eq_mw)).add_to(imap_tract)

        # Todo: deal with json files in repositories
        # folium.GeoJson(json.load(open(ROOT_DIR + "\\data\\network_models\\2010_Census_Tracts.geojson")),
        #                name='Network').add_to(imap_tract)
        geo = json.load(open(ROOT_DIR + "\\data\\network_models\\2010_Census_Tracts.geojson"))
        geo['features'] = [x for x in geo['features'] if
                           (self.eq_lat - self.deg_limit < float(x['properties']['INTPTLAT10']) <
                            self.eq_lat + self.deg_limit and
                            self.eq_long - self.deg_limit < float(x['properties']['INTPTLON10'])
                            < self.eq_long + self.deg_limit)]
        folium.GeoJson(geo, name='Network').add_to(imap_tract)

        folium.LayerControl().add_to(imap_tract)
        Fullscreen().add_to(imap_tract)

        return [contour_fig, imap, imap_tract]
