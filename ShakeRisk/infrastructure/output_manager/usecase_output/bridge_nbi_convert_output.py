import folium
from folium.plugins import FastMarkerCluster

from Definitions import LIMIT_STATES
from core.methods.utils import Timing
from core.dto.usecase_requests.bridge_nbi_convert_request import BridgeNbiConvertRequest
from core.dto.usecase_responses.response_object import ResponseSuccess
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *

color_dict = {
    LIMIT_STATES[0]: 'black',
    LIMIT_STATES[1]: 'green',
    LIMIT_STATES[2]: 'beige',
    LIMIT_STATES[3]: 'orange',
    LIMIT_STATES[4]: 'red'
}


class BridgeNBIConvertOutput(OutputBase):

    def __init__(self, request: BridgeNbiConvertRequest, response: ResponseSuccess):
        self.df = response.value
        self.timing = Timing()

    def execute(self):

        # output = {
        #     FIGURE: {
        #         FOLIUM_MAP: [{'Earthquake location map': self.dynamic_eq_map()}]
        #     },
        #     CONSOLE: [self.console_output()],
        #     FILE: {
        #         DF_TO_CSV: [{'nbi_data_conversion': self.df}]
        #     }
        # }

        output = OutputObject()

        output.figure.add_folium_map('Earthquake location map', self.dynamic_eq_map())

        output.file.add_dataframe_to_csv('nbi_data_conversion', self.df)

        output.console.add_info(self.console_output())
        return output

    def console_output(self):
        console = """Total Number of Bridges: {}""".format(
            len(self.df))
        return console

    def dynamic_eq_map(self):
        imap = folium.Map(location=(self.df['Latitude'].mean(), self.df['Longitude'].mean()),
                          zoom_start=8, control_scale=True)

        # callback = ('function (row) {'
        #             'return L.marker([row[0], row[1]) };')

        cluster = FastMarkerCluster(data=self.df[['Latitude', 'Longitude']].values.tolist())
        cluster.add_to(imap)

        self.timing.log("Map")
        return imap
