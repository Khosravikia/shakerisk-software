import numpy as np
import scipy.interpolate
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
import folium
from folium.plugins import MarkerCluster, Fullscreen
import plotly.graph_objects as go
from branca.colormap import LinearColormap
from branca.element import Template, MacroElement

from Definitions import IM_DISPLAY_LIMITS, IM_DISPLAY_NAMES, LIMIT_STATES, LIMIT_STATES_DATABASE, LIMIT_STATES_COLORS, \
    JET_COLORS, \
    CUSTOM_BRIDGE_CLASSES, CUSTOM_BRIDGE_CLASSES_COLORS, LIMIT_STATES_DISPLAY
from core.methods.utils import Timing
from core.dto.usecase_requests.bridge.bridge_damage_estimation_request import BridgeDamageEstimationRequest
from core.dto.usecase_responses.bridge.bridge_damage_estimation_response import BridgeDamageEstimationResponse
from infrastructure.output_manager.figure_utils import plotly_fig2array, custom_marker_cluster_limit_state_style
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *

color_dict = {
    LIMIT_STATES_DATABASE[0]: 'black',
    LIMIT_STATES_DATABASE[1]: 'green',
    LIMIT_STATES_DATABASE[2]: 'beige',
    LIMIT_STATES_DATABASE[3]: 'orange',
    LIMIT_STATES_DATABASE[4]: 'red'
}


class BridgeDamageEstimationOutput(OutputBase):

    def __init__(self, request: BridgeDamageEstimationRequest, response: BridgeDamageEstimationResponse):
        self.analyze_name = request.name
        self.damage_estimations = response.damage_estimations
        self.shaking_levels = response.shaking_levels
        self.im = request.intensity_model.measure
        self.eq_lat = request.earthquake.location.latitude
        self.eq_long = request.earthquake.location.longitude
        self.eq_mw = request.earthquake.magnitude
        self.deg_limit = request.deg_limit
        self.structure_path = request.bridge.filter_state
        self.required_classifications = list(response.required_classifications - {None})
        self.timing = Timing()

    def execute(self):

        # output_dict = {
        #     FIGURE: {
        #         FOLIUM_MAP: [{'Map View': self.dynamic_map()}],
        #         PLOTLY_GRAPH: [{'Summary of Results': self.dynamic_states_bars()},
        #                        {'Summary of Classes': self.dynamic_class_costs()}],
        #         MATPLOTLIB_FIGURE: [
        #             {'Static Result': self.static_map_and_number_of_classes()}
        #         ]
        #     },
        #     CONSOLE: [self.console_output()],
        #     FILE: {
        #         DF_TO_CSV: [{'bridge_damage_estimation': self.damage_estimations}]
        #     }
        # }
        #
        # return OutputObject.from_dict(output_dict)

        output = OutputObject()

        output.figure.add_folium_map('Map View', self.dynamic_map())
        output.figure.add_plotly_graph('Summary of Results', self.dynamic_states_bars())
        output.figure.add_plotly_graph('Summary of Classes', self.dynamic_class_costs())
        output.figure.add_matplotlib_figure('Static Result', self.static_map_and_number_of_classes())

        output.file.add_dataframe_to_csv('bridge_damage_estimation', self.damage_estimations)

        output.console.add_info(self.console_output())
        return output

    def console_output(self):
        console = """Total Number of Bridges: {}
Total Number of Damaged Bridges: {}
Total Cost: ${:0,.0f}M""".format(
                len(self.damage_estimations),
                len(self.damage_estimations[self.damage_estimations['State'] != LIMIT_STATES_DATABASE[0]]),
                self.damage_estimations['Repair_Cost'].sum() / 1000000)
        return console

    def _add_shaking_level_raster(self, imap):
        # Add shaking level

        df_shake = self.shaking_levels

        fig_data = go.Contour(
            z=df_shake['im'],
            x=df_shake['longitude'],  # horizontal axis
            y=df_shake['latitude'],  # vertical axis
            colorscale='Jet',
            contours_coloring='heatmap',
            showscale=False,
            line_width=0
        )

        lay = go.Layout(
            paper_bgcolor='rgba(0,0,0,0)',
            plot_bgcolor='rgba(0,0,0,0)',
            xaxis={'visible': False},
            yaxis={'visible': False},
            margin={'l': 0, 'r': 0, 't': 0, 'b': 0}
        )
        contour_fig = go.Figure(data=fig_data, layout=lay)
        img = plotly_fig2array(contour_fig, 1000, 1000)

        folium.raster_layers.ImageOverlay(
            name='Shaking Levels',
            image=img,
            bounds=[[self.eq_lat - self.deg_limit, self.eq_long - self.deg_limit],
                    [self.eq_lat + self.deg_limit, self.eq_long + self.deg_limit]],
            opacity=0.5
        ).add_to(imap)
        # Color Bar
        LinearColormap(JET_COLORS, vmin=df_shake['im'].min(), vmax=df_shake['im'].max(),
                       caption='Shaking Level: {}'.format(IM_DISPLAY_NAMES[self.im])).add_to(imap)

        return imap

    def static_map_and_number_of_classes(self, convert_to_base64=False, img_format='png', dpi=250):

        if 'khosravikia_etal_2018' not in self.required_classifications:
            return None
        else:

            x = self.shaking_levels['longitude']
            y = self.shaking_levels['latitude']
            z = self.shaking_levels['im']

            [xi, yi] = np.meshgrid(
                np.linspace(min(x), max(x), 400),
                np.linspace(min(y), max(y), 400))

            zi = scipy.interpolate.griddata(np.array([x, y]).T, z, np.array([xi, yi]).T).T

            plt.rcParams["font.family"] = "Times New Roman"
            plt.rcParams["font.size"] = 20

            fig = plt.figure(figsize=(20, 8))

            # Define custom colormap
            bridges_in_contour = plt.subplot(1, 2, 1)
            cmap = mpl.cm.jet(np.arange(256))
            cmap = cmap[100:, ::]
            cmap = mpl.colors.ListedColormap(cmap, name='customColorMap', N=cmap.shape[0])

            plt.contourf(xi, yi, zi, np.linspace(IM_DISPLAY_LIMITS[self.im][0], IM_DISPLAY_LIMITS[self.im][1], 400),
                         cmap=cmap, zorder=0)

            cbar = plt.colorbar(pad=0.015)
            cbar.set_label(IM_DISPLAY_NAMES[self.im], rotation=270, labelpad=15)
            # plt.clim(im_min,im_max)

            plt.plot(self.eq_long, self.eq_lat, "*", markeredgecolor=[0, 0, 0],
                     markerfacecolor=[0., 0.12890625, 0.6484375], markersize=30, linewidth=2, zorder=1);

            # Find bridges and number of each custom class in each limit state and scatter bridges
            scats = []
            num_class_in_ls = np.zeros((len(LIMIT_STATES) - 1, len(CUSTOM_BRIDGE_CLASSES)), dtype=np.int16)
            for i, ls in enumerate(LIMIT_STATES_DATABASE):
                sub_struct = self.damage_estimations[self.damage_estimations['State'] == ls]
                scats.append(plt.scatter(sub_struct['longitude'], sub_struct['latitude'], linewidths=2,
                                         c=np.array([LIMIT_STATES_COLORS[i]]), zorder=2))
                for j, cc in enumerate(CUSTOM_BRIDGE_CLASSES):
                    if i > 0:
                        num_class_in_ls[i - 1, j] = len(sub_struct[sub_struct['khosravikia_etal_2018'] == CUSTOM_BRIDGE_CLASSES[j]])

            plt.legend(scats, LIMIT_STATES, loc='lower right', fontsize=24, framealpha=1, edgecolor=[0, 0, 0],
                       fancybox=False, borderpad=0.1, labelspacing=0.25, handletextpad=0)

            plt.xlabel('Longitude', fontsize=24)
            plt.ylabel('Latitude', fontsize=24)
            plt.xlim([min(x), max(x)])
            plt.ylim([min(y), max(y)])

            scalebar = AnchoredSizeBar(bridges_in_contour.transData, 0.0899, '10km', 'lower left', pad=0.1, color='white',
                                       frameon=False, )
            bridges_in_contour.add_artist(scalebar)

            bridges_bar = plt.subplot(1, 2, 2)
            # Draw stacked bar
            for i, cbc in enumerate(CUSTOM_BRIDGE_CLASSES):
                bottom = None
                if i > 0:
                    bottom = num_class_in_ls[:, 0:i].sum(axis=1)
                plt.bar(LIMIT_STATES[1:], num_class_in_ls[:, i], label=cbc, bottom=bottom, edgecolor=[0, 0, 0], linewidth=1,
                        color=CUSTOM_BRIDGE_CLASSES_COLORS[i])

            bridges_bar.set_axisbelow(True)  # Draw grid behind bars
            plt.grid()
            plt.ylim([0, num_class_in_ls.sum(axis=1).max() * 1.1])
            plt.legend(fontsize=24, framealpha=1, edgecolor=[0, 0, 0], fancybox=False, borderpad=0.2, labelspacing=0.25)
            for i, value in enumerate(num_class_in_ls.sum(axis=1)):
                plt.text(i, value + 2, str(value))
            plt.title('Total number of bridges= {:,}'.format(len(self.damage_estimations)))
            plt.ylabel('Number of bridges', fontsize=24)
            plt.xlabel('Damage level', fontsize=24)

            plt.subplots_adjust(wspace=0.2, )

            self.timing.log("Static Image")
            return fig

    def dynamic_map(self):
        imap = folium.Map(location=(self.eq_lat, self.eq_long), zoom_start=12, control_scale=True)

        # Bridges cluster - All in one cluster
        # cluster = MarkerCluster()
        # for (index, bridge) in df[df['State'] != LIMIT_STATES[0]].iterrows():
        #     folium.Marker(location=[bridge['Latitude'], bridge['Longitude']],
        #                   icon=folium.Icon(color=color_dict[bridge['State']])).add_to(cluster)
        # cluster.add_to(imap)

        # Bridges cluster - Each state in one cluster
        for state in LIMIT_STATES_DATABASE:
            cluster = MarkerCluster(name='Bridges - {}'.format(LIMIT_STATES_DISPLAY[state]), icon_create_function='''
                                        function(cluster) {{
                                            return L.divIcon({{html: '<div><span>' + cluster.getChildCount() + '</span></div>',
                                                          className: 'marker-cluster marker-cluster-state-{}',
                                                          iconSize: new L.Point(40, 40)}});
                                        }}'''.format(LIMIT_STATES_DISPLAY[state].split(' ')[0]))
            for (index, bridge) in self.damage_estimations[self.damage_estimations['State'] == state].iterrows():
                folium.Marker(location=[bridge['latitude'], bridge['longitude']],
                              icon=folium.Icon(color=color_dict[bridge['State']]),
                              tooltip='Facility ID: {} <br> Estimated Repair Cost: ${:0,.0f}K'
                              .format(bridge['facility_id'], bridge['Repair_Cost'] / 1000)).add_to(cluster)
            cluster.add_to(imap)

        # Eq marker
        folium.Marker(location=[self.eq_lat, self.eq_long], icon=folium.Icon(
            color='black', icon='star'), tooltip='Magnitude: {}'.format(self.eq_mw)).add_to(imap)

        # Add custom CSS
        # map_style = '<link rel="stylesheet" href="{}">'.format(
        #     staticfiles_storage.url('main/css/damage-estimation-map.css'))
        # imap.get_root().html.add_child(folium.Element(map_style))
        custom_style = '<style>{}</style>'.format(custom_marker_cluster_limit_state_style())
        imap.get_root().html.add_child(folium.Element(custom_style))
        # Add shaking level
        imap = self._add_shaking_level_raster(imap)

        # Add Legend
        # legend_html = '''
        #      <div style="position: fixed;
        #      bottom: 50px; left: 50px; width: 100px; height: 90px;
        #      border:2px solid grey; z-index:9999; font-size:14px;
        #      ">&nbsp; Cool Legend <br>
        #      &nbsp; East &nbsp; <i class="fa fa-map-marker fa-2x"
        #                   style="color:green"></i><br>
        #      &nbsp; West &nbsp; <i class="fa fa-map-marker fa-2x"
        #                   style="color:red"></i>
        #       </div>
        #      '''
        # imap.get_root().html.add_child(folium.Element(legend_html))
        template = """
                                    {% macro html(this, kwargs) %}

                                    <!doctype html>
                                    <html lang="en">
                                    <head>
                                      <meta charset="utf-8">
                                      <meta name="viewport" content="width=device-width, initial-scale=1">
                                      <title>jQuery UI Draggable - Default functionality</title>
                                      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

                                      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

                                      <script>
                                      $( function() {
                                        $( "#maplegend" ).draggable({
                                                        start: function (event, ui) {
                                                            $(this).css({
                                                                right: "auto",
                                                                top: "auto",
                                                                bottom: "auto"
                                                            });
                                                        }
                                                    });
                                    });

                                      </script>
                                    </head>
                                    <body>


                                    <div id='maplegend' class='maplegend' 
                                        style='position: absolute; z-index:9999; border:2px solid grey; background-color:rgba(255, 255, 255, 0.8);
                                         border-radius:6px; padding: 10px; font-size:14px; right: 20px; bottom: 20px;'>

                                    <div class='legend-title'>Damage State</div>
                                    <div class='legend-scale'>
                                      <ul class='legend-labels'>
                                        <li><span style='background:black;opacity:0.7;'></span>No Damage</li>
                                        <li><span style='background:rgb(69, 255, 17);opacity:0.7;'></span>Slight</li>
                                        <li><span style='background:rgb(255, 252, 16);opacity:0.7;'></span>Moderate</li>
                                        <li><span style='background:rgb(255, 190, 133);opacity:0.7;'></span>Extensive</li>
                                        <li><span style='background:rgb(255, 69, 69);opacity:0.7;'></span>Complete</li>

                                      </ul>
                                    </div>
                                    </div>

                                    </body>
                                    </html>

                                    <style type='text/css'>
                                      .maplegend .legend-title {
                                        text-align: left;
                                        margin-bottom: 5px;
                                        font-weight: bold;
                                        font-size: 90%;
                                        }
                                      .maplegend .legend-scale ul {
                                        margin: 0;
                                        margin-bottom: 5px;
                                        padding: 0;
                                        float: left;
                                        list-style: none;
                                        }
                                      .maplegend .legend-scale ul li {
                                        font-size: 80%;
                                        list-style: none;
                                        margin-left: 0;
                                        line-height: 18px;
                                        margin-bottom: 2px;
                                        }
                                      .maplegend ul.legend-labels li span {
                                        display: block;
                                        float: left;
                                        height: 16px;
                                        width: 30px;
                                        margin-right: 5px;
                                        margin-left: 0;
                                        border: 1px solid #999;
                                        }
                                      .maplegend .legend-source {
                                        font-size: 80%;
                                        color: #777;
                                        clear: both;
                                        }
                                      .maplegend a {
                                        color: #777;
                                        }
                                    </style>
                                    {% endmacro %}"""

        macro = MacroElement()
        macro._template = Template(template)

        imap.get_root().add_child(macro)

        folium.LayerControl().add_to(imap)
        Fullscreen().add_to(imap)

        self.timing.log("Map")
        return imap

    def dynamic_states_bars(self):
        bar_data = pd.DataFrame(self.damage_estimations['State'].value_counts().drop(LIMIT_STATES_DATABASE[0]).sort_index())
        bar_figure_data = go.Bar(
            x=bar_data.index,
            y=bar_data['State'],
            text=bar_data['State'],
            textposition='outside',
            cliponaxis=False,  # Temp fix for outside text getting clipped
            marker_color=list(color_dict.values())[1:]
        )
        layout = go.Layout(
            title="Total Cost: ${:0,.0f}M".format(self.damage_estimations['Repair_Cost'].sum() / 1000000),
            xaxis_title="Total Number of Structues: {}".format(len(self.damage_estimations)),
            font=dict(
                family="Times New Roman",
                size=12
            ),
            # height=260
        )
        bar_fig = go.Figure(data=bar_figure_data, layout=layout)

        self.timing.log("States Bar")
        return bar_fig

    def dynamic_class_costs(self, required_classification_index=0):
        classification = self.required_classifications[required_classification_index]
        bar_data = self.damage_estimations[[classification, 'Repair_Cost']].groupby(
            classification).sum()
        bar_figure_data = go.Bar(
            x=bar_data.index,
            y=bar_data['Repair_Cost'],
            text=bar_data['Repair_Cost'],
            texttemplate='%{text:.2s}',
            textposition='outside',
            cliponaxis=False,  # Temp fix for outside text getting clipped
            #             marker_color=list(color_dict.values())[1:]
        )
        layout = go.Layout(
            title="Total Cost: ${:0,.0f}M".format(bar_data['Repair_Cost'].sum() / 1000000),
            #     xaxis_title="Total Number of Bridges: {}".format(len(self.inspections)),
            font=dict(
                family="Times New Roman",
                size=12
            )
        )
        bar_fig = go.Figure(data=bar_figure_data, layout=layout)

        self.timing.log("Class Cost Bar")
        return bar_fig
