import folium
from folium.plugins import MarkerCluster, Fullscreen
import plotly.graph_objects as go
from branca.colormap import LinearColormap

from Definitions import IM_DISPLAY_NAMES, LIMIT_STATES,  JET_COLORS
from core.methods.utils import Timing
from core.dto.usecase_requests.bridge.bridge_inspection_prioritization_request import BridgeInspectionPrioritizationRequest
from core.dto.usecase_responses.bridge.bridge_inspection_prioritization_response import BridgeInspectionPrioritizationResponse
from infrastructure.output_manager.figure_utils import plotly_fig2array
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *

color_dict = {
            LIMIT_STATES[0]: 'black',
            LIMIT_STATES[1]: 'green',
            LIMIT_STATES[2]: 'beige',
            LIMIT_STATES[3]: 'orange',
            LIMIT_STATES[4]: 'red'
        }


class BridgeInspectionPrioritizationOutput(OutputBase):
    def __init__(self, request: BridgeInspectionPrioritizationRequest, response: BridgeInspectionPrioritizationResponse):
        self.inspections = response.inspections
        self.shaking_levels = response.shaking_levels
        self.state_name = request.state_name
        self.im = request.intensity_model.measure
        self.eq_lat = request.earthquake.location.latitude
        self.eq_long = request.earthquake.location.longitude
        self.eq_mw = request.earthquake.magnitude
        self.deg_limit = request.deg_limit
        self.state = request.bridge.filter_state
        self.timing = Timing()

    def execute(self):
        # output = {
        #     FIGURE: {
        #         FOLIUM_MAP: [{'Map View': self.dynamic_map()}]
        #     },
        #     CONSOLE: [self.console_output()],
        #     FILE: {
        #         DF_TO_CSV: [{'bridge_inspection_prioritization': self.inspections}]
        #     }
        # }

        output = OutputObject()
        output.figure.add_folium_map('Map View', self.dynamic_map())

        output.file.add_dataframe_to_csv('bridge_inspection_prioritization', self.inspections)

        output.console.add_info(self.console_output())

        return output

    def console_output(self):
        console = """Total Number of Bridges Meeting the Condition: {}""".format(
            len(self.inspections)
        )
        return console

    def _add_shaking_level_raster(self, imap):
        # Add shaking level

        df_shake = self.shaking_levels

        fig_data = go.Contour(
            z=df_shake['im'],
            x=df_shake['longitude'],  # horizontal axis
            y=df_shake['latitude'],  # vertical axis
            colorscale='Jet',
            contours_coloring='heatmap',
            showscale=False,
            line_width=0
        )

        lay = go.Layout(
            paper_bgcolor='rgba(0,0,0,0)',
            plot_bgcolor='rgba(0,0,0,0)',
            xaxis={'visible': False},
            yaxis={'visible': False},
            margin={'l': 0, 'r': 0, 't': 0, 'b': 0}
        )
        contour_fig = go.Figure(data=fig_data, layout=lay)
        img = plotly_fig2array(contour_fig, 1000, 1000)

        folium.raster_layers.ImageOverlay(
            name='Shaking Levels',
            image=img,
            bounds=[[self.eq_lat - self.deg_limit, self.eq_long - self.deg_limit],
                    [self.eq_lat + self.deg_limit, self.eq_long + self.deg_limit]],
            opacity=0.5
        ).add_to(imap)
        # Color Bar
        LinearColormap(JET_COLORS, vmin=df_shake['im'].min(), vmax=df_shake['im'].max(),
                       caption='Shaking Level: {}'.format(IM_DISPLAY_NAMES[self.im])).add_to(imap)

        return imap

    def dynamic_map(self):
        df = self.inspections
        imap = folium.Map(location=(self.eq_lat, self.eq_long), zoom_start=12, control_scale=True)

        imap = self._add_shaking_level_raster(imap)

        # Bridges cluster
        cluster = MarkerCluster(name='Bridges')
        for (index, bridge) in df.iterrows():
            folium.Marker(location=[bridge['latitude'], bridge['longitude']],
                          icon=folium.Icon(color=color_dict[self.state_name]),
                          tooltip='Facility ID: {} <br> Damage State Probability: %{:0,.2f}'
                          .format(bridge['facility_id'], bridge['StateProb'] * 100)).add_to(cluster)
        cluster.add_to(imap)

        # Eq marker
        folium.Marker(location=[self.eq_lat, self.eq_long], icon=folium.Icon(
            color='black', icon='exclamation-sign'), tooltip='Magnitude: {}'.format(self.eq_mw)).add_to(imap)

        folium.LayerControl().add_to(imap)
        Fullscreen().add_to(imap)

        self.timing.log("Map")
        return imap
