import json
from pathlib import Path
import folium
from folium.plugins import MarkerCluster, Fullscreen
import numpy as np
import plotly.graph_objects as go
from branca.colormap import LinearColormap
from branca.element import Template, MacroElement

from core.methods.utils import Timing
from core.dto.usecase_requests.network.network_damage_estimation_request import NetworkDamageEstimationRequest
from core.dto.usecase_responses.network.network_damage_estimation_response import NetworkDamageEstimationResponse
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *
from infrastructure.output_manager.figure_utils import plotly_fig2array, custom_marker_cluster_limit_state_style
from Definitions import LIMIT_STATES, JET_COLORS, IM_DISPLAY_NAMES, ROOT_DIR, LIMIT_STATES_DATABASE, LIMIT_STATES_DISPLAY

color_dict = {
    LIMIT_STATES_DATABASE[0]: 'black',
    LIMIT_STATES_DATABASE[1]: 'green',
    LIMIT_STATES_DATABASE[2]: 'beige',
    LIMIT_STATES_DATABASE[3]: 'orange',
    LIMIT_STATES_DATABASE[4]: 'red'
}


class NetworkDamageEstimationOutput(OutputBase):

    def __init__(self, request: NetworkDamageEstimationRequest, response: NetworkDamageEstimationResponse):
        self.im = request.intensity_model.measure
        self.eq_lat = request.earthquake.location.latitude
        self.eq_long = request.earthquake.location.longitude
        self.eq_mw = request.earthquake.magnitude
        self.deg_limit = request.deg_limit
        self.network_type = request.transportation_network.location_name
        self.tstts_before = response.tstts_before
        self.tstts_after = response.tstts_after
        self.indirect_cost = response.indirect_cost
        self.shaking_levels = response.shaking_levels
        self.bridge_data = response.bridge_data
        self.broken_links = response.broken_links
        self.timing = Timing()

    def execute(self):
        # output = {
        #     FIGURE: {
        #         FOLIUM_MAP: [{'Map View': self.dynamic_map()}],
        #         PLOTLY_GRAPH: [{'TSTT Results': self.dynamic_tstt_bar()},
        #                        {'Costs': self.dynamic_costs_bar()}]
        #     },
        #     CONSOLE: [self.console_output()],
        #     FILE: {
        #         DF_TO_CSV: [{'network_damage_estimation': self.bridge_data}]
        #     }
        # }

        output = OutputObject()

        output.figure.add_folium_map('Map View', self.dynamic_map())
        output.figure.add_plotly_graph('TSTT Results', self.dynamic_tstt_bar())
        output.figure.add_plotly_graph('Costs', self.dynamic_costs_bar())

        output.file.add_dataframe_to_csv('network_damage_estimation', self.bridge_data)

        output.console.add_info(self.console_output())

        return output

    def console_output(self):
        console = """TSTT Increase: {:0,.0f} (Per Hour)
Total Indirect Loss: ${:0,.1f}M
Total Cost: ${:0,.1f}M""".format(
            self.tstts_after - self.tstts_before,
            self.indirect_cost/1000000,
            (self.bridge_data['Repair_Cost'].sum() + self.indirect_cost)/1000000
        )
        return console

    def _add_shaking_level_raster(self, imap):
        # Add shaking level

        df_shake = self.shaking_levels

        fig_data = go.Contour(
            z=df_shake['im'],
            x=df_shake['longitude'],  # horizontal axis
            y=df_shake['latitude'],  # vertical axis
            colorscale='Jet',
            contours_coloring='heatmap',
            showscale=False,
            line_width=0
        )

        lay = go.Layout(
            paper_bgcolor='rgba(0,0,0,0)',
            plot_bgcolor='rgba(0,0,0,0)',
            xaxis={'visible': False},
            yaxis={'visible': False},
            margin={'l': 0, 'r': 0, 't': 0, 'b': 0}
        )
        contour_fig = go.Figure(data=fig_data, layout=lay)
        img = plotly_fig2array(contour_fig, 1000, 1000)

        folium.raster_layers.ImageOverlay(
            name='Shaking Levels',
            image=img,
            bounds=[[self.eq_lat - self.deg_limit, self.eq_long - self.deg_limit],
                    [self.eq_lat + self.deg_limit, self.eq_long + self.deg_limit]],
            opacity=0.5
        ).add_to(imap)
        # Color Bar
        LinearColormap(JET_COLORS, vmin=df_shake['im'].min(), vmax=df_shake['im'].max(),
                       caption='Shaking Level: {}'.format(IM_DISPLAY_NAMES[self.im])).add_to(imap)

        return imap

    def dynamic_map(self):
        df = self.bridge_data
        imap = folium.Map(location=(self.eq_lat, self.eq_long), zoom_start=12, control_scale=True)

        # Bridges cluster - All in one cluster
        # cluster = MarkerCluster()
        # for (index, bridge) in df[df['State'] != LIMIT_STATES[0]].iterrows():
        #     folium.Marker(location=[bridge['Latitude'], bridge['Longitude']],
        #                   icon=folium.Icon(color=color_dict[bridge['State']])).add_to(cluster)
        # cluster.add_to(imap)

        # Bridges cluster - Each state in one cluster
        for state in LIMIT_STATES_DATABASE:
            cluster = MarkerCluster(name='Bridges - {}'.format(LIMIT_STATES_DISPLAY[state]), icon_create_function='''
                            function(cluster) {{
                                return L.divIcon({{html: '<div><span>' + cluster.getChildCount() + '</span></div>',
                                              className: 'marker-cluster marker-cluster-state-{}',
                                              iconSize: new L.Point(40, 40)}});
                            }}'''.format(LIMIT_STATES_DISPLAY[state].split(' ')[0]))
            for (index, bridge) in df[df['State'] == state].iterrows():
                folium.Marker(location=[bridge['latitude'], bridge['longitude']],
                              icon=folium.Icon(color=color_dict[bridge['State']]),
                              tooltip='Facility ID: {} <br>'
                              .format(bridge['facility_id'])).add_to(cluster)
            cluster.add_to(imap)

        # Eq marker
        folium.Marker(location=[self.eq_lat, self.eq_long], icon=folium.Icon(
            color='black', icon='star'), tooltip='Magnitude: {}'.format(self.eq_mw)).add_to(imap)


        # Add custom CSS
        # map_style = '<link rel="stylesheet" href="{}">'.format(
        #     staticfiles_storage.url('main/css/damage-estimation-map.css'))
        # imap.get_root().html.add_child(folium.Element(map_style))
        custom_style = '<style>{}</style>'.format(custom_marker_cluster_limit_state_style())
        imap.get_root().html.add_child(folium.Element(custom_style))

        # Add shaking level
        imap = self._add_shaking_level_raster(imap)

        # Add Network Map
        # Todo: Add reposioty stuff
        style_function = lambda x: {'color': '#ff0000', 'weight': 3} if ('(' + str(
            x['properties']['init_node']) + ',' + str(
            x['properties']['term_node']) + ')') in self.broken_links else {'color': '#000000',
                                                                                'weight': 1.5}
        # style_function = lambda x: {'color': '#000000', 'weight': 0.67}
        folium.GeoJson(json.load(open(
            Path(ROOT_DIR, "data\\network_models\\", self.network_type, self.network_type + ".geojson")))
            , style_function=style_function, name='Network').add_to(imap)

        template = """
                                            {% macro html(this, kwargs) %}

                                            <!doctype html>
                                            <html lang="en">
                                            <head>
                                              <meta charset="utf-8">
                                              <meta name="viewport" content="width=device-width, initial-scale=1">
                                              <title>jQuery UI Draggable - Default functionality</title>
                                              <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

                                              <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                              <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

                                              <script>
                                              $( function() {
                                                $( "#maplegend" ).draggable({
                                                                start: function (event, ui) {
                                                                    $(this).css({
                                                                        right: "auto",
                                                                        top: "auto",
                                                                        bottom: "auto"
                                                                    });
                                                                }
                                                            });
                                            });

                                              </script>
                                            </head>
                                            <body>


                                            <div id='maplegend' class='maplegend' 
                                                style='position: absolute; z-index:9999; border:2px solid grey; background-color:rgba(255, 255, 255, 0.8);
                                                 border-radius:6px; padding: 10px; font-size:14px; right: 20px; bottom: 20px;'>

                                            <div class='legend-title'>Damage State</div>
                                            <div class='legend-scale'>
                                              <ul class='legend-labels'>
                                                <li><span style='background:black;opacity:0.7;'></span>No Damage</li>
                                                <li><span style='background:rgb(69, 255, 17);opacity:0.7;'></span>Slight</li>
                                                <li><span style='background:rgb(255, 252, 16);opacity:0.7;'></span>Moderate</li>
                                                <li><span style='background:rgb(255, 190, 133);opacity:0.7;'></span>Extensive</li>
                                                <li><span style='background:rgb(255, 69, 69);opacity:0.7;'></span>Complete</li>

                                              </ul>
                                            </div>
                                            </div>

                                            </body>
                                            </html>

                                            <style type='text/css'>
                                              .maplegend .legend-title {
                                                text-align: left;
                                                margin-bottom: 5px;
                                                font-weight: bold;
                                                font-size: 90%;
                                                }
                                              .maplegend .legend-scale ul {
                                                margin: 0;
                                                margin-bottom: 5px;
                                                padding: 0;
                                                float: left;
                                                list-style: none;
                                                }
                                              .maplegend .legend-scale ul li {
                                                font-size: 80%;
                                                list-style: none;
                                                margin-left: 0;
                                                line-height: 18px;
                                                margin-bottom: 2px;
                                                }
                                              .maplegend ul.legend-labels li span {
                                                display: block;
                                                float: left;
                                                height: 16px;
                                                width: 30px;
                                                margin-right: 5px;
                                                margin-left: 0;
                                                border: 1px solid #999;
                                                }
                                              .maplegend .legend-source {
                                                font-size: 80%;
                                                color: #777;
                                                clear: both;
                                                }
                                              .maplegend a {
                                                color: #777;
                                                }
                                            </style>
                                            {% endmacro %}"""

        macro = MacroElement()
        macro._template = Template(template)

        imap.get_root().add_child(macro)

        folium.LayerControl().add_to(imap)
        Fullscreen().add_to(imap)

        self.timing.log('Map')
        return imap

    def dynamic_tstt_bar(self):
        bar_figure_data = go.Bar(
            x=['TSTT Before Earthquake', 'TSTT After Earthquake'],
            y=np.floor([self.tstts_before, self.tstts_after]),
            text=np.floor([self.tstts_before, self.tstts_after]),
            textposition='outside',
            cliponaxis=False,  # Temp fix for outside text getting clipped
            # marker_color=list(color_dict.values())[1:]
        )
        layout = go.Layout(
            # title="Total Cost: ${:0,.0f}M".format(df['Repair_Cost'].sum() / 1000000),
            yaxis_title="Per Hour",
            font=dict(
                family="Times New Roman",
                size=12
            )
        )
        bar_fig = go.Figure(data=bar_figure_data, layout=layout)

        self.timing.log("TSTT Bar")
        return bar_fig

    def dynamic_costs_bar(self):
        costs = np.floor([self.bridge_data['Repair_Cost'].sum(), self.indirect_cost,
                          self.bridge_data['Repair_Cost'].sum() + self.indirect_cost])
        bar_figure_data = go.Bar(
            x=['Direct Loss', 'Indirect Loss', 'Total Loss'],
            y=costs,
            text=costs,
            textposition='outside',
            cliponaxis=False,  # Temp fix for outside text getting clipped
            # marker_color=list(color_dict.values())[1:]
        )
        layout = go.Layout(
            # title="Total Cost: ${:0,.0f}M".format(df['Repair_Cost'].sum() / 1000000),
            # yaxis_title="Per Hour",
            font=dict(
                family="Times New Roman",
                size=12
            )
        )
        bar_costs_fig = go.Figure(data=bar_figure_data, layout=layout)

        self.timing.log("Cost Bar")
        return bar_costs_fig
