# import json
# from pathlib import Path
# import folium
# from folium.plugins import MarkerCluster, Fullscreen
# import numpy as np
import plotly.graph_objects as go
# from branca.colormap import LinearColormap
# from branca.element import Template, MacroElement

from core.methods.utils import Timing
from core.dto.usecase_requests.network.network_inspection_prioritization_request import NetworkInspectionPrioritizationRequest
from core.dto.usecase_responses.network.network_inspection_prioritization_response import NetworkInspectionPrioritizationResponse
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *
# from infrastructure.output_manager.figure_utils import plotly_fig2array
from Definitions import LIMIT_STATES, JET_COLORS, IM_DISPLAY_NAMES, ROOT_DIR

color_dict = {
    LIMIT_STATES[0]: 'black',
    LIMIT_STATES[1]: 'green',
    LIMIT_STATES[2]: 'beige',
    LIMIT_STATES[3]: 'orange',
    LIMIT_STATES[4]: 'red'
}


class NetworkInspectionPrioritizationOutput(OutputBase):

    def __init__(self, request: NetworkInspectionPrioritizationRequest, response: NetworkInspectionPrioritizationResponse):
        self.recovery_data = response.recovery_data
        self.timing = Timing()

    def execute(self):
        # output = {
        #     FIGURE: {
        #         PLOTLY_GRAPH: [{'Recovery steps': self.dynamic_recovery_steps()}]
        #     },
        #     CONSOLE: [],
        #     FILE: {
        #         DF_TO_CSV: [{'network_inspection_prioritization': self.recovery_data}]
        #     }
        # }

        output = OutputObject()

        output.figure.add_plotly_graph('Recovery steps', self.dynamic_recovery_steps())

        output.file.add_dataframe_to_csv('network_inspection_prioritization', self.recovery_data)

        return output

    def dynamic_recovery_steps(self):
        df = self.recovery_data

        step_fig = go.Scatter(x=df['Day'], y=df['TSTT'],
                              # name='Training' + MODEL_EVALUATION_METRICS[req.metrics],
                              mode='lines+markers', line={"shape": 'hv'},
                              hovertemplate=
                              'TSTT: %{y:.0f}' +
                              '<br>Day: %{x}<br>' +
                              '<b>%{text}</b>',
                              text=['Link: {}'.format(i) for i in df['Link']],
                              )

        lay = go.Layout(
            font=dict(
                family="Times New Roman",
                size=12
            ),
            xaxis=dict(title='Day'),
            yaxis=dict(title='TSTT (Per Hour)')
            # title="{} on Test Dataset: {}".format(MODEL_EVALUATION_METRICS[req.metrics], response.test_eval[1])
        )
        fig = go.Figure(data=step_fig, layout=lay)

        self.timing.log("Recovery Steps")
        return fig
