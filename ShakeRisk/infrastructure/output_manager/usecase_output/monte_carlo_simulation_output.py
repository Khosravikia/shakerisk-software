from core.dto.usecase_requests.monte_carlo_simulation_request import MonteCarloSimulationRequest
from core.dto.usecase_responses.monte_carlo_simulation_response import MonteCarloSimulationResponse
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *


class MonteCarloSimulationOutput(OutputBase):

    def __init__(self, request: MonteCarloSimulationRequest, response: MonteCarloSimulationResponse):
        self.result = response.result

    def execute(self):
        #
        # output = {
        #     FIGURE: {
        #     },
        #     CONSOLE: ['Answer is: {} .'.format(self.result)],
        #     FILE: {
        #     }
        # }

        output = OutputObject()

        output.console.add_info('Answer is: {} .'.format(self.result))

        return output
