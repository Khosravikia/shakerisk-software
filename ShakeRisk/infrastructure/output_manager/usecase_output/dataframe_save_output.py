from core.dto.usecase_requests.dataframe_save_request import DataFrameSaveRequest
from core.dto.usecase_responses.response_object import ResponseSuccess
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *


class DataFrameSaveOutput(OutputBase):

    def __init__(self, request: DataFrameSaveRequest, response: ResponseSuccess):
        self.df = response.value

    def execute(self):

        # output = {
        #     FIGURE: {
        #         DATAFRAME_TABLE: [{'Table view': self.df}]
        #     },
        #     CONSOLE: [],
        #     FILE: {
        #         DF_TO_CSV: [{'dataframe_save': self.df}]
        #     }
        # }

        output = OutputObject()

        output.figure.add_dataframe_table('Table view', self.df)

        output.file.add_dataframe_to_csv('dataframe_save', self.df)

        return output

