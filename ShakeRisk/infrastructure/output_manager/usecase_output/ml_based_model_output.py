# import numpy as np
# import plotly.graph_objects as go

# from Definitions import MODEL_EVALUATION_METRICS
from core.dto.usecase_requests.ml_based_model_request import MLBasedModelRequest
from core.dto.usecase_responses.ml_based_model_response import MLBasedModelResponse
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *


class MLBasedModelOutput(OutputBase):

    avialable_model_object_types = {
        'TensorFlow': TF_MODEL_TO_FILE,
        'Scikit-Learn': SK_LEARN_MODEL_TO_FILE
    }

    def __init__(self, request: MLBasedModelRequest, response: MLBasedModelResponse):
        self.additional_info = response.additional_info
        self.evaluations = response.evaluations
        self.model = response.model
        self.model_object_type = response.model_object_type
        self.random_effect_values = response.random_effect_values
        self.model_name = request.ml_model_options.model_name
        # self.train_data_result = self.additional_info.train_data_result
        # self.test_data_result = self.additional_info.test_data_result
        # self.df = response.history
        # self.history = response.history
        # self.metrics = request.ml_model_options.metrics

    def execute(self):
        # output = {
        #     FIGURE: {
        #         # PLOTLY_GRAPH: [{'Model graph': self.dynamic_history_plot()}]
        #         # DATAFRAME_TABLE: [{'Evaluations' +: self.evaluations}]
        #     },
        #     CONSOLE: [self.console_string()],
        #     FILE: {
        #         self.check_object_model_type(): [{'trained_' + self.model_name: self.model}],
        #         DICT_TO_JSON: [{'trained_' + self.model_name + '_info': self.additional_info.to_dict()}],
        #         DF_TO_CSV: [{'trained_' + self.model_name + '_evaluation': self.evaluations}]
        #     }
        # }
        #
        # if self.random_effect_values is not None:
        #     self.random_effect_values['Group'] = self.random_effect_values.index
        #     self.random_effect_values = self.random_effect_values[['Group', self.random_effect_values.columns[0]]]
        #     output[FILE][DF_TO_CSV].append(
        #         {'Trained_Model_Random_Effects': self.random_effect_values}
        #     )

        output = OutputObject()

        output.file.add(self.check_object_model_type(), 'trained_' + self.model_name, self.model)
        output.file.add_dict_to_json('trained_' + self.model_name + '_info', self.additional_info.to_dict())
        output.file.add_dataframe_to_csv('trained_' + self.model_name + '_evaluation', self.evaluations)

        if self.random_effect_values is not None:
            self.random_effect_values['Group'] = self.random_effect_values.index
            self.random_effect_values = self.random_effect_values[['Group', self.random_effect_values.columns[0]]]
            output.file.add_dataframe_to_csv('Trained_Model_Random_Effects', self.random_effect_values)

        output.console.add_info(self.console_string())

        return output

    def check_object_model_type(self):
        if self.model_object_type in self.avialable_model_object_types:
            return self.avialable_model_object_types[self.model_object_type]
        else:
            return OBJECT_TO_PICKLE_FILE

    def console_string(self):
        return '''ML training finished.
Training data result:
    RMSE:       {:.3f},
    R2:         {:.3f},
    Mean:       {:.3f},
    STD:        {:.3f},
Test data result:
    RMSE:       {:.3f},
    R2:         {:.3f},
    Mean:       {:.3f},
    STD:        {:.3f}'''.format(
            self.evaluations.rmse[0],
            self.evaluations.r2[0],
            self.evaluations.error_mean[0],
            self.evaluations.error_std[0],
            self.evaluations.rmse[1],
            self.evaluations.r2[1],
            self.evaluations.error_mean[1],
            self.evaluations.error_std[1],
        )

    # def dynamic_history_plot(self):
    #     line_fig_mse = go.Scatter(x=self.df['epoch'], y=self.df['mse'],
    #                               name='Training' + MODEL_EVALUATION_METRICS[self.metrics],
    #                               mode='lines+markers')
    #     line_fig_mse_val = go.Scatter(x=self.df['epoch'], y=self.df['val_mse'],
    #                                   name='Validation ' + MODEL_EVALUATION_METRICS[self.metrics],
    #                                   mode='lines+markers')
    #     lay = go.Layout(
    #         font=dict(
    #             family="Times New Roman",
    #             size=12
    #         ),
    #         yaxis=dict(range=[0, np.max([self.df['val_mse'].max(), self.df['mse'].max()])]),
    #         title="{} on Test Dataset: {}".format(MODEL_EVALUATION_METRICS[self.metrics], self.test_eval[1])
    #     )
    #     error_fig = go.Figure(data=[line_fig_mse, line_fig_mse_val], layout=lay)
    #     return error_fig
