import folium

from Definitions import LIMIT_STATES
from core.methods.utils import Timing
from core.dto.usecase_requests.eq_data_get_request import EqDataGetRequest
from core.dto.usecase_responses.response_object import ResponseSuccess
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *

color_dict = {
    LIMIT_STATES[0]: 'black',
    LIMIT_STATES[1]: 'green',
    LIMIT_STATES[2]: 'beige',
    LIMIT_STATES[3]: 'orange',
    LIMIT_STATES[4]: 'red'
}


class EqDataGetOutput(OutputBase):

    def __init__(self, request: EqDataGetRequest, response: ResponseSuccess):
        self.df = response.value
        self.timing = Timing()

    def execute(self):

        # output = {
        #     FIGURE: {
        #         FOLIUM_MAP: [{'Earthquake location map': self.dynamic_eq_map()}]
        #     },
        #     CONSOLE: [self.console_output()],
        #     FILE: {
        #         DF_TO_CSV: [{'earthquake_data': self.df}]
        #     }
        # }

        output = OutputObject()

        output.figure.add_folium_map('Earthquake location map', self.dynamic_eq_map())

        output.file.add_dataframe_to_csv('earthquake_data', self.df)

        output.console.add_info(self.console_output())

        return output

    def console_output(self):
        console = """Total Number of Earthquakes: {}""".format(
            len(self.df))
        return console

    def dynamic_eq_map(self):
        imap = folium.Map(location=(self.df['latitude'].mean(), self.df['longitude'].mean()),
                          zoom_start=8, control_scale=True)

        # callback = ('function (row) {'
        #             'return L.marker([row[0], row[1]) };')

        for (index, eq) in self.df.iterrows():
            folium.Marker(location=[eq['latitude'], eq['longitude']],
                          icon=folium.Icon(color='black'),
                          tooltip='Latitude: {} <br> Longitude: {} <br> Magnitude: {} <br> Year: {} <br> State: {}'
                          .format(eq['latitude'], eq['longitude'], eq['mag'], eq['year'], eq['state'])).add_to(
                imap)
        self.timing.log("Map")
        return imap
