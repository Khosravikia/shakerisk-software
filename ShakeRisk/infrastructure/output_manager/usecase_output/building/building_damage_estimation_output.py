import json
from pathlib import Path
import pandas as pd
import folium
from folium.plugins import Fullscreen
from folium.features import Choropleth, GeoJsonTooltip, GeoJsonPopup
import plotly.graph_objects as go
from branca.colormap import LinearColormap
# Todo: Take care of loading geoJsons

from Definitions import IM_DISPLAY_NAMES, LIMIT_STATES, JET_COLORS, ROOT_DIR, LIMIT_STATES_DATABASE
from core.methods.utils import Timing
from core.dto.usecase_requests.building.building_damage_estimation_request import BuildingDamageEstimationRequest
from core.dto.usecase_responses.building.building_damage_estimation_response import BuildingDamageEstimationResponse
from infrastructure.output_manager.figure_utils import plotly_fig2array
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *

color_dict = {
    LIMIT_STATES_DATABASE[0]: 'black',
    LIMIT_STATES_DATABASE[1]: 'green',
    LIMIT_STATES_DATABASE[2]: 'beige',
    LIMIT_STATES_DATABASE[3]: 'orange',
    LIMIT_STATES_DATABASE[4]: 'red'
}


class BuildingDamageEstimationOutput(OutputBase):

    def __init__(self, request: BuildingDamageEstimationRequest, response: BuildingDamageEstimationResponse):
        self.damage_estimations = response.damage_estimations
        self.shaking_levels = response.shaking_levels
        self.im = request.intensity_model.measure
        self.eq_lat = request.earthquake.location.latitude
        self.eq_long = request.earthquake.location.longitude
        self.eq_mw = request.earthquake.magnitude
        self.deg_limit = request.deg_limit
        self.structure_path = request.building.filter_state
        self.required_classifications = list(response.required_classifications - {None})
        self.timing = Timing()

    def execute(self):

        # output = {
        #     FIGURE: {
        #         FOLIUM_MAP: [{'Map View': self.dynamic_map()}],
        #         PLOTLY_GRAPH: [
        #             # {'Summary of Results': self.dynamic_states_bars()},
        #             {'Summary of Classes': self.dynamic_class_costs()},
        #             {'Summary of Classes Percentages': self.dynamic_class_costs_percentage()}
        #         ]
        #     },
        #     CONSOLE: [self.console_output()],
        #     FILE: {
        #         DF_TO_CSV: [{'building_damage_estimation': self.damage_estimations}]
        #     }
        # }

        output = OutputObject()

        output.figure.add_folium_map('Map View', self.dynamic_map())
        output.figure.add_plotly_graph('Summary of Classes', self.dynamic_class_costs())
        output.figure.add_plotly_graph('Summary of Classes Percentages', self.dynamic_class_costs_percentage())

        output.file.add_dataframe_to_csv('building_damage_estimation', self.damage_estimations)

        output.console.add_info(self.console_output())


        return output

    def console_output(self):
        # Total Number of Buildings: {}
        # Total Number of Damaged Buildings: {}
        console = """Total Cost: ${:0,.0f}M""".format(
            # len(self.damage_estimations),
            # len(self.damage_estimations[self.damage_estimations['State'] != LIMIT_STATES_DATABASE[0]]),
            self.damage_estimations['Repair_Cost'].sum() / 1000000
        )
        return console

    def _add_shaking_level_raster(self, imap):
        # Add shaking level

        df_shake = self.shaking_levels

        fig_data = go.Contour(
            z=df_shake['im'],
            x=df_shake['longitude'],  # horizontal axis
            y=df_shake['latitude'],  # vertical axis
            colorscale='Jet',
            contours_coloring='heatmap',
            showscale=False,
            line_width=0
        )

        lay = go.Layout(
            paper_bgcolor='rgba(0,0,0,0)',
            plot_bgcolor='rgba(0,0,0,0)',
            xaxis={'visible': False},
            yaxis={'visible': False},
            margin={'l': 0, 'r': 0, 't': 0, 'b': 0}
        )
        contour_fig = go.Figure(data=fig_data, layout=lay)
        img = plotly_fig2array(contour_fig, 1000, 1000)

        folium.raster_layers.ImageOverlay(
            name='Shaking Levels',
            image=img,
            bounds=[[self.eq_lat - self.deg_limit, self.eq_long - self.deg_limit],
                    [self.eq_lat + self.deg_limit, self.eq_long + self.deg_limit]],
            opacity=0.5
        ).add_to(imap)
        # Color Bar
        LinearColormap(JET_COLORS, vmin=df_shake['im'].min(), vmax=df_shake['im'].max(),
                       caption='Shaking Level: {}'.format(IM_DISPLAY_NAMES[self.im])).add_to(imap)

        return imap

    def dynamic_map(self, required_classification_index=0):
        classification = self.required_classifications[required_classification_index]
        df = self.damage_estimations
        # Todo: Stop mixing state id and tract id after converting tract geojson
        df['tract_id'] = df['state_id'].astype(str) + df['tract_id'].astype(str)

        unit = 'K'
        conversion_unit = 1000

        # Calculate total cost for each tract
        total_costs = df[['tract_id', 'Repair_Cost']].groupby('tract_id').sum() / conversion_unit
        total_costs = total_costs.reset_index()
        total_costs.columns = ['GEOID10', 'Repair_Cost']

        tracts = pd.unique(df['tract_id']).astype(str)

        # Load and limit geoJson
        # Todo: deal with json files in repositories
        if self.structure_path is None:
            self.structure_path = "TX"
        geo = json.load(open(Path(ROOT_DIR, "data\\buildings\\", self.structure_path + ".geojson")))
        features = [x for x in geo['features'] if
                    (self.eq_lat - self.deg_limit < float(x['properties']['INTPTLAT10']) <
                     self.eq_lat + self.deg_limit and
                     self.eq_long - self.deg_limit < float(x['properties']['INTPTLON10'])
                     < self.eq_long + self.deg_limit and
                     # Filter features by available tract data
                     x['properties']['GEOID10'] in tracts)]

        # Add each class and total cost information to geoJson
        all_classes = pd.unique(df[classification])
        for tract in tracts:
            # Find current tract index in features
            feature_index = next(
                (index for (index, d) in enumerate(features) if d['properties']['GEOID10'] == tract), None)
            df_tract = df[df['tract_id'] == tract]
            # Add current class state and repair cost to geoJson
            for index, row in df_tract.iterrows():
                features[feature_index]['properties'].update({
                    row[classification] + ' State': row['State'],
                    row[classification] + ' Repair Cost': '${:0,.0f}{}'.format(row['Repair_Cost'] / conversion_unit,
                                                                               unit)
                })
            # Use '-' for classes not present in current tract.
            for clas in list(set(all_classes) - set(df_tract[classification].values)):
                features[feature_index]['properties'].update({
                    clas + ' State': '-',
                    clas + ' Repair Cost': '-'
                })
            # Add total cost to geoJson
            features[feature_index]['properties'].update({
                'Total Cost': '${:0,.0f}{}'.format(
                    total_costs[total_costs['GEOID10'] == tract]['Repair_Cost'].iloc[0], unit)
            })

        geo['features'] = features

        # List of geoJson properties to show as tooltip on folium
        tooltip_list = []
        for clas in all_classes:
            tooltip_list.append(clas + ' State')
            tooltip_list.append(clas + ' Repair Cost')

        imap = folium.Map(location=(self.eq_lat, self.eq_long), zoom_start=12, control_scale=True)

        imap = self._add_shaking_level_raster(imap)

        geo_json = Choropleth(geo_data=geo, highlight=True,
                              data=total_costs, columns=('GEOID10', 'Repair_Cost'),
                              key_on='feature.properties.GEOID10',
                              legend_name='Total Repair Cost ({}$)'.format(unit), name='Main Result').add_to(imap)
        GeoJsonPopup(fields=tooltip_list).add_to(geo_json.geojson)
        GeoJsonTooltip(fields=['GEOID10', 'Total Cost'],
                       aliases=['Tract ID:', 'Total Cost:']).add_to(geo_json.geojson)

        folium.LayerControl().add_to(imap)
        Fullscreen(position='topright').add_to(imap)

        self.timing.log("Map")
        return imap

    def dynamic_states_bars(self):
        bar_data = pd.DataFrame(
            self.damage_estimations['State'].value_counts().drop(LIMIT_STATES_DATABASE[0]).sort_index())
        bar_figure_data = go.Bar(
            x=bar_data.index,
            y=bar_data['State'],
            text=bar_data['State'],
            textposition='outside',
            cliponaxis=False,  # Temp fix for outside text getting clipped
            marker_color=list(color_dict.values())[1:]
        )
        layout = go.Layout(
            title="Total Cost: ${:0,.0f}M".format(self.damage_estimations['Repair_Cost'].sum() / 1000000),
            xaxis_title="Total Number of Structues: {}".format(len(self.damage_estimations)),
            font=dict(
                family="Times New Roman",
                size=12
            ),
            # height=260
        )
        bar_fig = go.Figure(data=bar_figure_data, layout=layout)

        self.timing.log("States Bars")

        return bar_fig

    def dynamic_class_costs(self, required_classification_index=0):
        classification = self.required_classifications[required_classification_index]
        bar_data = self.damage_estimations[[classification, 'Repair_Cost']].groupby(classification).sum()
        bar_figure_data = go.Bar(
            x=bar_data.index,
            y=bar_data['Repair_Cost'],
            text=bar_data['Repair_Cost'],
            texttemplate='%{text:.2s}',
            textposition='outside',
            cliponaxis=False,  # Temp fix for outside text getting clipped
            #             marker_color=list(color_dict.values())[1:]
        )
        layout = go.Layout(
            title="Total Cost: ${:0,.0f}M".format(bar_data['Repair_Cost'].sum() / 1000000),
            #     xaxis_title="Total Number of Bridges: {}".format(len(self.inspections)),
            font=dict(
                family="Times New Roman",
                size=12
            )
        )
        bar_fig = go.Figure(data=bar_figure_data, layout=layout)

        self.timing.log("Class Costs Bar")

        return bar_fig

    def dynamic_class_costs_percentage(self, required_classification_index=0):
        classification = self.required_classifications[required_classification_index]
        bar_data = self.damage_estimations[[classification, 'Repair_Cost']].groupby(classification).sum()['Repair_Cost']\
                   /self.damage_estimations[[classification, 'cost']].groupby(classification).sum()['cost']
        bar_figure_data = go.Bar(
            x=bar_data.index,
            y=bar_data,
            text=bar_data,
            texttemplate='%{text:.2f}',
            textposition='outside',
            cliponaxis=False,  # Temp fix for outside text getting clipped
            #             marker_color=list(color_dict.values())[1:]
        )
        layout = go.Layout(
            title="Total Class Cost/Total Class Exposure",
            yaxis={'range': [0, 1]},
            #     xaxis_title="Total Number of Bridges: {}".format(len(self.inspections)),
            font=dict(
                family="Times New Roman",
                size=12
            )
        )
        bar_fig = go.Figure(data=bar_figure_data, layout=layout)

        self.timing.log("Class Cost Percentage Bar")

        return bar_fig
