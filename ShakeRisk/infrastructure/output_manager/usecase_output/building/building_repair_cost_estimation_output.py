import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go

from Definitions import LIMIT_STATES, LIMIT_STATES_DATABASE
from core.methods.utils import Timing
from core.dto.usecase_requests.building.building_repair_cost_estimation_request import \
    BuildingRepairCostEstimationRequest
from core.dto.usecase_responses.building.building_repair_cost_estimation_response import \
    BuildingRepairCostEstimationResponse
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *

color_dict = {
    LIMIT_STATES[0]: 'black',
    LIMIT_STATES[1]: 'green',
    LIMIT_STATES[2]: 'beige',
    LIMIT_STATES[3]: 'orange',
    LIMIT_STATES[4]: 'red'
}


class BuildingRepairCostEstimationOutput(OutputBase):

    def __init__(self, request: BuildingRepairCostEstimationRequest, response: BuildingRepairCostEstimationResponse):
        self.df = response.value
        self.timing = Timing()

    def execute(self):

        # output = {
        #     FIGURE: {
        #         PLOTLY_GRAPH: [{'Total Cost Histogram ': self.dynamic_total_cost_histogram()},
        #                        # {'Damage Box and Whisker plot': self.dynamic_states_box_plot()},
        #                        # {'Damage Distribution': self.dynamic_states_histogram()}
        #                        ]
        #     },
        #     CONSOLE: [self.console_output()],
        #     FILE: {
        #         DF_TO_CSV: [{'building_loss_estimation': self.df}]
        #     }
        # }

        output = OutputObject()
        output.figure.add_plotly_graph('Total Cost Histogram ', self.dynamic_total_cost_histogram())

        output.file.add_dataframe_to_csv('building_loss_estimation', self.df)

        output.console.add_info(self.console_output())
        return output

    def console_output(self):
        console = """
Mean Total Cost : ${:0,.2f}M
Median of Total Cost: ${:0,.2f}M
\u03C3 of Total Cost : ${:0,.2f}M""".format(
            self.df['Total Cost'].mean() / 1000000,
            self.df['Total Cost'].median() / 1000000,
            self.df['Total Cost'].std() / 1000000
        )
        return console

    def dynamic_total_cost_histogram(self):

        fig_data = go.Histogram(x=self.df['Total Cost'] / 1000000, histnorm='probability',
                                marker=dict(line=dict(width=1)),
                                opacity=0.7)
        lay = go.Layout(xaxis_title="Repair Cost (Million Dollars)", yaxis_title='Probability',
                        font=dict(
                            family="Times New Roman",
                            size=12,
                        ))
        hist_fig = go.Figure(data=fig_data, layout=lay)

        self.timing.log("Total Cost Histogram")
        return hist_fig

    def dynamic_states_box_plot(self):

        fig_data = []
        for state in LIMIT_STATES[1:]:
            fig_data.append(go.Box(y=self.df[state], name=state, marker_color=color_dict[state]))
        lay = go.Layout(yaxis_title='Number of Structures', title='Total Number of Structures: {}'
                        .format(int(self.df.iloc[0, :-1].sum())),
                        font=dict(
                            family="Times New Roman",
                            size=12,
                        ))
        states_box_fig = go.Figure(data=fig_data, layout=lay)

        self.timing.log("State Box Plot")
        return states_box_fig

    def dynamic_states_histogram(self):
        # States Histogram
        fig_data = []
        for state in LIMIT_STATES[1:]:
            fig_data.append(
                go.Histogram(x=self.df[state], name=state, histnorm='probability', marker=dict(line=dict(width=1)),
                             opacity=0.7, marker_color=color_dict[state]))

        lay = go.Layout(
            barmode='stack', yaxis_title='Probability', xaxis_title='Number of Structues',
            font=dict(
                family="Times New Roman",
                size=12,
            ))
        states_hist = go.Figure(data=fig_data, layout=lay)

        self.timing.log("State Histogram")
        return states_hist

    def static_map_and_limit_states(self):
        plt.rcParams["font.family"] = "Times New Roman"
        plt.rcParams["font.size"] = 20

        fig = plt.figure(figsize=(20, 8))
        state_box = plt.subplot(1, 2, 1)

        boxprops = dict(color='blue', linewidth=2)
        medianprops = dict(linewidth=2, color='red')
        flierprops = dict(marker="P", markerfacecolor='red', markersize=10, linestyle='None',
                          markeredgecolor=[0, 0, 0, 0])
        capprops = dict(linewidth=2)
        whiskerprops = dict(linestyle='--', linewidth=2)
        plt.boxplot(self.df.iloc[:, 1:-1].T, labels=LIMIT_STATES[1:], boxprops=boxprops,
                    medianprops=medianprops,
                    flierprops=flierprops, whiskerprops=whiskerprops, capprops=capprops)

        state_box.set_axisbelow(True)  # Draw grid behind bars
        plt.grid()
        plt.xlabel('Damage estimates', fontsize=24)
        plt.ylabel('Number of damaged bridges', fontsize=24)

        cost_hist = plt.subplot(1, 2, 2)

        costs = self.df['Total Cost']
        weights = np.ones_like(costs) / float(len(costs))
        plt.hist(costs / 1000000, weights=weights, edgecolor='black', linewidth=1)

        plt.text(0.65, 0.9, 'Mean = ${:0,.2f}M'.format(costs.mean() / 1000000), transform=cost_hist.transAxes,
                 fontsize=24)
        plt.text(0.65, 0.8, '\u03C3 = ${:0,.2f}M'.format(costs.std() / 1000000), transform=cost_hist.transAxes,
                 fontsize=24)

        cost_hist.set_axisbelow(True)  # Draw grid behind bars
        plt.grid()
        plt.xlabel('Repair cost estimates (Million dollars)', fontsize=24)
        plt.ylabel('Probability', fontsize=24)

        plt.subplots_adjust(wspace=0.2)

        self.timing.log("Static Image")
        return fig
