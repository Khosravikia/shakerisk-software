import json
from pathlib import Path
import pandas as pd
import folium
from folium.plugins import Fullscreen
from folium.features import Choropleth, GeoJsonTooltip, GeoJsonPopup
import plotly.graph_objects as go
from branca.colormap import LinearColormap

from Definitions import IM_DISPLAY_NAMES, LIMIT_STATES, JET_COLORS, ROOT_DIR
from core.methods.utils import Timing
from core.dto.usecase_requests.building.building_inspection_prioritization_request import \
    BuildingInspectionPrioritizationRequest
from core.dto.usecase_responses.building.building_inspection_prioritization_response import \
    BuildingInspectionPrioritizationResponse
from infrastructure.output_manager.figure_utils import plotly_fig2array
from infrastructure.output_manager.output_base import OutputBase
from infrastructure.output_manager.output_object import OutputObject
from infrastructure.output_manager.output_types import *

# Todo: Take care of loading geoJsons

color_dict = {
    LIMIT_STATES[0]: 'black',
    LIMIT_STATES[1]: 'green',
    LIMIT_STATES[2]: 'beige',
    LIMIT_STATES[3]: 'orange',
    LIMIT_STATES[4]: 'red'
}


class BuildingInspectionPrioritizationOutput(OutputBase):
    def __init__(self, request: BuildingInspectionPrioritizationRequest,
                 response: BuildingInspectionPrioritizationResponse):
        self.inspections = response.inspections
        self.shaking_levels = response.shaking_levels
        self.state_name = request.state_name
        self.im = request.intensity_model.measure
        self.eq_lat = request.earthquake.location.latitude
        self.eq_long = request.earthquake.location.longitude
        self.eq_mw = request.earthquake.magnitude
        self.deg_limit = request.deg_limit
        self.state = request.building.filter_state
        self.required_classifications = list(response.required_classifications - {None})
        self.timing = Timing()

    def execute(self):
        # output = {
        #     FIGURE: {
        #         FOLIUM_MAP: [{'Map View': self.dynamic_map()}]
        #     },
        #     CONSOLE: [self.console_output()],
        #     FILE: {
        #         DF_TO_CSV: [{'building_inspection_prioritization': self.inspections}]
        #     }
        # }

        output = OutputObject()

        output.figure.add_folium_map('Map View', self.dynamic_map())

        output.file.add_dataframe_to_csv('building_inspection_prioritization', self.inspections)

        return output

    def console_output(self):
        console = """
Total Number of Building Classes Meeting the Condition: {}""".format(
            len(self.inspections)
        )
        return console

    def _add_shaking_level_raster(self, imap):
        # Add shaking level

        df_shake = self.shaking_levels

        fig_data = go.Contour(
            z=df_shake['im'],
            x=df_shake['longitude'],  # horizontal axis
            y=df_shake['latitude'],  # vertical axis
            colorscale='Jet',
            contours_coloring='heatmap',
            showscale=False,
            line_width=0
        )

        lay = go.Layout(
            paper_bgcolor='rgba(0,0,0,0)',
            plot_bgcolor='rgba(0,0,0,0)',
            xaxis={'visible': False},
            yaxis={'visible': False},
            margin={'l': 0, 'r': 0, 't': 0, 'b': 0}
        )
        contour_fig = go.Figure(data=fig_data, layout=lay)
        img = plotly_fig2array(contour_fig, 1000, 1000)

        folium.raster_layers.ImageOverlay(
            name='Shaking Levels',
            image=img,
            bounds=[[self.eq_lat - self.deg_limit, self.eq_long - self.deg_limit],
                    [self.eq_lat + self.deg_limit, self.eq_long + self.deg_limit]],
            opacity=0.5
        ).add_to(imap)
        # Color Bar
        LinearColormap(JET_COLORS, vmin=df_shake['im'].min(), vmax=df_shake['im'].max(),
                       caption='Shaking Level: {}'.format(IM_DISPLAY_NAMES[self.im])).add_to(imap)

        return imap

    def dynamic_map(self, required_classification_index=0):
        classification = self.required_classifications[required_classification_index]
        df = self.inspections
        # Todo: Stop mixing state id and tract id after converting tract geojson
        df['tract_id'] = df['state_id'].astype(str) + df['tract_id'].astype(str)

        # Load and limit geoJson
        # Todo: deal with json files in repositories
        if self.state is None:
            self.state = "TX"
        geo = json.load(open(Path(ROOT_DIR, "data\\buildings\\", self.state + ".geojson")))

        total_num = df[['tract_id', 'StateProb']].groupby('tract_id').count()
        total_num = total_num.reset_index()
        total_num.columns = ['GEOID10', 'Count']

        tracts = pd.unique(df['tract_id']).astype(str)

        features = [x for x in geo['features'] if
                    (self.eq_lat - self.deg_limit < float(x['properties']['INTPTLAT10']) <
                     self.eq_lat + self.deg_limit and
                     self.eq_long - self.deg_limit < float(x['properties']['INTPTLON10'])
                     < self.eq_long + self.deg_limit and
                     # Filter features by available tract data
                     x['properties']['GEOID10'] in tracts
                     )]

        # Add each class probability to geoJson
        all_classes = pd.unique(df[classification])
        for tract in tracts:
            feature_index = next(
                (index for (index, d) in enumerate(features) if d['properties']['GEOID10'] == tract), None)
            df_tract = df[df['tract_id'] == tract]

            for index, row in df_tract.iterrows():
                features[feature_index]['properties'].update({
                    row[classification] + ' Probability': row['StateProb'],
                })
            for clas in list(set(all_classes) - set(df_tract[classification].values)):
                features[feature_index]['properties'].update({
                    clas + ' Probability': '-',
                })
            features[feature_index]['properties'].update({
                'Total number of cases: ': str(total_num[total_num['GEOID10'] == tract]['Count'].iloc[0])
            })
        geo['features'] = features

        # List of geoJson properties to show as tooltip on folium
        tooltip_list = []
        for clas in all_classes:
            tooltip_list.append(clas + ' Probability')

        imap = folium.Map(location=(self.eq_lat, self.eq_long), zoom_start=12, control_scale=True)

        imap = self._add_shaking_level_raster(imap)

        geo_json = Choropleth(geo_data=geo, highlight=True,
                              data=total_num, columns=('GEOID10', 'Count'), key_on='feature.properties.GEOID10',
                              legend_name='Total number of cases').add_to(imap)

        GeoJsonPopup(fields=tooltip_list).add_to(geo_json.geojson)
        GeoJsonTooltip(fields=['GEOID10', 'Total number of cases: '],
                       aliases=['Tract ID:', 'Total number of cases:']).add_to(geo_json.geojson)

        folium.LayerControl().add_to(imap)
        Fullscreen().add_to(imap)

        self.timing.log("Map")
        return imap
