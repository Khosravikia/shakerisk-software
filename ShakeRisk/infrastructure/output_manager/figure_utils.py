from io import BytesIO
from base64 import b64encode
import numpy as np
from PIL import Image
from .output_types import MATPLOTLIB_FIGURE, FOLIUM_MAP, PLOTLY_GRAPH, DATAFRAME_TABLE


# Convert each figure type to its html format
def figure_html_converter(figure_type, figure):
    figure_converted = ''

    if figure_type == FOLIUM_MAP:
        figure_converted = figure.get_root().render()

    elif figure_type == PLOTLY_GRAPH:
        figure_converted = figure.to_html(include_plotlyjs=True, full_html=True,
                                          default_width='100%',
                                          default_height='100%')
    elif figure_type == MATPLOTLIB_FIGURE:
        fig_base64 = matplotlib_to_base64(figure)
        fig_base64 = fig_base64[2:-1]  # Remove b'' from string to show in image tag
        figure_converted = "data:image/png;charset=utf-8;base64," + fig_base64

    elif figure_type == DATAFRAME_TABLE:
        figure_converted = figure.to_html(max_rows=6, max_cols=6, index=False, justify='unset', border=0)

    return figure_converted


# Convert plotly figure to numpy array
def plotly_fig2array(fig, width, height):
    fig_bytes = fig.to_image(format="png", width=width, height=height)
    buf = BytesIO(fig_bytes)
    img = Image.open(buf)
    #     return np.asarray(img) #With this returned img will be read-only
    return np.array(img)  # This will make a copy of original image making it writable


# Convert matplotlib figure to base64 string
def matplotlib_to_base64(fig, img_format='png', dpi=250) -> str:
    figure_string_io_bytes = BytesIO()
    fig.savefig(figure_string_io_bytes, format=img_format, dpi=dpi)
    figure_string_io_bytes.seek(0)
    return str(b64encode(figure_string_io_bytes.read()))


# Custom coloring for clusters of folium for each state of damage
def custom_marker_cluster_limit_state_style():
    return """
    .marker-cluster-state-No{
      background-color: rgba(157, 157, 157, 0.6);
    }
    .marker-cluster-state-No div{
      background-color: rgba(53, 53, 53, 0.6);
    }
    
    .marker-cluster-state-Slight{
      background-color: rgba(159, 255, 132, 0.6);
    }
    .marker-cluster-state-Slight div{
      background-color: rgba(69, 255, 17, 0.6);
    }
    
    .marker-cluster-state-Moderate{
      background-color: rgba(255, 253, 100, 0.6);
    }
    .marker-cluster-state-Moderate div{
      background-color: rgba(255, 252, 16, 0.6);
    }
    
    .marker-cluster-state-Extensive{
      background-color: rgba(255, 190, 133, 0.6);
    }
    .marker-cluster-state-Extensive div{
      background-color: rgba(253, 153, 66, 0.6);
    }
    
    .marker-cluster-state-Complete{
      background-color: rgba(255, 136, 136, 0.6);
    }
    .marker-cluster-state-Complete div{
      background-color: rgba(255, 69, 69, 0.6);
    }
    """