import shutil
from infrastructure.output_manager.output_types import *


def _add_to_dict(source_dict, item_type, item_name, item):
    if item_type not in source_dict:
        source_dict.update({item_type: []})
    source_dict[item_type].append({item_name: item})
    return source_dict


def _apply(source_dict, function, replace=True):
    for item_type, item_list in source_dict.items():
        for item_index, item_object in enumerate(item_list):
            for item_name, item in item_object.items():
                if replace:
                    source_dict[item_type][item_index] = {item_name: function(item_type, item)}
                else:
                    function(item_type, item)

    return source_dict


# def _items(source_dict):
#     for item_type, item_list in source_dict.items():
#         for item_index, item_object in enumerate(item_list):
#             for item_name, item in item_object.items():
#                 yield item_type, item_name, item  # , item_index


class OutputObject:
    """
    A helper class to facilitate use of output dictionary

    Final dictionary format example:
    {
        INFO: {
            ANALYSIS_GIVEN_NAME: name,
            SUCCESS: True,
            HAS_WARNING: False,
            HAS_ERROR: False
        },
        FIGURE: {
            FOLIUM_MAP: [{'Map View': self.dynamic_map()}],
            PLOTLY_GRAPH: [{'Summary of Results': self.dynamic_states_bars()},
                           {'Summary of Classes': self.dynamic_class_costs()}],
            MATPLOTLIB_FIGURE: [
                {'Static Result': self.static_map_and_number_of_classes()}
            ]
        },
        FILE: {
            DF_TO_CSV: [{'bridge_damage_estimation': self.damage_estimations}]
        },
        CONSOLE: ['info', self.console_output()]
    }
    """
    class Info:
        def __init__(self, name=None):
            self.infos = {
                ANALYSIS_GIVEN_NAME: name,
                SUCCESS: True,
                HAS_WARNING: False,
                HAS_ERROR: False
            }

        # def set_name(self, name):
        #     self.infos[ANALYSIS_GIVEN_NAME] = name

        @property
        def name(self):
            return self.infos[ANALYSIS_GIVEN_NAME]

        @name.setter
        def name(self, name):
            self.infos[ANALYSIS_GIVEN_NAME] = name

        # def set_success(self, success: bool):
        #     self.infos[SUCCESS] = success

        @property
        def success(self):
            return self.infos[SUCCESS]

        @success.setter
        def success(self, success: bool):
            self.infos[SUCCESS] = success

        def set_has_warning(self):
            self.infos[HAS_WARNING] = True

        def set_has_error(self):
            self.infos[HAS_ERROR] = True

    class Figure:
        def __init__(self):
            self.figures = {}

        def add(self, figure_type, name, figure):
            self.figures = _add_to_dict(self.figures, figure_type, name, figure)

        def add_folium_map(self, name, figure):
            self.add(FOLIUM_MAP, name, figure)

        def add_plotly_graph(self, name, figure):
            self.add(PLOTLY_GRAPH, name, figure)

        def add_matplotlib_figure(self, name, figure):
            self.add(MATPLOTLIB_FIGURE, name, figure)

        def add_dataframe_table(self, name, figure):
            self.add(DATAFRAME_TABLE, name, figure)

        def apply(self, function, replace=True):
            self.figures = _apply(self.figures, function, replace)

        def items(self):
            for item_type, item_list in self.figures.items():
                for item_index, item_object in enumerate(item_list):
                    for item_name, item in item_object.items():
                        yield item_type, item_name, item  # , item_index

    class File:
        def __init__(self):
            self.files = {}

        def add(self, file_type, name, file):
            self.files = _add_to_dict(self.files, file_type, name, file)

        def add_dataframe_to_csv(self, name, dataframe):
            self.add(DF_TO_CSV, name, dataframe)

        def add_tf_model_to_file(self, name, tf_model):
            self.add(TF_MODEL_TO_FILE, name, tf_model)

        def add_sk_learn_model_to_file(self, name, sk_learn_model):
            self.add(SK_LEARN_MODEL_TO_FILE, name, sk_learn_model)

        def add_dict_to_json(self, name, dictionary):
            self.add(DICT_TO_JSON, name, dictionary)

        def add_object_to_pickle(self, name, obj):
            self.add(OBJECT_TO_PICKLE_FILE, name, obj)

        def apply(self, function, replace=False):
            self.files = _apply(self.files, function, replace)

        def items(self):
            for item_type, item_list in self.files.items():
                for item_index, item_object in enumerate(item_list):
                    for item_name, item in item_object.items():
                        yield item_type, item_name, item  # , item_index

        def clear(self):
            self.files = {}

    class Console:
        def __init__(self):
            self.messages = []

        def add(self, msg_type, msg, prepend=False):
            if not prepend:
                self.messages.append([msg_type, msg])
            else:
                self.messages.insert(0, [msg_type, msg])

        def add_info(self, msg, prepend=False):
            self.add(CONSOLE_INFO, msg, prepend)

        def add_warning(self, msg, prepend=False):
            self.add(CONSOLE_WARN, msg, prepend)

        def add_error(self, msg, prepend=False):
            self.add(CONSOLE_ERROR, msg, prepend)

    def __init__(self, name=None):
        self.info = self.Info(name)
        self.figure = self.Figure()
        self.file = self.File()
        self.console = self.Console()

    def __bool__(self):
        return self.info.infos[SUCCESS]

    # Todo: Modify has warning/error while adding message
    @property
    def has_warning(self):
        for msg in self.console.messages:
            if msg[0] == CONSOLE_WARN:
                return True
        return False

    @property
    def has_error(self):
        for msg in self.console.messages:
            if msg[0] == CONSOLE_ERROR:
                return True
        return False

    def print_console(self, terminal_width=shutil.get_terminal_size().columns):
        analysis_name = ''
        if bool(self.info.name):
            analysis_name = ' "{}"'.format(self.info.name)

        if self.info.success:
            finish_status = " successfully"
            if self.has_error:
                finish_status = " with errors"
            elif self.has_warning:
                finish_status = " with warnings"
            self.console.add_info("Processing{} analysis finished{}.".format(analysis_name, finish_status), prepend=True)
        else:
            self.console.add_error("Processing{} analysis failed.".format(analysis_name), prepend=True)

        print('_' * terminal_width)
        for msg in self.console.messages:
            print('{}: {}'.format(msg[0].title(), msg[1]))
        print('#' * terminal_width)

    def to_dict(self) -> dict:
        if self.has_warning:
            self.info.set_has_warning()
        if self.has_error:
            self.info.set_has_error()
        # for msg in self.console.messages:
        #     if msg[0] == CONSOLE_WARN:
        #         self.info.set_has_warning()
        #     if msg[0] == CONSOLE_ERROR:
        #         self.info.set_has_error()

        return {
            INFO: self.info.infos,
            FIGURE: self.figure.figures,
            FILE: self.file.files,
            CONSOLE: self.console.messages
        }

    @classmethod
    def from_dict(cls, adict):
        output = cls()

        if INFO in adict:
            output.info.name = adict[INFO].get(ANALYSIS_GIVEN_NAME, None)
            output.info.success = adict[INFO].get(SUCCESS, True)

        if FIGURE in adict:
            for figure_type, figure_list in adict[FIGURE].items():
                for figure_object in figure_list:
                    for figure_name, figure in figure_object.items():
                        output.figure.add(figure_type, figure_name, figure)

        if FILE in adict:
            for file_type, file_list in adict[FILE].items():
                for file_object in file_list:
                    for file_name, file in file_object.items():
                        output.file.add(file_type, file_name, file)

        if CONSOLE in adict:
            for msg_object in adict[CONSOLE]:
                if isinstance(msg_object, dict):
                    for msg in msg_object.items():
                        output.console.add(msg[0], msg[1])
                else:
                    output.console.add_info(msg_object)

        return output
