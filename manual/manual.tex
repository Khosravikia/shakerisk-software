\documentclass[12pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage{blindtext}
\usepackage{xspace}
\usepackage{graphicx}
\graphicspath{{figures/}} % Base figure directory

\usepackage{tabularx, ragged2e, array}
\usepackage{multirow}
%\usepackage{ltablex}
\renewcommand\arraystretch{1.2}
%\newcolumntype{C}{>{\centering\bfseries\arraybackslash}X}
\newcolumntype{R}{>{\raggedright\arraybackslash}X}
%\newcolumntype{X}{>{\centering\arraybackslash}X}
%\newcolumntype{x}[1]{>{\raggedright}X}

\usepackage{enumitem} % Control item spacing(in object definition)

\usepackage{caption, subcaption}
\usepackage{chngcntr} % Make figure/table numbers from 2 => 1.2
\counterwithin{figure}{section}
\counterwithin{table}{section}
\usepackage[section]{placeins} % Make sure floats stay in their section

\usepackage[authoryear, round]{natbib} % Cite year only option
\bibliographystyle{plainnat}  % Compatible with natbib package
\renewcommand{\bibsection}{} % Remove "Refrences" from printed bibilography (for natbib)
%\setcitestyle{authoryear,open={(},close={)}}

\usepackage{listings}
\lstset{
	backgroundcolor=\color{gray!10},
	frame=single, 
	basicstyle=\ttfamily % stop "--" from getting merged
%	numbers=left, numberstyle=\tiny, stepnumber=2, numbersep=5pt,
}%

%\usepackage{easyReview} % Use for "track changes" and review comments

\usepackage{tocbibind} % Automatically add toc, list of figures and tables, and biblography to toc

\usepackage{hyperref} % Should be loaded after most packages
\usepackage{xcolor}
\hypersetup{
	colorlinks,
	linkcolor={black},
	citecolor={black!50!black},
	urlcolor={blue!80!black}
}

% New environment for object definitions:
% #1: object display name
% #2: object unique JSON name
% #3: object label (obj: + object unique JSON name without escaping _)
% #4: object description
% content: parameters in two column format without header and beginning and ending \hline
\newenvironment{objectDefinition}[4]
{\subsubsection{#1}\label{#3} (#2) 
	\setlist[itemize]{
		nosep
	}
	#4
	
	\vspace{0.2in}
	\centering\noindent\tabularx{\textwidth}{lR} \hline
	Parameter & Description \\
	\hline
}
{ \hline\endtabularx
}

% New environment for columns definitions:
% #1: Display name
% #2: label (obj: + object unique JSON name without escaping _)
% #3: Table description
% content: parameters in two column format without header and beginning and ending \hline
\newenvironment{columnsDefinition}[3]
{\subsubsection{#1 DataFrame}\label{df:#2}
	\setlist[itemize]{
		nosep
	}
	#3
	
	\vspace{0.2in}
	\centering\noindent\tabularx{\textwidth}{lR} \hline
	Column & Description \\
	\hline
}
{ \hline\endtabularx
}

% Object dataframe parameter
\newcommand\objectDefinitionDataFrame[1]{
	DataFrame & \nameref{obj:data_frame} (dataframe) [source=``dataframe"]
	
	\nameref{obj:data_frame} object name. See Section~\ref{#1} for DataFrame structure.\\ 
}
% Object required classification parameter
\newcommand\objectDefinitionRequiredClassification{
Requried Classification & string or \nameref{obj:param_text} (required\_classification) [source=``dataframe"]

Classification system that model requires. See Section~\ref{sec:dataframe_classification} for more.\\
}
% Analysis object, output path row
\newcommand\objectDefinitionOutputPathRow{
	Output Path & string or \nameref{obj:param_text} (output\_path)
	
	Absolute path to existing folder where analysis output file(s) will be saved.\\
}
% Analysis object, degree limit row
\newcommand\objectDefinitionDegreeLimit{
	Processing Degree Limit & float (deg\_limit)
	Limit all data retrievals to rectangular area of earthquake\space latitude $\pm deg\_limit$ and earthquake longitude $\pm deg\_limit$\\
}
% Dataframe columns, class row
\newcommand\dataframeColumnsClassRow{
	Class & string (classification)
	
	See Section~\ref{sec:dataframe_classification} for more.\\
}
% Paremeter mention
\newcommand\textVar[1]{``\textbf{#1}"}

% "\xspace" Makes sure variable can be used as "\shakeriskVersion" (instead of \shakeriskVersion{}) without skipping whitespaces after usage
\newcommand\shakeriskVersion{0.2.1\xspace}
\newcommand\shakeriskVersionName{second version of \textit{ShakeRisk}\xspace}
\newcommand\shakeriskReleaseDate{01/26/2021\xspace}





\title{SHAKERISK \shakeriskVersion OPERATING MANUAL}
\author{Farid Khosravikia, Ph.D.}
\date{August 2020 }

\begin{document}


\pagenumbering{Alph} % Get rid of "destination with the same identifier (name{page.1}) has been already used, duplicate ignored" warning
\maketitle
\thispagestyle{empty} % Make sure title page is not numbered


\newpage
%\renewcommand{\thesection}{\roman{section}} 
\pagenumbering{roman}

\section*{Abstract}
\addcontentsline{toc}{section}{Abstract}
\input{./Sections/abstract.tex}


\newpage
\section*{Software Development Team}
\addcontentsline{toc}{section}{Software Development Team}
\input{./Sections/software_development_team.tex}

%\cleardoublepage

\newpage
%\addcontentsline{toc}{section}{Contents}
\tableofcontents{Table of Contents}

\newpage
\listoftables

\newpage
\listoffigures

\newpage
\renewcommand{\thesection}{\arabic{section}} 
\pagenumbering{arabic}
\section{Introduction}
\input{./Sections/introduction.tex}

\section{Version And Features}
\input{./Sections/version_and_features.tex}

\section{Programming Language} \label{sec:programming_language}
\input{./Sections/programming_language.tex}

\section{Graphical User Interface} \label{sec:graphical_user_interface}
\input{./Sections/graphical_user_interface.tex}

\section{Existing Data, Models, and Analyses in \textit{ShakeRisk}}
\input{./Sections/existing_data_models_and_analyses.tex}


\section{Request Analysis In \textit{ShakeRisk}} \label{sec:request_analysis_in_shakerisk}
\input{./Sections/request_analysis_in_shakerisk.tex}

\section{Structure and Design Architecture} \label{sec:structure_and_design_architecture}
\input{./Sections/structure_and_design_architecture.tex}

\section{Installation and Launching \textit{ShakeRisk}} \label{sec:installation_and_launching}
\input{./Sections/installation_and_launching.tex}

\section{License Agreement}
\input{./Sections/licence_agreement.tex}

\newpage
%\section*{References}
\bibliography{manual}


\end{document}
