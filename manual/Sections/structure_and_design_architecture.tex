\textit{ShakeRisk} is developed using clean architecture \citep{martin2018} computer programming principles such that it can be continuously growing, meaning existing features can be improved, and new features can be implemented. Clean architecture philosophy divides the computer program elements into different levels (as shown in Figure~\ref{fig:design_architecture_clean}), starting with high-level rules at the core and ending with low-level tools in outer layers. The essential feature of the clean architecture design is the dependency rule among layers, stating that source code dependencies can only point inwards. That is, the dependency among layers in clean architecture is only defined in one direction, which is going inward from outer to the inner layer. This way, codes written in inner layers do not rely on those in the outer layers. It helps to encapsulate the logic behind the software at the core while keeping it separated from the delivery mechanism, databases, and tools in outer rings. For example, the logic behind calculating damage to structures should not depend on whether structure data come from an offline SQL database or an online source.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=\textwidth]{design_architecture_clean.jpg}
	\caption{A schematic view of the clean architecture}	
	\label{fig:design_architecture_clean}
\end{figure}

Although this rule does not directly relate to the concept of inversion of control, it brings the concept of inversion of control in the application software. Inversion of control \citep{johnson1988} is a principle in software engineering by which the control of objects in the program is transferred to containers or frameworks. Inversion of control is used to increase modularity of the program and make it extensible, and it has applications in object-oriented programming. Thus, this one-directional dependency rule, which forms the basis of clean architecture, makes it easy to add new features (i.e., data sources, models, analysis, and user interfaces) and customize existing ones without the need to modify existing code. In the following sections, a brief introduction of each layer of the clean architecture presented in Figure~\ref{fig:design_architecture_clean} is provided.

\subsection{Entities}
Entities, as demonstrated in Figure~\ref{fig:design_architecture_clean}, form the innermost layer in clean architecture and are global objects containing the most high-level rules. They are not affected by any other layers, and the data structure and methods in this layer are created independently of other layers in the software. Each entity consists of a list of attributes defining that entity and a method to validate whether the entity object can be created in the software given the information in the software or from users. 
As an example, “bridge” is defined as an entity with its own attributes (e.g., latitude, longitude, span number, and so on) and validation method in the ShakeRisk software. When bridge data are imported from a database or by users, the validation method validates whether the data satisfy all attributes defined for a bridge entity. If so, a bridge object with the attributes already defined in the bridge entity is created to be used in the analysis requested by users. Otherwise, it sends an error indicating that the imported data do not satisfy the attributes defined for the entity in the software. This way, the attributes assigned to the bridge object are independent of how the data are imported and used in the software. It solely depends on the bridge entity description. “Fragility models” and “$V_{s30}$ maps” are other examples of the entities defined in ShakeRisk.

\subsection{Use Cases}
The second inner layer consists of use case classes. For every analysis presented in Table~\ref{tab:available_analysis}, a use case class is defined and assigned in the software to that particular analysis. The use case class contains the required calculation code for conducting the corresponding analysis.
When users request a particular analysis, an object is created from the corresponding use case class for that analysis and is called from the interface adapter layer (which will be discussed shortly) through a request model. Each use case consists of its own request model in the software. As shown in Figure~\ref{fig:design_architecture_process}, each time the use case is called, the interface adapter layer creates a request object using the request model class. Then, the adopter feeds the request information to the request object and sends it to the use case object. The request information consists of the list of the data, models, and other inputs provided by users for running the requested analysis. 
Once the use case is called and receives the required information (i.e., data, models, and other assumptions listed in the request model object) from the interface adapter layer, it processes the request according to the logic defined in the code to conduct the analysis. Thereafter, as illustrated in Figure~\ref{fig:design_architecture_process}, the use case returns the results in the structure defined by a response model to the interface adapter layer. Similar to the request model, each use case has its own response model. After each analysis, the response model creates a response object to define the structure of the results received from the use case object. The output of the response model is sent back to the interface adapters to present the results of the analysis in the outer layer to the user.
A use case only depends on the definition of the corresponding analyses and entities. Changes in use cases are not expected to influence the entities available in the system, and use cases are typically independent of changes in the external layers such as databases, user interface, or any of the existing models in the software. 

\subsection{Interface Adapters}
This layer is the interface between the outermost details level and the application layer (i.e., use case layer). It consists of a set of adapters (e.g., gateways, controller, and presenters) to handle the information flow (e.g., data and user requests) between use cases and the objects in the outer layer (e.g., frameworks, databases, user interfaces). 
A gateway is an object to encapsulate access to an external system or resource. In particular, as seen in Figure~\ref{fig:design_architecture_process}, it converts data in the outer layer to a format defined in the corresponding entity and sends it to a use case object.  Recall that when users request analysis, the associated use case is called in ShakeRisk through a request model, which lists the information required for conducting the analysis. Part of this information is data (e.g., Vs30 information for estimating ground shaking levels analysis). Data in the architecture presented in Figure~\ref{fig:design_architecture_clean} are in the outer layer, and they can come from three different sources: an internal database repository, external data fetched online, and external data provided by users. In ShakeRisk, the repository handler manages this converting process and deals with all three different database sources. By doing so, the objects in the inner layers, like use cases and entities, are independent of the initial source and structure of the database. As a result, a database can be easily updated, changed, and re-structured in ShakeRisk without any changes in the internal layers (e.g., use cases and entities).  
The controller, as seen in Figure~\ref{fig:design_architecture_process}, is an object to receive the request from the outer layer, convert it into a request model format, and pass it to the corresponding use case. In ShakeRisk, requests from the outer layer come from either the graphical interface or command line interface using a JSON file. 
All requests received from the ShakeRisk graphical interface are handled by the Django server. The server receives the request from the front-end, validates it, and converts it to a form suitable for the use case the user requested. The Django server is also responsible for converting the returned results from the use case through the response request model to a form suitable for the front-end.
When ShakeRisk is called from the command line interface, the command line interpreter in ShakeRisk receives the request from the outer layer, converts the requests into a request model, and passes it to the corresponding use case. The command line interpreter works in the same fashion as the Django server does in the graphical user interface of the software. Both are controller objects in the interface adapter layer.
The presenter, as seen in Figure~\ref{fig:design_architecture_process}, is an object to get results from the use case through the response model and transform it into a proper format before passing it to the outer layer. In the web version of ShakeRisk, this process is handled by the Django server.

\subsection{Database, Models, and User-Interface}
This layer is the outermost layer on the diagram presented in Figure~\ref{fig:design_architecture_clean}. It is the largest layer of the software since there are a variety of databases and libraries. The material in this layer is constantly changing, and being in the outermost layer prevents these changes from affecting other inner layers of the software. This layer of ShakeRisk consists of the following tools:
\textbf{Databases}: Recall that the data in \textit{ShakeRisk} can come from three different sources: an internal database repository, which consists of a SQL database; external data fetched online; and external data provided by users. A list of the internal repositories in ShakeRisk is presented in Section~\ref{sec:existing_data_repositories}. Being in the outer layer, the structure and content of the data can be updated and changed without any effect on the code written in the core levels (i.e., use cases and entities).
\textbf{Models}: The list of the available models in \textit{ShakeRisk} is provided in Section~\ref{sec:existing_data_models}. Note that, as discussed in Section~\ref{sec:existing_data_models}, users can also implement new models into \textit{ShakeRisk} by uploading a script file of their model or by using input analyses like “ML-based Model.” All these models are in the outer layer to ensure that any changes in the model libraries will not affect the objects defined in the project core (i.e., use cases and entities).
\textbf{Graphical User Interface}: The graphical user interface (i.e., web-interface) is in this layer. The JSON files required for launching ShakeRisk from the command line interface are also considered to be in this layer.


\begin{figure}[htbp]
	\centering
	\includegraphics[width=\textwidth]{design_architecture_process.jpg}
	\caption{Process of conducting analysis in \textit{ShakeRisk}}	
	\label{fig:design_architecture_process}
\end{figure}

\subsubsection{Database Schema}\label{sec:database_schema}
Figure~\ref{fig:design_architecture_schema} shows \textit{ShakeRisk} database schema implemented in SQLite. Most of the tables can be recognized from their DataFrame format in Section~\ref{sec:dataframe_columns}. Main difference is the relational structure constructed from primary and foreign keys to help flexibility, extendibility, and avoidance of data redundancy. By storing repeated pieces of information (e.g., class names) in their own table, not only they can be reused in the main data by only including a key, which takes minimum space, but also changes to these separate tables will be instantly applied to all of data. Below are some of the mentioned tables with a brief explanation:

\begin{itemize}
	\item \textbf{Facility Type:} Different facility types (e.g., Bridge).
	\item \textbf{Classification:} Classification systems available and their facility type.
	\item \textbf{Class:} All classes of all available classification systems.
	\item \textbf{Models:} Models implemented in \textit{ShakeRisk} alongside their type (e.g., fragility, restoration, etc.), facility type, and classification systems each one requires.
	\item \textbf{Bridge Classification:} In order to support multiple classes for a single structure, classes are stored in a separated table, linked with main structure table using a unique ID. This makes it possible for the separated table (i.e, bridge classification) to have multiple entries for a single bridge, meaning multiple classes.
	\item \textbf{Building Classification:} Same as bridge classification, but in case of buildings total cost of each class  is stored as well.
\end{itemize}


\newpage
\begin{figure}[htbp]
	\centering
	\includegraphics[height=\textheight]{schema_part1.jpg}
%	\caption{A schematic view of the clean architecture}	
%	\label{fig:design_architecture_clean}
\end{figure}
\begin{figure}[htbp]
	\centering
	\includegraphics[height=\textheight]{schema_part2.jpg}
		\caption{Database Schema of \textit{ShakeRisk} \shakeriskVersion}	
		\label{fig:design_architecture_schema}
\end{figure}
