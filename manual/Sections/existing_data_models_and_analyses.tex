\textit{ShakeRisk} has a growing database and library of models and analyses. They are implemented to identify or estimate the occurrence, magnitude, location, and intensity of hazards, the performance of structures and infrastructure, and the ensuing consequences, such as economic, socioeconomic, and environmental consequences. The following sections introduce a list of the available databases and libraries in \textit{ShakeRisk} \shakeriskVersion. 

\subsection{Analysis}
Two types of analyses are included in \textit{ShakeRisk}. The first type consists of main analyses, which can be used to estimate the regional impact of earthquakes. The second type consists of input analyses, which retrieve databases from online sources or receive data from users, analyze the data, and create data repositories and models that can be later employed by users in the main analyses. The descriptions of these analyses are also provided in Table~\ref{tab:available_analysis}

\begin{table}[htb]
	\caption{Available analyses in \textit{ShakeRisk} \shakeriskVersion.}
	\centering
	\begin{tabularx}{\textwidth}{ >{\raggedright\arraybackslash}p{2cm} >{\raggedright\arraybackslash}p{1.5cm} R }
		\hline
		Analysis & Analysis type & Description\\ 
		\hline
		NBI data conversion & Input analysis & Retrieve and compile bridge information from the NBI website (\url{https://www.fhwa.dot.gov/bridge/nbi/ascii.cfm}) based on user-specified search criteria (e.g., culverts or non-culverts). This information can be used in the software for risk assessment of highway bridges in different states.\\
		\hline
		Earthquake data & Input analysis & Retrieve and compile earthquake and ground motion data from the USGS website (\url{https://earthquake.usgs.gov/earthquakes/search/}) based on user-specified search criteria (e.g., time range, location, magnitude range). The outputs of this analysis can be used as a database in the software for developing new ML-based ground motion models.\\
		\hline
		ML-based model & Input analysis & Create an ML-based regression model for a given dataset. This analysis can be used independently or to develop a new ML-based ground motion model for the database provided by users. Supported ML models are Artificial Neuron Network, Random Forest, and Support Vector Machine.\\
		\hline
		Shaking Evaluation & Main analysis & Estimate ground shaking levels after an earthquake scenario with different intensity measures.\\
		\hline
		Damage & Main analysis & Generate scenarios of the extent of damage to structural portfolios with the corresponding repair costs after an earthquake scenario. (Available for bridge, building and transportation network infrastructures)\\
		\hline
		Loss & Main analysis & Generate the distribution of economic loss to structural portfolios after an earthquake using Monte Carlo simulation. (Available for bridge and building infrastructure)\\
		\hline
		Inspection prioritization &  Main analysis & Determine post-earthquake inspection plans given the probability and severity of structural damage after an earthquake. (Available for bridge, building and network infrastructure)\\
		\hline
	\end{tabularx}
\label{tab:available_analysis}
\end{table}
\FloatBarrier
\subsection{Models}\label{sec:existing_data_models}
Models in \textit{ShakeRisk} are defined as functions and tools that can be used in the analyses. This section discusses currently available models and the implementation of new models in \textit{ShakeRisk}. 

\subsubsection{Ground Motion Models (GMMs)}\label{sec:existing_data_gmms}
Ground motion models (GMM) provide shaking level estimates at different locations around an earthquake’s epicenter using intensity measures such as peak ground acceleration, peak ground velocity, and spectral acceleration at discrete periods. Input data for GMMs are typically source characteristics (e.g., magnitude), source-to-site distance, and local site conditions (characterized by $V_{s30}$). Table~\ref{tab:gmms_implemented}  shows a list of GMMs that are currently implemented in \textit{ShakeRisk} and Table~\ref{tab:gmms_implemented_supported_ims} shows intensity measures supported by each GMM.

\begin{table}[htb]
	\caption{GMMs implemented in \textit{ShakeRisk} \shakeriskVersion.}
	\centering
	\begin{tabularx}{\textwidth}{ c R R }
		\hline
		GMM name in \textit{ShakeRisk} & Reference & Location of interest \\
		\hline
		 Khosravikia et al. 2019 & \cite{khosravikia2019} & OK, TX, and KS \\
		 \hline
		 Zalachoris, Rathje. 2019 & \cite{zalachoris2019} & OK, TX, and KS \\
		 \hline
		 Hassani, Atkinson. 2015 & \cite{hassani2015} & Central and Eastern North America \\
		 \hline
		 Boore et al. 2014 & \cite{boore2014} & NGA-West 2 \\
		 \hline
	\end{tabularx}
	\label{tab:gmms_implemented}
\end{table}

\subsubsection{Spatial Correlation Models}\label{sec:existing_data_spatial_correlation_models}

%Spatial correlation models make it possible to estimate shaking levels with a probabilistic approach, taking into account the uncertainty of models. This can be achieved without spatial correlation by simply adding the uncertainty to calculated values, but using spatial correlation takes into account the relation close points on the ground have with each other on and makes probabilistic estimations more reasonable.

Table~\ref{tab:spatial_correlations_implemented} shows a list of spatial correlation models implemented in \textit{ShakeRisk}.

\begin{table}[htb]
	\caption{Spatial correlation models implemented in \textit{ShakeRisk} \shakeriskVersion.}
	\centering
	\begin{tabularx}{\textwidth}{ X X }
		\hline
		Model name in \textit{ShakeRisk} & Reference \\
		\hline
		Jayram, Baker. 2009 & \cite{jayaram2009} \\
	\end{tabularx}
	\label{tab:spatial_correlations_implemented}
\end{table} 

\subsubsection{Structural Fragility Models}\label{sec:existing_data_fragility_model}
Structural fragility models provide the probability of different levels of damage (i.e., slight, moderate, extensive, and complete) given ground shaking estimates. Fragility models for different types of structures that are currently implemented in \textit{ShakeRisk} are shown in Table \ref{tab:fragility_models_implemented}. For each of these “structure types,” there are different fragility functions that correspond to specific structural characteristics and detailing/design practices within the broader description of “bridge” or “building.”

\begin{table}[htb]
	\caption{Fragility models implemented in \textit{ShakeRisk} \shakeriskVersion.}
	\centering
	\begin{tabularx}{\textwidth}{ X R X X}
		\hline
		Structure type & Reference & Location of interest & Supported Intensity Measure \\
		\hline
		Building & HAZUS (\citeyear{hazus2003}) & USA & PGA \\
		\hline
		Bridge & HAZUS (\citeyear{hazus2003}) & USA & PGA \\
		\hline
		Bridge & \cite{khosravikiaclayton2020} & TX, OK, and KS  & PGA, PGV\\
		\hline
	\end{tabularx}
	\label{tab:fragility_models_implemented}
\end{table} 


\subsubsection{Unit Construction Cost Models}\label{sec:existing_data_unit_construction_cost_model}
Unit construction cost models used in \textit{ShakeRisk} aim to estimate the replacement cost of damaged structures based on the structural characteristics of ``bridge" or ``building". Unit construction cost models for different types of structures that are currently implemented in \textit{ShakeRisk} are shown in Table~\ref{tab:unit_construction_cost_models_implemented}.

\begin{table}[htb]
	\caption{Unit construction cost models implemented in \textit{ShakeRisk} \shakeriskVersion.}
	\centering
	\begin{tabularx}{\textwidth}{ X X}
		\hline
		Structure type & Source  \\
		\hline
		Building & HAZUS (\citeyear{hazus2003})  \\
		\hline
		Bridge & TX\_TXDot  \\
		\hline
		Bridge & OK\_NBI  \\
		\hline
	\end{tabularx}
	\label{tab:unit_construction_cost_models_implemented}
\end{table}

\subsubsection{Repair Cost Models}\label{sec:existing_data_repair_cost_model}
Repair cost models estimate the repair cost of damaged structures based on the severity of damage and replacement cost. Repair cost models for different types of structures that are currently implemented in \textit{ShakeRisk} are shown in Table~\ref{tab:repair_cost_models_implemented}.

\begin{table}[htb]
	\caption{Repair cost models implemented in \textit{ShakeRisk} \shakeriskVersion.}
	\centering
	\begin{tabularx}{\textwidth}{ X X}
		\hline
		Structure type & Source  \\
		\hline
		Bridge & \cite{basoz1999}  \\
		\hline
		Building & HAZUS (\citeyear{hazus2003})  \\
		\hline
	\end{tabularx}
	\label{tab:repair_cost_models_implemented}
\end{table}

\subsubsection{Restoration Models}\label{sec:existing_data_restoration_model}
In \textit{ShakeRisk}, restoration models are used to estimate number of days needed for repairing a structure, according to its severity of damage. Table~\ref{tab:restoration_models_implemented} show restoration models available in \textit{ShakeRisk}.
\begin{table}[htb]
	\caption{Restoration models implemented in \textit{ShakeRisk} \shakeriskVersion.}
	\centering
	\begin{tabularx}{\textwidth}{ X X}
		\hline
		Structure type & Source  \\
		\hline
		Bridge & HAZUS (2003)  \\
		\hline
	\end{tabularx}
	\label{tab:restoration_models_implemented}
\end{table}

\subsubsection{System Models}\label{sec:existing_data_system_model}
Table~\ref{tab:restoration_models_implemented} shows implemented system models in \textit{ShakeRisk} \shakeriskVersion.

\begin{table}[htb]
	\caption{System models implemented in \textit{ShakeRisk} \shakeriskVersion.}
	\centering
	\begin{tabularx}{\textwidth}{ X X}
		\hline
		Model Name & Source  \\
		\hline
		Frank Wolfe & ?  \\
		\hline
	\end{tabularx}
	\label{tab:restoration_models_implemented}
\end{table}


\subsubsection{Implementation of New Models in ShakeRisk}
Users can also implement new models in \textit{ShakeRisk} and use them for their analysis. In this regard, users can define their model values according to templates provided for each model type, import them using  \nameref{obj:data_frame} object which supports CSV and text structures, and use them for running analysis.  

Moreover, users can use the ``ML-based Model" input analysis to train new models in \textit{ShakeRisk}. For example, they can train a ground motion model given a ground motion dataset and use that model in the main analyses. 
 
Imported models will not be permanently added to \textit{ShakeRisk}, but advanced users familiar with SQLite can add their model directly to \textit{ShakeRisk} model repository according to schema demonstrated at Section~\ref{sec:database_schema}.


\begin{table}[htb]
	\caption{Supported Intensity Measures by GMMs in \textit{ShakeRisk} \shakeriskVersion.}
	\centering
	\begin{tabularx}{\textwidth}{ c R }
		\hline
		GMM name in \textit{ShakeRisk} & Supported Intensity Measures \\
		\hline
		Khosravikia et al. 2019 & PGV, PGA, Sa(0.050), Sa(0.060), Sa(0.080), Sa(0.100), Sa(0.150), Sa(0.200), Sa(0.250), Sa(0.300), Sa(0.350), Sa(0.400), Sa(0.450), Sa(0.500), Sa(0.600), Sa(0.700), Sa(0.800), Sa(0.900), Sa(1.000), Sa(1.500), Sa(2.000), Sa(2.500) \\
		\hline
		Zalachoris, Rathje. 2019 & PGA, PGV, Sa(0.050), Sa(0.500), Sa(0.100), Sa(1.000), Sa(2.000), Sa(2.500) ,Sa(4.000), Sa(5.000),Sa(10.000) \\
		\hline
		Hassani, Atkinson. 2015 & PGA, PGV, Sa(0.050), Sa(0.500), Sa(0.100), Sa(1.000), Sa(2.000), Sa(2.500) ,Sa(4.000), Sa(5.000),Sa(10.000) \\
		\hline
		Boore et al. 2014 & PGV, PGA, Sa(0.010), Sa(0.020), Sa(0.022), Sa(0.025), Sa(0.029), Sa(0.030), Sa(0.032), Sa(0.035), Sa(0.036), Sa(0.040), Sa(0.042), Sa(0.044), Sa(0.045), Sa(0.046), Sa(0.048), Sa(0.050), Sa(0.055), Sa(0.060), Sa(0.065), Sa(0.067), Sa(0.070), Sa(0.075), Sa(0.080), Sa(0.085), Sa(0.090), Sa(0.095), Sa(0.100), Sa(0.110), Sa(0.120), Sa(0.130), Sa(0.133), Sa(0.140), Sa(0.150), Sa(0.160), Sa(0.170), Sa(0.180), Sa(0.190), Sa(0.200), Sa(0.220), Sa(0.240), Sa(0.250), Sa(0.260), Sa(0.280), Sa(0.290), Sa(0.300), Sa(0.320), Sa(0.340), Sa(0.350), Sa(0.360), Sa(0.380), Sa(0.400), Sa(0.420), Sa(0.440), Sa(0.450), Sa(0.460), Sa(0.480), Sa(0.500), Sa(0.550), Sa(0.600), Sa(0.650), Sa(0.667), Sa(0.700), Sa(0.750), Sa(0.800), Sa(0.850), Sa(0.900), Sa(0.950), Sa(1.000), Sa(1.100), Sa(1.200), Sa(1.300), Sa(1.400), Sa(1.500), Sa(1.600), Sa(1.700), Sa(1.800), Sa(1.900), Sa(2.000), Sa(2.200), Sa(2.400), Sa(2.500), Sa(2.600), Sa(2.800), Sa(3.000), Sa(3.200), Sa(3.400), Sa(3.500), Sa(3.600), Sa(3.800), Sa(4.000), Sa(4.200), Sa(4.400), Sa(4.600), Sa(4.800), Sa(5.000), Sa(5.500), Sa(6.000), Sa(6.500), Sa(7.000), Sa(7.500), Sa(8.000), Sa(8.500), Sa(9.000), Sa(9.500), Sa(10.000) \\
		\hline
	\end{tabularx}
	\label{tab:gmms_implemented_supported_ims}
\end{table}

\FloatBarrier
\subsection{DATA REPOSITORY}\label{sec:existing_data_repositories}

\subsubsection{Internal Data Repository}
\textit{ShakeRisk} consists of already available databases such as $V_{s30}$ maps, structural and ground motion data. The data inside of the software are managed using a SQL database structure. This option provides the opportunity for easily using and extending the databases available in the software. Table~\ref{tab:structural_data_implemented} and Table~\ref{tab:vs30_data_implemented}, respectively, show the structural and $V_{s30}$ data available in \textit{ShakeRisk}.

\begin{table}[htb]
	\begin{center}
	\caption{Structural data implemented in \textit{ShakeRisk} \shakeriskVersion.}	
	\label{tab:structural_data_implemented}
	\begin{tabularx}{\textwidth}{ R X X X}
		\hline
		Structure type & Location & Source & Year  \\
		\hline
		Bridge & US & \cite{nbi2019} & 2020  \\
		\hline
		Building & TX & ? & ?  \\
		\hline
		Transportation Network & Anaheim & ? & ? \\
		\hline
	\end{tabularx}
	\end{center}
\end{table}


\begin{table}[htb]
	\caption{$V_{s30}$ data implemented in \textit{ShakeRisk} \shakeriskVersion.}
	\centering
	\begin{tabularx}{\textwidth}{ X X R X}
		\hline
		Location & Source & Description & Year
		\\\hline
		TX, OK & \cite{usgs2020} & Slope-based & 2020
		\\\hline
		TX, OK & \cite{li2020} & In-situ measurements and p-wave seismogram method & 2020
		\\\hline
	\end{tabularx}
	\label{tab:vs30_data_implemented}
\end{table}

\subsubsection{Implementation of New Data Repository}
Users can import their own database into \textit{ShakeRisk} and run analyses using their database. In this regard, for each type of data, \textit{ShakeRisk} provides a template file with the required information in the software. These template files ensure that the imported data satisfy the required information for running scenarios in \textit{ShakeRisk}. Users can fill out the template using their database and import it into \textit{ShakeRisk} as a \nameref{obj:data_frame} in CSV or text sturcture. Headings (e.g., variable names) of the data file should be the same as the template file so that \textit{ShakeRisk} understands the parameters of the imported database.

Moreover, \textit{ShakeRisk} is also able to download online data for analyses. This option is provided by the ``NBI data conversion" and ``Earthquake data" input analyses; see Table~\ref{tab:available_analysis} for more details. 

Note that the imported data will not be added to permanent data repository in \textit{ShakeRisk}, but advanced users familiar with SQLite can add their data directly to \textit{ShakeRisk} data repository according to schema demonstrated at Section~\ref{sec:database_schema}.
%\subsection{Structure Classification}
%
%\begin{table}[htb]
%	\caption{Structure classifications available in \textit{ShakeRisk} \shakeriskVersion.}
%	\centering
%	\begin{tabularx}{\textwidth}{ X X X}
%		\hline
%		Structure Type & Classification Name & Source
%		\\\hline
%		Bridge & NBI & \cite{nbi2019}
%		\\\hline
%		Bridge & HAZUS & HAZUS(\citeyear{hazus2003})
%		\\\hline
%		Bridge & Khosravikia & ?\\
%		\hline
%		Building & HAZUS & HAZUS(\citeyear{hazus2003})\\
%		\hline
%	\end{tabularx}
%	\label{tab:vs30_data_implemented}
%\end{table}
